$(function () {
    // $(document).on('change', '#cedula_identidad', function () {
    //
    // })
    // $(document).on('click', '.btn-submit', function (element) {
    //     var formname = $(this).data('form');
    //     let form = document.getElementById(formname);
    //
    //     form.addEventListener('submit', (e) => {
    //         if (form.checkValidity() === true) {
    //
    //             console.log(form);
    //             console.log($(form).serialize());
    //             console.log($(form).attr('action'));
    //             submitAjax($(form).serialize(), $(form).attr('action'))
    //             e.preventDefault();
    //             e.stopPropagation();
    //         }
    //     }, false);
    // });
});

window.validarCedulaEstudiante = function validarCedulaEstudiante(input, url, mensaje, redirect) {
    var data = {cedula: $(input).val()};
    sendAjaxPost(data, url, function () {
        window.showCustomError(mensaje, function () {
            window.location.replace(redirect);
        });
    });
}

window.cargarInstitucionInfo = function cargarInstitucionInfo(institucion_id, url, id) {
    var data = {id: institucion_id};
    sendAjaxPost(data, url, function (response) {
        //console.log(response);
        $('#' + id).html(response.html);
        console.log(theme);
    });
}

window.cargarCampoRegistroLibro = function cargarCampoRegistroLibro(material_docente_id, url, id) {
    var data = {id: material_docente_id};
    sendAjaxPost(data, url, function (response) {
        $('#' + id).attr('placeholder', response.placeholder);

        new Cleave('#' + id, {
            delimiter: '-',
            blocks: response.blocks
        });
    });
}

function sendAjaxPost(data, url, success, error) {
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (respuesta) {
            if (respuesta) {
                if (success != undefined) {
                    success(respuesta);
                }
            }
        },
        error: function (response) {
            console.log(response);
            if (error != undefined) {
                error();
            }
        }
    });
}
