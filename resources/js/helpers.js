$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

if (document.querySelector('.check-toggle')) {
    $('.check-toggle').each(function (e, element) {
        $(element).bootstrapToggle();
    });
}

if (document.querySelector('.ckeditor-content')) {
    $('.ckeditor-content').each(function (e, element) {
        // console.log(window.CKFinder);
        window.ClassicEditor.create(element, {
            // plugins: [SimpleUploadAdapter, CKFinder],
            // toolbar: ['imageUpload'],
            // ckfinder: {
            //     // Upload the images to the server using the CKFinder QuickUpload command.
            //     uploadUrl: 'https://example.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json'
            // }
        }).then(function (editor) {
            //console.log(editor);
        })["catch"](function (error) {
            console.error(error);
        });
    });
}

if (document.querySelector('.datepicker')) {
    $('.datepicker').datepicker();
}

if (document.querySelector('.select2')) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $('.select2').select2({
        language: "es",
        ajax: {
            data: function (params) {
                var query = {
                    _token: CSRF_TOKEN,
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
        },
    });

    $('.select2.remote').select2({
        language: "es",
        ajax: {
            //dataType: 'json',
            data: function (params) {
                var query = {
                    _token: CSRF_TOKEN,
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
        },
    });
}

if (document.querySelector('.dropify')) {
    $('.dropify').dropify({
        tpl: {
            wrap: '<div class="dropify-wrapper user"></div>',
            loader: '<div class="dropify-loader"></div>',
            message: '<div class="dropify-message"><span class="file-icon"></span> <p class="text-uppercase">Arrastra y suelta aquí para subir</p><button type="button" class="mt-3 cs-file-drop-btn btn btn-primary btn-sm">O seleccione archivo</button></div>',
            preview: '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message fs-12">Arrastra y suelta o haz clic para reemplazar</p></div></div></div>',
            filename: '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
            clearButton: '<button type="button" class="dropify-clear">Quitar</button>',
            errorLine: '<p class="dropify-error">{{ error }}</p>',
            errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
        }
    });
}

if (document.querySelector('#spinner')) {
    spinner = $('#spinner');
    spinner.hide();
}

window.showCustomSucces = function showCustomSucces(msn, reload, url) {
    Swal.fire({
        icon: 'success',
        title: 'Correcto',
        text: msn,
        //footer: '<a href>Why do I have this issue?</a>'
    }).then(function (result) {
        if (reload != undefined) {
            if (url != undefined) {
                console.log(url);
                location.href = url;
            } else {
                location.reload();
            }
        }
    });
};

window.showDeletedDialog = function showDeletedDialog(text) {
    window.Swal.fire(
        'Deleted!',
        text,
        'Success'
    );
}

window.showCustomError = function showCustomError(text, callback) {
    window.Swal.fire({
        icon: 'error',
        title: '<h5 class="text-danger">Error en la informaci&oacute;n ingresada</h5>',
        html: text,
        //text: text
    }).then((result) => {
        if (result.value) {
            if (callback != undefined) {
                callback();
            }
        }
    });
};

window.showDialog = function showDialog(title, callback) {
    Swal.fire({
        title: "<label class='text-primary h5'><strong>" + title + "</strong></label>",
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: `Confirmar`,
        denyButtonText: `Cancelar`,
        confirmButtonColor: '#766df4',
        cancelButtonColor: '#d33',
    }).then((result) => {
        if (callback != undefined) {
            callback();
        }
    });
}

window.errorAjax = function errorAjax(err) {
    var errors = $.parseJSON(err.responseText);
    var mns = "";

    $.each(errors, function (key, error) {
        if (key == 'errors') {
            $.each(error, function (item, value) {
                mns += value[0] + '<br>';
            });
        }

        if (key == 'message') {
            mns += error + '<br>';
        }
    });

    window.showCustomError(mns);
}

window.responseAjax = function responseAjax(data, reload) {
    reload = (reload) ? reload : false;

    if (data.status) {
        if (reload) {
            window.location.reload();
        }

        window.showCustomSucces(data.message);
    } else {
        window.showCustomError(data.errors)
    }
}

window.deleteModalAjax = function deleteModalAjax(id) {
    $('#' + id).on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var data_id = button.data('id'); // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.


        var form = $('#' + id).find("form");
        var action = $(form).attr('data-action').slice(0, -1);
        action += data_id;

        $(form).attr('action', action);

        var modal = $(this);
        modal.find('.modal-title').text('Borrar registro #: ' + data_id);
    });
}

window.disableInputForm = function disableInputForm(id) {
    $("#" + id + " input").prop('disabled', true);
    $("#" + id + " select").prop('disabled', true);
}


/**
 * A Javascript module to loadeding/refreshing options of a select2 list box using ajax based on selection of another select2 list box.
 *onchange and onload jquery
 * @url : https://gist.github.com/ajaxray/187e7c9a00666a7ffff52a8a69b8bf31
 * @auther : Anis Uddin Ahmad <anis.programmer@gmail.com>
 *
 * Live demo - https://codepen.io/ajaxray/full/oBPbQe/
 * w: http://ajaxray.com | t: @ajaxray
 */
window.Select2Cascade = (function (window, $) {
    function Select2Cascade(parent, child, url, select2Options) {
        var afterActions = [];
        var options = select2Options || {};

        // Register functions to be called after cascading data loading done
        this.then = function (callback) {
            afterActions.push(callback);
            return this;
        };

        parent.on("change", function (e) {
            child.prop("disabled", true);
            getInstituciones(this, child, url, options, afterActions);
        });
    }

    function getInstituciones(obj, child, url, options, afterActions) {
        var _url = (url.slice(0, -1)) + $(obj).val();
        var id_ = $(child).data('id');
        var selected = '';

        $.getJSON(_url, function (items) {
            var newOptions = '<option value="">-- Select --</option>';
            $.each(items, function (index, item) {
                selected = (item.id == id_) ? ' selected' : '';
                newOptions += '<option value="' + item.id + '" ' + selected + '>' + item.text + '</option>';
            });

            child.select2('destroy').html(newOptions).prop("disabled", false)
                .select2(options);

            afterActions.forEach(function (callback) {
                callback(parent, child, items);
            });
        });
    }

    return Select2Cascade;

})(window, $);

/***
 *
 * @param url
 * @param callback
 * @param type
 * @param data
 * @param noalert
 * @param callbackerror
 * @param id_form
 */
window.sendAjax = function sendAjax(url, callback, type, data, noalert, callbackerror, id_form, sendfile) {
    if (id_form != undefined) {
        window.removeErrorForm(id_form);
    }

    type = type ? type : 'POST';
    var params = {
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        type: type,
        success: function success(result) {
            $('#alert-ajax').hide();

            if (callback != undefined) {
                callback(result);
            }

            if (noalert != undefined && noalert) {
                return true;
            }

            window.responseAjax(result);
        },
        error: function error(xhr, ajaxOptions, thrownError) {
            var json = $.parseJSON(xhr.responseText);

            if (callbackerror != undefined) {
                callbackerror(json.errors);
            }
        }
    };

    if (data && data != undefined) {
        params.data = data;
    }

    if (sendfile && sendfile != undefined) {
        params.cache = false;
        params.contentType = false;
        params.processData = false;
    }

    $.ajax(params);
};

window.setErrorForm = function setErrorForm(errors, id_form) {
    window.removeErrorForm(id_form);
    $.each(errors, function (id_element, error) {
        var obj = null;
        if ($("#" + id_form).find("[name='" + id_element + "']").length > 0) {
            obj = $("#" + id_form).find("[name='" + id_element + "']");
        }

        if (!obj) {
            if ($("#" + id_form + " #" + id_element).length > 0) {
                obj = $("#" + id_form + " #" + id_element); //.find('#' + id_element);
            }
        }

        if (!obj) {
            obj = $("#" + id_form).find(".file");
        }

        $.each(error, function (key, value) {
            $(obj).parent().append('<span class="error-form d-block text-danger" style="font-size: 0.8rem;">' + value + '</span>');
        });
    });
};

window.removeErrorForm = function removeErrorForm(id_form) {
    $("#" + id_form + " .error-form").remove();
};
