@extends('layouts.login.app')

@section('content')
    <div class="cs-view show" id="signin-view">
        <h1 class="h2">Ingresar</h1>

        <p class="font-size-ms text-muted mb-4">
            Inicie sesi&oacute;n en su cuenta utilizando el correo electr&oacute;nico y la contraseña proporcionados
            durante el registro.
        </p>

        <form class="login-form" action="{{ route('auth.login') }}" method="post" role="form">
            {{--<form class="login-form needs-validation" action="{{ route('login') }}" method="post" role="form" novalidate>--}}
            @csrf
            <div class="input-group-overlay form-group">
                <div class="input-group-prepend-overlay">
                    <span class="input-group-text">
                        <i class="fe-mail"></i>
                    </span>
                </div>
                <input id="email" type="email"
                       class="form-control prepended-form-control @error('email') is-invalid @enderror" name="email"
                       value="{{ old('email') }}" type="email" placeholder="Email" required>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="input-group-overlay cs-password-toggle form-group">
                <div class="input-group-prepend-overlay">
                    <span class="input-group-text">
                        <i class="fe-lock"></i>
                    </span>
                </div>
                <input id="password" type="password"
                       class="form-control prepended-form-control @error('password') is-invalid @enderror"
                       name="password"
                       placeholder="Password" required autocomplete="current-password">
                <label class="cs-password-toggle-btn">
                    <input class="custom-control-input" type="checkbox">
                    <i class="fe-eye cs-password-toggle-indicator"></i>
                    <span class="sr-only">Show password</span>
                </label>
            </div>
            <div class="d-flex justify-content-between align-items-center form-group">
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="keep-signed-2">
                    <label class="custom-control-label" for="keep-signed-2">Mantenerme registrado</label>
                </div>
            </div>
            <button class="btn btn-primary btn-block" type="submit">Ingresar</button>
            <p class="font-size-sm pt-3 mb-0">
                ¿No tienes una cuenta?
                <a class="font-weight-medium" href="#modal-signin"
                   data-toggle="modal" data-view="#modal-signup-view">
                    Registrarse
                </a>
            </p>
        </form>
    </div>
@endsection
