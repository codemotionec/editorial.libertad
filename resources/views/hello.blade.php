@extends('layouts.corporativa.app')

@section('sidebar')
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="{{asset('video/bg.mp4')}}" type="video/mp4">
    </video>
@endsection

@section('content')
    <div class="row h-100">
        <div class="col-12 my-auto">
            <div class="masthead-content text-white py-5 py-md-0">
                <h1 class="mb-3">Bienvenidos!</h1>
                <p class="mb-5">
                    We're working hard to finish the development of this site. Our target launch date is
                    <strong>January 2019</strong>! Sign up for updates using the form below!
                </p>
                <div class="input-group input-group-newsletter">
                    <input type="email" class="form-control" placeholder="Enter email..."
                           aria-label="Enter email..." aria-describedby="basic-addon">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button">Notify Me!</button>
                    </div>
                </div>

            </div>
            <div class="masthead-content py-2">
                <a href="{{route('login')}}" class="text-white"><i class="far fa-user"></i> Acceder</a>
            </div>
            <div class="masthead-content py-2">
                <a href="{{route('register')}}" class="text-white"><i class="fas fa-users-cog"></i> Registrarse</a>
            </div>
        </div>
    </div>
@endsection

