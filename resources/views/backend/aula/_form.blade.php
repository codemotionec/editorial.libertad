@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">Creaci&oacute;n de aulas</h6>
            </div>
        </div>

    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="row">
                <div class="col-4">
                    <label for="canton_id" class="bmd-label-floating">Ciudad</label>
                    <select class="form-control select2" id="canton_id"
                            data-ajax--url="{{route('api.canton.select2')}}" data-ajax--data-type="json"
                            data-allow-clear="true"
                            data-placeholder="Seleccionar..." required>
                    </select>

                    <div class="invalid-feedback">Por favor seleccione la ciudad</div>
                    <div class="valid-feedback"><i class="fe-check-circle"></i></div>
                </div>

                <div class="col-8">
                    <label for="institucion_id" class="bmd-label-floating">Instituci&oacute;n</label>
                    <select class="form-control select2" id="institucion_id" name="institucion_id"
                            {{--data-ajax--url="{{route('api.institucion.select2')}}" --}}
                            {{--data-ajax--data-type="json"--}}
                            {{--data-allow-clear="true"--}}
                            data-placeholder="Seleccionar..." required>
                        {{--@if($institucion)--}}
                            {{--<option value="{{$institucion->id}}">{{$institucion->nombre}}</option>--}}
                        {{--@endif--}}
                    </select>
                </div>
                <div id="institucion-info" class=" col-12">

                </div>
            </div>


            <div class="invalid-feedback">Por favor seleccione la instituci&oacute;n</div>
            <div class="valid-feedback"><i class="fe-check-circle"></i></div>

            @error('institucion_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="paralelo_id" class="bmd-label-floating">Curso</label>
            <select class="form-control select2" name="curso_id" id="curso_id"
                    data-ajax--url="{{route('api.curso.select2')}}" data-ajax--data-type="json"
                    data-allow-clear="true"
                    data-placeholder="Seleccionar...">
                @if($model->curso_id)
                    <option value="{{$model->curso_id}}">{{$model->curso->nombre}}</option>
                @endif
            </select>

            @error('curso_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="paralelo" class="bmd-label-floating">Paralelo</label>
            <input class="form-control" type="text" id="paralelo" name="paralelo" data-format="custom"
                   data-delimiter="-" data-blocks="2" placeholder="??" value="{{ old('paralelo',$model->paralelo) }}"
                   required>

            @error('paralelo')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        <input type="submit" value="Enviar" class="btn btn-primary">

    </div>
</div>
