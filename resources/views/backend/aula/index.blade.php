@extends('layouts.aplicacion.app')

@section('content')
    @include('includes.forms.filter.institucion',['name_route'=>'admin.aula.index'])

    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Listado de aulas registradas
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás la parametrización de las aulas de las Instituciones
                    </div>
                </div>
                <div class="col col-3 text-right">
                    <a class="btn btn-success btn-sm" href="{{ route('admin.aula.create') }}">
                        <i class="fe-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Ciudad</th>
                    <th>Instituci&oacute;n</th>
                    <th>Curso</th>
                    <th>Paralelo</th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th>
                        <small>Updated_at</small>
                    </th>
                    <th class="text-center"><i class="fas fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($aulas as $aula)
                    <tr>
                        <td>{{ $aula->institucion->nombre_canton }}</td>
                        <td>{{ $aula->institucion->nombre }}</td>
                        <td>{{ $aula->curso->nombre}}</td>
                        <td>{{ $aula->paralelo}}</td>
                        <td class="created_at">
                            <small>{{ $aula->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="created_at">
                            <small>{{ $aula->updated_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.aula.show',$aula->id) }}"
                               class="nav-link-style mr-1" data-toggle="tooltip" title="{{__('REVISAR')}}">
                                <i class="fe-eye"></i>
                            </a>

                            <a href="{{ route('admin.aula.edit',$aula->id) }}"
                               class="nav-link-style mr-1" data-toggle="tooltip" title="{{__('EDITAR')}}">
                                <i class="fe-edit"></i>
                            </a>

                            <a data-toggle="modal" data-target="#deleteModal" data-id="{{ $aula->id }}"
                                    class="nav-link-style mr-1 text-danger">
                                <i class="fe-trash-2"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="m-3">
                {{
                $aulas
                ->appends(['created_at' => request('created_at'),'search' => request('search'),])
                ->links()
                }}
            </div>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'admin.aula.destroy'])



@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
    </script>
@endsection