@extends('layouts.backend.app')

@section('sidebar')
    <div class="row ">
        <div class="col col-7 ">

        </div>
        <div class="col col-4 ">
            <nav aria-label="breadcrumb">
                {{ Breadcrumbs::render('admin.curso.create', $model ?? '') }}
            </nav>
        </div>
        <div class="col col-1 text-center">
            <a class="btn btn-success btn-sm mt-2" href="{{ route('admin.curso.create') }}">
                <i class="fa fa-plus"></i>
            </a>
        </div>
    </div>
@endsection

@section('content')
    @include('backend.aula._form')
    <script>
        window.onload = function () {
            $("input").prop('disabled', true);
            $("select").prop('disabled', true);
        };
    </script>
@endsection