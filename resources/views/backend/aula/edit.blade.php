@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.aula.create', $model ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <form action="{{ route("admin.aula.update",$model->id) }}" method="POST" enctype='multipart/form-data'>
        @method('PUT')
        @include('backend.aula._form')
    </form>
@endsection
