<form action="{{route('admin.opcion-pregunta-desarrollo.store.'.strtolower($pregunta->tipoPregunta->codigo),$pregunta->id)}}"
      id="form-opciones-pregunta" method="post"
      enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col col-8">
            @switch(trim($pregunta->tipoPregunta->codigo))
                @case('SELECCION')
                <label for="descripcion" class="small text-black-50">Descripcion</label>
                <input type="text" class="form-control" name="descripcion" id="descripcion" required>
                @break
                @case('IMAGEN')
                <label for="imagen" class="small text-black-50">Imagen</label>
                {{--<label for="imagen" class="bmd-label-floating">Imagen</label>--}}
                <input type="file" class="dropify" name="imagen" id="imagen"
                       @if($model->imagen) data-default-file="{{url('storage/instituciones/'.$model->imagen)}}" @endif
                />
                @break
            @endswitch
        </div>
        <div class="col col-2">
            <label for="respuesta" class="small text-black-50">¿Respuesta Correcta?</label>
            <input type="checkbox" name="respuesta" id="respuesta" class="form-control check-toggle"
                   data-toggle="toggle" data-style="slow" data-on="{{__('On')}}" data-off="{{__('Off')}}">
        </div>
        <div class="col col-2 text-right">
            <label for="respuesta" style="display: block;">&nbsp;</label>
            <button type="button" id="btn-guardar-opcion" class="btn btn-primary">GUARDAR</button>
        </div>
    </div>
</form>
