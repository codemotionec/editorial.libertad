{{--@php--}}
{{--$respuestas = \App\Models\RespuestaDesarrollo::obtenerRespuestaDesarrolloDesarrolloAll($desarrollo->id);--}}
{{--@endphp--}}


{{--<dl class="row pb-0 mb-0">--}}
{{--@foreach($respuestas as $respuesta)--}}
{{--<dd class="col-sm-10 small text-uppercase">--}}
{{--<i class="far fa-check-square text-success"></i> {{$respuesta->tipoRespuesta->descripcion}}--}}
{{--</dd>--}}
{{--<dd class="col-sm-2 text-center">--}}
{{--<a data-toggle="modal" data-target="#deleteModal-respuesta"--}}
{{--data-id="{{ $respuesta->id }}">--}}
{{--<i class="fa fa-trash text-danger"></i>--}}
{{--</a>--}}
{{--</dd>--}}
{{--@endforeach--}}
{{--</dl>--}}
{{--<div class="jumbotron">--}}
{{--<h2>OPCIONES DE PREGUNTA</h2>--}}
{{--<hr class="my-4">--}}
{{--<p class="pb-3">It uses utility classes for typography and spacing to space content out within the larger container.</p>--}}
{{--<a href="#" class="btn btn-primary btn-shadow" role="button">Learn more</a>--}}
{{--</div>--}}

<div class="jumbotron">
    <label class="h5 text-primary">OPCIONES DE PREGUNTA</label>
    <div class="lead">{!! $pregunta->pregunta !!}</div>
    <hr class="my-2">
    @include('backend.opcion_pregunta._form',['pregunta'=>$pregunta])
    <div id="detalle-opciones-preguntas">
        @php
            echo \App\Http\Controllers\OpcionPreguntaDesarrolloController::index(null,$pregunta);
        @endphp
    </div>
</div>