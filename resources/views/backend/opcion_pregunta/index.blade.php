{{--<div class="card mt-3">--}}
{{--<div class="card-body">--}}
<table class="table table-striped mt-3">
    <thead class="text-primary">
    <tr>
        <th>Opcion</th>
        <th class="text-center">¿Es correcta?</th>
        <th class="text-center"><i class="fas fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($opcionesPreguntaDesarrollo as $opcionPreguntaDesarrollo)
        <tr>
            @if($opcionPreguntaDesarrollo->descripcion)
                <td class="w-50 text-justify">
                    {!! $opcionPreguntaDesarrollo->descripcion !!}
                </td>
            @endif
            @if($opcionPreguntaDesarrollo->imagen)
                <td class="text-justify">
                    <img width="100" class="img-thumbnail" src="{{asset('storage/preguntas/imagen/'.$opcionPreguntaDesarrollo->imagen)}}"
                         alt="">
                </td>
            @endif
            <td class="text-center fs-14">
                @if($opcionPreguntaDesarrollo->respuesta)
                    <i class="fe-check-circle text-success"></i>
                @else
                    <i class="fe-x-circle text-danger"></i>
                @endif
            </td>
            <td class="text-center">
                <div class="text-center" style="width: 120px; margin: 0 auto;">
                    <a data-toggle="modal" data-target="#deleteModal-opcion"
                            data-id="{{ $opcionPreguntaDesarrollo->id }}"
                            class="nav-link-style text-danger">
                        <i class="fe-trash-2"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{--</div>--}}
{{--</div>--}}