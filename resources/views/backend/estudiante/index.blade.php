@extends('layouts.aplicacion.app')

@section('content')
    @include('includes.forms.search-grid',['name_route'=>'admin.institucion.index'])

    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Listado de Estudiantes
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás los estudiantes registrados en la aplicaci&oacute;n
                    </div>
                </div>
                <div class="col col-3 text-right">
                    {{--<a class="btn btn-success mt-3 mb-3" href="{{ route('admin.institucion.create') }}">--}}
                    {{--<i class="fa fa-plus"></i>--}}
                    {{--</a>--}}
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    {{--<th>Docente</th>--}}
                    <th>Estudiante</th>
                    <th>Instituci&oacute;n</th>
                    <th>Aula</th>
                    <th>Año lectivo</th>
                    <th>Estado</th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th class="text-center">
                        <i class="fas fa-cogs"></i>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($estudiantes as $estudiante)
                    @php
                        $estadoRegistro = $estudiante->estadoRegistro;
                    @endphp
                    <tr>
                        <td>{{ $estudiante->nombre_estudiante }}</td>
                        <td>{{ $estudiante->nombre_institucion }}</td>
                        <td>{{ $estudiante->nombre_aula}}</td>
                        <td>{{ $estudiante->anioLectivo->descripcion }}</td>
                        <td>
                            {!! \App\Helpers\Helper::obtenerIconEstado($estadoRegistro->codigo) !!}
                            <small>{{$estadoRegistro->nombre}}</small>
                        </td>
                        <td class="created_at">
                            <small>{{ $estudiante->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.estudiante.aprobar',$estudiante->id) }}"
                               class="btn btn-sm btn-outline-success mr-1">
                                <i class="fe-check"></i>
                            </a>
                            <a href="{{ route('admin.estudiante.cancelar',$estudiante->id) }}"
                               class="btn btn-sm btn-outline-danger">
                                <i class="fe-x"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="m-3">
                {{
                $estudiantes
                ->appends(['created_at' => request('created_at'),'search' => request('search'),])
                ->links()
                }}
            </div>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'admin.institucion.destroy'])

    {{--<script>--}}
    {{--window.onload = function () {--}}
    {{--$('#deleteModal').on('show.bs.modal', function (event) {--}}
    {{--var button = $(event.relatedTarget); // Button that triggered the modal--}}
    {{--var id = button.data('id'); // Extract info from data-* attributes--}}
    {{--// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).--}}
    {{--// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.--}}

    {{--action = $('#formDelete').attr('data-action').slice(0, -1);--}}
    {{--action += id;--}}
    {{--console.log(action);--}}

    {{--$('#formDelete').attr('action', action);--}}

    {{--var modal = $(this);--}}
    {{--modal.find('.modal-title').text('Borrar el Perido Lectivo: ' + id);--}}
    {{--});--}}
    {{--};--}}
    {{--</script>--}}

@endsection