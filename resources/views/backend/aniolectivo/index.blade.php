@extends('layouts.aplicacion.app')

@section('content')
    @include('includes.forms.search-grid',['name_route'=>'admin.anio-lectivo.index'])

    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Listado de Periodos Lectivos
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás la parametrización de los pediodos lectivos
                    </div>
                </div>
                <div class="col col-3 text-right">
                    <a class="btn btn-success btn-sm" href="{{ route('admin.anio-lectivo.create') }}">
                        <i class="fe-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="table-responsive">

            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Descripcion</th>
                    <th>Año lectivo</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th>
                        <small>Updated_at</small>
                    </th>
                    <th class="text-center"><i class="fas fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($aniosLectivos as $anioLectivo)
                    <tr>
                        <td>{{ $anioLectivo->descripcion }}</td>
                        <td>{{ $anioLectivo->anio }}</td>
                        <td>{{ \App\Helpers\Helper::formatDate( $anioLectivo->fecha_inicio)  }}</td>
                        <td>{{ \App\Helpers\Helper::formatDate( $anioLectivo->fecha_fin)  }}</td>
                        <td class="created_at">
                            <small>{{ $anioLectivo->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td>
                            <div>
                                <small>{{ $anioLectivo->updated_at->format('M d Y h:i') }}</small>
                            </div>
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.anio-lectivo.show',$anioLectivo->id) }}"
                               class="nav-link-style mr-1" data-toggle="tooltip" title="{{__('REVISAR')}}">
                                <i class="fe-eye"></i>
                            </a>
                            <a href="{{ route('admin.anio-lectivo.edit',$anioLectivo->id) }}"
                               class="nav-link-style mr-1" data-toggle="tooltip" title="{{__('EDITAR')}}">
                                <i class="fe-edit"></i>
                            </a>
                            <a data-toggle="modal" data-target="#deleteModal" data-id="{{ $anioLectivo->id }}"
                                    class="nav-link-style text-danger">
                                <i class="fe fe-trash-2"></i>
                            </a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="m-3">
                {{
                $aniosLectivos
                    ->appends(['created_at' => request('created_at'),'search' => request('search'),])
                    ->links()
                }}
            </div>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'admin.anio-lectivo.destroy'])

@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
    </script>
@endsection