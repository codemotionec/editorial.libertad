@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.anio-lectivo.create', $model ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')

    <form action="{{ route("admin.anio-lectivo.store") }}" method="POST" class="needs-validation" novalidate>
        @include('backend.aniolectivo._form')
    </form>
@endsection