@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.anio-lectivo.create', $model ?? '') }}
@endsection

@section('content')
    <form id="form-view-aniolectivo">
        @include('backend.aniolectivo._form')
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        window.disableInputForm('form-view-aniolectivo');
    </script>
@endsection