@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">Periodo Lectivo</h6>
            </div>
        </div>

    </div>

    <div class="card-body">
        <div class="form-group">
            <label for="descripcion" class="bmd-label-floating">Descripci&oacute;n</label>
            <input class="form-control" type="text" name="descripcion" id="descripcion"
                   value="{{ old('descripcion',$model->descripcion) }}" required>

            @error('descripcion')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label pl-0">Fecha inicio</label>
                    <div class="input-group-overlay">
                        <input class="form-control appended-form-control cs-date-picker cs-date-range" type="text"
                               placeholder="Seleccionar fecha"
                               data-datepicker-options='{"altInput": false, "altFormat": "Y-m-d", "dateFormat": "Y-m-d"}'
                               data-linked-input="#return-date" name="fecha_inicio" id="fecha_inicio"
                               value="{{old('fecha_inicio',$model->fecha_inicio)}}" required>

                        <div class="input-group-append-overlay">
                            <span class="input-group-text"><i class="fe-calendar"></i></span>
                        </div>
                    </div>
                    @error('fecha_inicio')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label pl-0">Fecha fin</label>
                    <div class="input-group-overlay">
                        <input class="form-control appended-form-control cs-date-picker" type="text"
                               placeholder="Seleccionar fecha"
                               data-datepicker-options='{"altInput": true, "altFormat": "Y-m-d", "dateFormat": "Y-m-d"}'
                               id="return-date" name="fecha_fin" value="{{old('fecha_fin',$model->fecha_fin)}}"
                               required>
                        <div class="input-group-append-overlay">
                            <span class="input-group-text"><i class="fe-calendar"></i></span>
                        </div>
                    </div>
                    @error('fecha_fin')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
        </div>

        {{--<div class="form-group">--}}
        {{--<label for="fecha_inicio">Fecha Inicio</label>--}}
        {{--<div class="input-group">--}}
        {{--<input class="form-control datepicker" type="text" name="fecha_inicio" id="fecha_inicio"--}}
        {{--data-date-format="yyyy-mm-dd"--}}
        {{--value="{{ old('fecha_inicio',$model->fecha_inicio) }}" required>--}}
        {{--<div class="input-group-addon">--}}
        {{--<span class="glyphicon glyphicon-th "></span>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--@error('fecha_inicio')--}}
        {{--<small class="text-danger">{{ $message }}</small>--}}
        {{--@enderror--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
        {{--<label for="fecha_fin">Fecha Fin</label>--}}
        {{--<div class="input-group">--}}
        {{--<input class="form-control datepicker datepicker" type="text" name="fecha_fin" id="fecha_fin"--}}
        {{--data-date-format="yyyy-mm-dd"--}}
        {{--value="{{ old('fecha_fin',$model->fecha_fin) }}" required>--}}
        {{--<div class="input-group-addon">--}}
        {{--<span class="glyphicon glyphicon-th"></span>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--@error('fecha_fin')--}}
        {{--<small class="text-danger">{{ $message }}</small>--}}
        {{--@enderror--}}
        {{--</div>--}}

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        <input type="submit" value="Enviar" class="btn btn-primary">

    </div>
</div>