@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.abreviatura.create', $model ?? '') }}
@endsection

@section('content')

    @include('includes.validation-error-admin')

    <form action="{{ route("admin.abreviatura.update",$model->id) }}" class="needs-validation" method="POST" novalidate>
        @method('PUT')
        @include('backend.abreviatura._form')
    </form>

@endsection