@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.abreviatura.create', $model ?? '') }}
@endsection

@section('content')
    <form id="form-view-abreviatura">
        @include('backend.abreviatura._form')
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        window.disableInputForm('form-view-abreviatura');
    </script>
@endsection


