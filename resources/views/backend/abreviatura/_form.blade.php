@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">Abreviaturas</h6>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="form-group">
            <label for="descripcion" class="bmd-label-floating">Descripci&oacute;n</label>
            <input class="form-control" type="text" name="descripcion" id="descripcion"
                   value="{{ old('descripcion',$model->descripcion) }}" required>

            @error('descripcion')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="codigo" class="bmd-label-floating">Código</label>
            <input class="form-control" type="text" name="codigo" id="codigo"
                   value="{{ old('codigo',$model->codigo) }}" required>

            @error('codigo')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        <input type="submit" value="Enviar" class="btn btn-primary">

    </div>
</div>