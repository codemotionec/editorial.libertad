@extends('layouts.aplicacion.app')

@section('content')
    @include('includes.forms.search-grid',['name_route'=>'admin.abreviatura.index'])

    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Listado de Abreviaturas
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás la parametrización de las abreviaturas
                    </div>
                </div>
                <div class="col col-3 text-right">
                    <a class="btn btn-success btn-sm" href="{{ route('admin.abreviatura.create') }}">
                        <i class="fe-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="table-responsive">

            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Descripcion</th>
                    <th>Código</th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th>
                        <small>Updated_at</small>
                    </th>
                    <th class="text-center"><i class="fas fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($abreviaturas as $abreviatura)
                    <tr>
                        <td>{{ $abreviatura->descripcion }}</td>
                        <td>{{ $abreviatura->codigo }}</td>
                        <td class="created_at">
                            <small>{{ $abreviatura->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td>
                            <div>
                                <small>{{ $abreviatura->updated_at->format('M d Y h:i') }}</small>
                            </div>
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.abreviatura.show',$abreviatura->id) }}"
                               class="nav-link-style mr-1" data-toggle="tooltip" title="{{__('REVISAR')}}">
                                <i class="fe-eye"></i>
                            </a>
                            <a href="{{ route('admin.abreviatura.edit',$abreviatura->id) }}"
                               class="nav-link-style mr-1" data-toggle="tooltip" title="{{__('EDITAR')}}">
                                <i class="fe-edit-2"></i>
                            </a>
                            <a data-toggle="modal" data-target="#deleteModal" data-id="{{ $abreviatura->id }}"
                               class="nav-link-style text-danger">
                                <i class="fe-trash-2"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="m-3">
                {{
                $abreviaturas
                    ->appends(['created_at' => request('created_at'),'search' => request('search'),])
                    ->links()
                }}
            </div>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'admin.abreviatura.destroy'])

@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
    </script>
@endsection