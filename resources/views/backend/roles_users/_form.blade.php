@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">Instituci&oacute;n</h6>
            </div>
        </div>

    </div>

    <div class="card-body">
        <div class="form-group">
            <label for="nombre" class="bmd-label-floating">Nombre</label>
            <input class="form-control" type="text" name="nombre" id="nombre"
                   value="{{ old('nombre',$model->nombre) }}">

            @error('nombre')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="direccion" class="bmd-label-floating">Direccion</label>
            <input class="form-control" type="text" name="direccion" id="direccion"
                   value="{{ old('direccion',$model->direccion) }}">

            @error('direccion')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="canton_id" class="bmd-label-floating">Canton</label>
            <select class="form-control select2 remote" name="canton_id" id="canton_id"
                    data-ajax--url="{{route('api.canton.select2')}}" data-ajax--data-type="json" data-ajax--cache="true"
                    data-placeholder="Seleccionar...">
                @if($model->canton_id)
                    <option value="{{$model->canton_id}}">{{$model->canton->nombre}}</option>
                @endif
            </select>

            @error('canton_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="imagen" class="bmd-label-floating">Imagen</label>
            <input type="file" class="dropify" name="imagen" id="imagen"
                   data-default-file="{{url('storage/instituciones/'.$model->imagen)}}"
            />

            @error('canton_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        <input type="submit" value="Enviar" class="btn btn-primary">

    </div>
</div>
