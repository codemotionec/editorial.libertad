@extends('layouts.aplicacion.app')

@section('content')
    @include('includes.forms.search-grid',['name_route'=>'admin.roles_users.index'])

    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Permisos en el sistema
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás los permisos en el sistema de los usuarios
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Usuario</th>
                    <th>Perfil</th>
                    <th>Estado</th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th>
                        <small>Updated_at</small>
                    </th>
                    <th class="text-center">
                        <i class="fas fa-cogs"></i>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($rolesUsers as $rolesUser)
                    @php
                        $perfil = $rolesUser->user->perfil;
                    @endphp
                    <tr>
                        <td>{{ $perfil->apellido}} {{ $perfil->nombre}}</td>
                        <td>
                            {{$rolesUser->rol->name}}
                        </td>
                        <td>
                            @php
                                echo \App\Helpers\Helper::obtenerIconEstado($rolesUser->estadoRegistro->codigo)
                            @endphp
                            <small>{{ $rolesUser->estadoRegistro->nombre }}</small>
                        </td>
                        <td class="created_at">
                            <small>{{ $rolesUser->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="created_at">
                            <div>
                                <small>{{ $rolesUser->updated_at->format('M d Y h:i') }}</small>
                            </div>
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.roles_users.aprobar',$rolesUser->id) }}"
                               class="btn btn-sm btn-outline-success mr-1" data-toggle="tooltip" data-placement="top"
                               title="Aprobar">
                                <i class="fas fa-check"></i>
                            </a>
                            <a href="{{ route('admin.roles_users.cancelar',$rolesUser->id) }}"
                               class="btn btn-sm btn-outline-danger" data-toggle="tooltip" data-placement="top"
                               title="Cancelar">
                                <i class="fas fa-times"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="m-3">
                {{--{{--}}
                {{--$instituciones--}}
                {{--->appends(['created_at' => request('created_at'),'search' => request('search'),])--}}
                {{--->links()--}}
                {{--}}--}}
            </div>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'admin.institucion.destroy'])

@endsection