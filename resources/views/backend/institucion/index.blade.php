@extends('layouts.aplicacion.app')

@section('content')
    @include('includes.forms.filter.institucion',['name_route'=>'admin.institucion.index'])

    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Listado de Instituciones
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás la parametrización de las Instituciones
                    </div>
                </div>
                <div class="col col-3 text-right">
                    <a class="btn btn-success btn-sm" href="{{ route('admin.institucion.create') }}">
                        <i class="fe-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Ciudad</th>
                    <th>Codigo</th>
                    <th>Descripcion</th>
                    <th>Nivel Educaci&oacute;n</th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th>
                        <small>Updated_at</small>
                    </th>

                    <th class="text-center"><i class="fas fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($instituciones as $institucion)
                    <tr>
                        <td class="py-3 align-middle">
                            {{ $institucion->nombre_canton}}
                        </td>
                        <td class="align-middle">
                            {{ $institucion->codigo}}
                        </td>
                        <td class="py-3 align-middle">{{ $institucion->nombre }}</td>
                        <td class="py-3 align-middle" style="width: 150px;">
                            <small>{{ $institucion->nivel_educacion }}</small>
                        </td>

                        <td class="py-3 align-middle created_at">
                            <small>{{ $institucion->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="py-3 align-middle created_at">
                            {{--<div>--}}
                            <small>{{ $institucion->updated_at->format('M d Y h:i') }}</small>
                            {{--</div>--}}
                        </td>

                        <td class="py-3 align-middle created_at text-center">
                            <a href="{{ route('admin.institucion.show',$institucion->id) }}"
                               class="nav-link-style mr-2" data-toggle="tooltip" title="REVISAR">
                                <i class="fe-eye"></i>
                            </a>

                            <a href="{{ route('admin.institucion.edit',$institucion->id) }}"
                               class="nav-link-style" data-toggle="tooltip" title="EDITAR">
                                <i class="fe-edit mr-2"></i>
                            </a>

                            <a href="" data-toggle="modal" data-target="#deleteModal" data-id="{{ $institucion->id }}"
                               class="nav-link-style text-danger">
                                <i class="fe-trash-2 mr-2"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="m-3">
                {{
                $instituciones
                    ->appends(['created_at' => request('created_at'),'search' => request('search'),])
                    ->links()
                }}
            </div>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'admin.institucion.destroy'])
@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
    </script>
@endsection