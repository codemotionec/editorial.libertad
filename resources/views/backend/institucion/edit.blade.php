@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.institucion.create', $model ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <form action="{{ route("admin.institucion.update",$model->id) }}" class="needs-validation" method="POST"
          enctype='multipart/form-data' novalidate>
        @method('PUT')
        @include('backend.institucion._form')
    </form>
@endsection
