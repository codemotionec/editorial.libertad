@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.institucion.create', $model ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <form action="{{ route("admin.institucion.store") }}" method="POST" class="needs-validation" enctype='multipart/form-data' novalidate>
        @include('backend.institucion._form')
    </form>
@endsection

{{--@section('scripts')--}}
    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function () {--}}
            {{--// $('.select2').select2({--}}
            {{--//     ajax: {--}}
            {{--//         dataType: 'json',--}}
            {{--//         data: function (params) {--}}
            {{--//             var query = {--}}
            {{--//                 search: params.term,--}}
            {{--//                 type: 'public'--}}
            {{--//             }--}}
            {{--//--}}
            {{--//             return query;--}}
            {{--//         },--}}
            {{--//         processResults: function (data) {--}}
            {{--//             return {--}}
            {{--//                 results: data--}}
            {{--//             };--}}
            {{--//         },--}}
            {{--//         cache: true--}}
            {{--//     },--}}
            {{--// });--}}
        {{--});--}}
    {{--</script>--}}
{{--@endsection--}}