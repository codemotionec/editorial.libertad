@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.institucion.create', $model ?? '') }}
@endsection

@section('content')
    <form id="form-view-institucion">
        @include('backend.institucion._form')
    </form>

@endsection

@section('scripts')
    <script type="text/javascript">
        window.disableInputForm('form-view-institucion');
    </script>
@endsection