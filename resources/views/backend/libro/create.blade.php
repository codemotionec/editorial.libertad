@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.libro.create', $model ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <form class="needs-validation" action="{{ route("admin.libro.store") }}" method="POST" enctype='multipart/form-data'
          novalidate>
        @include('backend.libro._form')
    </form>
@endsection