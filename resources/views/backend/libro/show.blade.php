@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.libro.create', $model ?? '') }}
@endsection

@section('content')
    <form id="form-view-libro">
        @include('backend.libro._form')
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        window.disableInputForm('form-view-libro');
    </script>
@endsection
