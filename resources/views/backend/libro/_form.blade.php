@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">libro</h6>
            </div>
        </div>

    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label for="direccion" class="bmd-label-floating">Titulo</label>
                    <input class="form-control" type="text" name="titulo" id="titulo"
                           value="{{ old('titulo',$model->titulo) }}" required>

                    @error('titulo')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <label for="curso_id" class="bmd-label-floating">Curso</label>
                <select class="form-control select2" name="curso_id" id="curso_id"
                        data-ajax--url="{{route('api.curso.select2')}}" data-ajax--cache="true"
                        data-placeholder="Seleccionar..." required>
                    @if($model->curso_id)
                        <option value="{{$model->curso_id}}">{{$model->curso->nombre}}</option>
                    @endif
                </select>

                @error('curso_id')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
        </div>


        <div class="form-group">
            <div class="row mb-2">
                <div class="col-sm-12 col-md-6 py-2">
                    <label for="asignatura_id" class="bmd-label-floating">Asignatura</label>
                    <select class="form-control select2" name="asignatura_id" id="asignatura_id"
                            data-ajax--url="{{route('api.asignatura.select2')}}" data-ajax--data-type="json"
                            data-ajax--cache="true"
                            data-placeholder="Seleccionar..." required>
                        @if($model->asignatura_id)
                            <option value="{{$model->asignatura_id}}">{{$model->asignatura->nombre}}</option>
                        @endif
                    </select>

                    @error('asignatura_id')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="col-sm-12 col-md-6">
                    <label for="direccion" class="bmd-label-floating">Autor</label>
                    <input class="form-control" type="text" name="autor" id="autor"
                           value="{{ old('autor',$model->autor) }}" required>

                    @error('autor')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <label for="portada" class="bmd-label-floating">Portada</label>
                        {{--@include('includes.forms.file-input',['file_name'=>'portada', 'file_id'=>'portada', 'file_default'=>($model->portada) ? url('storage/portada_libro/'.$model->portada):null])--}}
                        <input type="file" class="dropify" name="portada" id="portada"
                               @if($model->portada) data-default-file="{{url('storage/portada_libro/'.$model->portada)}}" @endif>

                        @error('portada')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <label for="direccion" class="bmd-label-floating">Editorial</label>
                        <input class="form-control" type="text" name="editorial" id="editorial"
                               value="{{ old('editorial',$model->editorial) }}" required>

                        @error('editorial')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>

            {{--<div class="form-group">--}}
            {{--<div class="row">--}}
            {{--<div class="col-12">--}}
            {{--<label for="portada" class="bmd-label-floating">Portada</label>--}}
            {{--@include('includes.forms.file-input',['file_name'=>'portada', 'file_id'=>'portada', 'file_default'=>($model->portada) ? url('storage/portada_libro/'.$model->portada):null])--}}
            {{--<input type="file" class="dropify" name="portada" id="portada"--}}
            {{--@if($model->portada) data-default-file="{{url('storage/portada_libro/'.$model->portada)}}" @endif>--}}

            {{--@error('portada')--}}
            {{--<small class="text-danger">{{ $message }}</small>--}}
            {{--@enderror--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

            <div class="form-group">
                <div class="row">
                    <div class="col-12">
                        <label for="resumen" class="bmd-label-floating">Resumen</label>
                        <textarea class="form-control ckeditor-content" id="resumen" name="resumen">
                            {{$model->resumen}}
                        </textarea>

                        @error('resumen')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-12">
                        <label for="contenido" class="bmd-label-floating">Contenido</label>
                        <textarea class="form-control ckeditor-content" id="contenido" name="contenido">
                            {{$model->contenido}}
                        </textarea>

                        @error('contenido')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>


            <input type="hidden" id="token" value="{{ csrf_token() }}">

            <input type="submit" value="Enviar" class="btn btn-primary">

        </div>
    </div>
</div>
