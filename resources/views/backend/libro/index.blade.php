@extends('layouts.aplicacion.app')

@section('content')
    @include('includes.forms.search-grid',['name_route'=>'admin.libro.index'])

    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Listado de Libros
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás la parametrización de las Instituciones
                    </div>
                </div>
                <div class="col col-3 text-right">
                    <a class="btn btn-sm btn-success" href="{{ route('admin.libro.create') }}">
                        <i class="fe-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="table-responsive">

            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th style="width: 100px;">&nbsp;</th>
                    <th>Libro</th>
                    <th>Curso</th>
                    <th>Asignatura</th>
                    <th>Registro</th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th>
                        <small>Updated_at</small>
                    </th>

                    <th class="text-center"><i class="fas fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($libros as $libro)
                    <tr>
                        <td>
                            @if($libro->portada)
                                <img src="{{url('storage/portada_libro/'.$libro->portada)}}"
                                     alt="{{$libro->titulo}}" class="img-thumbnail" alt="{{$libro->portada}}">
                            @endif
                        </td>
                        <td class="py-3 align-middle">{{ $libro->titulo }}</td>
                        <td class="py-3 align-middle">{{ $libro->curso->nombre}}</td>
                        <td class="py-3 align-middle">{{ $libro->asignatura->nombre}}</td>
                        <td class="py-3 align-middle text-center">{{ $libro->anio_registro}}</td>
                        <td class="py-3 align-middle">
                            <small>{{ $libro->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="py-3 align-middle">
                            <div>
                                <small>{{ $libro->updated_at->format('M d Y h:i') }}</small>
                            </div>
                        </td>

                        <td class="py-3 align-middle text-center">
                            <a href="{{ route('admin.segmento_libro.index',$libro->id) }}"
                               class="nav-link-style mr-2 text-success" data-toggle="tooltip" data-placement="top"
                               title="ADMINISTRAR UNIDADES">
                                <i class="fe-upload"></i>
                            </a>

                            <a href="{{ route('admin.libro.show',$libro->id) }}"
                               class="nav-link-style mr-2" data-toggle="tooltip" data-placement="top"
                               title="REVISAR">
                                <i class="fe-eye"></i>
                            </a>

                            <a href="{{ route('admin.libro.edit',$libro->id) }}"
                               class="nav-link-style mr-2" data-toggle="tooltip" data-placement="top"
                               title="EDITAR">
                                <i class="fe-edit"></i>
                            </a>

                            <a href="" data-toggle="modal" data-target="#deleteModal" data-id="{{ $libro->id }}"
                               class="nav-link-style mr-2 text-danger" data-toggle="tooltip" data-placement="top"
                               title="ELIMINAR">
                                <i class="fe-trash-2"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="m-3">
                {{
                $libros
                    ->appends(['created_at' => request('created_at'),'search' => request('search'),])
                    ->links()
                }}
            </div>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'admin.libro.destroy'])


@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal', 'formDelete');
    </script>
@endsection