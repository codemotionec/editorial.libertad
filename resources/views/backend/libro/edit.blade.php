@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.libro.create', $model ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <form action="{{ route("admin.libro.update",$model->id) }}" method="POST" enctype='multipart/form-data' novalidate>
        @method('PUT')
        @include('backend.libro._form')
    </form>
@endsection
