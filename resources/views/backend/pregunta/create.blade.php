@extends('layouts.aplicacion.app')

@section('breadcrum')
    <div class="row ">
        {{--<div class="col col-7">--}}
        <div class="col col-5">
            @if(!$model->id)
                <div class="alert alert-primary" role="alert">
                    Crear Pregunta
                </div>
            @else
                <div class="alert alert-warning" role="alert">
                    @if(isset($opciones) && $opciones)
                        Ingresar opciones pregunta #{{$model->id}}
                    @else
                        Actualizar pregunta #{{$model->id}}
                    @endif

                </div>
            @endif
        </div>
        <div class="col col-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.home')}}">Inicio</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.libro.index')}}">Libros</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.segmento_libro.index',$test->segmentoLibro->libro_id)}}">
                            Unidades
                        </a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.tarea.index',$test->id)}}">
                            Test
                        </a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Pregunta</li>
                </ol>
            </nav>
        </div>
        <div class="col col-1 text-center">
            <a class="btn btn-success btn-sm mt-2"
               href="{{ route('admin.pregunta-desarrollo.index',$test->id) }}">
                <i class="fa fa-plus"></i>
            </a>
        </div>
    </div>
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <div class="row">
        <div class="col col-12">
            <form id="form-archivo"
                  @if(!$model->id)
                  action="{{ route("admin.pregunta-desarrollo.store",$test->id) }}"
                  @else
                  action="{{ route("admin.pregunta-desarrollo.update",$model->id) }}"
                  @endif
                  method="POST"
                  enctype='multipart/form-data'>
                @if(!$model->id)
                    @method('POST')
                @else
                    @method('PUT')
                @endif

                @include('backend.pregunta._form', compact('test','model'))
            </form>

        </div>
        <div class="col col-12">
            @include('backend.pregunta.index', compact('preguntaDesarrollos'))
        </div>

        <div class="col col-12">
            @if(isset($opciones) && $opciones)
                @include('backend.opcion_pregunta.create',['pregunta'=>$model])
            @endif
        </div>
    </div>
    @include('includes.forms.modal-delete',['name_route'=>'admin.pregunta-desarrollo.destroy'])

    @include('includes.forms.modal-delete',[
        'idmodal'=>'deleteModal-opcion',
        'formDelete'=>'formdelete-opcion',
        'name_route'=>'admin.opcion-pregunta-desarrollo.destroy',
        'button'=>'button',
        'id_button'=>"eliminar-opcion"
    ])
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '#btn-guardar-opcion', function () {
                opcionPregunta();
            });

            $(document).on('click', '#eliminar-opcion', function () {
                var url = $('#formdelete-opcion').attr('action');
                eliminarOpcionPregunta(url);
            });
        });

        window.deleteModalAjax('deleteModal');

        window.deleteModalAjax('deleteModal-opcion');

        {{--window.onload = function () {--}}
        {{--$.ajaxSetup({--}}
        {{--headers: {--}}
        {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--},--}}
        {{--});--}}

        {{--$('#deleteModal').on('show.bs.modal', function (event) {--}}
        {{--var button = $(event.relatedTarget); // Button that triggered the modal--}}
        {{--var id = button.data('id'); // Extract info from data-* attributes--}}
        {{--// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).--}}
        {{--// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.--}}

        {{--action = $('#formDelete').attr('data-action').slice(0, -1);--}}
        {{--action += id;--}}
        {{--console.log(action);--}}

        {{--$('#formDelete').attr('action', action);--}}

        {{--var modal = $(this);--}}
        {{--modal.find('.modal-title').text('Borrar Tarea #' + id);--}}
        {{--});--}}

        {{--$('#deleteModal-opcion').on('show.bs.modal', function (event) {--}}
        {{--var button = $(event.relatedTarget); // Button that triggered the modal--}}
        {{--var id = button.data('id'); // Extract info from data-* attributes--}}
        {{--// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).--}}
        {{--// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.--}}

        {{--action = $('#formdelete-opcion').attr('data-action').slice(0, -1);--}}
        {{--action += id;--}}

        {{--$('#formdelete-opcion').attr('action', action);--}}

        {{--var modal = $(this);--}}
        {{--modal.find('.modal-title').text('Borrar opcion respuesta del desarrollo #' + id);--}}
        {{--});--}}
        {{--};--}}

        function opcionPregunta() {
            let myForm = document.getElementById('form-opciones-pregunta');
            let formData = new FormData(myForm);
            var url = $(myForm).attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                dataType: 'Json',
                contentType: false,
                processData: false,
                success: function (data) {
                    window.showCustomSucces(data.success);
                    detalleOpciones();
                },
                error: function (data) {
                    window.showCustomError(data.responseText);
                }
            });
        }

        function eliminarOpcionPregunta(url) {
            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    window.showCustomSucces(data.success);
                    detalleOpciones()
                },
                error: function (data) {
                    window.showCustomError(data.responseText);
                }
            });
        }

        function detalleOpciones() {
            $.ajax({
                type: "GET",
                url: "{{route('admin.opcion-pregunta-desarrollo.index', (isset($model->id)) ? $model->id : 0)}}",
                data: {ajax: true},
                success: function (data) {
                    $('#detalle-opciones-preguntas').html(data.html);
                },
                error: function (data) {
                    window.showCustomError(data.responseText);
                }
            });
        }
    </script>
@endsection
