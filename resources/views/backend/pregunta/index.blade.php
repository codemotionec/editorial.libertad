<div class="card">
    <div class="card-header card-header-primary">
        <div class="row">
            <div class="col col-9">
                <h6 class="strong">Preguntas del test</h6>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="thead-dark">
            <tr>
                <th>Pregunta</th>
                <th>Tipo Pregunta</th>
                <th class="text-right">
                    <div class="text-right" style="width: 120px; margin: 0 auto;">
                        <i class="fas fa-cogs"></i>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($preguntaDesarrollos as $preguntaDesarrollo)
                <tr>
                    <td class="w-50 text-justify">{!! $preguntaDesarrollo->pregunta !!}</td>
                    <td class="text-justify">{!! $preguntaDesarrollo->tipoPregunta->descripcion !!}</td>
                    <td class="text-right">
                        <div class="text-right" style="width: 120px; margin: 0 auto;">
                            @if($preguntaDesarrollo->tipoPregunta->opciones)
                                <a href="{{route('admin.opcion-pregunta-desarrollo.create',$preguntaDesarrollo->id)}}"
                                   class="nav-link-style mr-1 text-primary"
                                   data-toggle="tooltip" data-placement="top"
                                   title="{{__('OPCIONES DE PREGUNTA')}}">
                                    <i class="fas fa-list-ul"></i>
                                </a>
                            @endif()
                            <a href="{{route('admin.pregunta-desarrollo.edit',$preguntaDesarrollo->id)}}"
                               class="nav-link-style mr-1"
                               data-toggle="tooltip" data-placement="top"
                               title="{{__('EDITAR')}}">
                                <i class="fe-edit-2"></i>
                            </a>
                            <a data-toggle="modal" data-target="#deleteModal"
                               data-id="{{ $preguntaDesarrollo->id }}"
                               class="nav-link-style text-danger">
                                <i class="fe-trash-2"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>