@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">
                    Test: {{$test->segmentoLibro->libro->titulo}} - {{$test->segmentoLibro->unidad}}
                </h6>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="row">
                <div class="col col-12 mt-2">
                    <blockquote class="blockquote">
                        <p class="mb-0 text-primary">Test</p>
                        <footer class="blockquote-footer text-justify">{!! $test->descripcion !!}</footer>
                    </blockquote>
                </div>
                <div class="col col-12 mt-2">
                    <label for="unidad" class="bmd-label-floating">Tipo Pregunta</label>
                    <select name="tipo_pregunta_id" id="tipo_pregunta_id" class="form-control select2"
                            data-ajax--url="{{route('api.tipo_pregunta.select2')}}">
                        @if($model->tipo_pregunta_id)
                            <option value="{{$model->tipo_pregunta_id}}">{{$model->tipoPregunta->descripcion}}</option>
                        @endif
                    </select>

                    @error('descripcion')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="col col-12 mt-2">
                    <label for="pregunta" class="bmd-label-floating">Pregunta</label>
                    <textarea name="pregunta" id="pregunta" class="form-control ckeditor-content">
                        {{$model->pregunta}}
                    </textarea>

                    @error('pregunta')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <input type="hidden" id="token" value="{{ csrf_token() }}">
            <input type="submit" value="Cargar" class="mt-3 btn btn-primary" id="cargar-libro">
        </div>
    </div>
</div>
