@csrf

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">Cursos</h6>
            </div>
        </div>

    </div>

    <div class="card-body">
        <div class="form-group">
            <input class="form-control" type="hidden" name="id" id="id" value="{{ old('id',$model->id) }}">
            <label for="descripcion" class="bmd-label-floating">Nombre</label>
            <input class="form-control" type="text" name="nombre" id="nombre"
                   value="{{ old('nombre',$model->nombre) }}" required>

            @error('nombre')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="key" class="bmd-label-floating">C&oacute;digo</label>
            <input class="form-control" type="text" id="codigo" name="codigo" data-format="custom"
                   data-delimiter="-" data-blocks="2" placeholder="00" value="{{ old('key',$model->codigo) }}"
                   required>
            {{--<input class="form-control" type="text" name="codigo" id="codigo"--}}
            {{--value="{{ old('key',$model->codigo) }}" required>--}}

            @error('codigo')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        <input type="submit" value="Enviar" class="btn btn-primary">

    </div>
</div>