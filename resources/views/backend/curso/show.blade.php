@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.paralelo.create', $model ?? '') }}
@endsection

@section('content')
    <form id="form-view-paralelo">
        @include('backend.curso._form')
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        window.disableInputForm('form-view-paralelo');
    </script>
@endsection