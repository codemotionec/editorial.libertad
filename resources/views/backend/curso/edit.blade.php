@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.curso.create', $model ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')

    <form action="{{ route("admin.curso.update",$model->id) }}" method="POST" class="needs-validation" novalidate>
        @method('PUT')
        @include('backend.curso._form')
    </form>
@endsection