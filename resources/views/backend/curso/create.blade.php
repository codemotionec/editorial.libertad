@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.curso.create', $model ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')

    <form action="{{ route("admin.curso.store") }}" method="POST" class="needs-validation" novalidate>
        @include('backend.curso._form')
    </form>
@endsection