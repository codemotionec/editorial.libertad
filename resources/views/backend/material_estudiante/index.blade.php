@extends('layouts.aplicacion.app')

@php
    use App\Helpers\CodigoRegistroLibro;
    use App\Helpers\Helper;
@endphp

@section('breadcrum')
    {{ Breadcrumbs::render('app.registro-libro.index', $model ?? '') }}
@endsection

@section('content')
    {{--@include('includes.forms.search-grid',['name_route'=>'admin.curso.index'])--}}

    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Libros registrados
                    </h4>
                    <div class="card-category">
                        Registro de libros registrados por los estudiantes
                    </div>
                </div>
                <div class="col col-3 text-right">
                    <a class="btn btn-success btn-sm" href="{{ route('app.material-estudiante.create') }}">
                        <i class="fe-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Estudiante</th>
                    <th>Institucion</th>
                    <th>Curso</th>
                    <th>Libro</th>
                    <th>C&oacute;digo</th>
                    <th>Registro</th>
                    <th>Estado</th>
                    <th class="text-center"><i class="fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($materialesEstudiante as $materialEstudiante)
                    @php
                        CodigoRegistroLibro::validarRegistroLibro($materialEstudiante->materialDocente->libro, $materialEstudiante->codigo);
                    @endphp

                    <tr>
                        <td class="py-3 align-middle">
                            {{$materialEstudiante->nombre_estudiante}}
                            <div class="text-success">{{$materialEstudiante->cedula_estudiante}}</div>
                        </td>
                        <td class="py-3 align-middle">
                            {{$materialEstudiante->estudiante->nombre_institucion}}
                            <div class="font-weight-medium text-heading mr-1">
                                {{$materialEstudiante->asignatura_libro}}
                            </div>
                        </td>
                        <td class="py-3 align-middle">
                            {{$materialEstudiante->estudiante->nombre_aula}}
                        </td>
                        <td class="py-3 align-middle">
                            <div class="media align-items-center">
                                <div class="media-body">
                                    {{$materialEstudiante->titulo_libro}}
                                    {!! \App\Helpers\Helper::obtenerEstadoEnCursoInactivo($materialEstudiante->estado_registro_anio_lectivo->codigo) !!}
                                </div>
                            </div>
                        </td>
                        <td class="py-3 align-middle">
                            <a data-toggle="tooltip" title="[ASIGNATURA]-[CURSO]-[CÓDIGO]">
                                {{$materialEstudiante->codigo}}
                                {!!CodigoRegistroLibro::$mensajePrincipal !!}
                            </a>
                            <small class="text-warning">
                                {!!CodigoRegistroLibro::$mensajeAdicional !!}
                            </small>
                        </td>
                        <td class="py-3 align-middle">
                            <small>{{ $materialEstudiante->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="py-3 align-middle">
                            {!! \App\Helpers\Helper::obtenerIconEstado($materialEstudiante->estadoRegistro->codigo) !!}
                            <small>{{$materialEstudiante->estadoRegistro->nombre}}</small>
                        </td>
                        <td class="py-3 align-middle text-center">
                            @if($materialEstudiante->estudiante->user->perfil)
                                <a href="{{ route('admin.material-estudiante.aprobar',$materialEstudiante->id) }}"
                                   class="btn btn-sm btn-outline-success mr-1">
                                    <i class="fas fa-check"></i>
                                </a>
                                <a href="{{ route('admin.material-estudiante.cancelar',$materialEstudiante->id) }}"
                                   class="btn btn-sm btn-outline-danger">
                                    <i class="fas fa-times"></i>
                                </a>
                            @else
                                <span for="" class="text-warning">Sin informaci&oacute;n <br>del estudiante</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'app.material-estudiante.destroy'])

@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
    </script>
@endsection