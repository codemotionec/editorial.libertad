@extends('layouts.aplicacion.app')

@section('breadcrum')
    <div class="row ">
        <div class="col col-7">
            @if(!$model->id)
                <div class="alert alert-primary" role="alert">
                    Crear nueva tarea
                </div>
            @else
                <div class="alert alert-warning" role="alert">
                    Actualizar tarea #{{$model->id}}
                </div>
            @endif
        </div>
        <div class="col col-4">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.home')}}">Inicio</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.libro.index')}}">Libros</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.segmento_libro.index',$segmentoLibro->libro_id)}}">
                            Unidades
                        </a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Tareas</li>
                </ol>
            </nav>
        </div>
        <div class="col col-1 text-center">
            <a class="btn btn-success btn-sm mt-2"
               href="{{ route('admin.tarea.index',$segmentoLibro->id) }}">
                <i class="fe-plus"></i>
            </a>
        </div>
    </div>
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <div class="row">
        <div class="col col-12">
            <form id="form-archivo"
                  @if(!$model->id)
                  action="{{ route("admin.tarea.store",$segmentoLibro->id) }}"
                  @else
                  action="{{ route("admin.tarea.update",$model->id) }}"
                  @endif
                  method="POST"
                  enctype='multipart/form-data'>
                @if(!$model->id)
                    @method('POST')
                @else
                    @method('PUT')
                @endif

                @include('backend.tarea._form', compact('segmentoLibro','model'))
            </form>
        </div>
        <div class="col col-12">
            @include('backend.tarea.index', compact('tareas'))
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'admin.tarea.destroy'])

@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
    </script>
@endsection
