@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.asignatura.create', $model ?? '') }}
@endsection

@section('content')

    @include('includes.validation-error-admin')

    <form action="{{ route("admin.asignatura.update",$model->id) }}" method="POST" class="needs-validation" novalidate>
        @method('PUT')
        @include('backend.asignatura._form')
    </form>

@endsection