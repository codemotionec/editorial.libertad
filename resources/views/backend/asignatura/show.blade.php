@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.asignatura.create', $model ?? '') }}
@endsection

@section('content')
    <form id="form-view-asignatura">
        @include('backend.asignatura._form')
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        window.disableInputForm('form-view-asignatura');
    </script>
@endsection