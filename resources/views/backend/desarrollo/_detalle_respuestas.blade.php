@php
    $respuestas = \App\Models\RespuestaDesarrollo::obtenerRespuestaDesarrolloDesarrolloAll($desarrollo->id);
@endphp


<dl class="row pb-0 mb-0">
    @foreach($respuestas as $respuesta)
        <dd class="col-sm-10 small text-uppercase">
            <i class="fe-check-circle text-success"></i> {{$respuesta->tipoRespuesta->descripcion}}
        </dd>
        <dd class="col-sm-2 text-center">
            <a data-toggle="modal" data-target="#deleteModal-respuesta"
               data-id="{{ $respuesta->id }}" class="nav-link-style">
                <i class="fe-trash-2 text-danger"></i>
            </a>
        </dd>
    @endforeach
</dl>

