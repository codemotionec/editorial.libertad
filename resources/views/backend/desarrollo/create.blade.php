@extends('layouts.aplicacion.app')

@section('breadcrum')
    <div class="row ">
        {{--<div class="col col-7">--}}
        <div class="col col-5">
            @if(!$model->id)
                <div class="alert alert-primary" role="alert">
                    Crear nuevo desarrollo
                </div>
            @else
                <div class="alert alert-warning" role="alert">
                    Actualizar desarrollo #{{$model->id}}
                </div>
            @endif
        </div>
        <div class="col col-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.home')}}">Inicio</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.libro.index')}}">Libros</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.segmento_libro.index',$tarea->segmentoLibro->libro_id)}}">
                            Unidades
                        </a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.tarea.index',$tarea->segmentoLibro->id)}}">
                            Tareas
                        </a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        Desarrollo
                    </li>
                </ol>
            </nav>
        </div>
        <div class="col col-1 text-center">
            <a class="btn btn-success btn-sm mt-2"
               href="{{ route('admin.desarrollo.index',$tarea->id) }}">
                <i class="fa fa-plus"></i>
            </a>
        </div>
    </div>
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <div class="row">
        <div class="col col-12">
            <form id="form-archivo"
                  @if(!$model->id)
                  action="{{ route("admin.desarrollo.store",$tarea->id) }}"
                  @else
                  action="{{ route("admin.desarrollo.update",$model->id) }}"
                  @endif
                  method="POST"
                  enctype='multipart/form-data'>
                @if(!$model->id)
                    @method('POST')
                @else
                    @method('PUT')
                @endif

                @include('backend.desarrollo._form', compact('tarea','model'))
            </form>
        </div>
        <div class="col col-12">
            @include('backend.desarrollo.index', compact('desarrollos'))
        </div>
    </div>
    @include('includes.forms.modal-delete',['name_route'=>'admin.desarrollo.destroy'])
    @include('includes.forms.modal-delete',[
     'idmodal'=>'deleteModal-respuesta',
     'formDelete'=>'formdelete-respuesta',
     'name_route'=>'admin.respuesta-desarrollo.destroy',
     'button'=>'button',
     'id_button'=>"eliminar-respuesta"
    ])
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            // });

            $(document).on('click', '.guardar-respuesta', function () {
                var id = $(this).data('id');
                respuestaDesarrollo(id);
            });

            $(document).on('click', '#eliminar-respuesta', function () {
                var url = $('#formdelete-respuesta').attr('action');
                eliminarRespuesta(url);
            });

            window.deleteModalAjax('deleteModal');

            window.deleteModalAjax('deleteModal-respuesta');

        });

        // (function () {
        //     window.onload = function () {
        //         $.ajaxSetup({
        //             headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //             },
        //         });

        // $('#deleteModal').on('show.bs.modal', function (event) {
        //     var button = $(event.relatedTarget); // Button that triggered the modal
        //     var id = button.data('id'); // Extract info from data-* attributes
        //     // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        //     // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        //
        //     action = $('#formDelete').attr('data-action').slice(0, -1);
        //     action += id;
        //     console.log(action);
        //
        //     $('#formDelete').attr('action', action);
        //
        //     var modal = $(this);
        //     modal.find('.modal-title').text('Borrar Tarea #' + id);
        // });
        //
        // $('#deleteModal-respuesta').on('show.bs.modal', function (event) {
        //     var button = $(event.relatedTarget); // Button that triggered the modal
        //     var id = button.data('id'); // Extract info from data-* attributes
        //     // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        //     // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        //
        //     action = $('#formdelete-respuesta').attr('data-action').slice(0, -1);
        //     action += id;
        //
        //     $('#formdelete-respuesta').attr('action', action);
        //
        //     var modal = $(this);
        //     modal.find('.modal-title').text('Borrar Respuesta del desarrollo #' + id);
        // });
        //     };
        // })();

        function respuestaDesarrollo(desarrollo_id) {
            var form = $('#form-respuesta-desarrollo-' + desarrollo_id);
            var url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                dataType: 'Json',
                success: function (data) {
                    window.showCustomSucces(data.success);
                    detallerRespuesta(desarrollo_id)
                },
                error: function (data) {
                    window.showCustomError(data.responseText);
                }
            });
        }

        function eliminarRespuesta(url) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "DELETE",
                url: url,
                success: function (data) {
                    window.showCustomSucces(data.success);
                    detallerRespuesta(data.desarrollo_id);
                },
                error: function (data) {
                    window.showCustomError(data.responseText);
                }
            });
        }

        function detallerRespuesta(id) {
            $.ajax({
                type: "GET",
                url: "{{route('admin.respuesta-desarrollo.index')}}",
                data: {id: id},
                success: function (data) {
                    $('#detalle-respuesta-desarrollo-' + id).html(data.html);
                },
                error: function (data) {
                    window.showCustomError(data.responseText);
                }
            });
        }
    </script>
@endsection
