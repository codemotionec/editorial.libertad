<div class="card">
    <div class="card-header card-header-primary">
        <div class="row">
            <div class="col col-9">
                <h6 class="strong">Respuesta para el desarrollo </h6>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="thead-dark">
            <tr>
                <th>Desarrollo</th>
                <th>Imagen</th>
                <th>Respuesta</th>
                <th class="text-center"><i class="fas fa-cogs"></i></th>
            </tr>
            </thead>
            <tbody>
            @foreach($desarrollos as $desarrollo)
                <tr>
                    <td class="w-50 text-justify">{!! $desarrollo->descripcion !!}</td>
                    <td>
                        <div style="width: 250px;">
                            <img src="{{url('storage/desarrollo/imagen/'.$model->imagen)}}" alt="{{$model->imagen}}">
                        </div>
                    </td>
                    <td>
                        <div style="width: 250px;">
                            <form id="form-respuesta-desarrollo-{{$desarrollo->id}}"
                                  action="{{route('admin.respuesta-desarrollo.store',$desarrollo->id)}}">
                                @csrf
                                <div id="detalle-respuesta-desarrollo-{{$desarrollo->id}}">
                                    @include('backend.desarrollo._detalle_respuestas',compact('desarrollo'))
                                </div>

                                <select name="tipo_respuesta_id" id="tipo_respuesta_id_{{$desarrollo->id}}"
                                        class="w-100 form-control select2"
                                        data-ajax--url="{{ route('api.tipo_respuesta.select2') }}">

                                </select>
                                <div class="mt-1 text-right">
                                    <button data-id="{{$desarrollo->id}}" type="button"
                                            class="guardar-respuesta btn btn-outline-primary btn-sm">
                                        Ingresar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="text-center" style="width: 80px">
                            <a href="{{route('admin.desarrollo.edit',$desarrollo->id)}}" class="nav-link-style mr-1"
                               data-toggle="tooltip" data-placement="top"
                               title="{{__('EDITAR')}}">
                                <i class="fe-edit-2"></i>
                            </a>
                            <a data-toggle="modal" data-target="#deleteModal" data-id="{{ $desarrollo->id }}"
                               class="nav-link-style text-danger">
                                <i class="fe-trash-2"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="m-3">
            {{--{{--}}
            {{--$libros--}}
            {{--->appends(['created_at' => request('created_at'),'search' => request('search'),])--}}
            {{--->links()--}}
            {{--}}--}}
        </div>
    </div>
</div>