@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">
                    Desarrollo: {{$tarea->segmentoLibro->libro->titulo}} - {{$tarea->segmentoLibro->unidad}}
                </h6>
            </div>
        </div>

    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="row">
                <div class="col col-12 mt-2">
                    <blockquote class="blockquote">
                        <p class="mb-0 text-primary">Tarea</p>
                        <footer class="blockquote-footer text-justify">{!! $tarea->objetivo !!}</footer>
                    </blockquote>
                </div>
                <div class="col col-md-8 mt-2">
                    <label for="unidad" class="bmd-label-floating">Desarrollo</label>
                    <textarea name="descripcion" id="descripcion" class="form-control ckeditor-content">
                        {{$model->descripcion}}
                    </textarea>

                    @error('descripcion')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="col col-md-4 mt-2">
                    <label for="imagen" class="bmd-label-floating">Imagen</label>
                    <input type="file" class="dropify" name="imagen" id="imagen"
                           @if($model->imagen) data-default-file="{{url('storage/desarrollo/imagen/'.$model->imagen)}}" @endif>

                    @error('imagen')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <input type="hidden" id="token" value="{{ csrf_token() }}">
            <input type="submit" value="Cargar" class="mt-3 btn btn-primary" id="cargar-libro">
        </div>
    </div>
</div>
