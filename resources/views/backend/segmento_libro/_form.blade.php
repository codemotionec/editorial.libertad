@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">libro</h6>
            </div>
        </div>

    </div>

    <div class="card-body">


        <div class="form-group">
            <div class="row">
                <div class="col col-12">
                    <label for="unidad" class="bmd-label-floating">Unidad</label>
                    <input type="text" class="form-control" name="unidad" id="unidad"/>

                    @error('unidad')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="col col-12">
                    <label for="documento" class="bmd-label-floating">Documento</label>
                    @include('includes.forms.file-input',['file_name'=>'documento', 'file_id'=>'documento'])
                    @error('documento')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="col col-12">
                    <label for="resumen" class="bmd-label-floating">resumen</label>
                    <textarea class="form-control ckeditor-content" name="resumen" id="resumen">

                    </textarea>

                    @error('resumen')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <input type="hidden" id="token" value="{{ csrf_token() }}">
            <input type="submit" value="Cargar" class="mt-3 btn btn-primary" id="cargar-libro">
        </div>
    </div>
</div>
