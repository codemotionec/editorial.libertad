@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.libro.create', $libro ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <div class="row">
        <div class="col col-6">
            <form id="form-archivo" action="{{ route("admin.segmento_libro.store",$libro->id) }}" method="POST"
                  enctype='multipart/form-data'>
                @method('POST')
                @include('backend.segmento_libro._form')
            </form>
        </div>
        <div class="col col-6">
            @include('backend.segmento_libro.index', compact('segmentoLibros'))
        </div>
    </div>


@endsection

{{--@section('scripts')--}}
    {{--<script type="text/javascript">--}}
        {{--$(function () {--}}
            {{--$(document).on("click", "#cargar-libro", function (event) {--}}
                {{--// console.log(window.ClassicEditor);--}}
                {{--// for (instance in window.ClassicEditor.instances) {--}}
                {{--//     console.log(instance);--}}
                {{--// }--}}
                {{--//--}}
                {{--// // window.ClassicEditor.instances[instance].updateElement();--}}
                {{--// // window.ClassicEditor.instances['resumen'].updateElement();//CKEditor  bilgileri  aktarıyor.--}}

                {{--// let myForm = document.getElementById('form-archivo');--}}
                {{--// let formData = new FormData(myForm);--}}

                {{--// console.log(myForm, 'Fin');--}}
                {{--// return false;--}}
                {{--//uploadImage(formData);--}}
            {{--});--}}
        {{--});--}}

        {{--function uploadImage(formData) {--}}
        {{--spinner.show();--}}

        {{--$.ajaxSetup({--}}
        {{--headers: {--}}
        {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--}--}}
        {{--});--}}

        {{--$.ajax({--}}
        {{--type: "POST",--}}
        {{--data: formData,--}}
        {{--dataType: 'JSON',--}}
        {{--contentType: false,--}}
        {{--processData: false,--}}
        {{--url: "{{ route('admin.segmento_libro.store',$libro->id) }}",--}}
        {{--success: function (data) {--}}
        {{--if (data.status) {--}}
        {{--window.location.reload();--}}
        {{--// window.showCustomSucces(data.message);--}}
        {{--} else {--}}
        {{--window.showCustomError(data.errors)--}}
        {{--}--}}

        {{--spinner.hide();--}}
        {{--},--}}
        {{--error: function (err) {--}}
        {{--window.showCustomError(err.responseText);--}}
        {{--spinner.hide();--}}
        {{--}--}}
        {{--});--}}
        {{--}--}}
    {{--</script>--}}
{{--@endsection--}}
