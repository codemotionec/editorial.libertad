<div class="card">
    <div class="card-header card-header-primary">
        <div class="row">
            <div class="col col-9">
                <h6 class="strong">Archivos</h6>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th>Descripci&oacute;n</th>
                <th>Archivo</th>
                <th>
                    <small>Created_at</small>
                </th>
                <th class="text-center"><i class="fas fa-cogs"></i></th>
            </tr>
            </thead>
            <tbody>
            @foreach($segmentoLibros as $segmentoLibro)
                <tr>
                    <td>{{ $segmentoLibro->unidad}}</td>
                    <td>
                        <a href="{{asset('storage/libros/'.$segmentoLibro->documento)}}" class="text-success"
                           target="_blank">Archivo
                            <i class="far fa-eye text-success"></i>
                        </a>
                    </td>
                    <td class="created_at">
                        <small>{{ $segmentoLibro->created_at->format('M d Y h:i') }}</small>
                    </td>
                    <td class="w-25">
                        <a href="{{route('admin.tarea.index', $segmentoLibro->id)}}"
                           class="nav-link-style mr-2 text-primary" data-toggle="tooltip"
                           data-placement="top" title="Ingresar Tareas">
                            <i class="fas fa-tasks"></i>
                        </a>
                        <a href="{{route('admin.test.index', $segmentoLibro->id)}}"
                           class="nav-link-style mr-2 text-success" data-toggle="tooltip"
                           data-placement="top" title="Ingresar Test">
                            <i class="far fa-question-circle"></i>
                        </a>
                        <a href="" data-toggle="modal" data-target="#deleteModal" data-id="{{ $segmentoLibro->id }}"
                           class="text-danger">
                            <i class="fe-trash-2"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="m-3">
            {{--{{--}}
            {{--$libros--}}
            {{--->appends(['created_at' => request('created_at'),'search' => request('search'),])--}}
            {{--->links()--}}
            {{--}}--}}
        </div>
    </div>
</div>

@include('includes.forms.modal-delete',['name_route'=>'admin.segmento_libro.destroy'])

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal', 'formDelete');
        // window.onload = function () {
        //     $('#deleteModal').on('show.bs.modal', function (event) {
        //         var button = $(event.relatedTarget); // Button that triggered the modal
        //         var id = button.data('id'); // Extract info from data-* attributes
        //         // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        //         // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        //
        //         action = $('#formDelete').attr('data-action').slice(0, -1);
        //         action += id;
        //         console.log(action);
        //
        //         $('#formDelete').attr('action', action);
        //
        //         var modal = $(this);
        //         modal.find('.modal-title').text('Borrar el Archivo: ' + id);
        //     });
        // };
    </script>
@endsection