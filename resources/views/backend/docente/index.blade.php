@extends('layouts.aplicacion.app')

@section('content')
    @include('includes.forms.filter.institucion',['name_route'=>'admin.docente.index'])

    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Listado de Docentes
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás los docentes registrados en la aplicaci&oacute;n
                    </div>
                </div>
                <div class="col col-3 text-right">
                    {{--<a class="btn btn-success mt-3 mb-3" href="{{ route('admin.institucion.create') }}">--}}
                    {{--<i class="fa fa-plus"></i>--}}
                    {{--</a>--}}
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Ciudad</th>
                    <th>Instituci&oacute;n</th>
                    <th>Docente</th>
                    <th>Aula</th>
                    <th class="text-center">
                        <small>¿Representante curso?</small>
                    </th>
                    <th>Estado</th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th>
                        <small>Updated_at</small>
                    </th>
                    <th class="text-center">
                        <i class="fas fa-cogs"></i>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($docentes as $docente)
                    @php
                        $estadoRegistro = $docente->estadoRegistro;
                    @endphp
                    <tr>

                        <td>{{ $docente->aula->institucion->nombre_canton}}</td>
                        <td>{{ $docente->nombre_institucion }}</td>
                        <td>
                            {{ $docente->nombre_docente}}
                            <div class="text-success small">{{ $docente->cedula_docente}}</div>
                        </td>
                        <td>{{ $docente->nombre_aula }}</td>
                        <td class="text-center small">{!! \App\Helpers\Helper::obtenerIconTruFalse($docente->representante_curso) !!} </td>
                        <td>
                            @php
                                echo \App\Helpers\Helper::obtenerIconEstado($estadoRegistro->codigo);
                            @endphp
                            <small>{{ $estadoRegistro->nombre }}</small>
                        </td>
                        <td class="created_at">
                            <small>{{ $docente->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="created_at">
                            <div>
                                <small>{{ $docente->updated_at->format('M d Y h:i') }}</small>
                            </div>
                        </td>
                        <td class="text-center">
                            @if($docente->user->perfil)
                                <a href="{{ route('admin.docente.aprobar',$docente->id) }}"
                                   class="btn btn-sm btn-outline-success mr-1">
                                    <i class="fas fa-check"></i>
                                </a>
                                <a href="{{ route('admin.docente.cancelar',$docente->id) }}"
                                   class="btn btn-sm btn-outline-danger">
                                    <i class="fas fa-times"></i>
                                </a>
                            @else
                                <span for="" class="text-warning">Sin informaci&oacute;n <br>del estudiante</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="m-3">
                {{--{{--}}
                {{--$docente--}}
                {{--->appends(['created_at' => request('created_at'),'search' => request('search'),])--}}
                {{--->links()--}}
                {{--}}--}}
            </div>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'admin.institucion.destroy'])

    {{--<script>--}}
    {{--window.onload = function () {--}}
    {{--$('#deleteModal').on('show.bs.modal', function (event) {--}}
    {{--var button = $(event.relatedTarget); // Button that triggered the modal--}}
    {{--var id = button.data('id'); // Extract info from data-* attributes--}}
    {{--// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).--}}
    {{--// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.--}}

    {{--action = $('#formDelete').attr('data-action').slice(0, -1);--}}
    {{--action += id;--}}
    {{--console.log(action);--}}

    {{--$('#formDelete').attr('action', action);--}}

    {{--var modal = $(this);--}}
    {{--modal.find('.modal-title').text('Borrar el Perido Lectivo: ' + id);--}}
    {{--});--}}
    {{--};--}}
    {{--</script>--}}

@endsection