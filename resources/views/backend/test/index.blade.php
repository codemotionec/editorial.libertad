<div class="card">
    <div class="card-header card-header-primary">
        <div class="row">
            <div class="col col-9">
                <h6 class="strong">Test Ingresados</h6>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="thead-dark">
            <tr>
                <th>Descripcion</th>
                <th>Calificaci&oacute;n</th>
                <th>Ejecuci&oacute;n</th>
                <th>Terminaci&oacute;n</th>
                <th>
                    <small>Created_at</small>
                </th>
                <th class="text-center"><i class="fas fa-cogs"></i></th>
            </tr>
            </thead>
            <tbody>
            @foreach($tests as $test)
                <tr>
                    <td class="w-25">{!! $test->descripcion !!}</td>
                    <td>{!! $test->tipoCalificacion->descripcion !!}</td>
                    <td>{!! \App\Helpers\Helper::formatDate($test->fecha_ejecucion) !!}</td>
                    <td>{!! \App\Helpers\Helper::formatDate($test->fecha_terminacion) !!}</td>
                    <td class="created_at">
                        <small>{{ $test->created_at->format('M d Y h:i') }}</small>
                    </td>
                    <td class="w-auto">
                        <a href="{{route('admin.pregunta-desarrollo.index', $test->id)}}" class="nav-link-style mr-1"
                           data-toggle="tooltip"
                           data-placement="top"
                           title="{{__('INGRESAR PREGUNTAS')}}">
                            <i class="fas fa-tasks"></i>
                        </a>
                        <a href="{{route('admin.test.edit',$test->id)}}" class="nav-link-style mr-1"
                           data-toggle="tooltip" data-placement="top"
                           title="{{__('EDITAR')}}">
                            <i class="fe-edit-2"></i>
                        </a>
                        <a data-toggle="modal" data-target="#deleteModal" data-id="{{ $test->id }}"
                                class="nav-link-style text-danger">
                            <i class="fe-trash-2"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@include('includes.forms.modal-delete',['name_route'=>'admin.test.destroy'])

<script>
    // window.onload = function () {
    //     $('#deleteModal').on('show.bs.modal', function (event) {
    //         var button = $(event.relatedTarget); // Button that triggered the modal
    //         var id = button.data('id'); // Extract info from data-* attributes
    //         // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    //         // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    //
    //         action = $('#formDelete').attr('data-action').slice(0, -1);
    //         action += id;
    //         console.log(action);
    //
    //         $('#formDelete').attr('action', action);
    //
    //         var modal = $(this);
    //         modal.find('.modal-title').text('Borrar el Archivo: ' + id);
    //     });
    // };
</script>
