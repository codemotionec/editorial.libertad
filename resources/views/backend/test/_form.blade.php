@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">
                    Test: {{$segmentoLibro->libro->titulo}} / {{$segmentoLibro->unidad}}
                </h6>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="row">
                <div class="col col-12 mt-2">
                    <label for="tipo_calificacion_id" class="bmd-label-floating">Tipo calificaci&oacute;n</label>
                    <select name="tipo_calificacion_id" id="tipo_calificacion_id"
                            data-ajax--url="{{route('api.tipo_calificacion.select2')}}"
                            class="form-control select2">
                        @if($model->tipo_calificacion_id)
                            <option value="{{$model->tipo_calificacion_id}}">{{$model->tipoCalificacion->descripcion}}</option>
                        @endif
                    </select>

                    @error('tipo_calificacion_id')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="col col-12 mt-2">
                    <label for="descripcion" class="bmd-label-floating">Descripcion</label>
                    <textarea name="descripcion" id="descripcion" class="form-control ckeditor-content">
                        {{old('descripcion',$model->descripcion)}}
                    </textarea>

                    @error('descripcion')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label pl-0">Fecha ejecuci&oacute;n</label>
                        <div class="input-group-overlay">
                            <input class="form-control appended-form-control cs-date-picker cs-date-range" type="text"
                                   placeholder="Seleccionar fecha"
                                   data-datepicker-options='{"altInput": false, "altFormat": "Y-m-d", "dateFormat": "Y-m-d"}'
                                   data-linked-input="#return-date" name="fecha_ejecucion" id="fecha_ejecucion"
                                   value="{{old('fecha_ejecucion',$model->fecha_ejecucion)}}">

                            <div class="input-group-append-overlay">
                                <span class="input-group-text"><i class="fe-calendar"></i></span>
                            </div>
                        </div>
                        @error('fecha_ejecucion')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label pl-0">Fecha terminaci&oacute;n</label>
                        <div class="input-group-overlay">
                            <input class="form-control appended-form-control cs-date-picker" type="text"
                                   placeholder="Seleccionar fecha"
                                   data-datepicker-options='{"altInput": true, "altFormat": "Y-m-d", "dateFormat": "Y-m-d"}'
                                   id="return-date" name="fecha_terminacion"
                                   value="{{old('fecha_terminacion',$model->fecha_terminacion)}}"
                                   required>
                            <div class="input-group-append-overlay">
                                <span class="input-group-text"><i class="fe-calendar"></i></span>
                            </div>
                        </div>
                        @error('fecha_terminacion')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>

            <input type="hidden" id="token" value="{{ csrf_token() }}">
            <input type="submit" value="Cargar" class="mt-3 btn btn-primary" id="cargar-libro">
        </div>
    </div>
</div>
