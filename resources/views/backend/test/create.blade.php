@extends('layouts.aplicacion.app')

@section('breadcrum')
    <div class="row ">
        {{--<div class="col col-7">--}}
        <div class="col col-5">
            @if(!$model->id)
                <div class="alert alert-primary" role="alert">
                    Crear nuevo test
                </div>
            @else
                <div class="alert alert-warning" role="alert">
                    Actualizar test #{{$model->id}}
                </div>
            @endif
        </div>
        <div class="col col-6">
            <nav aria-label="breadcrumb ">
                <ol class="breadcrumb mt-2">
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.home')}}">Inicio</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.libro.index')}}">Libros</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{route('admin.segmento_libro.index',$segmentoLibro->libro_id)}}">
                            Unidades
                        </a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Test</li>
                </ol>
            </nav>
        </div>
        <div class="col col-1 text-center">
            <a class="btn btn-success btn-sm mt-2"
               href="{{ route('admin.test.index',$segmentoLibro->id) }}">
                <i class="fe-plus"></i>
            </a>
        </div>
    </div>
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <div class="row">
        <div class="col col-12">
            <form id="form-archivo"
                  @if(!$model->id)
                  action="{{ route("admin.test.store",$segmentoLibro->id) }}"
                  @else
                  action="{{ route("admin.test.update",$model->id) }}"
                  @endif
                  method="POST"
                  enctype='multipart/form-data'>
                @if(!$model->id)
                    @method('POST')
                @else
                    @method('PUT')
                @endif

                @include('backend.test._form', compact('segmentoLibro','model'))
            </form>
        </div>
        <div class="col col-12">
            @include('backend.test.index', compact('tests'))
        </div>
    </div>
    @include('includes.forms.modal-delete',['name_route'=>'admin.test.destroy'])
@endsection

@section('scripts')
    <script type="text/javascript">
        // window.onload = function () {
        //     $('#deleteModal').on('show.bs.modal', function (event) {
        //         var button = $(event.relatedTarget); // Button that triggered the modal
        //         var id = button.data('id'); // Extract info from data-* attributes
        //         // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        //         // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        //
        //         action = $('#formDelete').attr('data-action').slice(0, -1);
        //         action += id;
        //
        //         $('#formDelete').attr('action', action);
        //
        //         var modal = $(this);
        //         modal.find('.modal-title').text('Borrar Test #' + id);
        //     });
        // };
    </script>
@endsection
