@extends('layouts.aplicacion.app')
@section('content')
{{--<main id="main">--}}
    {{--<!-- ======= Breadcrumbs ======= -->--}}
    {{--<div class="breadcrumbs" data-aos="fade-in">--}}
      {{--<div class="container">--}}
        {{--<h2>ERROR 404</h2>--}}
      {{--</div>--}}
    {{--</div><!-- End Breadcrumbs -->--}}
    {{--<div class="container mt-5 mb-5">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-8 offset-md-2 text-center">--}}
                {{--<p class="fs-28">El contenido al que intenta acceder no existe o eta inhabilitado.</p>--}}
                {{--<a class="fs-28 text-red bolder" href="/">Volver al inicio</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</main>--}}

<div class="container d-flex flex-column justify-content-center pt-5 mt-n6" style="flex: 1 0 auto;">
    <div class="pt-7 pb-5">
        <div class="text-center mb-2 pb-4">
            <h1 class="mb-5"><img class="d-inline-block" src="{{asset('img/404-text.svg')}}" alt="Error 404"/><span class="sr-only">Error 404</span>
            </h1>
            <h2>¡P&aacute;gina no encontrada!</h2>
            <p class="pb-2">Parece que no podemos encontrar la página que busca.</p>
            <a class="btn btn-translucent-primary mr-3" href="{{route('app.dashboard')}}">Ir al inicio</a>
            {{--<span>Or try</span>--}}
        </div>
        {{--<div class="input-group-overlay mx-auto" style="max-width: 390px;">--}}
            {{--<div class="input-group-prepend-overlay">--}}
                {{--<span class="input-group-text"><i class="fe-search"></i></span>--}}
            {{--</div>--}}
            {{--<input class="form-control prepended-form-control" type="text" placeholder="Search">--}}
        {{--</div>--}}
    </div>
</div>
@endsection