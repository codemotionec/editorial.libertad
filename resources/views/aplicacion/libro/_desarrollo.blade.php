@extends('layouts.aplicacion.app')

@section('breadcrum')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('app.dashboard')}}">Inicio</a></li>
            <li class="breadcrumb-item">
                <a href="{{route('app.desarrollo.index',$materialEstudiante->id)}}">Libro</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Tarea</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div id="outerContainer">
        <h3 class="text-aqua">{{$tarea->segmentoLibro->libro->titulo}}</h3>
        <h5 class="text-black-light">{{$tarea->segmentoLibro->unidad}}</h5>

        <div class="row">
            <div class="col col-12">
                <blockquote class="blockquote">
                    <p class="mb-0 text-black-50 small">{{$tarea->tipoDesarrollo->descripcion}}</p>
                    <footer class="blockquote-footer text-justify">{!! $tarea->objetivo !!}</footer>
                </blockquote>
            </div>
        </div>

        @if($desarrollos = \App\Models\Desarrollo::obtenerDesarrollosTareaAll($tarea->id))
            @foreach($desarrollos as $desarrollo)
                @php
                    $respuestasDesarrollo = \App\Models\RespuestaDesarrollo::obtenerRespuestaDesarrolloDesarrolloAll($desarrollo->id);
                @endphp

                <form action="{{route('app.desarrollo.store',['materialEstudiante'=>$materialEstudiante->id])}}"
                      method="post" id="frm-test-{{$desarrollo->id}}"
                      class="form-desarrollo needs-validation"
                      novalidate>
                    @csrf
                    @method('POST')
                    <div class="card my-4">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h6 class="my-1">{!! $desarrollo->descripcion !!}</h6>
                                <span class="work-check my-0" style="display: none; font-size: 25px">
                                    <i class="text-success fe-check-circle"></i>
                                </span>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    @include('aplicacion.libro._respuesta_desarrollo',compact('desarrollo', 'respuestasDesarrollo','materialEstudiante'))
                                </div>
                                <div class="col-md-6 py-4">
                                    <img src="{{url('storage/desarrollo/imagen/'.$desarrollo->imagen)}}" alt="">
                                </div>
                            </div>
                        </div>

                        <div class="card-footer text-center">
                            <button type="submit" class="btn btn-translucent-primary btn-submit"
                                    data-form="frm-test-{{$desarrollo->id}}">
                                <i class="spinner-grow spinner-grow-sm mr-2" role="status"
                                   aria-hidden="true"></i>
                                DESARROLLAR
                            </button>
                        </div>
                    </div>
                </form>

            @endforeach
        @endif
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/formSubmit.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.form-desarrollo').on('submit', (function (e) {
                e.preventDefault();
                var form = this;
                window.showDialog('Esta seguro que desea enviar la tarea', function () {
                    guardarDesarrollo(form);
                });
            }));
        });

        function guardarDesarrollo(form) {
            var formData = new FormData(form);
            var action = $(form).attr('action');
            var form_id = $(form).attr('id');

            window.sendAjax(action, function (data) {
                if (data.status == 'success') {
                    window.showCustomSucces(data.message);
                }
            }, 'POST', formData, true, function (errors) {
                window.setErrorForm(errors, form_id);
                //console.log(errors);
            }, form_id, true);
        }
    </script>
@endsection

