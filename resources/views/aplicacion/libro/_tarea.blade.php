@php
    if(!isset($avance)){
        $avance = $materialEstudiante ? \App\Helpers\Helper::obtenerAvanceTareaMaterialEstudiante($tarea, $materialEstudiante):null;
    }

    if(!isset($url_desarrollo)){
        $url_desarrollo = $materialEstudiante ? route('app.desarrollo.tarea',['material_estudiante' => $materialEstudiante->id, 'tarea' => $tarea->id]):null;
    }
@endphp
<div class="@if(isset($content)){{$content}} @else col-lg-4 col-md-4 @endif col-sm-6 mb-grid-gutter">
    <article class="card card-hover" style="height: 100%;">
        <div class="card-header">
            <div class="meta-link font-size-sm mb-2 text-primary">
                @if($avance==100)
                    <i class="fe-check-circle text-success mx-1"></i>
                @endif
                {!! $tarea->tipoDesarrollo->descripcion !!}
            </div>
        </div>
        <div class="card-body d-flex justify-content-between flex-column">
            <div class="media-body ml-1 mt-n1 text-justify">
                {!! $tarea->objetivo !!}
            </div>

            @if(\App\Helpers\Helper::validarFechaEjecucionConActual($tarea->fecha_ejecucion, $tarea->fecha_terminacion))
                <a href="{{$url_desarrollo}}"
                   class="btn btn-outline-primary btn-sm text-center w-50 mx-auto">
                    {{__('Desarrollar')}}
                </a>
            @else
                <div class="alert alert-warning" role="alert">
                    <i class="fe-alert-triangle font-size-xl mr-1"></i>
                    {{__('Desarrollo')}}:
                    <span class="meta-link font-size-xs">
                        <i class="fe-calendar mr-1 mt-n1"></i>&nbsp;
                        {{\App\Helpers\Helper::formatDate($tarea->fecha_ejecucion,'M d')}}
                    </span>
                    <span class="fe-arrow-right"></span>
                    <span class="meta-link font-size-xs">
                        <i class="fe-calendar mr-1 mt-n1"></i>&nbsp;
                        {{\App\Helpers\Helper::formatDate($tarea->fecha_terminacion,'M d')}}
                    </span>
                </div>
            @endif
        </div>
        <div class="card-footer">
            <div class="mt-3 text-right text-nowrap">
                <div class="font-size-sm mb-2">
                    <div class="meta-link font-size-xs text-primary">
                        <i class="fe-activity mr-1"></i>{{__('Avance')}}&nbsp;{{$avance}}%
                    </div>
                    <span class="meta-divider"></span>
                    <span class="meta-link font-size-xs">
                        <i class="fe-calendar mr-1 mt-n1"></i>&nbsp;{{\App\Helpers\Helper::formatDate($tarea->created_at->format('Y-m-d'),'M d')}}
                    </span>
                </div>
                <div class="progress mb-3" style="height: 4px;">
                    <div class="progress-bar" role="progressbar" style="width: {{$avance}}%" aria-valuenow="1"
                         aria-valuemin="0"
                         aria-valuemax="100">

                    </div>
                </div>
            </div>
        </div>
    </article>
</div>