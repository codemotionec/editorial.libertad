@foreach($respuestasDesarrollo as $respuestaDesarrollo)
    @php
        $desarrolloEstudiante = \App\Models\DesarrolloEstudiante::getDesarrolloEstudianteMaterialEstudianteRespuestaDesarrollo($materialEstudiante,$respuestaDesarrollo);
    @endphp
    <div class="row my-2">
        <div class="col">
            @switch($respuestaDesarrollo->tipoRespuesta->codigo)
                @case('ARCHIVO')
                <label class="text-primary text-uppercase">
                    Cargar un archivo
                    <small class="text-success ml-3">para justificar la tarea <b>*</b></small>
                </label>
                <input type="file" id="file_{{$respuestaDesarrollo->id}}" name="file[{{$respuestaDesarrollo->id}}]"
                       class="dropify file"
                       data-default-file="@if($desarrolloEstudiante->archivo??null){{url("storage/desarrollo_estudiante/{$materialEstudiante->estudiante_id}/".$desarrolloEstudiante->archivo)}}@endif"
                       data-allowed-file-extensions='["pdf", "txt","xls","xlsx","docx","doc"]' required>
                @break
                @case('IMAGEN')
                <label class="text-primary text-uppercase">
                    Cargar una imagen
                    <small class="text-success ml-3">para justificar la tarea <b>*</b></small>
                </label>
                <input type="file" id="file_{{$respuestaDesarrollo->id}}" name="file[{{$respuestaDesarrollo->id}}]"
                       class="dropify file"
                       data-default-file="@if($desarrolloEstudiante->archivo??null){{url("storage/desarrollo_estudiante/{$materialEstudiante->estudiante_id}/".$desarrolloEstudiante->archivo)}}@endif"
                       data-allowed-file-extensions='["jpg", "png","jpeg"]'
                       required>
                @break
                @case('AUDIO')
                <label class="text-primary text-uppercase">
                    Cargar audio
                    <small class="text-success ml-3">para justificar la tarea <b>*</b></small>
                </label>
                <input type="file" name="file[{{$respuestaDesarrollo->id}}]"
                       data-default-file="@if($desarrolloEstudiante->archivo??null){{url("storage/desarrollo_estudiante/{$materialEstudiante->estudiante_id}/".$desarrolloEstudiante->archivo)}}@endif"
                       class="dropify file" required>
                @break
                @case('VIDEO')
                <label class="text-primary text-uppercase">
                    Cargar video
                    <small class="text-success ml-3">para justificar la tarea <b>*</b></small>
                </label>
                <input type="file" id="file_{{$respuestaDesarrollo->id}}" name="file[{{$respuestaDesarrollo->id}}]"
                       data-default-file="@if($desarrolloEstudiante->archivo??null){{url("storage/desarrollo_estudiante/{$materialEstudiante->estudiante_id}/".$desarrolloEstudiante->archivo)}}@endif"
                       class="dropify file" required>
                @break
                @case('TEXTO')
                <label class="text-primary text-uppercase">Ingresar contenido </label>
                <textarea name="respuesta[{{$respuestaDesarrollo->id}}]" id="respuesta_{{$respuestaDesarrollo->id}}"
                          class="ckeditor-content" cols="30" rows="10"
                          required>@if($desarrolloEstudiante){{$desarrolloEstudiante->respuesta}}@endif</textarea>
                @break
            @endswitch
        </div>
    </div>
@endforeach