@php
    $libro = $materialEstudiante->materialDocente->libro;
@endphp

<div class="col-md-4 col-sm-6 mb-grid-gutter">
    <div class="card card-product card-hover">
        <a class="card-img-top" href="{{route('app.desarrollo.index',$materialEstudiante->id)}}">
            <img src="{{asset('storage/portada_libro/'.$libro->portada)}}" alt="{{$libro->title}}"/>
        </a>
        <div class="card-body">
            <a class="text-primary meta-link mb-1 text-uppercase"
               href="{{route('app.desarrollo.index',$materialEstudiante->id)}}">
                {{$libro->titulo}}
            </a>
            <h3 class="font-size-md font-weight-medium mb-2">
                <a class="meta-link text-justify" href="{{route('app.desarrollo.index',$materialEstudiante->id)}}">
                    {!! $libro->resumen !!}
                </a>
            </h3>
            <div class="text-center">
                <img width="80" class="img-thumbnail rounded-circle" src="{{asset('storage/instituciones/'.$materialEstudiante->estudiante->imagen_institucion)}}"
                     alt="{{$materialEstudiante->estudiante->nombre_institucion}}">
            </div>
            <div class="text-primary text-uppercase text-center">
                {{$materialEstudiante->estudiante->nombre_institucion}}
            </div>
            <div class="text-center">
                {{$materialEstudiante->estudiante->nombre_aula}}
            </div>
            <div class="text-center">
                {{$materialEstudiante->materialDocente->docente->nombre_docente}}
            </div>
        </div>
        <div class="card-footer text-center">

            <a class="btn btn-translucent-primary uppercase"
               href="{{route('app.desarrollo.index',$materialEstudiante->id)}}">
                {{__('Desarrollar')}}
            </a>
            {{--<div class="star-rating">--}}
            {{--<i class="sr-star fe-star-filled active"></i>--}}
            {{--<i class="sr-star fe-star-filled active"></i>--}}
            {{--<i class="sr-star fe-star-filled active"></i>--}}
            {{--<i class="sr-star fe-star"></i>--}}
            {{--<i class="sr-star fe-star"></i>--}}
            {{--</div>--}}
            <div class="d-flex align-items-center">
                <a class="btn-wishlist" href="#">
                    <i class="fe-heart"></i>
                    <span class="btn-tooltip">Wishlist</span>
                </a>
            </div>
        </div>
    </div>
</div>