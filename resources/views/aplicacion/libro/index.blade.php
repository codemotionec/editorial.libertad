@extends('layouts.aplicacion.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <!-- Content-->
            <div class="col-lg-10 py-4 mb-2 mb-sm-0 pb-sm-5">
                <div class="pb-4">
                    <h3 class="text-primary">{{$libro->titulo}}</h3>
                </div>

                <div class="text-justify">
                    {!! $libro->resumen !!}
                </div>

                {{--<h2 class="h3 pt-2 pb-4 text-primary">DESARROLLO</h2>--}}

                <div class="row position-relative no-gutters align-items-center border-top border-bottom mb-4">
                    <div class="col-md-12 py-3 pr-md-3">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link"
                                   id="link-segmento-general"
                                   data-toggle="tab"
                                   href="#segmento-general" role="tab"
                                   aria-controls="segmento-general" aria-selected="true">
                                    <i class="fe-layers"></i> GENERAL
                                </a>
                            </li>
                            @foreach($segmentoLibros as $segmentoLibro)
                                <li class="nav-item">
                                    <a class="nav-link @if ($loop->first) active show @endif"
                                       id="link-segmento-{{$segmentoLibro->id}}"
                                       data-toggle="tab"
                                       href="#segmento-{{$segmentoLibro->id}}" role="tab"
                                       aria-controls="segmento-{{$segmentoLibro->id}}" aria-selected="true">
                                        <i class="fe-layers"></i> {{$segmentoLibro->unidad}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="bd-example bd-example-tabs">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade"
                             id="segmento-general" role="tabpanel" aria-labelledby="home-tab">
                            <div class="text-justify">
                                {!! $libro->contenido !!}
                            </div>
                        </div>
                        @foreach($segmentoLibros as $segmentoLibro)
                            <div class="tab-pane fade @if ($loop->first) active show @endif"
                                 id="segmento-{{$segmentoLibro->id}}" role="tabpanel" aria-labelledby="home-tab">
                                @include('aplicacion.unidad_libro.index',compact('segmentoLibro','materialEstudiante'))
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
