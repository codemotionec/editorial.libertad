@php
    $respuestasDesarrollo = \App\Models\RespuestaDesarrollo::obtenerRespuestaDesarrolloDesarrolloAll($desarrollo->id);
@endphp

@foreach($respuestasDesarrollo as $respuestaDesarrollo)
    <div class="row">

        @switch($respuestaDesarrollo->tipoRespuesta->codigo)
            @case('ARCHIVO')
            <div class="col col-6">
                <input type="file" class="dropify">

            </div>
            @break
            @case('IMAGEN')
            <div class="col col-6">
                <input type="file" class="dropify">
            </div>
            @break
            @case('AUDIO')
            <div class="col col-6">
                <input type="file" class="dropify">
            </div>
            @break
            @case('VIDEO')
            <div class="col col-6">
                <input type="file" class="dropify">
            </div>
            @break
            @case('TEXTO')
            <div class="col col-12">
                <textarea name="" id="" class="ckeditor-content" cols="30" rows="10"></textarea>
            </div>
            @break
        @endswitch
    </div>
@endforeach