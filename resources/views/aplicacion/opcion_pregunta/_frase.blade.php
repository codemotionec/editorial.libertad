<div class="row">
    <div class="col col-12 text-left">
        <label class="text-primary small"><strong><i class="fas fa-info-circle"></i> ESPACIO A COMPLETAR CON MAYUSCULAS Y NEGRILLA</strong></label>
    </div>
    <div class="col col-12 ">
        <textarea name="pregunta{{$preguntaDesarrollo->id}}" id="pregunta_{{$preguntaDesarrollo->id}}" cols="30"
                  class="ckeditor-content"
                  rows="10">
            {!! $preguntaDesarrollo->pregunta !!}
        </textarea>
        <button type="button" class="btn btn-warning mt-3">{{__('Contestar')}}</button>
    </div>
</div>