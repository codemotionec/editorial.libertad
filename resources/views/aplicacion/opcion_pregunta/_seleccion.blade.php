@foreach($opcionesPregunta as $opcionPregunta)
    <div class="row  bg-white mb-2">
        <div class="col col-1 text-center text-black-50 py-2">
            <i class="fas fa-long-arrow-alt-right"></i>
        </div>
        <div class="col col-8 text-black-50 py-2">
            {!! $opcionPregunta->descripcion !!}
        </div>
        <div class="col col-3 text-right py-1">
            <input type="checkbox" class="form-control check-toggle">
        </div>
    </div>
@endforeach

<div class="row">
    <div class="col col-12 mt-3">
        <button type="button" class="btn btn-warning">{{__('Contestar')}}</button>
    </div>
</div>