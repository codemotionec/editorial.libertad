<div class="row">
    @foreach($opcionesPregunta as $opcionPregunta)
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover">
                <a class="card-img-top"
                   href="shop-single.html">
                    <img src="{{asset('storage/preguntas/imagen/'.$opcionPregunta->imagen)}}"
                         alt="{{$opcionPregunta->imagen}}"/>
                </a>
                <div class="card-footer">
                    <input type="checkbox" class="form-control check-toggle">
                </div>
            </div>
        </div>
        {{--<div class="card mr-2" style="width: 18rem;">--}}
        {{--<img height="200" class="card-img-top" src="{{asset('storage/preguntas/imagen/'.$opcionPregunta->imagen)}}"--}}
        {{--alt="Card image cap">--}}
        {{--<div class="card-body text-right">--}}
        {{--<input type="checkbox" class="form-control check-toggle">--}}
        {{--</div>--}}
        {{--</div>--}}
    @endforeach
</div>
<div class="row">
    <div class="col col-12 mt-3">
        <button type="button" class="btn btn-warning">{{__('Contestar')}}</button>
    </div>
</div>