@extends('layouts.aplicacion.app')
@section('breadcrum')
    {{ Breadcrumbs::render('app.registro-libro.index', '') }}
@endsection

@section('content')
    <div class="section-title">
        <h1 class="display-5">{{__('Libros')}}</h1>
        <p class="pb-2">Mis Libros para desarrollar</p>
    </div>

    <div class="row">
        @foreach($materialEstudiantes as $materialEstudiante)
            @include('aplicacion.libro._libro',compact('materialEstudiante'))
        @endforeach
    </div>

@endsection