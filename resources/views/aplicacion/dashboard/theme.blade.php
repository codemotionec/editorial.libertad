@extends('layouts.aplicacion.app')

@section('breadcrum')
    @include('layouts.aplicacion.includes.breadcrum')
@endsection

@section('content')
    <!-- Shop grid-->
    <div class="row">
        <!-- Item-->
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover">
                <a class="card-img-top"
                   href="shop-single.html">
                    <img src="img/shop/catalog/01.jpg" alt="Product thumbnail"/>
                </a>
                <div class="card-body">
                    <a class="meta-link font-size-xs mb-1" href="#">Men's
                        clothing</a>
                    <h3 class="font-size-md font-weight-medium mb-2">
                        <a class="meta-link"
                           href="shop-single.html">Simple
                            Cotton Gray T-shirt</a>
                    </h3>
                    <span class="text-heading font-weight-semibold">$19.00</span>
                </div>
                <div class="card-footer">
                    <div class="star-rating"><i class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star"></i><i class="sr-star fe-star"></i>
                    </div>
                    <div class="d-flex align-items-center"><a class="btn-wishlist" href="#"><i
                                    class="fe-heart"></i><span
                                    class="btn-tooltip">Wishlist</span></a><span
                                class="btn-divider"></span><a class="btn-addtocart" href="#"><i
                                    class="fe-shopping-cart"></i><span
                                    class="btn-tooltip">To Cart</span></a></div>
                </div>
            </div>
        </div>
        <!-- Item-->
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover"><a class="card-img-top"
                                                         href="shop-single.html"><img
                            src="img/shop/catalog/02.jpg" alt="Product thumbnail"/></a>
                <div class="card-body"><a class="meta-link font-size-xs mb-1" href="#">Electronics</a>
                    <h3 class="font-size-md font-weight-medium mb-2"><a class="meta-link"
                                                                        href="shop-single.html">Wireless
                            Headphones</a></h3><span
                            class="text-heading font-weight-semibold">$165.00</span>
                </div>
                <div class="card-footer">
                    <div class="star-rating"><i class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star"></i>
                    </div>
                    <div class="d-flex align-items-center"><a class="btn-wishlist" href="#"><i
                                    class="fe-heart"></i><span
                                    class="btn-tooltip">Wishlist</span></a><span
                                class="btn-divider"></span><a class="btn-addtocart" href="#"><i
                                    class="fe-shopping-cart"></i><span
                                    class="btn-tooltip">To Cart</span></a></div>
                </div>
            </div>
        </div>
        <!-- Item-->
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover"><span
                        class="badge badge-floating badge-pill badge-success">New</span><a
                        class="card-img-top" href="shop-single.html"><img src="img/shop/catalog/03.jpg"
                                                                          alt="Product thumbnail"/></a>
                <div class="card-body"><a class="meta-link font-size-xs mb-1" href="#">Accessories</a>
                    <h3 class="font-size-md font-weight-medium mb-2"><a class="meta-link"
                                                                        href="shop-single.html">Military
                            Cotton Cap</a></h3><span
                            class="text-heading font-weight-semibold">$28.00</span>
                </div>
                <div class="card-footer">
                    <div class="star-rating"><i class="sr-star fe-star"></i><i
                                class="sr-star fe-star"></i><i class="sr-star fe-star"></i><i
                                class="sr-star fe-star"></i><i class="sr-star fe-star"></i>
                    </div>
                    <div class="d-flex align-items-center"><a class="btn-wishlist" href="#"><i
                                    class="fe-heart"></i><span
                                    class="btn-tooltip">Wishlist</span></a><span
                                class="btn-divider"></span><a class="btn-addtocart" href="#"><i
                                    class="fe-shopping-cart"></i><span
                                    class="btn-tooltip">To Cart</span></a></div>
                </div>
            </div>
        </div>
        <!-- Item-->
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover"><span
                        class="badge badge-floating badge-pill badge-danger">Sale</span><a
                        class="card-img-top" href="shop-single.html"><img src="img/shop/catalog/04.jpg"
                                                                          alt="Product thumbnail"/></a>
                <div class="card-body"><a class="meta-link font-size-xs mb-1" href="#">Electronics</a>
                    <h3 class="font-size-md font-weight-medium mb-2"><a class="meta-link"
                                                                        href="shop-single.html">360
                            Degrees Camera</a></h3>
                    <del class="font-size-sm text-muted mr-1">$120.40</del>
                    <span class="text-heading font-weight-semibold">$98.75</span>
                </div>
                <div class="card-footer">
                    <div class="star-rating"><i class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star"></i>
                    </div>
                    <div class="d-flex align-items-center"><a class="btn-wishlist" href="#"><i
                                    class="fe-heart"></i><span
                                    class="btn-tooltip">Wishlist</span></a><span
                                class="btn-divider"></span><a class="btn-addtocart" href="#"><i
                                    class="fe-shopping-cart"></i><span
                                    class="btn-tooltip">To Cart</span></a></div>
                </div>
            </div>
        </div>
        <!-- Item-->
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover"><a class="card-img-top"
                                                         href="shop-single.html"><img
                            src="img/shop/catalog/05.jpg" alt="Product thumbnail"/></a>
                <div class="card-body"><a class="meta-link font-size-xs mb-1" href="#">Men's shoes</a>
                    <h3 class="font-size-md font-weight-medium mb-2"><a class="meta-link"
                                                                        href="shop-single.html">Sport
                            Running Sneakers</a></h3><span class="text-heading font-weight-semibold">$140.00</span>
                </div>
                <div class="card-footer">
                    <div class="star-rating"><i class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star"></i>
                    </div>
                    <div class="d-flex align-items-center"><a class="btn-wishlist" href="#"><i
                                    class="fe-heart"></i><span
                                    class="btn-tooltip">Wishlist</span></a><span
                                class="btn-divider"></span><a class="btn-addtocart" href="#"><i
                                    class="fe-shopping-cart"></i><span
                                    class="btn-tooltip">To Cart</span></a></div>
                </div>
            </div>
        </div>
        <!-- Item-->
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover"><a class="card-img-top"
                                                         href="shop-single.html"><img
                            src="img/shop/catalog/06.jpg" alt="Product thumbnail"/></a>
                <div class="card-body"><a class="meta-link font-size-xs mb-1" href="#">Backpacks</a>
                    <h3 class="font-size-md font-weight-medium mb-2"><a class="meta-link"
                                                                        href="shop-single.html">Vintage
                            Travel Backpack</a></h3><span class="text-heading font-weight-semibold">$82.00</span>
                </div>
                <div class="card-footer">
                    <div class="star-rating"><i class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i>
                    </div>
                    <div class="d-flex align-items-center"><a class="btn-wishlist" href="#"><i
                                    class="fe-heart"></i><span
                                    class="btn-tooltip">Wishlist</span></a><span
                                class="btn-divider"></span><a class="btn-addtocart" href="#"><i
                                    class="fe-shopping-cart"></i><span
                                    class="btn-tooltip">To Cart</span></a></div>
                </div>
            </div>
        </div>
        <!-- Item-->
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover"><a class="card-img-top"
                                                         href="shop-single.html"><img
                            src="img/shop/catalog/07.jpg" alt="Product thumbnail"/></a>
                <div class="card-body"><a class="meta-link font-size-xs mb-1" href="#">Accessories</a>
                    <h3 class="font-size-md font-weight-medium mb-2"><a class="meta-link"
                                                                        href="shop-single.html">3-Color
                            Sun Stash Hat</a></h3><span
                            class="text-heading font-weight-semibold">$25.99</span>
                </div>
                <div class="card-footer">
                    <div class="star-rating"><i class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star"></i><i class="sr-star fe-star"></i>
                    </div>
                    <div class="d-flex align-items-center"><a class="btn-wishlist" href="#"><i
                                    class="fe-heart"></i><span
                                    class="btn-tooltip">Wishlist</span></a><span
                                class="btn-divider"></span><a class="btn-addtocart" href="#"><i
                                    class="fe-shopping-cart"></i><span
                                    class="btn-tooltip">To Cart</span></a></div>
                </div>
            </div>
        </div>
        <!-- Item-->
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover"><a class="card-img-top"
                                                         href="shop-single.html"><img
                            src="img/shop/catalog/08.jpg" alt="Product thumbnail"/></a>
                <div class="card-body"><a class="meta-link font-size-xs mb-1" href="#">Electronics</a>
                    <h3 class="font-size-md font-weight-medium mb-2"><a class="meta-link"
                                                                        href="shop-single.html">Speaker
                            with Voice Control</a></h3><span class="text-heading font-weight-semibold">$49.99</span>
                </div>
                <div class="card-footer">
                    <div class="star-rating"><i class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star"></i>
                    </div>
                    <div class="d-flex align-items-center"><a class="btn-wishlist" href="#"><i
                                    class="fe-heart"></i><span
                                    class="btn-tooltip">Wishlist</span></a><span
                                class="btn-divider"></span><a class="btn-addtocart" href="#"><i
                                    class="fe-shopping-cart"></i><span
                                    class="btn-tooltip">To Cart</span></a></div>
                </div>
            </div>
        </div>
        <!-- Item-->
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover"><a class="card-img-top"
                                                         href="shop-single.html"><img
                            src="img/shop/catalog/09.jpg" alt="Product thumbnail"/></a>
                <div class="card-body"><a class="meta-link font-size-xs mb-1" href="#">Women's shoes</a>
                    <h3 class="font-size-md font-weight-medium mb-2"><a class="meta-link"
                                                                        href="shop-single.html">Women
                            Colorblock Sneakers</a></h3><span class="text-heading font-weight-semibold">$154.00</span>
                </div>
                <div class="card-footer">
                    <div class="star-rating"><i class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star"></i>
                    </div>
                    <div class="d-flex align-items-center"><a class="btn-wishlist" href="#"><i
                                    class="fe-heart"></i><span
                                    class="btn-tooltip">Wishlist</span></a><span
                                class="btn-divider"></span><a class="btn-addtocart" href="#"><i
                                    class="fe-shopping-cart"></i><span
                                    class="btn-tooltip">To Cart</span></a></div>
                </div>
            </div>
        </div>
        <!-- Item-->
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover"><a class="card-img-top"
                                                         href="shop-single.html"><img
                            src="img/shop/catalog/10.jpg" alt="Product thumbnail"/></a>
                <div class="card-body"><a class="meta-link font-size-xs mb-1" href="#">Accessories</a>
                    <h3 class="font-size-md font-weight-medium mb-2"><a class="meta-link"
                                                                        href="shop-single.html">Polarized
                            Sunglasses</a></h3><span class="text-heading font-weight-semibold">Out of stock</span>
                </div>
                <div class="card-footer">
                    <div class="star-rating"><i class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star"></i>
                    </div>
                    <div class="d-flex align-items-center"><a class="btn-wishlist" href="#"><i
                                    class="fe-heart"></i><span
                                    class="btn-tooltip">Wishlist</span></a><span
                                class="btn-divider"></span><a class="btn-details"
                                                              href="shop-single.html"><i
                                    class="fe-arrow-right"></i><span class="btn-tooltip">Details</span></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Item-->
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover"><a class="card-img-top"
                                                         href="shop-single.html"><img
                            src="img/shop/catalog/11.jpg" alt="Product thumbnail"/></a>
                <div class="card-body"><a class="meta-link font-size-xs mb-1" href="#">Men's shoes</a>
                    <h3 class="font-size-md font-weight-medium mb-2"><a class="meta-link"
                                                                        href="shop-single.html">Sport
                            Running Shoes</a></h3><span class="text-heading font-weight-semibold">$127.00</span>
                </div>
                <div class="card-footer">
                    <div class="star-rating"><i class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i>
                    </div>
                    <div class="d-flex align-items-center"><a class="btn-wishlist" href="#"><i
                                    class="fe-heart"></i><span
                                    class="btn-tooltip">Wishlist</span></a><span
                                class="btn-divider"></span><a class="btn-addtocart" href="#"><i
                                    class="fe-shopping-cart"></i><span
                                    class="btn-tooltip">To Cart</span></a></div>
                </div>
            </div>
        </div>
        <!-- Item-->
        <div class="col-md-4 col-sm-6 mb-grid-gutter">
            <div class="card card-product card-hover"><a class="card-img-top"
                                                         href="shop-single.html"><img
                            src="img/shop/catalog/12.jpg" alt="Product thumbnail"/></a>
                <div class="card-body"><a class="meta-link font-size-xs mb-1" href="#">Electronics</a>
                    <h3 class="font-size-md font-weight-medium mb-2"><a class="meta-link"
                                                                        href="shop-single.html">Smart
                            Watch Series 5</a></h3><span class="text-heading font-weight-semibold">$349.99</span>
                </div>
                <div class="card-footer">
                    <div class="star-rating"><i class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star-filled active"></i><i
                                class="sr-star fe-star"></i><i class="sr-star fe-star"></i>
                    </div>
                    <div class="d-flex align-items-center"><a class="btn-wishlist" href="#"><i
                                    class="fe-heart"></i><span
                                    class="btn-tooltip">Wishlist</span></a><span
                                class="btn-divider"></span><a class="btn-addtocart" href="#"><i
                                    class="fe-shopping-cart"></i><span
                                    class="btn-tooltip">To Cart</span></a></div>
                </div>
            </div>
        </div>
    </div>

@endsection