@extends('layouts.aplicacion.app')

@section('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/lightgallery.js@1.4.0/dist/css/lightgallery.min.css">
    <style type="text/css">
        .lg-outer {
            transition: background-color 600ms ease 0s;
            background-color: rgba(0, 0, 0, .9);
        }

        .calificacion-content img {
            max-height: 200px;
            width: auto;
            margin: 0 auto;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-4 mb-4 mb-lg-0">
            @include('aplicacion.tareas.calificar._sidebar',[
                'materialDocente' => $materialDocente,
                'segmentoLibro' => $tarea->segmentoLibro,
            ])
        </div>

        <div class="col-lg-8">
            <div class="d-flex flex-column h-100 bg-light rounded-lg box-shadow-lg p-4">
                <div class="py-2 p-md-3">
                    <h1 class="h3 mb-3 text-nowrap text-center text-sm-left">
                        Tareas
                        <span class="d-inline-block align-middle bg-faded-dark font-size-ms font-weight-medium rounded-sm py-1 px-2 ml-2">{{rand(1,20)}}</span>
                    </h1>

                    @foreach($materialEstudiantes as $materialEstudiante)
                        @include('aplicacion.tareas.calificar._estudiante', compact('materialEstudiante','tarea'))
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.2/js/lightgallery.js"></script>
    {{--<script src="https://cdn.jsdelivr.net/npm/lightgallery.js@1.4.0/dist/js/lg-thumbnail.min.js"></script>--}}
    {{--<script src="https://cdn.jsdelivr.net/npm/lightgallery.js@1.4.0/dist/js/lg-fullscreen.min.js"></script>--}}
    {{--<script src="{{asset('js/formSubmit.js')}}"></script>--}}
    <script type="text/javascript">
        $(document).ready(function () {
            $(".lightgallery").each(function () {
                var id = $(this).attr('id');
                $('#' + id).lightGallery();
            });


            $('.form-calificacion').on('submit', (function (e) {
                e.preventDefault();
                var form = this;
                // window.showDialog('Esta seguro que desea enviar la tarea', function () {
                guardarCalificacion(form);
                // });
            }));
        });

        function guardarCalificacion(form) {
            var formData = new FormData(form);
            var action = $(form).attr('action');
            var form_id = $(form).attr('id');

            window.sendAjax(action, function (data) {
                // if (data.status == 'success') {
                //     window.showCustomSucces(data.message);
                // }
            }, 'POST', formData, true, function (errors) {
                window.setErrorForm(errors, form_id);
                //console.log(errors);
            }, form_id, true);
        }
    </script>
@endsection

