<div class="bg-light rounded-lg box-shadow-lg">
    <div class="px-4 py-2 mb-1 text-center">
        <img class="d-block rounded-circle mx-auto my-2 w-50"
             src="{{asset('storage/portada_libro/'.$materialDocente->libro->portada)}}"
             alt="{{$materialDocente->libro->titulo}}"/>
    </div>
    <div class="px-4 py-2 mb-1 text-center">
        <h6 class="mb-0 pt-1">
            {{$materialDocente->libro->titulo}}
        </h6>
        <span class="text-muted font-size-sm">{{$materialDocente->libro->asignatura_nombre}}</span>
    </div>
    <div class="d-lg-block collapse pb-2" id="account-menu">
        <h3 class="d-block bg-secondary font-size-sm font-weight-semibold text-muted mb-0 px-4 py-3">
            UNIDADES
        </h3>
        @foreach($materialDocente->libro->segmentoLibros as $segmentoLibroFor)
            <a class="d-flex align-items-center nav-link-style px-4 py-3 border-top"
               href="{{route('app.tarea.calificar.segmento',[$materialDocente->id, $segmentoLibroFor->id])}}">
                <i class="fe-users font-size-lg opacity-60 mr-2"></i>{{$segmentoLibroFor->unidad}}
                @if($segmentoLibroFor->id == $segmentoLibro->id)
                    <span class="nav-indicator"></span>
                @endif
                <span class="text-muted font-size-sm font-weight-normal ml-auto">
                    {{--{{rand(1,10)}}--}}
                </span>
            </a>
        @endforeach
    </div>
</div>