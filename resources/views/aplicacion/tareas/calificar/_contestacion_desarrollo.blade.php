@php
    use App\Models\DesarrolloEstudiante;
    $respuestasDesarrollo = \App\Models\RespuestaDesarrollo::obtenerRespuestaDesarrolloDesarrolloAll($desarrollo->id);
@endphp

<div class="row calificacion-content">
    @foreach($respuestasDesarrollo as $respuestaDesarrollo)
        @if($desarrolloEstudiante = DesarrolloEstudiante::getDesarrolloEstudianteMaterialEstudianteRespuestaDesarrollo($materialEstudiante,$respuestaDesarrollo))
            @switch($respuestaDesarrollo->tipoRespuesta->codigo)
                @case('AUDIO')
                <div class="col col-6">
                    <label class="text-primary text-uppercase">Cargar audio</label>
                    <input type="file" class="dropify" required>
                </div>
                @break
                @case('VIDEO')
                <div class="col col-6">
                    <video controls autoplay>
                        <source src="{{url("storage/desarrollo_estudiante/{$materialEstudiante->estudiante_id}/".$desarrolloEstudiante->archivo)}}"
                                type="video/mp4">
                    </video>
                </div>
                @break
                @case('TEXTO')
                <div class="col col-6 d-flex align-items-center">
                    <div class="alert alert-info font-size-sm mb-4" role="alert">
                        {!! $desarrolloEstudiante->respuesta !!}
                    </div>
                </div>
                @break
                @default
                <div class="col col-6 d-flex justify-content-center align-items-center">
                    <div id="lightgallery-{{rand(0,9999)}}"
                         class="lightgallery mb-grid-gutter ">
                        <a href="{{url("storage/desarrollo_estudiante/{$materialEstudiante->estudiante_id}/".$desarrolloEstudiante->archivo)}}"
                           class="cs-gallery-item rounded-lg">
                            <img src="{{url("storage/desarrollo_estudiante/{$materialEstudiante->estudiante_id}/".$desarrolloEstudiante->archivo)}}"/>
                        </a>
                    </div>
                </div>
            @endswitch
        @endif
    @endforeach
</div>
