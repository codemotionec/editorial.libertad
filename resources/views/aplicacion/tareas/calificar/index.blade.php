@extends('layouts.aplicacion.app')
@php
    $tareas = \App\Models\Tarea::obtenerTareasSegmentoLibroAll($segmentoLibro->id)??[];
@endphp
@section('content')
    <div class="row">
        <div class="col-lg-4 mb-4 mb-lg-0">
            @include('aplicacion.tareas.calificar._sidebar',compact('materialDocente'))
        </div>

        <div class="col-lg-8">
            <div class="d-flex flex-column h-100 bg-light rounded-lg box-shadow-lg p-4">
                <div class="py-2 p-md-3">
                    <h1 class="h3 mb-3 text-nowrap text-center text-sm-left">
                        Tareas
                        <span class="d-inline-block align-middle bg-faded-dark font-size-ms font-weight-medium rounded-sm py-1 px-2 ml-2">{{rand(1,20)}}</span>
                    </h1>

                    <div class="row">
                        @if($tareas ?? $tareas->count()>0)
                            @foreach($tareas as $tarea)
                                @include('aplicacion.libro._tarea', [
                                    'tarea'=>$tarea,
                                    'materialEstudiante'=>[],
                                    'content'=>'col-lg-6 col-md-6',
                                    'url_desarrollo'=>route('app.tarea.calificar.tarea',[$materialDocente->id, $tarea->id]),
                                ])
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection