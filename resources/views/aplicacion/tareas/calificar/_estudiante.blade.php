@php
    if(!isset($avance)){
        $avance = $materialEstudiante ? \App\Helpers\Helper::obtenerAvanceTareaMaterialEstudiante($tarea, $materialEstudiante):null;
    }
@endphp
<div class="row d-md-flex align-items-center border-bottom">
    <div class="col-md-5 media media-ie-fix align-items-center mr-md-4 mb-4 mb-md-0">
        @if($perfil = $materialEstudiante->estudiante->user->perfil??null)
            <a class="d-block" href="#">
                <img class="d-block rounded-circle" width="90"
                     src="{{asset('storage/perfil/'.$perfil->imagen)}}" alt="{{$perfil->full_name}}"/>
            </a>
            <div class="media-body pl-3">
                <h2 class="font-size-base nav-heading mb-1"><a href="#">{{$perfil->full_name}}</a></h2>
                <div class="font-size-xs text-muted mb-2 pb-1">
                    <i class="fe-mail mr-1"></i>{{$materialEstudiante->estudiante->user->email??null}}
                    <div class="clearfix"></div>
                    <i class="fa fa-phone mr-1"></i>{{$perfil->celular}}
                </div>
            </div>
        @endif
    </div>
    <div class="col my-2 border-left px-3">
        <label class="mt-1 text-primary" style="line-height: 1.2;">
            {!! $tarea->tipoDesarrollo->descripcion !!}
        </label>
        <div class="clearfix"></div>
        {{--@if($loop->first)--}}
        {{--<i class="fe-check-circle text-success mr-1"></i>--}}
        {{--<small>Revisado</small>--}}
        {{--@else--}}
        <div class="status">
            <i class="fe-alert-circle text-warning mr-1"></i>
            <small>Sin Calificar</small>
        </div>

        <div class="clearfix"></div>

        <div class="d-flex justify-content-between">
            <div class="mt-3 text-right text-nowrap w-100">
                <div class="font-size-sm mb-2">
                    <div class="meta-link font-size-xs text-primary">
                        <i class="fe-activity mr-1"></i>{{__('Avance desarrollo')}}&nbsp;{{$avance}}%
                    </div>
                    <span class="meta-divider"></span>
                    <span class="meta-link font-size-xs">
                        <i class="fe-calendar mr-1 mt-n1"></i>&nbsp;{{\App\Helpers\Helper::formatDate($tarea->created_at->format('Y-m-d'),'M d')}}
                    </span>
                </div>
                <div class="progress mb-3" style="height: 4px;">
                    <div class="progress-bar" role="progressbar" style="width: {{$avance}}%" aria-valuenow="1"
                         aria-valuemin="0"
                         aria-valuemax="100">

                    </div>
                </div>
            </div>

            <div class=" m-3">
                <a class="btn btn-outline-primary" href="#tarea-{{$materialEstudiante->id}}"
                   data-toggle="modal" data-target="#tarea-{{$materialEstudiante->id}}">
                    Calificar
                </a>
            </div>
        </div>

        @include('aplicacion.tareas.calificar._modal_tarea',[
            'materialEstudiante'=>$materialEstudiante,
            'tarea'=>$tarea
        ])

    </div>
</div>