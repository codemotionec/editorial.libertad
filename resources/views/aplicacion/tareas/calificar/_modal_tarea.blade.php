<!-- Modal with tabs and forms -->
<div id="tarea-{{$materialEstudiante->id}}" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body tab-content py-4">
                <div id="outerContainer">
                    <div class="row">
                        <div class="col col-12">
                            <blockquote class="blockquote">
                                <p class="mb-0 text-black-50 small">{{$tarea->tipoDesarrollo->descripcion}}</p>
                                <footer class="blockquote-footer text-justify">{!! $tarea->objetivo !!}</footer>
                            </blockquote>
                        </div>
                    </div>

                    @if($desarrollos = \App\Models\Desarrollo::obtenerDesarrollosTareaAll($tarea->id))
                        @foreach($desarrollos as $desarrollo)
                            <div class="row">
                                <div class="col col-12 my-3">
                                    <form action="{{route('app.tarea.calificar.store', [$materialEstudiante->id, $desarrollo->id])}}"
                                          method="post" id="frm-test-{{$desarrollo->id}}"
                                          class="form-calificacion"
                                          novalidate>
                                        @csrf
                                        @method('POST')
                                        <div class="card">
                                            <div class="card-header bg-faded-primary light">
                                                <div class="h6">{!! $desarrollo->descripcion !!}</div>
                                            </div>
                                            <div class="card-body">
                                                @include('aplicacion.tareas.calificar._contestacion_desarrollo',compact('desarrollo','materialEstudiante'))

                                                <div class="row">
                                                    <div class="col">
                                                        <label class="mt-2" for="">Observaci&oacute;n</label>
                                                        <textarea class="form-control" name="observacion" cols="30"
                                                                  required></textarea>
                                                    </div>

                                                    <div class="col-md-3  d-flex justify-content-end flex-column">
                                                        <label class="text-primary text-uppercase" for="">Calificaci&oacute;n</label>
                                                        <input type="number" name="calificacion" class="form-control"
                                                               min="{{$calificacion_minima}}"
                                                               max="{{$calificacion_maxima}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer bg-faded-primary text-center">
                                                <button type="submit" class="btn btn-primary btn-sm">
                                                    <i class="fe-check-circle"></i> CALIFICAR
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            {{--<div class="modal-footer">--}}
            {{--<button class="mt-2 btn btn-primary btn-sm">Aprobar</button>--}}
            {{--<button class="mt-2 btn btn-danger btn-sm">Rechazar</button>--}}
            {{--</div>--}}
        </div>
    </div>
</div>