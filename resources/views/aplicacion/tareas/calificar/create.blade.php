@extends('layouts.backend.app')

@section('sidebar')
    <div class="row ">
        <div class="col col-8 "></div>
        <div class="col col-4 ">
            <nav aria-label="breadcrumb">
                {{ Breadcrumbs::render('admin.institucion.create', $model ?? '') }}
            </nav>
        </div>
    </div>
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <form action="{{ route("admin.institucion.store") }}" method="POST" enctype='multipart/form-data'>
        @include('backend.institucion._form')
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            // $('.select2').select2({
            //     ajax: {
            //         dataType: 'json',
            //         data: function (params) {
            //             var query = {
            //                 search: params.term,
            //                 type: 'public'
            //             }
            //
            //             return query;
            //         },
            //         processResults: function (data) {
            //             return {
            //                 results: data
            //             };
            //         },
            //         cache: true
            //     },
            // });
        });
    </script>
@endsection