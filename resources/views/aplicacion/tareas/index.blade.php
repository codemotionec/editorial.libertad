@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('app.docente.index', $model ?? '') }}
@endsection

@section('content')
    {{--@include('includes.forms.search-grid',['name_route'=>'admin.curso.index'])--}}
    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Seleccionar curso
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás las instituciones y cursos a las que perteneces
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>
                        <small>Libro</small>
                    </th>
                    <th>Instituci&oacute;n</th>
                    <th>Aula</th>
                    <th>Año lectivo</th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th>
                        <small>Updated_at</small>
                    </th>
                    <th class="text-center">
                        <i class="fa fa-cogs"></i>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($materialDocentes as $materialDocente)
                    <tr>
                        <td>{{ $materialDocente->libro->titulo}}</td>
                        <td>{{ $materialDocente->docente->nombre_institucion }}</td>
                        <td>{{ $materialDocente->docente->nombre_aula}}</td>
                        <td>{{ $materialDocente->docente->anioLectivo->descripcion }}</td>
                        <td class="created_at">
                            <small>{{ $materialDocente->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="created_at">
                            <small>{{ $materialDocente->updated_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="text-center">
                            <a href="{{route('app.tarea.calificar',$materialDocente->id)}}"
                               class="btn btn-primary btn-sm">
                                <i class="fe-check-circle"></i>
                                CALIFICAR
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'app.docente.destroy'])

@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
    </script>
@endsection