@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">Registro docentes</h6>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="form-group">
            <label for="institucion_id" class="bmd-label-floating">Instituci&oacute;n</label>
            <select class="form-control select2" id="institucion_id"
                    data-ajax--url="{{route('api.institucion.select2')}}" data-ajax--data-type="json"
                    data-allow-clear="true"
                    data-placeholder="Seleccionar..." required>
                @if($institucion)
                    <option value="{{$institucion->id}}">{{$institucion->nombre}}</option>
                @endif
            </select>

            <div class="invalid-feedback">Por favor seleccione la instituci&oacute;n</div>
            <div class="valid-feedback"><i class="fe-check-circle"></i></div>

            @error('institucion_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="aula_id" class="bmd-label-floating">Curso</label>
            <select class="form-control select2" name="aula_id" id="aula_id"
                    data-placeholder="Seleccionar..." required>
                @if($model->aula_id)
                    <option value="{{$model->aula_id}}">{{$model->curso->nombre}}-{{$model->paralelo}}</option>
                @endif
            </select>

            <div class="invalid-feedback">Por favor seleccione el aula</div>
            <div class="valid-feedback"><i class="fe-check-circle"></i></div>

            @error('aula_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="anio_lectivo_id" class="bmd-label-floating">Año lectivo</label>
            <select class="form-control select2" name="anio_lectivo_id" id="anio_lectivo_id"
                    data-ajax--url="{{route('api.anio-lectivo.select2')}}" data-ajax--data-type="json"
                    data-allow-clear="true"
                    data-placeholder="Seleccionar..." required>
                @if($model->anio_lectivo_id)
                    <option value="{{$model->anio_lectivo_id}}">{{$model->anioLectivo->descripcion}}</option>
                @endif
            </select>

            <div class="invalid-feedback">Por favor seleccione el año lectivo</div>
            <div class="valid-feedback"><i class="fe-check-circle"></i></div>

            @error('anio_lectivo_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="representante_curso" class="bmd-label-floating">¿Representante de curso?</label>
            <input type="checkbox" name="representante_curso" id="representante_curso" class="form-control check-toggle"
                   data-toggle="toggle" data-style="slow" data-on="{{__('On')}}" data-off="{{__('Off')}}">

            <div class="invalid-feedback">Por favor seleccione el año lectivo</div>
            <div class="valid-feedback"><i class="fe-check-circle"></i></div>

            @error('representante_curso')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        <input type="submit" value="Enviar" class="btn btn-primary">

    </div>
</div>
