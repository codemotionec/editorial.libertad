@php
    $tareas = \App\Models\Tarea::obtenerTareasSegmentoLibroAll($segmentoLibro->id)??[];
    $tests = \App\Models\Test::obtenerTestSegmentoLibroAll($segmentoLibro->id);
@endphp
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a href="#contenido-{{$segmentoLibro->id}}" class="nav-link active" data-toggle="tab" role="tab">
            <i class="fe-book-open mr-2"></i>
            Contenido
        </a>
    </li>
    <li class="nav-item">
        <a href="#tareas-{{$segmentoLibro->id}}" class="nav-link" data-toggle="tab" role="tab">
            <i class="fe-check-circle mr-2"></i>
            Tareas
        </a>
    </li>
    <li class="nav-item">
        <a href="#test-{{$segmentoLibro->id}}" class="nav-link" data-toggle="tab" role="tab">
            <i class="fe-alert-circle mr-2"></i>
            Test
        </a>
    </li>
</ul>

<!-- Tabs content -->
<div class="tab-content">
    <div class="tab-pane fade show active" id="contenido-{{$segmentoLibro->id}}" role="tabpanel">
        {{--<div class="card">--}}
        {{--<div class="card-body">--}}
        @include('aplicacion.unidad_libro._contenido',compact('segmentoLibro'))
        {{--</div>--}}
        {{--</div>--}}
    </div>
    <div class="tab-pane fade" id="tareas-{{$segmentoLibro->id}}" role="tabpanel">
        <div class="row">
            @if($tareas ?? $tareas->count()>0)
                @foreach($tareas as $tarea)
                    @include('aplicacion.libro._tarea', compact('tarea','materialEstudiante'))
                @endforeach
            @endif
        </div>
    </div>
    <div class="tab-pane fade" id="test-{{$segmentoLibro->id}}" role="tabpanel">
        @if($tests && $tests->count() > 0)
            <div class="cs-masonry-grid overflow-hidden" data-columns="3">
                @foreach($tests as $test)
                    @include('aplicacion.test._test', compact('test'))
                @endforeach
            </div>
        @endif
    </div>
</div>