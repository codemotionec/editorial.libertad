{{--<div class="rounded-lg pt-5 px-3 pl-sm-5 pr-sm-2">--}}
{{--<div class="d-sm-flex align-items-end text-center text-sm-left pl-2">--}}
{{--<div class="mr-sm-n6 pb-5">--}}
<div class="card">
    <div class="card-header bg-primary">
        <h3 class="text-sm-nowrap text-white">{{$segmentoLibro->unidad}}</h3>
    </div>
    <div class="card-body">
        <p class="pb-2 mx-auto mx-sm-0" style="max-width: 14rem;">{!! $segmentoLibro->resumen !!}</p>
    </div>
    <div class="card-footer">
        <a href="{{route('app.archivo.download', $segmentoLibro->id)}}" class="btn btn-primary mr-3">
            <i class="fe-download mr-2"></i>
            Descargar unidad
        </a>

        <a href="{{route('app.archivo.revista', $segmentoLibro->id)}}" class="btn btn-success btn-shadow"
           role="button">
            <i class="fe-book-open mr-2"></i>
            Contenido digital
        </a>
    </div>
</div>


{{--<div class="d-inline-block bg-faded-danger text-danger font-size-sm font-weight-medium rounded-sm px-3 py-2">--}}
{{--Limited time offer--}}
{{--</div>--}}
{{--<div class="cs-countdown pt-3 h4 justify-content-center justify-content-sm-start"--}}
{{--data-countdown="10/01/2021 07:00:00 PM">--}}
{{--<div class="cs-countdown-days"><span class="cs-countdown-value">373</span><span--}}
{{--class="cs-countdown-label font-size-xs text-muted">Days</span></div>--}}
{{--<div class="cs-countdown-hours"><span class="cs-countdown-value">08</span><span--}}
{{--class="cs-countdown-label font-size-xs text-muted">Hours</span></div>--}}
{{--<div class="cs-countdown-minutes"><span class="cs-countdown-value">21</span><span--}}
{{--class="cs-countdown-label font-size-xs text-muted">Mins</span></div>--}}
{{--</div>--}}

{{--</div>--}}
{{--<div>--}}
{{--<img class="img-thumbnail" src="{{asset('storage/portada_libro/'.$segmentoLibro->libro->portada)}}"--}}
{{--alt="{{$segmentoLibro->libro->title}}"/>--}}
{{--<img src="img/demo/shop-homepage/banner.png" alt="Promo banner">--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="jumbotron">--}}
{{--<h1>{{$segmentoLibro->unidad}}</h1>--}}
{{--<hr class="my-4">--}}
{{--<p class="pb-3">{!! $segmentoLibro->resumen !!}</p>--}}
{{--<a href="{{route('app.archivo.revista', $segmentoLibro->id)}}" class="btn btn-primary btn-shadow" role="button">Descargar--}}
{{--unidad</a>--}}
{{--</div>--}}