@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">Registro docentes</h6>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="form-group">
            <label for="docente_id" class="bmd-label-floating">Instituci&oacute;n</label>
            <select class="form-control select2" id="docente_id" name="docente_id"
                    data-ajax--url="{{route('api.docente-user.select2',\Illuminate\Support\Facades\Auth::user()->id)}}"
                    data-ajax--data-type="json"
                    data-allow-clear="true" data-placeholder="Seleccionar..." required>
                @if($model->docente_id)
                    <option value="{{$model->docente_id}}">
                        {{$model->nombre_institucion}} - {{$model->nombre_aula}}
                    </option>
                @endif
            </select>

            <div class="invalid-feedback">Por favor seleccione la instituci&oacute;n</div>
            <div class="valid-feedback"><i class="fe-check-circle"></i></div>

            @error('docente_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="libro_id" class="bmd-label-floating">Libro</label>
            <select class="form-control select2" name="libro_id" id="libro_id"
                    {{--data-ajax--url="{{route('api.libro.select2')}}" data-ajax--data-type="json"--}}
                    {{--data-allow-clear="true"--}}
                    data-placeholder="Seleccionar..." required>
                @if($model->libro_id)
                    <option value="{{$model->libro_id}}">{{$model->libro->titulo}}</option>
                @endif
            </select>

            <div class="invalid-feedback">Por favor seleccione el libro</div>
            <div class="valid-feedback"><i class="fe-check-circle"></i></div>

            @error('libro_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        <input type="submit" value="Enviar" class="btn btn-primary">

    </div>
</div>
