@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('app.material-docente.index', $model ?? '') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Material docente
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás los libros que vas a trabajar
                    </div>
                </div>
                <div class="col col-3 text-right">
                    @if(\Illuminate\Support\Facades\Auth::user()->perfil)
                        <a class="btn btn-success btn-sm" href="{{ route('app.material-docente.create') }}">
                            <i class="fe-plus"></i>
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Instituci&oacute;n</th>
                    <th>Libro</th>
                    <th>Curso</th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th>
                        <small>Updated_at</small>
                    </th>
                    {{--<th class="text-center">Estado</th>--}}
                    <th class="text-center"><i class="fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($materialesDocente as $materialDocente)
                    <tr>
                        <td>{{ $materialDocente->docente->nombre_institucion}}</td>
                        <td>{{ $materialDocente->libro->titulo }}</td>
                        <td>{{ $materialDocente->libro->curso->nombre}}</td>
                        {{--                        <td>{{ $docente->anioLectivo->descripcion }}</td>--}}
                        <td class="created_at">
                            <small>{{ $materialDocente->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="created_at">
                            <small>{{ $materialDocente->updated_at->format('M d Y h:i') }}</small>
                        </td>

                        {{--<td class="text-center">--}}
                        {{--{!! \App\Helpers\Helper::obtenerIconEstado($materialDocente->estadoRegistro->codigo) !!}--}}
                        {{--<small>{{$materialDocente->estadoRegistro->nombre}}</small>--}}
                        {{--</td>--}}
                        <td class="text-center">
                            <a data-toggle="modal" data-target="#deleteModal" data-id="{{ $materialDocente->id }}"
                               class="nav-link-style mr-1 text-danger">
                                <i class="fe-trash-2"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'app.material-docente.destroy'])

@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
    </script>
@endsection