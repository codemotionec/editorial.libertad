{{--<div class="card" style="width: 18rem;">--}}
{{--<div class="card-body">--}}
{{--<p class="card-text">--}}
{{--{!! $test->descripcion !!}--}}
{{--</p>--}}
{{--<a href="{{route('app.test.desarrollo',$test->id)}}" class="btn btn-outline-success">Desarrollar</a>--}}
{{--</div>--}}
{{--</div>--}}

<div class="cs-grid-item">
    <article class="card card-hover">
        <div class="card-body">
            <a class="media meta-link font-size-sm align-items-center pt-3"
               href="{{route('app.test.desarrollo',$test->id)}}">
                <div class="media-body ml-1 mt-n1 text-justify">
                    {!! $test->descripcion !!}
                </div>
            </a>

            <div class="row">
                <div class="col-md-5">
                    @if(\App\Helpers\Helper::validarFechaEjecucionConActual($test->fecha_ejecucion, $test->fecha_terminacion))
                        <a href="{{route('app.test.desarrollo',$test->id)}}"
                           class="btn btn-outline-primary btn-sm text-center">
                            Desarrollar
                        </a>
                    @else
                        <div class="text-warning" role="alert">
                            <i class="fe-clock mr-1 font-size-xs"></i>{{__('Activación')}}
                            <div class="clearfix"></div>
                            {{--<i class="fe-alert-triangle font-size-xl mr-1"></i>--}}
                            <span class="font-size-xs">
                                <i class="fe-calendar mr-1 mt-n1"></i>&nbsp;{{\App\Helpers\Helper::formatDate($tarea->fecha_ejecucion,'M d')}}
                            </span>
                            {{--<span class="fe-arrow-right"></span>--}}
                            {{--<span class="meta-link font-size-xs"><i class="fe-calendar mr-1 mt-n1"></i>&nbsp;{{\App\Helpers\Helper::formatDate($tarea->fecha_terminacion,'M d')}}</span>--}}
                        </div>
                    @endif
                </div>

                <div class="col-md-7">
                    <div class="font-size-sm mb-2 pt-1">
                        <a class="meta-link font-size-xs text-success"
                           href="{{route('app.tarea.desarrollo',$tarea->id)}}">
                            <i class="fe-check-circle"></i> Realizado
                        </a>
                        <span class="meta-divider"></span>
                        <a class="meta-link font-size-xs" href="{{route('app.test.desarrollo',$test->id)}}">
                            <i class="fe-calendar mr-1 mt-n1"></i>&nbsp;{{\App\Helpers\Helper::formatDate($test->created_at->format('Y-m-d'),'M d')}}
                        </a>
                    </div>
                </div>
            </div>
            <div class="mt-3 text-right text-nowrap">

            </div>
        </div>
    </article>
</div>