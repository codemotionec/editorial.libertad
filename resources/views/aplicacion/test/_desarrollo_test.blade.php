@php
    $preguntas = \App\Models\PreguntaDesarrollo::obtenerPreguntaDesarrolloTestAll($test->id);
@endphp
@extends('layouts.aplicacion.app')
@section('breadcrum')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('app.dashboard')}}">Inicio</a></li>
            <li class="breadcrumb-item">
                <a href="{{route('app.libro.revista',$test->segmento_libro_id)}}">Libro</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Test</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div id="outerContainer">
        <h3 class="text-aqua">{{$test->segmentoLibro->libro->titulo}}</h3>
        <h5 class="text-black-light">{{$test->segmentoLibro->unidad}}</h5>

        <div class="row">
            <div class="col col-12">
                <blockquote class="blockquote">
                    <p class="mb-0 text-black-50 small">Comprueba lo que Aprendiste</p>
                    <footer class="blockquote-footer text-justify">{!! $test->descripcion !!}</footer>
                </blockquote>
            </div>
        </div>


        @foreach($preguntas as $pregunta)
            <div class="row">
                <div class="col col-12">
                    <div class="jumbotron">
                        {!! $pregunta->pregunta !!}
                        @php
                            echo \App\Http\Controllers\Aplicacion\LibroController::opcionesPreguntas(null,$pregunta);
                        @endphp
                    </div>
                </div>
            </div>
        @endforeach


    </div>
@endsection
