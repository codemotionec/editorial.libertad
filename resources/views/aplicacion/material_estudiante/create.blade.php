@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('app.registro-libro.create', $model ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <form action="{{ route("app.material-estudiante.store") }}" method="POST" enctype='multipart/form-data'
          class="needs-validation" novalidate>
        @include('aplicacion.material_estudiante._form')
    </form>
@endsection

@section('scripts')
    <script src="{{asset('js/formSubmit.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var select2Options = {width: 'resolve'};
            var apiUrl = "{{route('api.material-docente.select2',0)}}";
            window.Select2Cascade($('#estudiante_id'), $('#material_docente_id'), apiUrl, select2Options);

            $('#material_docente_id').on('select2:select', function () {
                window.cargarCampoRegistroLibro($('#material_docente_id').val(), "{{route('api.material-docente.codigo')}}", 'codigo');
            });
        });
    </script>
@endsection

