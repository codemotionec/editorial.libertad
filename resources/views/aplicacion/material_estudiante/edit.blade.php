@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('admin.libro.create', $model ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <form action="{{ route("app.material-estudiante.update",$model->id) }}" method="POST" enctype='multipart/form-data'
          novalidate>
        @method('PUT')
        @include('aplicacion.material_estudiante._form')
    </form>
@endsection


@section('scripts')
    <script src="{{asset('js/formSubmit.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var select2Options = {width: 'resolve'};
            var apiUrl = "{{route('api.libro-estudiante.select2',0)}}";
            window.Select2Cascade($('#estudiante_id'), $('#libro_id'), apiUrl, select2Options);


            window.cargarCampoRegistroLibro($('#material_docente_id').val(), "{{route('api.material-docente.codigo')}}", 'codigo');

            $('#material_docente_id').on('select2:select', function () {
                window.cargarCampoRegistroLibro($('#material_docente_id').val(), "{{route('api.material-docente.codigo')}}", 'codigo');
            });
        });
    </script>
@endsection

