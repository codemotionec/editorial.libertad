@extends('layouts.aplicacion.app')
@php
    use App\Helpers\CodigoRegistroLibro;
    use App\Helpers\Helper;
@endphp

@section('breadcrum')
    {{ Breadcrumbs::render('app.registro-libro.index', $model ?? '') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Mis Libros
                    </h4>
                    <div class="card-category">
                        Registro de libros ingresados para desarrollar
                    </div>
                </div>
                <div class="col col-3 text-right">
                    <a class="btn btn-success btn-sm" href="{{ route('app.material-estudiante.create') }}">
                        <i class="fe-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>&nbsp;</th>
                    <th>Asignatura</th>
                    <th>Libro</th>
                    <th>C&oacute;digo</th>
                    <th>Registro</th>
                    <th>Estado</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($materialesEstudiante as $materialEstudiante)
                    @php
                        CodigoRegistroLibro::validarRegistroLibro($materialEstudiante->materialDocente->libro, $materialEstudiante->codigo);
                    @endphp

                    <tr>
                        <td>
                            <img class="img-thumbnail" width="100"
                                 src="{{asset('/storage/portada_libro/'.$materialEstudiante->portada_libro)}}"
                                 alt="{{$materialEstudiante->titulo_libro}}"/>
                        </td>
                        <td class="py-3 align-middle">
                            <div class="media-body">
                                <span class="font-weight-medium text-heading mr-1">
                                    {{$materialEstudiante->asignatura_libro}}
                                </span>
                            </div>
                        </td>
                        <td class="py-3 align-middle">
                            <div class="media align-items-center">
                                <div class="media-body">
                                    {{$materialEstudiante->titulo_libro}}
                                    {!! Helper::obtenerEstadoEnCursoInactivo($materialEstudiante->estado_registro_anio_lectivo->codigo) !!}
                                </div>
                            </div>
                        </td>
                        <td class="py-3 align-middle">
                            {{$materialEstudiante->codigo}}
                            {!! CodigoRegistroLibro::$mensajePrincipal !!}
                        </td>
                        <td class="py-3 align-middle">
                            <small>{{ $materialEstudiante->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="py-3 align-middle">
                            {!! Helper::obtenerIconEstado($materialEstudiante->estadoRegistro->codigo) !!}
                            <small>{{$materialEstudiante->estadoRegistro->nombre}}</small>
                        </td>
                        <td class="py-3 align-middle text-center">
                            <a href="{{route('app.material-estudiante.edit',$materialEstudiante->id)}}"
                               data-toggle="tooltip" title="{{__('EDITAR')}}"
                               class="nav-link-style mr-1">
                                <i class="fe-edit"></i>
                            </a>
                            <a data-toggle="modal" data-target="#deleteModal" data-id="{{ $materialEstudiante->id }}"
                               class="nav-link-style mr-1 text-danger">
                                <i class="fe-trash-2"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'app.material-estudiante.destroy'])

@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
    </script>
@endsection