@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">Registro de libro</h6>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="form-group">
            <label for="docente_id" class="bmd-label-floating">Instituci&oacute;n</label>
            <input type="hidden" name="id" id="id" value="{{old('id',$model->id)}}">
            <select class="form-control select2" id="estudiante_id" name="estudiante_id"
                    data-ajax--url="{{route('api.estudiante-user.select2',\App\Helpers\Sesiones::estudiante()->user->id)}}"
                    data-ajax--data-type="json"
                    data-allow-clear="true" data-placeholder="Seleccionar..." required>
                @if($model->estudiante_id)
                    <option value="{{$model->estudiante_id}}">
                        {{$model->estudiante->nombre_institucion}} - {{$model->estudiante->nombre_aula}}
                    </option>
                @endif
            </select>

            <div class="invalid-feedback">Por favor seleccione la instituci&oacute;n</div>
            <div class="valid-feedback"><i class="fe-check-circle"></i></div>

            @error('estudiante_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="libro_id" class="bmd-label-floating">Libro</label>
            <select class="form-control select2" name="material_docente_id" id="material_docente_id"
                    data-placeholder="Seleccionar..." required>
                @if($model->material_docente_id)
                    <option value="{{$model->material_docente_id}}">{{$model->materialDocente->libro->nombre_libro}}</option>
                @endif
            </select>

            <div class="invalid-feedback">Por favor seleccione el libro</div>
            <div class="valid-feedback"><i class="fe-check-circle"></i></div>

            @error('material_docente_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="libro_id" class="bmd-label-floating">C&oacute;digo de registro</label>
            <input class="form-control" type="text" id="codigo" name="codigo" data-format="custom" data-delimiter="-"
                   data-blocks="3 4 5" placeholder="000-0000-00000" value="{{old('codigo',$model->codigo)}}">
        </div>

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        <input type="submit" value="Enviar" class="btn btn-primary">

    </div>
</div>
