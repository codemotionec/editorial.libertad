@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <form action="{{route('app.user.rol_user.store',$user->getAuthIdentifier())}}" method="POST">
            @csrf
            <div class="row">
                <div class="col col-8 text-aqua">
                    <h6 class="m-0 font-weight-bold">Permisos Asignados</h6>
                </div>
                {{--<div class="col col-8">--}}
                {{--<div class="row">--}}
                {{--<div class="col col-md-9">--}}
                {{--<select name="rol_id" id="rol_id" class="form-control select2"--}}
                {{--data-ajax--url="{{route('api.roles.select2')}}" data-ajax--data-type="json"--}}
                {{--data-ajax--cache="true"--}}
                {{--data-placeholder="Seleccionar...">--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--<div class="col col-md-3">--}}
                {{--<button type="submit" class="btn btn-success btn-sm float-right">--}}
                {{--<i class="fa fa-plus"></i>--}}
                {{--</button>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </form>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th>Descripcion</th>
                <th>Estado</th>
                <th>
                    <small>Created_at</small>
                </th>
                {{--<th><small>Updated_at</small></th>--}}
                <th class="text-center">
                    <i class="fas fa-cogs"></i>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($rolesUser as $rolUser)
                @php
                    $estadoRegistro = $rolUser->estadoRegistro;
                @endphp
                <tr>
                    <td>{{ $rolUser->rol->name }}</td>
                    <td class="text-left">
                        {!! \App\Helpers\Helper::obtenerIconEstado($estadoRegistro->codigo) !!}
                        {{--@switch($estadoRegistro->codigo)--}}
                        {{--@case('RE')--}}
                        {{--<i class="text-warning fas fa-exclamation-circle mr-1" data-toggle="tooltip"--}}
                        {{--title="{{$estadoRegistro->nombre}}"></i>--}}
                        {{--@break--}}
                        {{--@case('AP')--}}
                        {{--<i class="fe-check-circle text-success font-size-xl mt-1" data-toggle="tooltip"--}}
                        {{--title="{{$estadoRegistro->nombre}}"></i>--}}
                        {{--<i class="text-success far fa-check-circle mr-1" data-toggle="tooltip"--}}
                        {{--title="{{$estadoRegistro->nombre}}"></i>--}}
                        {{--@break--}}
                        {{--@case('NP')--}}
                        {{--<i class="text-danger fas fa-ban mr-1" data-toggle="tooltip"--}}
                        {{--title="{{$estadoRegistro->nombre}}"></i>--}}
                        {{--@break--}}
                        {{--@endswitch--}}
                        <small>{{ $estadoRegistro->nombre }}</small>
                    </td>
                    <td class="created_at">
                        <small>{{ $rolUser->created_at->format('M d Y h:i') }}</small>
                    </td>
                    <td class="text-center">
                        <a data-toggle="modal" data-target="#deleteModal" data-id="{{ $rolUser->id }}"
                           class="nav-link-style text-danger">
                            <i class="fe-trash-2"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@include('includes.forms.modal-delete',['name_route'=>'app.user.rol_user.destroy'])
