@extends('layouts.aplicacion.app')

@section('content')
    <div class="row" xmlns="">
        <div class="col-md-6">
            @include('includes.validation-error')
            <form action="{{route('app.user.perfil.store',$user->getAuthIdentifier())}}" method="POST"
                  enctype='multipart/form-data'>
                @include('aplicacion.user._form_perfil',compact('perfil'))
            </form>
        </div>
        <div class="col-md-6">
            @include('aplicacion.user._roles',compact('user','rolesUser'))
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
    </script>
@endsection