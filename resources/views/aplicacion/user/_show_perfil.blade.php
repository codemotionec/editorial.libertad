@csrf
<div class="card shadow mb-4">
    {{--<div class="card-header py-3">--}}
    {{--<div class="row">--}}
    {{--<div class="col col-8 text-aqua">--}}
    {{--<h6 class="font-weight-bold">Perfil</h6>--}}
    {{--</div>--}}
    {{--<div class="col col-4 text-aqua text-primary">--}}
    {{--{{$perfil->abreviatura->codigo}}--}}
    {{--<div class="form-group pt-3">--}}
    {{--<label for="abreviatura_id" class="bmd-label-floating "></label>--}}
    {{--<select name="abreviatura_id" id="abreviatura_id" class="form-control select2"--}}
    {{--data-ajax--url="{{route('api.abreviatura.select2')}}" data-ajax--cache="true"--}}
    {{--data-placeholder="Seleccionar..." required>--}}
    {{--@if($perfil->abreviatura_id)--}}
    {{--<option value="{{$perfil->abreviatura_id}}">--}}
    {{--{{$perfil->abreviatura->codigo}}--}}
    {{--</option>--}}
    {{--@endif--}}
    {{--</select>--}}

    {{--<div class="invalid-feedback">Por favor seleccione abreviatura</div>--}}
    {{--<div class="valid-feedback"><i class="fe-check-circle"></i></div>--}}

    {{--@error('abreviatura_id')--}}
    {{--<small class="text-danger">{{ $message }}</small>--}}
    {{--@enderror--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    <div class="card-body">
        <div class="row">
            <div class="col col-6">
                <div class="form-group">
                    <label for="nombre" class="bmd-label-floating text-primary">Nombre</label>
                    <div class="clearfix"></div>
                    <small class="text-primary">{{$perfil->abreviatura->codigo}}.</small>&nbsp;{{$perfil->nombre}}
                    {{--<input class="form-control" type="text" name="nombre" id="nombre"--}}
                    {{--value="{{ old('nombre',$perfil->nombre) }}" required>--}}

                    {{--@error('nombre')--}}
                    {{--<small class="text-danger">{{ $message }}</small>--}}
                    {{--@enderror--}}
                </div>
            </div>
            <div class="col col-6">
                <div class="form-group">
                    <label for="apellido" class="bmd-label-floating text-primary">Apellido</label>
                    <div class="clearfix"></div>
                    {{$perfil->apellido}}
                    {{--<input class="form-control" type="text" name="apellido" id="apellido"--}}
                    {{--value="{{ old('apellido',$perfil->apellido) }}" required>--}}

                    {{--@error('apellido')--}}
                    {{--<small class="text-danger">{{ $message }}</small>--}}
                    {{--@enderror--}}
                </div>
            </div>
            {{--</div>--}}
            {{--<div class="row">--}}
            <div class="col col-6">
                <div class="form-group">
                    <label for="cedula_identidad" class="bmd-label-floating text-primary">Cedula Identidad</label>
                    <div class="clearfix"></div>
                    {{$perfil->cedula_identidad}}
                    {{--<input class="form-control" type="text" name="cedula_identidad" id="cedula_identidad"--}}
                    {{--value="{{ old('cedula_identidad',$perfil->cedula_identidad) }}" required>--}}
                    {{--@error('cedula_identidad')--}}
                    {{--<small class="text-danger">{{ $message }}</small>--}}
                    {{--@enderror--}}
                </div>
            </div>
            <div class="col col-6">
                <div class="form-group">
                    <label for="fecha_nacimiento" class="bmd-label-floating text-primary">Fecha Nacimiento</label>
                    <div class="clearfix"></div>
                    {{$perfil->fecha_nacimiento}}
                    {{--<div class="form-group w-100 mb-sm-4 mr-sm-3">--}}
                    {{--<label class="form-label">Depart</label>--}}
                    {{--<div class="input-group-overlay">--}}
                    {{--<input class="form-control appended-form-control datepicker" placeholder="Choose date"--}}
                    {{--type="text" name="fecha_nacimiento"--}}
                    {{--id="fecha_nacimiento"--}}
                    {{--data-date-format="yyyy-mm-dd"--}}
                    {{--value="{{ old('fecha_nacimiento',$perfil->fecha_nacimiento) }}" required>--}}
                    {{--<div class="input-group-append-overlay">--}}
                    {{--<span class="input-group-text"> <i class="fe-calendar"></i> </span>--}}
                    {{--</div>--}}
                    {{--<div class="text-primary text-right">--}}
                    {{--<small>Ejemplo:&nbsp;<strong>{{date('Y-m-d')}}</strong></small>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@error('fecha_nacimiento')--}}
                    {{--<small class="text-danger">{{ $message }}</small>--}}
                    {{--@enderror--}}
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col col-6">
                <div class="form-group">
                    <label for="telefono" class="bmd-label-floating text-primary">Telefono</label>
                    <div class="clearfix"></div>
                    {{$perfil->telefono}}
                    {{--<input class="form-control" type="text" name="telefono" id="telefono"--}}
                    {{--value="{{ old('telefono',$perfil->telefono) }}">--}}

                    {{--@error('telefono')--}}
                    {{--<small class="text-danger">{{ $message }}</small>--}}
                    {{--@enderror--}}
                </div>
            </div>
            <div class="col col-6">
                <div class="form-group">
                    <label for="celular" class="bmd-label-floating text-primary">Celular</label>
                    <div class="clearfix"></div>
                    {{$perfil->celular}}
                    {{--<input class="form-control" type="text" name="celular" id="celular"--}}
                    {{--value="{{ old('celular',$perfil->celular) }}">--}}
                    {{--<div class="input-group-addon">--}}
                    {{--<span class="glyphicon glyphicon-th "></span>--}}
                    {{--</div>--}}

                    {{--@error('celular')--}}
                    {{--<small class="text-danger">{{ $message }}</small>--}}
                    {{--@enderror--}}
                </div>
            </div>
        </div>


        <div class="form-group">
            <label for="direccion" class="bmd-label-floating text-primary">Direccion</label>
            <div class="clearfix"></div>
            {{$perfil->direccion}}
            {{--<input class="form-control" type="text" name="direccion" id="direccion"--}}
            {{--value="{{ old('direccion',$perfil->direccion) }}">--}}

            {{--@error('direccion')--}}
            {{--<small class="text-danger">{{ $message }}</small>--}}
            {{--@enderror--}}
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <img src="{{url('storage/perfil/'.$perfil->imagen)}}" class="img-thumbnail rounded-circle" alt="Circle image">
                </div>
            </div>
            {{--<label for="imagen" class="bmd-label-floating">Imagen de perfil</label>--}}
            {{--<div class="cs-gallery">--}}
                {{--<a href="{{url('storage/perfil/'.$perfil->imagen)}}" class="cs-gallery-item rounded-lg"--}}
                   {{--data-sub-html='<h6 class="font-size-sm text-light">Gallery image caption</h6>'>--}}
                    {{--<img src="{{url('storage/perfil/'.$perfil->imagen)}}" alt="Gallery thumbnail">--}}
                    {{--<span class="cs-gallery-caption">Gallery image caption</span>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<input type="file" class="dropify" name="imagen" id="imagen"--}}
                   {{--data-default-file="{{url('storage/perfil/'.$perfil->imagen)}}"--}}
            {{--/>--}}

            {{--<div class="invalid-feedback">Por favor ingrese una imagen</div>--}}
            {{--<div class="valid-feedback"><i class="fe-check-circle"></i></div>--}}

            {{--@error('imagen')--}}
            {{--<small class="text-danger">{{ $message }}</small>--}}
            {{--@enderror--}}
        </div>

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        {{--<input type="submit" value="Enviar" class="btn btn-primary">--}}

    </div>
</div>