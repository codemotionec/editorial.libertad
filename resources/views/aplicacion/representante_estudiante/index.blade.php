@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('app.representante-estudiante.index', '') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Mis Estudiantes
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás los estudiantes a los que representas
                    </div>
                </div>
                <div class="col col-3 text-right">
                    @if(\Illuminate\Support\Facades\Auth::user()->perfil)
                        <a class="btn btn-success btn-sm mr-2"
                           href="{{ route('app.representante-estudiante.create') }}">
                            <i class="fe-plus"></i>
                        </a>

                        <a class="btn btn-primary btn-sm" data-toggle="modal"
                           data-target="#modalEstudiante">
                            <i class="fe-search"></i>
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Instituci&oacute;n</th>
                    <th>Estudiante</th>
                    <th>Aula</th>
                    <th>Año lectivo</th>
                    <th class="text-center">
                        <div style="line-height: 14px;">
                            Autoriz&oacute;
                            <div class="clearfix"></div>
                            <small>Al estudiante</small>
                        </div>
                    </th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th>
                        <small>Updated_at</small>
                    </th>
                    <th class="text-center">
                        <div style="line-height: 14px;">
                            Estado
                            <div class="clearfix"></div>
                            <small>Representante</small>
                        </div>
                    </th>
                    <th class="text-center">
                        <i class="fa fa-cogs"></i>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($representanteEstudiantes as $representanteEstudiante)
                    <tr>
                        <td>{{ $representanteEstudiante->estudiante->nombre_institucion }}</td>
                        <td>{{ $representanteEstudiante->estudiante->nombre_estudiante}}</td>
                        <td>{{ $representanteEstudiante->estudiante->nombre_aula}}</td>
                        <td>{{ $representanteEstudiante->estudiante->anioLectivo->descripcion }}</td>
                        <td>
                            @if($representanteEstudiante->estudiante->nombre_autoriza)
                                <small class="text-success">{{ $representanteEstudiante->estudiante->nombre_autoriza }}</small>
                            @else
                                {!! \App\Helpers\Helper::obtenerIconEstado($representanteEstudiante->estadoRegistro->codigo) !!}
                            @endif
                        </td>
                        <td class="created_at">
                            <small>{{ $representanteEstudiante->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="created_at">
                            <small>{{ $representanteEstudiante->updated_at->format('M d Y h:i') }}</small>
                        </td>

                        <td class="text-center">
                            {!! \App\Helpers\Helper::obtenerIconEstado($representanteEstudiante->estadoRegistro->codigo) !!}
                            <small>{{$representanteEstudiante->estadoRegistro->nombre}}</small>
                        </td>
                        <td class="text-center">
                            <a href="{{route('app.representante-estudiante.edit', $representanteEstudiante->id)}}"
                               class="nav-link-style mr-1" data-toggle="tooltip" title="Editar">
                                <i class="fe-edit"></i>
                            </a>

                            <a data-toggle="modal" data-target="#deleteModal"
                               data-id="{{ $representanteEstudiante->id }}"
                               class="nav-link-style mr-1 text-danger">
                                <i class="fe-trash-2"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'app.representante-estudiante.destroy'])

    <div id="modalEstudiante" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalEstudiante"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{route('app.representante-estudiante.asignar')}}" class="needs-validation" method="POST"
                          novalidate>
                        @method('POST')
                        @csrf
                        @include('aplicacion.representante_estudiante._form_estudiante',compact('model'))
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
        var select2Options = {width: 'resolve'};
        var apiUrl = "{{route('api.aula-institucion.select2',0)}}";
        window.Select2Cascade($('#institucion_id'), $('#aula_id'), apiUrl, select2Options);

        var apiUrl_ = "{{route('api.institucion.select2',0)}}";
        window.Select2Cascade($('#canton_id'), $('#institucion_id'), apiUrl_, select2Options);

        var apiUrl = "{{route('api.estudiantes-aula.select2',0)}}";
        window.Select2Cascade($('#aula_id'), $('#estudiante_id'), apiUrl, select2Options);

        $('#institucion_id').on('select2:select', function (e) {
            window.cargarInstitucionInfo(e.params.data['id'], "{{route('api.institucion.info')}}",'institucion-info');
        });
    </script>
@endsection