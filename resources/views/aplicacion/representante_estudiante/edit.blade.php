@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('app.representante-estudiante.create', $model ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <form action="{{ route("app.representante-estudiante.update",$representanteEstudiante->id) }}" method="POST"
          enctype='multipart/form-data'
          class="needs-validation" novalidate>
        @method('PUT')
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <input class="form-control" type="hidden" name="perfil_id" id="perfil_id"
                       value="{{ $perfil->id }}">
                @include('aplicacion.estudiante._form')

            </div>
            <div class="col-lg-6 col-sm-12">
                @include('aplicacion.user._form_perfil')
                <div class="col-12 text-right">
                    <button type="submit" id="submit-perfil-estudiante" class="btn btn-primary">
                        <span class="spinner-grow spinner-grow-sm mr-2" role="status" aria-hidden="true"></span>
                        REGISTRAR ESTUDIANTE
                    </button>

                    {{--<input type="submit" value="Enviar" class="btn btn-primary">--}}
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script src="{{asset('js/formSubmit.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var select2Options = {width: 'resolve'};

            var apiUrl_ = "{{route('api.institucion.select2',0)}}";
            window.Select2Cascade($('#canton_id'), $('#institucion_id'), apiUrl_, select2Options);

            var apiUrl = "{{route('api.aula-institucion.select2',0)}}";
            window.Select2Cascade($('#institucion_id'), $('#aula_id'), apiUrl, select2Options);

            $('input[type=submit]').not('#submit-perfil-estudiante').hide();

            {{--$(document).on('change', '#cedula_identidad', function () {--}}
            {{--var mensaje = "{{__('Existe un estudiante registrado con esta cedula, por favor buscar y agregar el estudiante.')}}";--}}
            {{--var redirect = "{{route('app.representante-estudiante.index')}}";--}}
            {{--window.validarCedulaEstudiante($(this), "{{route('api.perfil.cedula')}}", mensaje, redirect);--}}
            {{--});--}}

            $('#canton_id').trigger('change');

            $('#institucion_id').trigger('change');

            {{--$('#canton_id').on('select2-loaded', function (e) {--}}
            {{--console.log(e);--}}
            {{--window.cargarInstitucionInfo(e.params.data['id'], "{{route('api.institucion.info')}}",'institucion-info');--}}
            {{--});--}}

            $('#institucion_id').on('select2:select', function (e) {
                window.cargarInstitucionInfo(e.params.data['id'], "{{route('api.institucion.info')}}", 'institucion-info');
            });
        });
    </script>
@endsection
