@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('app.estudiante.index', $model ?? '') }}
@endsection

@section('content')
    {{--@include('includes.forms.search-grid',['name_route'=>'admin.curso.index'])--}}

    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Estudiantes
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás las instituciones a las que preteneces
                    </div>
                </div>
                <div class="col col-3 text-right">
                    <a class="btn btn-success btn-sm" href="{{ route('app.estudiante.create') }}">
                        <i class="fe-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Instituci&oacute;n</th>
                    <th>Aula</th>
                    <th>Año lectivo</th>
                    <th>Autoriza</th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th>
                        <small>Updated_at</small>
                    </th>
                    <th class="text-center">Estado</th>
                    <th class="text-center"><i class="fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($estudiantes as $estudiante)
                    <tr>
                        <td>{{ $estudiante->aula->institucion->nombre }}</td>
                        <td>{{ $estudiante->aula->curso->nombre}} ({{ $estudiante->aula->paralelo}})</td>
                        <td>{{ $estudiante->anioLectivo->descripcion }}</td>
                        <td>
                            @if($estudiante->nombre_autoriza)
                                <small class="text-success">{{ $estudiante->nombre_autoriza }}</small>
                            @else
                                {!! \App\Helpers\Helper::obtenerIconEstado($estudiante->estadoRegistro->codigo) !!}
                            @endif
                        </td>
                        <td class="created_at">
                            <small>{{ $estudiante->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="created_at">
                            <small>{{ $estudiante->updated_at->format('M d Y h:i') }}</small>
                        </td>

                        <td class="text-center">
                            {!! \App\Helpers\Helper::obtenerIconEstado($estudiante->estadoRegistro->codigo) !!}
                            <small>{{$estudiante->estadoRegistro->nombre}}</small>
                        </td>
                        <td class="text-center">
                            <a data-toggle="modal" data-target="#deleteModal" data-id="{{ $estudiante->id }}"
                               class="nav-link-style mr-1 text-danger">
                                <i class="fe-trash-2"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'app.estudiante.destroy'])

@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
    </script>
@endsection