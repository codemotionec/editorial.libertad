@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('app.estudiante.create', $model ?? '') }}
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <form action="{{ route("app.estudiante.store") }}" method="POST" enctype='multipart/form-data'
          class="needs-validation" novalidate>
        @include('aplicacion.estudiante._form')
    </form>
@endsection

@section('scripts')
    <script src="{{asset('js/formSubmit.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var select2Options = {width: 'resolve'};
            var apiUrl = "{{route('api.aula-institucion.select2',0)}}";
            window.Select2Cascade($('#institucion_id'), $('#aula_id'), apiUrl, select2Options);

            var apiUrl_ = "{{route('api.institucion.select2',0)}}";
            window.Select2Cascade($('#canton_id'), $('#institucion_id'), apiUrl_, select2Options);


            $('#institucion_id').on('select2:select', function (e) {
                window.cargarInstitucionInfo(e.params.data['id'], "{{route('api.institucion.info')}}",'institucion-info');
            });
            // var cascadLoading = new window.Select2Cascade($('#institucion_id'), $('#aula_id'), apiUrl, select2Options);
            // // cascadLoading.then(function (parent, child, items) {
            // //     // Dump response data
            // //     console.log(items);
            // // });
        });
    </script>
@endsection
