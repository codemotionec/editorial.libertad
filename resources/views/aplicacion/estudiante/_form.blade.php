@csrf
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col col-8 text-aqua">
                <h6 class="m-0 font-weight-bold">Registro estudiantes</h6>
            </div>
        </div>

    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="row">
                <div class="col-12">
                    <label for="canton_id" class="bmd-label-floating">Ciudad</label>
                    <select class="form-control select2" id="canton_id"
                            data-ajax--url="{{route('api.canton.select2')}}" data-ajax--data-type="json"
                            data-allow-clear="true"
                            data-placeholder="Seleccionar..." required>
                        @if(isset($canton) && $canton)
                            <option value="{{$canton->id}}">{{$canton->nombre}}</option>
                        @endif
                    </select>

                    <div class="invalid-feedback">Por favor seleccione la ciudad</div>
                    <div class="valid-feedback"><i class="fe-check-circle"></i></div>
                </div>

                <div class="mt-2 col-12">
                    <label for="institucion_id" class="bmd-label-floating">Instituci&oacute;n</label>
                    <select class="form-control select2" id="institucion_id" name="institucion_id"
                            @if($institucion)
                            data-id="{{$institucion->id}}"
                            @endif
                            {{--@if($institucion->canton)--}}
                            {{--data-ajax--url="{{route('api.institucion.select2', $institucion->canton_id)}}"--}}
                            {{--@endif--}}
                            {{--data-ajax--data-type="json"--}}
                            {{--data-allow-clear="true"--}}
                            data-placeholder="Seleccionar..." required>
                        @if($institucion)
                            <option value="{{$institucion->id}}">{{$institucion->nombre}}</option>
                        @endif
                    </select>
                </div>
                <div id="institucion-info" class=" col-12">

                </div>
            </div>


            <div class="invalid-feedback">Por favor seleccione la instituci&oacute;n</div>
            <div class="valid-feedback"><i class="fe-check-circle"></i></div>

            @error('institucion_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="aula_id" class="bmd-label-floating">Curso</label>
            <select class="form-control select2" name="aula_id" id="aula_id"
                    @if($model->aula_id)
                    data-id="{{$model->aula_id}}"
                    @endif
                    {{--@if($institucion)--}}
                    {{--data-ajax--url="{{route('api.aula-institucion.select2', $institucion->id)}}"--}}
                    {{--@endif--}}
                    data-placeholder="Seleccionar..." required>
                @if($model->aula_id)
                    <option value="{{$model->aula_id}}">{{$model->nombre_aula}}</option>
                @endif
            </select>

            <div class="invalid-feedback">Por favor seleccione el aula</div>
            <div class="valid-feedback"><i class="fe-check-circle"></i></div>

            @error('aula_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="anio_lectivo_id" class="bmd-label-floating">Año lectivo</label>
            <select class="form-control select2" name="anio_lectivo_id" id="anio_lectivo_id"
                    data-ajax--url="{{route('api.anio-lectivo.select2')}}" data-ajax--data-type="json"
                    data-allow-clear="true"
                    data-placeholder="Seleccionar..." required>
                @if($model->anio_lectivo_id)
                    <option value="{{$model->anio_lectivo_id}}">{{$model->anioLectivo->descripcion}}</option>
                @endif
            </select>

            <div class="invalid-feedback">Por favor seleccione el año lectivo</div>
            <div class="valid-feedback"><i class="fe-check-circle"></i></div>

            @error('anio_lectivo_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <input type="hidden" id="token" value="{{ csrf_token() }}">

        <input type="submit" value="Enviar" class="btn btn-primary">

    </div>
</div>
