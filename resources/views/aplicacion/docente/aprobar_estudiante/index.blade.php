@extends('layouts.aplicacion.app')

@section('content')
    @include('includes.forms.search-grid',['name_route'=>'admin.institucion.index'])
    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Listado de Estudiantes
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás los estudiantes registrados en la aplicaci&oacute;n
                    </div>
                </div>
                <div class="col col-3 text-right">
                    {{--<a class="btn btn-success mt-3 mb-3" href="{{ route('admin.institucion.create') }}">--}}
                    {{--<i class="fa fa-plus"></i>--}}
                    {{--</a>--}}
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    {{--<th>Docente</th>--}}
                    <th>Estudiante</th>
                    <th>Instituci&oacute;n</th>
                    <th>Aula</th>
                    <th>Año lectivo</th>
                    <th class="text-center">
                        <div style="line-height: 14px;">
                            Estudiante
                            <div class="clearfix"></div>
                            <small>
                                ¿Estado?
                            </small>
                        </div>
                    </th>
                    <th class="text-center">
                        <div style="line-height: 14px;">
                            Representante
                            <div class="clearfix"></div>
                            <small>
                                ¿Estado?
                            </small>
                        </div>
                    </th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th class="text-center">
                        <i class="fas fa-cogs"></i>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($estudiantes as $estudiante)
                    @php
                        $estadoRegistro = $estudiante->estadoRegistro;
                    @endphp
                    <tr>
                        <td>
                            <div style="white-space: nowrap">
                                {{ $estudiante->nombre_estudiante }}
                                <div class="text-success small">{{ $estudiante->cedula_estudiante }}</div>
                            </div>
                        </td>
                        <td>{{ $estudiante->nombre_institucion }}</td>
                        <td>{{ $estudiante->nombre_aula}}</td>
                        <td>{{ $estudiante->anioLectivo->descripcion }}</td>
                        <td class="text-center">
                            <div style="white-space: nowrap">
                                {!! \App\Helpers\Helper::obtenerIconEstado($estadoRegistro->codigo) !!}
                                <small>{{$estadoRegistro->nombre}}</small>
                            </div>
                        </td>
                        <td class="text-justify">
                            @if($estudiante->representantes)
                                <div class="accordion" id="orders-accordion" style="max-width: 200px;">
                                    {{--<div class="card">--}}
                                    {{--<div class="card-header">--}}
                                    {{--<div class="accordion-heading">--}}
                                    {{--<a class="nav-link-style d-flex flex-wrap align-items-center justify-content-between  pr-2"--}}
                                    <a class="nav-link-style"
                                       href="#representante-{{$estudiante->id}}" role="button"
                                       data-toggle="collapse"
                                       aria-expanded="true"
                                       aria-controls="representante-{{$estudiante->id}}">
                                        {{--<div class="font-size-sm font-weight-medium text-nowrap mr-2">--}}
                                        <i class="fe-eye mr-1"></i>
                                        <small>Representantes</small>
                                        {{--</div>--}}
                                    </a>
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="collapse" id="representante-{{$estudiante->id}}"
                                         data-parent="#orders-accordion">
                                        @foreach($estudiante->representantes as $representante)
                                            <div class="mt-2 pt-2 border-top text-center" style="white-space: nowrap;">
                                                {!! \App\Helpers\Helper::obtenerIconEstado($representante->estadoRegistro->codigo) !!}
                                                <div class="clearfix"></div>
                                                <small>{{$representante->estadoRegistro->nombre}}</small>
                                                <div class="clearfix"></div>
                                                <div class="text-center mt-1 mb-1 ">
                                                    <small>
                                                        <a href="#representante-modal-{{$representante->id}}" class="nav-link-style mb-2" data-toggle="modal"
                                                           data-target="#representante-modal-{{$representante->id}}">
                                                            &nbsp;{{$representante->nombre_representante}}
                                                        </a>
                                                    </small>
                                                    <div class="clearfix"></div>
                                                    <a href="{{route('app.docente.representante.aprobar', $representante->id)}}"
                                                       class="btn btn-outline-success btn-sm mr-2">
                                                        <small>&nbsp;SI</small>
                                                    </a>
                                                    <a href="" class="btn btn-outline-danger btn-sm">
                                                        <small>&nbsp;NO</small>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Modal markup -->
                                            <div class="modal fade" id="representante-modal-{{$representante->id}}"
                                                 tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Representante</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            @include('aplicacion.user._show_perfil',['perfil'=>$representante->user->perfil])
                                                        </div>
                                                        {{--<div class="modal-footer">--}}
                                                        {{--<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>--}}
                                                        {{--<button type="button" class="btn btn-primary btn-sm">Save changes</button>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    {{--</div>--}}
                                </div>
                            @endif
                        </td>
                        <td class="created_at">
                            <small>{{ $estudiante->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="text-center">
                            <div style="white-space: nowrap">
                                @if($estudiante->user->perfil)
                                    <a href="{{ route('app.docente.estudiante.aprobar',$estudiante->id) }}"
                                       class="btn btn-sm btn-outline-success mr-1">
                                        <i class="fe-check"></i>
                                    </a>
                                    <a href="{{ route('app.docente.estudiante.cancelar',$estudiante->id) }}"
                                       class="btn btn-sm btn-outline-danger">
                                        <i class="fe-x"></i>
                                    </a>
                                @else
                                    <span for="" class="text-warning">Sin informaci&oacute;n <br>del estudiante</span>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="m-3">
                {{
                $estudiantes
                ->appends(['created_at' => request('created_at'),'search' => request('search'),])
                ->links()
                }}
            </div>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'admin.institucion.destroy'])

    {{--<script>--}}
    {{--window.onload = function () {--}}
    {{--$('#deleteModal').on('show.bs.modal', function (event) {--}}
    {{--var button = $(event.relatedTarget); // Button that triggered the modal--}}
    {{--var id = button.data('id'); // Extract info from data-* attributes--}}
    {{--// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).--}}
    {{--// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.--}}

    {{--action = $('#formDelete').attr('data-action').slice(0, -1);--}}
    {{--action += id;--}}
    {{--console.log(action);--}}

    {{--$('#formDelete').attr('action', action);--}}

    {{--var modal = $(this);--}}
    {{--modal.find('.modal-title').text('Borrar el Perido Lectivo: ' + id);--}}
    {{--});--}}
    {{--};--}}
    {{--</script>--}}

@endsection