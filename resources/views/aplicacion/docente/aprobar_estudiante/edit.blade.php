@extends('layouts.backend.app')

@section('sidebar')
    <div class="row ">
        <div class="col col-7 "></div>
        <div class="col col-4 ">
            <nav aria-label="breadcrumb">
                {{ Breadcrumbs::render('admin.institucion.create', $model ?? '') }}
            </nav>
        </div>
        <div class="col col-1 text-center">
            <a class="btn btn-success btn-sm mt-2" href="{{ route('admin.institucion.create') }}">
                <i class="fa fa-plus"></i>
            </a>
        </div>
    </div>
@endsection

@section('content')
    @include('includes.validation-error-admin')
    <form action="{{ route("admin.institucion.update",$model->id) }}" method="POST" enctype='multipart/form-data'>
        @method('PUT')
        @include('backend.institucion._form')
    </form>
@endsection
