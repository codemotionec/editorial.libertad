@extends('layouts.aplicacion.app')

@section('breadcrum')
    {{ Breadcrumbs::render('app.docente.index', $model ?? '') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header card-header-primary">
            <div class="row">
                <div class="col col-9">
                    <h4 class="card-title">
                        Docente
                    </h4>
                    <div class="card-category">
                        Aqui encontrarás las instituciones a las que preteneces
                    </div>
                </div>
                <div class="col col-3 text-right">
                    @if(\Illuminate\Support\Facades\Auth::user()->perfil)
                        <a class="btn btn-success btn-sm" href="{{ route('app.docente.create') }}">
                            <i class="fe-plus"></i>
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Instituci&oacute;n</th>
                    <th>Aula</th>
                    <th>Año lectivo</th>
                    <th>
                        <small>Tutor Aula</small>
                    </th>
                    <th>
                        <small>Autorizaci&oacute;n</small>
                    </th>
                    <th>
                        <small>Created_at</small>
                    </th>
                    <th>
                        <small>Updated_at</small>
                    </th>
                    <th class="text-center">Estado</th>
                    <th class="text-center"><i class="fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($docentes as $docente)
                    {{--{{dd($estudiante->aula, $estudiante->anioLectivo)}}--}}
                    <tr>
                        <td>{{ $docente->nombre_institucion }}</td>
                        <td>{{ $docente->nombre_aula}}</td>
                        <td>{{ $docente->anioLectivo->descripcion }}</td>
                        <td class="text-center">
                            {!! \App\Helpers\Helper::obtenerIconTruFalse($docente->representante_curso) !!}
                        </td>
                        <td class="text-center">
                            <small>{{$docente->nombre_autoriza}}</small>
                        </td>
                        <td class="created_at">
                            <small>{{ $docente->created_at->format('M d Y h:i') }}</small>
                        </td>
                        <td class="created_at">
                            <small>{{ $docente->updated_at->format('M d Y h:i') }}</small>
                        </td>

                        <td class="text-center">
                            {!! \App\Helpers\Helper::obtenerIconEstado($docente->estadoRegistro->codigo) !!}
                            <small>{{$docente->estadoRegistro->nombre}}</small>
                        </td>
                        <td class="text-center">
                            <a data-toggle="modal" data-target="#deleteModal" data-id="{{ $docente->id }}"
                               class="nav-link-style mr-1 text-danger">
                                <i class="fe-trash-2"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('includes.forms.modal-delete',['name_route'=>'app.docente.destroy'])

@endsection

@section('scripts')
    <script type="text/javascript">
        window.deleteModalAjax('deleteModal');
    </script>
@endsection