<div class="pb-2">
    <div class="section-title-left">
        <h2>{{__('Estudiante')}}</h2>
    </div>

    <ul class="list-no-style no-padding">
        <li class="">
            <a href="#" class="fs-10 uppercase text-black-light">
                {{__('Institución / Curso')}}
            </a>
        </li>
        <li class="">
            <a href="#" class="fs-10 uppercase text-black-light">
                {{__('Registrar Libro')}}
            </a>
        </li>
    </ul>
</div>