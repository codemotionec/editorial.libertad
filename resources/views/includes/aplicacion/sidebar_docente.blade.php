<div class="pb-2">
    <div class="section-title-left mb-2">
        <h2>{{__('Docente')}}</h2>
    </div>

    <ul class="list-no-style no-padding">
        <li class="my-1">
            <a href="{{route('app.docente.create')}}" class="fs-12 uppercase text-black-light">
                <i class="fas fa-school mr-2"></i>{{__('Mis Instituciones')}}
            </a>
        </li>

        <li class="my-1">
            <a href="#" class="fs-12 uppercase text-black-light">
                <i class="fas fa-signature mr-2"></i>{{__('Mis asignaturas')}}
            </a>
        </li>
    </ul>
</div>