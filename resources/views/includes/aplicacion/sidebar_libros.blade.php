@if($segmentoLibros)
    <h2 class="fs-22 mb-3 bolder uppercase mt-4">{{ __($libro->titulo) }}</h2>
    <ul class="list-no-style no-padding">
        @foreach($segmentoLibros as $_segmentoLibro)
            <li>
                <a href="{{route('app.archivo.revista',$_segmentoLibro->id) }}" class="
                @if($segmentoLibro && $_segmentoLibro->id == $segmentoLibro->id)
                        text-aqua
                @endif">
                    {{$_segmentoLibro->unidad}}
                </a>
            </li>
        @endforeach
    </ul>
    <hr>
@endif