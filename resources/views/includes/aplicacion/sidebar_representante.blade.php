<div class="pb-2">
    <div class="section-title-left">
        <h2>{{__('Representante')}}</h2>
    </div>

    <ul class="list-no-style no-padding">
        <li class="">
            <a href="#" class="fs-10 uppercase text-black-light">
                Registrar estudiante
            </a>
        </li>
        <li class="">
            <a href="#" class="fs-10 uppercase text-black-light">
                Registrar Libro Estudiante
            </a>
        </li>
    </ul>
</div>