<div class="cs-file-drop-area">
    <div class="cs-file-drop-icon czi-cloud-upload"></div>
    <span class="cs-file-drop-message">{{__('Arrastra y suelta aquí para subir')}}</span>
    <input type="file" class="cs-file-drop-input" name="{{$file_name}}" id="{{$file_id}}">
    <button type="button" class="cs-file-drop-btn btn btn-primary btn-sm">
        {{__('O seleccione archivo')}}
    </button>
</div>