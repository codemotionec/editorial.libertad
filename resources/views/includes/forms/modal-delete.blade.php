@php
    $id = (isset($idmodal) && $idmodal) ? $idmodal : 'deleteModal';
    $idform = (isset($formDelete) && $formDelete) ? $formDelete : 'formDelete';
    $button = (isset($button) && $button) ? $button : 'submit';
    $id_button = (isset($id_button) && $id_button) ? $id_button : 'delete-modal-btn';
@endphp
<div class="modal fade" id="{{$id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>¿Seguro que desea borrar el registro seleccionado?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

                <form id="{{$idform}}" method="POST" action="{{ route($name_route,0) }}"
                      data-action="{{ route($name_route,0) }}">
                    @method('DELETE')
                    @csrf
                    <button type="{{$button}}" id="{{$id_button}}" class="btn btn-danger">Borrar</button>
                </form>

            </div>
        </div>
    </div>
</div>