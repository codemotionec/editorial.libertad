@php
    $canton = (request('filter_canton_id')) ? \App\Models\Canton::find((request('filter_canton_id'))):null;
    $institucion = (request('filter_institucion_id')) ? \App\Models\Institucion::find((request('filter_institucion_id'))):null;

@endphp
<form action="{{ route($name_route) }}" class="mb-2">
    <div class="pb-2 tns-item tns-slide-active" id="tns2-item0">
        <article class="card card-hover h-100 border-0 box-shadow pt-2 pb-2 mx-1">
            {{--<span class="badge badge-lg badge-floating badge-floating-right badge-dark">Filtros</span>--}}
            <div class="card-body pt-2 px-4 px-xl-5">
                <div class="row">
                    <div class="col-sm-12 col-md-3">
                        <label for="canton_id" class="bmd-label-floating">Ciudad</label>
                        <select class="form-control select2" id="filter_canton_id" name="filter_canton_id"
                                data-ajax--url="{{route('api.canton.select2')}}" data-ajax--data-type="json"
                                data-allow-clear="true"
                                data-placeholder="Seleccionar..." >
                            @if($canton)
                                <option value="{{$canton->id}}">{{$canton->nombre}}</option>
                            @endif
                        </select>

                        <div class="invalid-feedback">Por favor seleccione la ciudad</div>
                        <div class="valid-feedback"><i class="fe-check-circle"></i></div>
                    </div>

                    <div class="col-sm-12 col-md-4">
                        <label for="institucion_id" class="bmd-label-floating">Instituci&oacute;n</label>
                        <select class="form-control select2" id="filter_institucion_id" name="filter_institucion_id"
                                @if($institucion)
                                data-id="{{$institucion->id}}"
                                @endif

                                {{--data-ajax--url="{{route('api.institucion.select2')}}" --}}
                                {{--data-ajax--data-type="json"--}}
                                {{--data-allow-clear="true"--}}
                                data-placeholder="Seleccionar..." >
                            @if($institucion)
                                <option value="{{$institucion->id}}">{{$institucion->nombre}}</option>
                            @endif
                        </select>

                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-4">
                        <label for="search" class="bmd-label-floating">Contenido</label>
                        <input type="text" value="{{ request('search') }}" id="search" name="search"
                               placeholder="Buscar..."
                               class="form-control form-control-sm">
                    </div>

                    <div class="col-md-1 text-right">
                        <button type="submit" class="btn btn-sm btn-primary mt-4 mr-4">
                            <i class="fa fa-search"></i>
                        </button>
                        <a href="{{route($name_route)}}"><i class="fe-trash-2"></i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div id="filter_institucion_info" class=" col-12"></div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</form>

@section('scripts-filter')
    <script src="{{asset('js/formSubmit.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var select2Options = {width: 'resolve'};

            var apiUrl_ = "{{route('api.institucion.select2',0)}}";
            window.Select2Cascade($('#filter_canton_id'), $('#filter_institucion_id'), apiUrl_, select2Options);

            $('#filter_canton_id').trigger('change');

            $('#filter_institucion_id').on('select2:select', function (e) {
                window.cargarInstitucionInfo(e.params.data['id'], "{{route('api.institucion.info')}}", 'filter_institucion_info');
            });
        });
    </script>
@endsection