<form action="{{ route($name_route) }}" class="mb-2">
    <div class="tns-item tns-slide-active" id="tns2-item0">
        <article class="card card-hover h-100 border-0 box-shadow pt-2 mx-1">
            {{--<span class="badge badge-lg badge-floating badge-floating-right badge-dark">Filtros</span>--}}
            <div class="card-body pt-2 px-4 px-xl-5">
                <div class="row">
                    <div class="col-md-4 pb-2">
                        <select name="created_at" class="form-control form-control-sm">
                            <option value="DESC">Descendente</option>
                            <option {{ request('created_at') == "ASC" ? "selected" : '' }}  value="ASC">
                                Ascendente
                            </option>
                        </select>
                    </div>
                    <div class="col-md-7">
                        <input type="text" value="{{ request('search') }}" name="search" placeholder="Buscar..."
                               class="form-control form-control-sm">
                    </div>

                    <div class="col-md-1 text-right">
                        <button type="submit" class="btn btn-sm btn-primary mt-1 mr-4">
                            <i class="fa fa-search"></i>
                        </button>
                        <a href="{{route($name_route)}}"><i class="fe-trash-2"></i></a>
                    </div>
                </div>
            </div>
        </article>
    </div>

</form>