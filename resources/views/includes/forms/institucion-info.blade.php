<div class="alert alert-info mt-2">
    @if($institucion->imagen)
        <img style="max-width: 90px;" src="{{asset('storage/instituciones/'.$institucion->imagen)}}"
             class="float-left img-thumbnail rounded-0 mr-2" alt="{{$institucion->nombre}}">
    @endif

    {{--<img class="float-left navbar-tool-icon-box-img" src="">--}}
    <strong>Sector</strong>: {{$institucion->nombre_parroquia}}
    <div class="clearfix"></div>
    <strong>Direcci&oacute;n</strong>: {{$institucion->direccion}}
    <div class="clearfix"></div>
    <strong>Sostenimiento</strong>: {{$institucion->sostenimiento}}
    <div class="clearfix"></div>
    <strong>Nivel Educaci&oacute;n</strong>: {{$institucion->nivel_educacion}}
</div>