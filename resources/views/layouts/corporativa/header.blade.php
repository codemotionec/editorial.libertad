@php
    //$menu_items = config('sidebarPublico.menu');
    //$menu_perfil = config('sidebarPublico.menu_perfil');
    $path = (Request::path() != '/') ? '/'. Request::path() : '/';
    $user = auth()->user();
@endphp

<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div id="secondary-nav" class="secondary-nav">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4">
                    <ul class="d-flex w-100 justify-content-between no-margin no-padding padding-tb-7 list-no-style">
                        <li><a href="{{route('corporativa.acerca_de')}}">{{__('Acerca de')}}</a></li>
                        <li><a href="/trabaja-con-nosotros">{{__('Trabaja con Nosotros')}}</a></li>
                        <li><a href="{{route('corporativa.contacto')}}">{{__('Contacto')}}</a></li>
                    </ul>
                </div>
                <div class="col-lg-8 padding-tb-7 text-right">
                    <a href="{{route('app.dashboard')}}">{{__('Ingresar')}} <i class="fas fa-user"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container d-flex align-items-center">

        <div class="logo mr-auto">
            <img src="{{asset('images/LOGO-LIBERTAD.jpg')}}" alt="Logo">
            {{--<a href="{{route('corporativa.home')}}">Editorial Libertad</a>--}}
        </div>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="active"><a href="{{route('corporativa.home')}}">{{__('Inicio')}}</a></li>
                <li><a href="{{route('corporativa.libreria')}}">Librería</a></li>
                <li><a href="{{route('corporativa.instituciones')}}">Instituciones</a></li>
                {{--<li class="drop-down"><a href="">Recursos</a>--}}
                {{--<ul>--}}
                {{--<li><a href="#">Un Link</a></li>--}}
                {{--<li class="drop-down"><a href="#">Recurso 1</a>--}}
                {{--<ul>--}}
                {{--<li><a href="#">Recurso A 1</a></li>--}}
                {{--<li><a href="#">Recurso A 2</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}
                {{--<li><a href="#">Recurso 2</a></li>--}}
                {{--<li><a href="#">Recurso 3</a></li>--}}
                {{--<li><a href="#">Recurso 4</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}
            </ul>
        </nav><!-- .nav-menu -->

        <a href="{{route('auth.register')}}" class="get-started-btn">{{__('Registrarse')}}</a>

    </div>
</header><!-- End Header -->