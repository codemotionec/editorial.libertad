<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="descriptison">
    <meta content="" name="keywords">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- CSS -->
    <link rel="stylesheet" href="{{asset("css/app.frontend.css")}}">
    <link href="{{asset('assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
    <title>Editorial Libertad @yield('page_title')</title>
    @yield('head')
</head>
<body>

<!-- ERRORS ALERT -->
<div class="error-container" id="errorDiv">
    {{-- @include('includes.validation-error') --}}
    {{-- @include('includes.session-flash-status') --}}
    {{-- {{ json_encode(session()->all()) }} --}}
    {{-- {{ json_encode(Auth::user()) }} --}}
</div>

@yield('body_afteropeningtag')
@include('layouts.corporativa.header')
@yield('content_before')

<div class="page-container">
    @yield('content')
</div>

<script src="{{asset('js/app.js')}}"></script>
{{--<script src="{{asset('assets/vendor/php-email-form/validate.js')}}"></script>--}}
<script src="{{ asset('js/aplicacion.js') }}"></script>

@yield('content_after')
@include('layouts.corporativa.footer')
@yield('body_beforeclosingtag')
</body>
</html>
