<div class="cs-widget mb-5">
    <h3 class="cs-widget-title">Brand</h3>
    <div class="custom-control custom-checkbox mb-2">
        <input class="custom-control-input" type="checkbox" id="adidas" checked>
        <label class="custom-control-label text-nav" for="adidas">Adidas<span
                    class="font-size-xs text-muted ml-2">425</span></label>
    </div>
    <div class="custom-control custom-checkbox mb-2">
        <input class="custom-control-input" type="checkbox" id="bilabong">
        <label class="custom-control-label text-nav" for="bilabong">Bilabong<span
                    class="font-size-xs text-muted ml-2">27</span></label>
    </div>
    <div class="custom-control custom-checkbox mb-2">
        <input class="custom-control-input" type="checkbox" id="klein">
        <label class="custom-control-label text-nav" for="klein">Calvin Klein<span
                    class="font-size-xs text-muted ml-2">365</span></label>
    </div>
    <div class="custom-control custom-checkbox mb-2">
        <input class="custom-control-input" type="checkbox" id="electrolux">
        <label class="custom-control-label text-nav" for="electrolux">Electrolux<span
                    class="font-size-xs text-muted ml-2">56</span></label>
    </div>
    <div class="custom-control custom-checkbox mb-2">
        <input class="custom-control-input" type="checkbox" id="motorolla">
        <label class="custom-control-label text-nav" for="motorolla">Motorolla<span
                    class="font-size-xs text-muted ml-2">104</span></label>
    </div>
    <div class="custom-control custom-checkbox mb-2">
        <input class="custom-control-input" type="checkbox" id="nb">
        <label class="custom-control-label text-nav" for="nb">New Balance<span
                    class="font-size-xs text-muted ml-2">197</span></label>
    </div>
</div>