<!-- Menu-->
<ul class="navbar-nav">
    <li class="nav-item dropdown dropdown-mega">
        <a class="nav-link dropdown-toggle" href="{{route('app.dashboard')}}">
            Inicio
        </a>
    </li>
    <li class="nav-item dropdown dropdown-mega">
        <a class="nav-link dropdown-toggle" href="#"
           data-toggle="dropdown">Estudiantes & Representantes</a>
        <div class="dropdown-menu">
            <a class="dropdown-column dropdown-column-img bg-secondary"
               href="{{route('app.material-estudiante.index')}}"
               style="background-image: url({{asset('img/estudiante.jpg')}});">
            </a>
            <div class="dropdown-column">
                <h5 class="dropdown-header">Estudiante</h5>
                <a class="dropdown-item" href="{{route('app.estudiante.index')}}">
                    {{__('Mis instituciones')}}
                </a>
                <a class="dropdown-item" href="{{route('app.material-estudiante.index')}}">
                    {{__('Mis libros')}}
                </a>
                <a class="dropdown-item"
                   href="{{route('admin.institucion.index')}}">
                    {{__('Revisar mis tareas')}}
                </a>
                <a class="dropdown-item" href="{{route('admin.curso.index')}}">
                    {{__('Revisar mis evaluaciones')}}
                </a>
            </div>
            <div class="dropdown-column">
                <h5 class="dropdown-header">Representante</h5>
                <a class="dropdown-item" href="{{route('app.representante-estudiante.index')}}">
                    {{__('Registrar estudiantes')}}
                </a>
            </div>
        </div>
    </li>
    <li class="nav-item dropdown dropdown-mega">
        <a class="nav-link dropdown-toggle" href="#"
           data-toggle="dropdown">Docentes</a>
        <div class="dropdown-menu">
            <a class="dropdown-column dropdown-column-img bg-secondary"
               href="{{route('app.docente.index')}}"
               style="background-image: url({{asset('img/docente.jpg')}});">
            </a>
            <div class="dropdown-column">
                <a class="dropdown-item" href="{{route('app.docente.index')}}">
                    {{__('Mis instituciones')}}
                </a>
                <a class="dropdown-item" href="{{route('app.material-docente.index')}}">
                    {{__('Mis libros')}}
                </a>
                <a class="dropdown-item" href="{{route('app.docente.estudiante.index')}}">
                    {{__('Mis estudiantes')}}
                </a>
                <a class="dropdown-item"
                   href="{{route('app.tarea.index')}}">
                    {{__('Calificar tareas')}}
                </a>
                <a class="dropdown-item" href="{{route('admin.curso.index')}}">
                    {{__('Calificar evaluaciones')}}
                </a>
            </div>
        </div>
    </li>

    @if($roles = \App\Helpers\Sesiones::roles())
        @foreach($roles as $rol)
            @if($rol->key=='ADMIN')
                <li class="nav-item dropdown dropdown-mega">
                    <a class="nav-link dropdown-toggle" href="#"
                       data-toggle="dropdown">Administrar</a>
                    <div class="dropdown-menu">
                        <div class="dropdown-column">
                            <h5 class="dropdown-header">Contenido</h5>
                            <a class="dropdown-item" href="{{route('admin.libro.index')}}">
                                Creacion de Libros
                            </a>
                            <a class="dropdown-item"
                               href="{{route('admin.institucion.index')}}">
                                Creaci&oacute;n de Instituciones
                            </a>
                            <a class="dropdown-item" href="{{route('admin.aula.index')}}">
                                Asignaci&oacute;n de aulas
                            </a>
                        </div>
                        <div class="dropdown-column">
                            <h5 class="dropdown-header">Permisos</h5>
                            <a class="dropdown-item" href="{{route('admin.docente.index')}}">
                                Aprobaci&oacute;n de docentes
                            </a>
                            <a class="dropdown-item" href="{{route('admin.material-estudiante.index')}}">
                                Aprobaci&oacute;n de contenido estudiantil
                            </a>
                        </div>
                        <div class="dropdown-column">
                            <h5 class="dropdown-header">Parametrizacion</h5>
                            <a class="dropdown-item" href="{{route('admin.curso.index')}}">
                                Parametrizaci&oacute;n de cursos
                            </a>
                            <a class="dropdown-item" href="{{route('admin.asignatura.index')}}">
                                Parametrizaci&oacute;n de asignaturas
                            </a>
                            <a class="dropdown-item" href="{{route('admin.anio-lectivo.index')}}">
                                Parametrizaci&oacute;n de periodos lectivos
                            </a>
                            <a class="dropdown-item" href="{{route('admin.abreviatura.index')}}">
                                Parametrizaci&oacute;n de abreviaturas
                            </a>
                        </div>
                    </div>
                </li>
            @endif
        @endforeach
    @endif
</ul>