@php
    $path = (Request::path() != '/') ? '/'. Request::path() : '/';
    $user = auth()->user();
    $perfil = (isset($user->perfil)) ? $user->perfil : [];
@endphp

{{--@if($perfil)--}}
<div class="d-flex align-items-center order-lg-3 ml-lg-auto">
    <div class="navbar-tool dropdown">
        <a class="navbar-tool-icon-box" href="#">
            <img class="navbar-tool-icon-box-img"
                 src="{{($perfil) ? asset('storage/perfil/'.$perfil->imagen):asset('img/user.jpg')}}"
                 alt="{{($perfil) ? $perfil->nombre : ''}}"/>
        </a>
        <a class="navbar-tool-label dropdown-toggle" href="#">
            <small>Hola,</small>
            {{($perfil) ? mb_strtoupper($perfil->nombre) : 'User'}}

            @if(($estudiante = \App\Helpers\Sesiones::estudiante(true)))
                <div class="clearfix"></div>
                <span class="text-primary">
                    {{ucfirst($estudiante->perfil->nombre)}}<i class="fe-check-circle ml-2"></i>
                </span>
            @endif
        </a>
        <ul class="dropdown-menu dropdown-menu-right" style="width: 15rem;">
            <li>
                <a class="dropdown-item d-flex align-items-center" href="{{route('app.user.perfil')}}">
                    <i class="fas fa-cogs opacity-60 mr-2"></i> {{__('Mi perfil')}}
                </a>
            </li>
            <li>
                <a class="dropdown-item d-flex align-items-center" href="{{route('app.dashboard')}}">
                    <i class="fas fa-sync-alt opacity-60 mr-2"></i> {{__('Cambiar contraseña')}}
                </a>
            </li>

            <li>
                <a class="dropdown-item d-flex align-items-center" href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fas fa-sign-out-alt opacity-60 mr-2"></i> {{ __('Cerrar sesión') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
            @if($representados = \App\Helpers\Sesiones::representados())
                @foreach($representados as $representado)
                    <li class="border-top">
                        <a class="dropdown-item d-flex align-items-center"
                           href="{{route('app.representante-estudiante.cambiar', $representado->user->id)}}">
                            @if(($estudiante = \App\Helpers\Sesiones::estudiante()) && $estudiante->user->id == $representado->user->id)
                                <i class="fe-user-check mr-2 text-success"></i>
                            @else
                                <i class="fe-user mr-2"></i>
                            @endif
                            {{$representado->perfil->nombre. ' '.$representado->perfil->apellido}}
                        </a>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>
{{--@endif--}}