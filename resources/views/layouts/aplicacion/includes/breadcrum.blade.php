<nav aria-label="breadcrumb">
    <ol class="py-1 my-2 breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Shop grid</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Left sidebar</li>
    </ol>
</nav>