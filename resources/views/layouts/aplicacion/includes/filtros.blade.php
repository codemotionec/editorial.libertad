
<!-- Active filters-->
<div class="d-flex flex-wrap align-items-center mb-2">
    <label class="mr-3 mt-1 mb-2">Your selection:</label><a class="active-filter mr-2 my-2"
                                                            href="#">Clothing</a><a
            class="active-filter mr-2 my-2" href="#">Shirts &amp; Tops</a><a
            class="active-filter mr-2 my-2" href="#">Brand name</a><a class="active-filter my-2"
                                                                      href="#">Size</a>
</div>
<!-- Sorting + Pager-->

<div class="d-flex justify-content-between align-items-center py-3 mb-3">
    <div class="d-flex justify-content-center align-items-center">
        <select class="form-control custom-select mr-2" style="width: 15.25rem;">
            <option value="popularity">Sort by popularity</option>
            <option value="rating">Sort by average rating</option>
            <option value="newness">Sort by newness</option>
            <option value="price: low to high">Sort by price: low to high</option>
            <option value="price: high to low">Sort by price: high to low</option>
        </select>
        <div class="d-none d-sm-block font-size-sm text-nowrap pl-1 mb-1">of 135 products</div>
    </div>
    <div class="d-none d-lg-flex justify-content-center align-items-center">
        <label class="pr-1 mr-2">Show</label>
        <select class="form-control custom-select mr-2" style="width: 5rem;">
            <option value="12">12</option>
            <option value="24">24</option>
            <option value="36">36</option>
            <option value="48">48</option>
            <option value="60">60</option>
        </select>
        <div class="font-size-sm text-nowrap pl-1 mb-1">products per page</div>
    </div>
</div>