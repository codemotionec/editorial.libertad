
<div class="cs-widget mt-n1 mb-5">
    <h3 class="cs-widget-title">Price</h3>
    <div class="px-2">
        <div class="cs-range-slider" data-start-min="250" data-start-max="680" data-min="0"
             data-max="1000" data-step="1">
            <div class="cs-range-slider-ui"></div>
            <div class="d-flex">
                <div class="w-50 pr-2 mr-2">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend"><span
                                    class="input-group-text">$</span></div>
                        <input class="form-control cs-range-slider-value-min" type="text">
                    </div>
                </div>
                <div class="w-50 pl-2">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend"><span
                                    class="input-group-text">$</span></div>
                        <input class="form-control cs-range-slider-value-max" type="text">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>