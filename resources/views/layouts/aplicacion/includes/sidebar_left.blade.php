<div class="cs-offcanvas-cap navbar-box-shadow px-4 mb-3">
    <h5 class="mt-1 mb-0">Refine your selection</h5>
    <button class="close lead" type="button" data-toggle="offcanvas"
            data-offcanvas-id="shop-sidebar"><span aria-hidden="true">&times;</span></button>
</div>
<div class="cs-offcanvas-body px-4 pt-3 pt-lg-0 pl-lg-0 pr-lg-2 pr-xl-4" data-simplebar>
    <!-- Categories-->
    <div class="cs-widget cs-widget-categories mb-5">
        <h3 class="cs-widget-title">PERMISOS</h3>
        <ul id="categories">
            <li>
                <a class="cs-widget-link" href="#clothing" data-toggle="collapse">
                    Estudiante
                </a>
                <ul class="collapse show" id="clothing" data-parent="#categories">
                    <li>
                        <a class="cs-widget-link" href="#">
                            Libros
                            <small class="text-muted pl-1 ml-2">1</small>
                        </a>
                    </li>
                    <li>
                        <a class="cs-widget-link" href="#">
                            Revisar avances
                        </a>
                    </li>
                    <li>
                        <a class="cs-widget-link" href="{{route('app.estudiante.detalle-registro-libro')}}">
                            Registrar libro
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="cs-widget-link collapsed" href="#shoes" data-toggle="collapse">
                    Docentes
                </a>
                <ul class="collapse" id="shoes" data-parent="#categories">
                    <li>
                        <a class="cs-widget-link" href="#">
                            Revisar Calificaciones
                            <small class="text-muted pl-1 ml-2">1</small>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="cs-widget-link collapsed" href="#electronics" data-toggle="collapse">
                    Representante
                </a>
                <ul class="collapse" id="electronics" data-parent="#categories">
                    <li>
                        <a class="cs-widget-link" href="#">
                            Revisar Avances
                            <small class="text-muted pl-1 ml-2">1</small>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="cs-widget-link collapsed" href="#accessories" data-toggle="collapse">
                    <i class="fa fa-cogs opacity-60 mr-1 fs-12"></i> Usuario
                </a>
                <ul class="collapse" id="accessories" data-parent="#categories">
                    <li><a class="cs-widget-link" href="{{route('app.user.perfil')}}">Perfil Usuario</a></li>
                    <li><a class="cs-widget-link" href="">Cambiar contraseña</a></li>
                    <li>
                        <a class="cs-widget-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form-sidebar').submit();">
                            <i class="fas fa-sign-out-alt opacity-60 mr-2"></i> {{ __('Logout') }}
                        </a>

                        <form id="logout-form-sidebar" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </div>

    {{--@include('layouts.aplicacion.includes.rango-precio')--}}

    {{--@include('layouts.aplicacion.includes.marcas')--}}

    {{--@include('layouts.aplicacion.includes.tamanios')--}}
</div>