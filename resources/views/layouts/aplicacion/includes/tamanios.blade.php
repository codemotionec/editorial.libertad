<div class="cs-widget mb-5">
    <h3 class="cs-widget-title">Size</h3>
    <div class="custom-control custom-checkbox mb-2">
        <input class="custom-control-input" type="checkbox" id="xs">
        <label class="custom-control-label text-nav" for="xs">XS<span
                    class="font-size-xs text-muted ml-2">38</span></label>
    </div>
    <div class="custom-control custom-checkbox mb-2">
        <input class="custom-control-input" type="checkbox" id="s" checked>
        <label class="custom-control-label text-nav" for="s">S<span
                    class="font-size-xs text-muted ml-2">173</span></label>
    </div>
    <div class="custom-control custom-checkbox mb-2">
        <input class="custom-control-input" type="checkbox" id="m">
        <label class="custom-control-label text-nav" for="m">M<span
                    class="font-size-xs text-muted ml-2">226</span></label>
    </div>
    <div class="custom-control custom-checkbox mb-2">
        <input class="custom-control-input" type="checkbox" id="l">
        <label class="custom-control-label text-nav" for="l">L<span
                    class="font-size-xs text-muted ml-2">345</span></label>
    </div>
    <div class="custom-control custom-checkbox mb-2">
        <input class="custom-control-input" type="checkbox" id="xl">
        <label class="custom-control-label text-nav" for="xl">XL<span
                    class="font-size-xs text-muted ml-2">104</span></label>
    </div>
</div>