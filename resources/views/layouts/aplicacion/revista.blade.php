<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Editorial Libertad @yield('page_title')</title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Around - Multipurpose Bootstrap Template">
    <meta name="keywords"
          content="bootstrap, business, consulting, coworking space, services, creative agency, dashboard, e-commerce, mobile app showcase, multipurpose, product landing, shop, software, ui kit, web studio, landing, html5, css3, javascript, gallery, slider, touch, creative">
    <meta name="author" content="Codemotion">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('site.webmanifest')}}">
    <link rel="mask-icon" color="#5bbad5" href="safari-pinned-tab.svg">
    <meta name="msapplication-TileColor" content="#766df4">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Page loading styles-->
    <style>
        /*.cs-page-loading {*/
        /*position: fixed;*/
        /*top: 0;*/
        /*right: 0;*/
        /*bottom: 0;*/
        /*left: 0;*/
        /*width: 100%;*/
        /*height: 100%;*/
        /*-webkit-transition: all .4s .2s ease-in-out;*/
        /*transition: all .4s .2s ease-in-out;*/
        /*background-color: #fff;*/
        /*opacity: 0;*/
        /*visibility: hidden;*/
        /*z-index: 9999;*/
        /*}*/

        /*.cs-page-loading.active {*/
        /*opacity: 1;*/
        /*visibility: visible;*/
        /*}*/

        /*.cs-page-loading-inner {*/
        /*position: absolute;*/
        /*top: 50%;*/
        /*left: 0;*/
        /*width: 100%;*/
        /*text-align: center;*/
        /*-webkit-transform: translateY(-50%);*/
        /*transform: translateY(-50%);*/
        /*-webkit-transition: opacity .2s ease-in-out;*/
        /*transition: opacity .2s ease-in-out;*/
        /*opacity: 0;*/
        /*}*/

        /*.cs-page-loading.active > .cs-page-loading-inner {*/
        /*opacity: 1;*/
        /*}*/

        /*.cs-page-loading-inner > span {*/
        /*display: block;*/
        /*font-family: 'Inter', sans-serif;*/
        /*font-size: 1rem;*/
        /*font-weight: normal;*/
        /*color: #737491;*/
        /*}*/

        /*.cs-page-spinner {*/
        /*display: inline-block;*/
        /*width: 2.75rem;*/
        /*height: 2.75rem;*/
        /*margin-bottom: .75rem;*/
        /*vertical-align: text-bottom;*/
        /*border: .15em solid #766df4;*/
        /*border-right-color: transparent;*/
        /*border-radius: 50%;*/
        /*-webkit-animation: spinner .75s linear infinite;*/
        /*animation: spinner .75s linear infinite;*/
        /*}*/

        /*@-webkit-keyframes spinner {*/
        /*100% {*/
        /*-webkit-transform: rotate(360deg);*/
        /*transform: rotate(360deg);*/
        /*}*/
        /*}*/

        /*@keyframes spinner {*/
        /*100% {*/
        /*-webkit-transform: rotate(360deg);*/
        /*transform: rotate(360deg);*/
        /*}*/
        /*}*/

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url({{asset('assets/vendor/pdf-flipbook-master/pics/loader.gif')}}) center no-repeat #fff;
        }
    </style>
    <!-- Page loading scripts-->
    <script>
        (function () {
            window.onload = function () {
                var preloader = document.querySelector('.cs-page-loading');
                preloader.classList.remove('active');
                setTimeout(function () {
                    preloader.remove();
                }, 2000);
            };
        })();

    </script>

    <link rel="stylesheet" media="screen" href="{{asset('css/theme.css')}}">
    @yield('head')
</head>
<!-- Body-->
<body tabindex="1" class="loadingInProgress">
<div class="se-pre-con"></div>
{{--<!-- Page loading spinner-->--}}
{{--<div class="cs-page-loading active">--}}
{{--<div class="cs-page-loading-inner">--}}
{{--<div class="cs-page-spinner"></div>--}}
{{--<span>Loading...</span>--}}
{{--</div>--}}
{{--</div>--}}
{{--<main class="cs-page-wrapper">--}}
@include('layouts.aplicacion.header')

{{--<div class="cs-sidebar-enabled">--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<!-- Sidebar-->--}}
            {{--<div class="cs-sidebar col-lg-3 pt-lg-5">--}}
            {{--<div class="cs-offcanvas-collapse" id="shop-sidebar">--}}
            {{--@include('layouts.aplicacion.includes.sidebar_left')--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-12 cs-content py-4 mb-2 mb-sm-0 pb-sm-5">--}}
                @yield('breadcrum')
                {{--@include('includes.session-flash-status')--}}
                {{--@if(!($perfil = \Illuminate\Support\Facades\Auth::user()->perfil))--}}
                {{--<div class="alert alert-danger" role="alert">--}}
                {{--<i class="fe-x-circle font-size-xl mt-n1 mr-3"></i>--}}
                {{--Estimado usuario: Por favor ingresar los datos en su <a href="{{route('app.user.perfil')}}"--}}
                {{--class="alert-link"><strong>Perfil</strong>--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--@endif--}}
                @yield('content')
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--</main>--}}

{{--@include('layouts.aplicacion.footer')--}}
{{--<!-- Sidebar toggle button-->--}}
{{--<button class="btn btn-primary btn-sm cs-sidebar-toggle" type="button" data-toggle="offcanvas"--}}
{{--data-offcanvas-id="shop-sidebar"><i class="fe-filter font-size-base mr-2"></i>Filters--}}
{{--</button>--}}
{{--<!-- Back to top button-->--}}
{{--<a class="btn-scroll-top" href="#top" data-scroll>--}}
{{--<span class="btn-scroll-top-tooltip text-muted font-size-sm mr-2">Top</span>--}}
{{--<i class="btn-scroll-top-icon fe-arrow-up"> </i>--}}
{{--</a>--}}
{{--<!-- Vendor scrits: js libraries and plugins-->--}}
{{--<script src="vendor/jquery/dist/jquery.slim.min.js"></script>--}}
{{--<script src="vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="vendor/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>--}}
{{--<script src="vendor/simplebar/dist/simplebar.min.js"></script>--}}
{{--<script src="vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>--}}
{{--<script src="vendor/nouislider/distribute/nouislider.min.js"></script>--}}
{{--<!-- Main theme script-->--}}

<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/helpers.js')}}"></script>
<script src="{{ asset('js/aplicacion.js') }}"></script>
@yield('scripts')
{{--@yield('scripts-filter')--}}
</body>
</html>