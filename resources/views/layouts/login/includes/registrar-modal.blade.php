<div class="cs-view" id="modal-signup-view">
    <div class="modal-header border-0 bg-dark px-4">
        <h4 class="modal-title text-light">Reg&iacute;strate</h4>
        <button class="close text-light" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body px-4">
        <p class="font-size-ms text-muted">
            El registro demora menos de un minuto pero le brinda control total sobre su contenido.
        </p>
        <form method="POST" id="form-ajax-register" class="needs-validation" action="{{ route('auth.store') }}" novalidate>
            @csrf
            <div class="form-group">
                <input id="email-register" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                       value="{{ old('email') }}" placeholder="Email" autocomplete="email" required>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="cs-password-toggle form-group">
                <input id="password-register" name="password" class="form-control @error('password') is-invalid @enderror"
                       type="password"
                       placeholder="Password" required>
                <label class="cs-password-toggle-btn">
                    <input class="custom-control-input" type="checkbox">
                    <i class="fe-eye cs-password-toggle-indicator"></i>
                    <span class="sr-only">Show password</span>
                </label>
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="cs-password-toggle form-group">
                <input id="password-confirm" class="form-control" type="password" placeholder="Confirm password"
                       name="password_confirmation" required>
                <label class="cs-password-toggle-btn">
                    <input class="custom-control-input" type="checkbox">
                    <i class="fe-eye cs-password-toggle-indicator"></i>
                    <span class="sr-only">Show password</span>
                </label>
            </div>
            <button class="btn btn-primary btn-block" type="submit">
                {{__('Registrar')}}
            </button>
            <p class="font-size-sm pt-3 mb-0">
                ¿Ya tienes una cuenta?
                <a href='{{route('login')}}' class='font-weight-medium'>
                    Ingresar
                </a>
            </p>
        </form>
    </div>
</div>