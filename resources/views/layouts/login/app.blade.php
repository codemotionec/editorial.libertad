<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Editorial Libertad @yield('page_title')</title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Around - Multipurpose Bootstrap Template">
    <meta name="keywords"
          content="bootstrap, business, consulting, coworking space, services, creative agency, dashboard, e-commerce, mobile app showcase, multipurpose, product landing, shop, software, ui kit, web studio, landing, html5, css3, javascript, gallery, slider, touch, creative">
    <meta name="author" content="Createx Studio">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('site.webmanifest')}}">
    <link rel="mask-icon" color="#5bbad5" href="{{asset('img/safari-pinned-tab.svg')}}">
    <meta name="msapplication-TileColor" content="#766df4">
    <meta name="theme-color" content="#ffffff">
    <!-- Page loading styles-->
    <style>
        .cs-page-loading {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%;
            -webkit-transition: all .4s .2s ease-in-out;
            transition: all .4s .2s ease-in-out;
            background-color: #fff;
            opacity: 0;
            visibility: hidden;
            z-index: 9999;
        }

        .cs-page-loading.active {
            opacity: 1;
            visibility: visible;
        }

        .cs-page-loading-inner {
            position: absolute;
            top: 50%;
            left: 0;
            width: 100%;
            text-align: center;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            -webkit-transition: opacity .2s ease-in-out;
            transition: opacity .2s ease-in-out;
            opacity: 0;
        }

        .cs-page-loading.active > .cs-page-loading-inner {
            opacity: 1;
        }

        .cs-page-loading-inner > span {
            display: block;
            font-family: 'Inter', sans-serif;
            font-size: 1rem;
            font-weight: normal;
            color: #737491;
        }

        .cs-page-spinner {
            display: inline-block;
            width: 2.75rem;
            height: 2.75rem;
            margin-bottom: .75rem;
            vertical-align: text-bottom;
            border: .15em solid #766df4;
            border-right-color: transparent;
            border-radius: 50%;
            -webkit-animation: spinner .75s linear infinite;
            animation: spinner .75s linear infinite;
        }

        @-webkit-keyframes spinner {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes spinner {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

    </style>
    <!-- Page loading scripts-->
    <script>
        (function () {
            window.onload = function () {
                var preloader = document.querySelector('.cs-page-loading');
                preloader.classList.remove('active');
                setTimeout(function () {
                    preloader.remove();
                }, 2000);
            };
        })();

    </script>
    <!-- Vendor Styles-->
{{--<link rel="stylesheet" media="screen" href="vendor/simplebar/dist/simplebar.min.css"/>--}}
<!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="{{asset('css/theme.css')}}">
</head>
<!-- Body-->
<body>
<!-- Page loading spinner-->
<div class="cs-page-loading active">
    <div class="cs-page-loading-inner">
        <div class="cs-page-spinner"></div>
        <span>Loading...</span>
    </div>
</div>
<main class="cs-page-wrapper d-flex flex-column">
    <!-- Sign In Modal-->
    <div class="modal fade" id="modal-signin" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content border-0">

                @include('layouts.login.includes.login-modal')

                @include('layouts.login.includes.registrar-modal')

                {{--<div class="modal-body text-center px-4 pt-2 pb-4">--}}
                {{--<hr>--}}
                {{--<p class="font-size-sm font-weight-medium text-heading pt-4">Or sign in with</p>--}}
                {{--<a class="social-btn sb-facebook sb-lg mx-1 mb-2" href="#"><i class="fe-facebook"></i></a>--}}
                {{--<a class="social-btn sb-twitter sb-lg mx-1 mb-2" href="#"><i class="fe-twitter"></i></a>--}}
                {{--<a class="social-btn sb-instagram sb-lg mx-1 mb-2" href="#"><i class="fe-instagram"></i></a>--}}
                {{--<a class="social-btn sb-google sb-lg mx-1 mb-2" href="#"><i class="fe-google"></i></a>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <!-- Navbar (Floating dark)-->
    <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
    @include('layouts.login.header')

    <div class="d-none d-md-block position-absolute w-50 h-100 bg-size-cover"
         style="top: 0; right: 0; background-image: url({{asset('img/login-font.jpg')}});">

    </div>
    <!-- Actual content-->
    <section class="container d-flex align-items-center pt-7 pb-3 pb-md-4" style="flex: 1 0 auto;">
        <div class="w-100 pt-3">
            <div class="row">
                <div class="col-lg-4 col-md-6 offset-lg-1">
                    <!-- Sign in view-->
                    @include('includes.session-flash-status')
                    @yield('content')

                    {{--<div class="border-top text-center mt-4 pt-4">--}}
                    {{--<p class="font-size-sm font-weight-medium text-heading">--}}
                    {{--Or sign in with--}}
                    {{--</p>--}}
                    {{--<a class="social-btn sb-facebook sb-outline sb-lg mx-1 mb-2" href="#">--}}
                    {{--<i class="fe-facebook"></i>--}}
                    {{--</a>--}}
                    {{--<a class="social-btn sb-twitter sb-outline sb-lg mx-1 mb-2" href="#">--}}
                    {{--<i class="fe-twitter"></i>--}}
                    {{--</a>--}}
                    {{--<a class="social-btn sb-instagram sb-outline sb-lg mx-1 mb-2" href="#">--}}
                    {{--<i class="fe-instagram"></i>--}}
                    {{--</a>--}}
                    {{--<a class="social-btn sb-google sb-outline sb-lg mx-1 mb-2" href="#">--}}
                    {{--<i class="fe-google"></i>--}}
                    {{--</a>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </section>
</main>
<footer class="cs-footer py-4">
    <div class="container font-size-sm mb-0 py-3">
        <span class="text-muted mr-1">© All rights reserved. Made by</span>
        <a class="nav-link-style" href="https://createx.studio/" target="_blank" rel="noopener">Codemotion</a>
    </div>
</footer>
<!-- Back to top button-->
<a class="btn-scroll-top" href="#top" data-scroll>
    <span class="btn-scroll-top-tooltip text-muted font-size-sm mr-2">Top</span>
    <i class="btn-scroll-top-icon fe-arrow-up"> </i>
</a>
<!-- Vendor scrits: js libraries and plugins-->
{{--<script src="vendor/jquery/dist/jquery.slim.min.js"></script>--}}
{{--<script src="vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="vendor/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>--}}
{{--<script src="vendor/simplebar/dist/simplebar.min.js"></script>--}}
{{--<script src="vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>--}}
<!-- Main theme script-->
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/helpers.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('submit', '#form-ajax-register', function (e) {
            var data = $(this).serialize();
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: data,
                success: function (response) {
                    window.responseAjax(response)
                },
                error: function (err) {
                    window.errorAjax(err);
                }
            });
        });
    });
</script>
</body>
</html>