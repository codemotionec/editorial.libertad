<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class AsignaturaDocente extends Model
{
    use SoftDeletes;
    protected $table = 'asignatura_docente';
    public static $search = null;
    public static $paginate = 10;
    protected $fillable = ['docente_id', 'asignatura_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function docente()
    {
        return $this->belongsTo(Docente::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function asignatura()
    {
        return $this->belongsTo(Asignatura::class);
    }

    /**
     * @return Builder
     */
    public static function builderAsignaturaDocente()
    {
        $query = AsignaturaDocente::orderBy('asignatura_docente.created_at', request('created_at', 'DESC'));

        if (self::$search) {
            //$query->orWhere('descripcion', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    public static function obtenerAsignaturaDocenteLibroDocenteOne($libro, $docente)
    {
        $rs = self::builderAsignaturaDocente();
        $rs->where('asignatura_id', (is_object($libro)) ? $libro->asignatura_id : (Libro::find($libro))->asignatura_id)
            ->where('docente_id', (is_object($docente)) ? $docente->id : $docente);

        return $rs->first() ?? [];
    }

    public static function obtenerAsignaturaDocenteEstudianteAll($estudiante, $libro)
    {
        $libro = (is_object($libro)) ? $libro : Libro::find($libro);
        $estudiante = (is_object($estudiante)) ? $estudiante : Estudiante::find($estudiante);

        $rs = self::builderAsignaturaDocente();
        $rs->select('asignatura_docente.*')
            ->join('docente', 'docente.id', '=', 'asignatura_docente.docente_id')
            ->where('docente.aula_id', $estudiante->aula_id)
            ->where('asignatura_docente.asignatura_id', $libro->asignatura_id);

        return $rs->get() ?? [];

    }
}
