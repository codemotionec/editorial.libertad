<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Roles extends Model
{
    use SoftDeletes;

    protected $table = 'roles';

    /**
     * @return mixed
     */
    public static function obtenerRolesRegistro()
    {
        //return Roles::whereNotIn(['key', ['ADMIN', 'WEB']]);
        return Roles::orderby('name')->get();
    }

    /**
     * @return mixed
     */
    public static function obtenerRolAdmin()
    {
        return Roles::where('key', 'ADMIN')->first();
    }

    /**
     * @return mixed
     */
    public static function obtenerRolEstudiante()
    {
        return Roles::where('key', 'E')->first();
    }

    /**
     * @return mixed
     */
    public static function obtenerRolDocente()
    {
        return Roles::where('key', 'D')->first();
    }

    /**
     * @return mixed
     */
    public static function obtenerRolRepresentante()
    {
        return Roles::where('key', 'R')->first();
    }
}
