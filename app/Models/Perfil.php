<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class Perfil extends Model
{
    public static $search = null;
    protected $fillable = [
        'nombre',
        'apellido',
        'abreviatura_id',
        'cedula_identidad',
        'direccion',
        'telefono',
        'celular',
        'fecha_nacimiento',
        'imagen'
    ];

    protected $table = 'perfil';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function abreviatura()
    {
        return $this->belongsTo(Abreviatura::class);
    }

    public function getFullNameAttribute()
    {
        return $this->nombre . ' ' . $this->apellido;
    }

    /**
     * @return Builder
     */
    public static function builderPerfil()
    {
        $query = Perfil::orderBy('created_at', request('created_at', 'DESC'));

        if (self::$search) {
//            $query->orWhere('nombre', 'like', '%' . self::$search . '%')
//                ->orWhere('codigo', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @param $cedula_identidad
     * @return Model|Builder|null|object
     */
    public static function obtenerPerfilCedulaIdentidad($cedula_identidad)
    {
        return self::builderPerfil()->where('cedula_identidad', $cedula_identidad)->first();
    }
}
