<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class Desarrollo extends Model
{
    use SoftDeletes;

    public static $paginate = 10;
    public static $search = null;

    protected $fillable = ['tarea_id', 'descripcion', 'imagen'];
    protected $table = 'desarrollo';

    public function tarea()
    {
        return $this->belongsTo(Tarea::class);
    }

    /**
     * @return Builder
     */
    public static function builderDesarrollo()
    {
        $query = Desarrollo::orderBy('created_at', request('created_at', 'ASC'));

        if (self::$search) {
//            $query->orWhere('resumen', 'like', '%' . self::$search . '%')
//                ->orWhere('anio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_inicio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_fin', 'like', '%' . self::$search . '%')
//            ;
        }

        return $query;
    }

    /**
     * @param integer $tarea_id
     * @return array|\Illuminate\Support\Collection
     */
    public static function obtenerDesarrollosTareaAll($tarea_id)
    {
        $query = self::builderDesarrollo()
            ->where('tarea_id', is_object($tarea_id) ? $tarea_id->id : $tarea_id);

        return $query->get() ?? [];

    }
}
