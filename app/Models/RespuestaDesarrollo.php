<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class RespuestaDesarrollo extends Model
{
    use SoftDeletes;

    public static $paginate = 10;
    public static $search = null;
    protected $table = 'respuesta_desarrollo';
    protected $fillable = [
        'desarrollo_id',
        'tipo_respuesta_id',
    ];

    public function tipoRespuesta()
    {
        return $this->belongsTo(TipoRespuesta::class);
    }

    public function desarrollo()
    {
        return $this->belongsTo(Desarrollo::class);
    }

    /**
     * @return Builder
     */
    public static function builderRespuestaDesarrollo()
    {
        $query = RespuestaDesarrollo::orderBy('created_at', request('created_at', 'ASC'));

        if (self::$search) {
//            $query->orWhere('resumen', 'like', '%' . self::$search . '%')
//                ->orWhere('anio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_inicio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_fin', 'like', '%' . self::$search . '%')
//            ;
        }

        return $query;
    }

    public static function obtenerRespuestaDesarrolloDesarrolloAll($desarrollo_id)
    {
        $query = self::builderRespuestaDesarrollo();
        if (is_array($desarrollo_id)) {
            $query->whereIn('desarrollo_id', $desarrollo_id);
        } else {
            $query->where('desarrollo_id', is_object($desarrollo_id) ? $desarrollo_id->id : $desarrollo_id);
        }

        return $query->get() ?? [];
    }
}
