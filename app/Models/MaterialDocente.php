<?php

namespace App\Models;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;

class MaterialDocente extends Model
{
    public static $pagination = 10;
    public static $search = null;

    protected $table = 'material_docente';
    protected $fillable = ['libro_id', 'docente_id', 'estado_registro_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function libro()
    {
        return $this->belongsTo(Libro::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estadoRegistro()
    {
        return $this->belongsTo(EstadoRegistro::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function docente()
    {
        return $this->belongsTo(Docente::class);
    }

    /**
     * @return Builder
     */
    public static function builderMaterialDocente()
    {
        $query = MaterialDocente::orderBy('material_docente.created_at', request('created_at', 'DESC'));

        if (self::$search) {
//            $query->orWhere('nombre', 'like', '%' . self::$search . '%')
//                ->orWhere('anio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_inicio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_fin', 'like', '%' . self::$search . '%')
//            ;
        }

        return $query;
    }

    /**
     * @param $user_id
     * @return array|Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function obtenerMaterialDocenteUserAll($user_id)
    {
        $rs = self::builderMaterialDocente();
        $rs->select('material_docente.*');
        $rs->join('docente', 'docente.id', '=', 'material_docente.docente_id')
            ->join('users', 'users.id', '=', 'docente.user_id')
            ->where('users.id', (is_object($user_id)) ? $user_id->id : $user_id);

        return $rs->get() ?? [];
    }


    /**
     * @param Libro|integer|null $libro
     * @param Estudiante|integer $estudiante
     * @return array|Model|Builder|null|object
     */
    public static function obtenerMaterialDocenteEstudianteOne($libro = null, $estudiante, $all = false)
    {
        $libro = (is_object($libro)) ? $libro : Libro::find($libro);
        $estudiante = (is_object($estudiante)) ? $estudiante : Estudiante::find($estudiante);

        $rs = self::builderMaterialDocente();
        $rs->select('material_docente.*')
            ->join('docente', 'docente.id', '=', 'material_docente.docente_id')
            ->join('libro', 'libro.id', '=', 'material_docente.libro_id')
            ->where('docente.aula_id', $estudiante->aula_id)
            ->where('docente.anio_lectivo_id', $estudiante->anio_lectivo_id);

        if ($libro) {
            $rs->where('material_docente.libro_id', $libro->id);
        }

        if ($all) {
            return $rs->get() ?? [];
        }

        return $rs->first() ?? [];
    }

    /**
     * @param Libro|integer $libro
     * @param Estudiante|integer $estudiante
     * @return array|Model|Builder|null|object
     */
    public static function obtenerMaterialDocenteLibroOne($docente, $libro)
    {
        $aprobado = EstadoRegistro::obtenerEstadoRegistroAprobado();
        $revision = EstadoRegistro::obtenerEstadoRegistroRevision();

        $rs = self::builderMaterialDocente();
        $rs->select('material_docente.*')
            ->join('docente', 'docente.id', '=', 'material_docente.docente_id')
            ->where('docente.id', (is_object($docente)) ? $docente->id : $docente)
            ->where('libro_id', (is_object($libro)) ? $libro->id : $libro)
            ->whereIn('material_docente.estado_registro_id', [$aprobado->id, $revision->id]);

        return $rs->first() ?? [];
    }


}
