<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AsignaturaDocenteEstudiante extends Model
{
    protected $table = 'asignatura_docente_estudiante';
    protected $fillable = ['estudiante_id', 'asignatura_docente_id', 'estado_registro_id'];
}
