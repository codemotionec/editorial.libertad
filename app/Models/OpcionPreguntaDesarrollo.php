<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OpcionPreguntaDesarrollo extends Model
{
    use SoftDeletes;
    public static $paginate = 10;
    public static $search = null;
    protected $fillable = ['pregunta_desarrollo_id', 'tipo_calificacion_id', 'descripcion', 'imagen', 'respuesta'];
    protected $table = 'opcion_pregunta_desarrollo';

    public function preguntaDesarrollo()
    {
        return $this->belongsTo(PreguntaDesarrollo::class);
    }

    public function tipoCalificacion()
    {
        return $this->belongsTo(TipoCalificacion::class);
    }

    public static function builderOpcionPreguntaDesarrollo()
    {
        $query = OpcionPreguntaDesarrollo::orderBy('created_at', request('created_at', 'ASC'));

        if (self::$search) {
//            $query->orWhere('resumen', 'like', '%' . self::$search . '%')
//                ->orWhere('anio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_inicio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_fin', 'like', '%' . self::$search . '%')
//            ;
        }

        return $query;
    }

    public static function obtenerOpcionPreguntaDesarrolloPreguntaDesarrolloAll($pregunta_desarrollo_id)
    {
        $query = self::builderOpcionPreguntaDesarrollo();
        $query->where('pregunta_desarrollo_id', $pregunta_desarrollo_id);

        return $query->get() ?? [];
    }
}
