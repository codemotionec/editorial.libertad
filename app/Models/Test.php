<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model
{
    use SoftDeletes;

    public static $paginate = 10;
    public static $search = null;
    protected $table = 'test';
    protected $fillable = [
        'segmento_libro_id',
        'tipo_calificacion_id',
        'asignacion_docente_id',
        'tarea_id',
        'descripcion',
        'fecha_ejecucion',
        'fecha_terminacion'
    ];

    public function segmentoLibro()
    {
        return $this->belongsTo(SegmentoLibro::class);
    }

    public function tipoCalificacion()
    {
        return $this->belongsTo(TipoCalificacion::class);
    }

    public function tarea()
    {
        return $this->belongsTo(Tarea::class);
    }

    public static function builderTest()
    {
        $query = Test::orderBy('created_at', request('created_at', 'ASC'));

        if (self::$search) {
//            $query->orWhere('resumen', 'like', '%' . self::$search . '%')
//                ->orWhere('anio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_inicio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_fin', 'like', '%' . self::$search . '%')
//            ;
        }

        return $query;
    }

    public static function obtenerTestSegmentoLibroAll($segmento_libro_id)
    {
        $query = self::builderTest();
        $query->where('segmento_libro_id', $segmento_libro_id);

        return $query->get() ?? [];
    }

}
