<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class TipoRespuesta extends Model
{
    use SoftDeletes;
    public static $paginate = 10;
    public static $search = null;

    protected $table = 'tipo_respuesta';

    protected $fillable = [
        'descripcion',
        'codigo',
    ];

    /**
     * @return Builder
     */
    public static function builderTipoRespuesta()
    {
        $query = TipoRespuesta::orderBy('created_at', request('created_at', 'ASC'));

        if (self::$search) {
//            $query->where('institucion.nombre', 'like', '%' . self::$search . '%')
//                ->orwhere('paralelo.descripcion', 'like', '%' . self::$search . '%')
//                ->orwhere('anio_lectivo.descripcion', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public static function obtenerTipoRespuestaAll()
    {
        return self::builderTipoRespuesta()->get() ?? [];
    }
}
