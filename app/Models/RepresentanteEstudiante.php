<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class RepresentanteEstudiante extends Model
{
    public static $search = null;
    public static $paginate = 10;
    protected $table = 'representante_estudiante';
    protected $fillable = ['user_id', 'estudiante_id', 'estado_registro_id', 'user_autoriza_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estadoRegistro()
    {
        return $this->belongsTo(EstadoRegistro::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estudiante()
    {
        return $this->belongsTo(Estudiante::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getNombreRepresentanteAttribute()
    {
        if ($this->user->perfil) {
            return $this->user->perfil->nombre . ' ' . $this->user->perfil->apellido;
        }

        return false;
    }

    /**
     * @return Builder
     */
    public static function builderRepresentanteEstudiante()
    {
        $query = RepresentanteEstudiante::orderBy('created_at', request('created_at', 'ASC'));

        if (self::$search) {
//            $query->orWhere('resumen', 'like', '%' . self::$search . '%')
//                ->orWhere('anio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_inicio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_fin', 'like', '%' . self::$search . '%')
//            ;
        }

        return $query;
    }

    public static function obtenerRepresentanteEstudianteUser($user)
    {
        $rs = self::builderRepresentanteEstudiante()
            ->where('user_id', (is_object($user)) ? $user->id : $user);

        return $rs->paginate(self::$paginate) ?? [];

    }

    public static function obtenerRepresentanteEstudiante($user, $estudiante)
    {
        $rs = self::builderRepresentanteEstudiante();
        $rs->where('user_id', (is_object($user)) ? $user->id : $user)
            ->where('estudiante_id', (is_object($estudiante)) ? $estudiante->id : $estudiante);

        return $rs->first() ?? [];
    }
}
