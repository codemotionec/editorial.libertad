<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class MaterialEstudiante extends Model
{

    public static $paginate = 10;
    public static $search = null;
    protected $table = 'material_estudiante';
    protected $fillable = [
        'material_docente_id',
        'estudiante_id',
        'estado_registro_id',
        'user_id',
        'codigo',
        'user_autoriza_id'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userAutoriza()
    {
        return $this->belongsTo(User::class, 'user_autoriza_id', 'id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estudiante()
    {
        return $this->belongsTo(Estudiante::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function materialDocente()
    {
        return $this->belongsTo(MaterialDocente::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estadoRegistro()
    {
        return $this->belongsTo(EstadoRegistro::class);
    }

    /**
     * @return mixed
     */
    public function getTituloLibroAttribute()
    {
        return $this->materialDocente->libro->titulo;
    }

    /**
     * @return mixed
     */
    public function getPortadaLibroAttribute()
    {
        return $this->materialDocente->libro->portada;
    }

    /**
     * @return mixed
     */
    public function getAsignaturaLibroAttribute()
    {
        return $this->materialDocente->libro->asignatura->nombre;
    }

    /**
     * @return mixed
     */
    public function getEstadoRegistroAnioLectivoAttribute()
    {
        return $this->estudiante->anioLectivo->estadoRegistro;
    }

    /**
     * @return mixed
     */
    public function getNombreEstudianteAttribute()
    {
        return $this->estudiante->nombre_estudiante;
    }


    /**
     * @return mixed
     */
    public function getCedulaEstudianteAttribute()
    {
        return $this->estudiante->cedula_estudiante;
    }


    /**
     * @return Builder
     */
    public static function builderMaterialEstudiante()
    {
        $query = MaterialEstudiante::orderBy('material_estudiante.created_at', request('created_at', 'DESC'));

        if (self::$search) {
            //$query->orWhere('descripcion', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @param User|integer $user
     * @return array
     */
    public static function obtenerMaterialEstudianteUser($user, $aprobados = false)
    {
        $rs = self::builderMaterialEstudiante();
        $rs->select('material_estudiante.*')
            ->join('estudiante', 'estudiante.id', '=', 'material_estudiante.estudiante_id')
            ->where('estudiante.user_id', (is_object($user)) ? $user->id : $user);

        if ($aprobados) {
            $rs->where('material_estudiante.estado_registro_id', ($estado = EstadoRegistro::obtenerEstadoRegistroAprobado()) ? $estado->id : null);
        }

        return $rs->get() ?? [];
    }

    /**
     * @param User|integer $user
     * @return array
     */
    public static function obtenerMaterialEstudianteMaterialDocenteEstudiante($materialDocente, $estudiante)
    {
        $aprobado = EstadoRegistro::obtenerEstadoRegistroAprobado();
        $revision = EstadoRegistro::obtenerEstadoRegistroRevision();

        $rs = self::builderMaterialEstudiante();
        $rs->where('material_docente_id', (is_object($materialDocente)) ? $materialDocente->id : $materialDocente)
            ->where('estudiante_id', (is_object($estudiante)) ? $estudiante->id : $estudiante)
            ->whereIn('estado_registro_id', [$aprobado->id, $revision->id]);

        return $rs->first() ?? [];
    }

    /**
     * @param User|integer $user
     * @return array
     */
    public static function obtenerMaterialEstudiantePaginate()
    {
        $rs = self::builderMaterialEstudiante();
        $rs->select('material_estudiante.*')
            ->join('estudiante', 'estudiante.id', '=', 'material_estudiante.estudiante_id')//            ->where('estudiante.user_id', $user->id)
        ;

        return $rs->paginate(self::$paginate) ?? [];
    }

    /**
     * @param User|integer $user
     * @return array
     */
    public static function obtenerMaterialEstudianteMaterialDocente($materialDocente)
    {
        $rs = self::builderMaterialEstudiante();
        $rs->select('material_estudiante.*')
            ->join('estudiante', 'estudiante.id', '=', 'material_estudiante.estudiante_id')
            ->where('material_docente_id', is_object($materialDocente) ? $materialDocente->id : $materialDocente);


        return $rs->get() ?? [];
    }
}
