<?php

namespace App\Models;

use Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class Aula extends Model
{
    public $nombreCompleto = '';
    use SoftDeletes;

    public static $paginate = 10;
    public static $search = null;
    public static $filter_canton_id = null;
    public static $filter_institucion_id = null;

    protected $table = 'aula';
    protected $fillable = ['institucion_id', 'curso_id', 'paralelo'];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        if (request('filter_canton_id')) {
            self::$filter_canton_id = request('filter_canton_id');
        }

        if (request('filter_institucion_id')) {
            self::$filter_institucion_id = request('filter_institucion_id');
        }

        if (request('search')) {
            self::$search = request('search');
        }
    }

    public function institucion()
    {
        return $this->belongsTo(Institucion::class);
    }

    public function curso()
    {
        return $this->belongsTo(Curso::class);
    }

    /**
     * @return mixed
     */
    public function getNombreInstitucionAttribute()
    {
        return $this->institucion->nombre;
    }

    public function getNombreCursoAttribute()
    {
        return "{$this->curso->nombre} ({$this->paralelo})";
    }

    /**
     * @return Builder
     */
    public static function builderAula()
    {
        $query = Aula::join('institucion', 'institucion.id', '=', 'aula.institucion_id')
            ->join('curso', 'curso.id', '=', 'aula.curso_id');

        if (self::$search) {

            $query->where(function ($query) {
                $query->where('curso.nombre', 'like', '%' . self::$search . '%')
                    ->orwhere('aula.paralelo', 'like', '%' . self::$search . '%');
            });
        }

        if (self::$filter_canton_id) {
            $query->where('institucion.canton_id', self::$filter_canton_id);
        }

        if (self::$filter_institucion_id) {
            $query->where('aula.institucion_id', self::$filter_institucion_id);
        }

        // dd(self::$filter_canton_id, self::$filter_institucion_id, $query->toSql());

        return $query;
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public static function obtenerAulasAll()
    {
        return self::builderAula()->get() ?? [];
    }

    /**
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerAulasPaginateAll()
    {
        return self::builderAula()->paginate(self::$paginate) ?? [];
    }

    /**
     * @param integer $institucion_id
     * @param integer $paralelo_id
     * @param integer $anio_lectivo_id
     * @return array|Model|Builder|null|object
     */
    public static function obtenerAulaInstitucionOne($institucion_id, $curso_id, $paralelo)
    {
        $query = self::builderAula();
        $query->select('aula.*')
            ->where('institucion_id', $institucion_id)
            ->where('curso_id', $curso_id)
            ->where('paralelo', $paralelo);

        return $query->first() ?? [];
    }

    /**
     * @param Institucion|integer $institucion
     * @return array|\Illuminate\Support\Collection
     */
    public static function obtenerAulasPorInstitucion($institucion)
    {
        $query = self::builderAula();
        $query->select('aula.*')
            ->where('institucion_id', (is_object($institucion)) ? $institucion->id : $institucion);

        return $query->get() ?? [];
    }
}
