<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PreguntaDesarrollo extends Model
{
    use SoftDeletes;
    public static $paginate = 10;
    public static $search = null;
    protected $fillable = ['test_id', 'tipo_pregunta_id', 'pregunta'];
    protected $table = 'pregunta_desarrollo';


    public function test()
    {
        return $this->belongsTo(Test::class);
    }

    public function tipoPregunta()
    {
        return $this->belongsTo(TipoPregunta::class);
    }

    public static function builderPreguntaDesarrollo()
    {
        $query = PreguntaDesarrollo::orderBy('created_at', request('created_at', 'ASC'));

        if (self::$search) {
//            $query->orWhere('resumen', 'like', '%' . self::$search . '%')
//                ->orWhere('anio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_inicio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_fin', 'like', '%' . self::$search . '%')
//            ;
        }

        return $query;
    }

    public static function obtenerPreguntaDesarrolloTestAll($test_id)
    {
        $query = self::builderPreguntaDesarrollo();
        $query->where('test_id', $test_id);

        return $query->get() ?? [];
    }
}
