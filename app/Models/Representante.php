<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class Representante extends Model
{
    use SoftDeletes;
    protected $table = 'representante';
    public static $search = null;
    public static $paginate = 10;
    protected $fillable = ['estado_registro_id', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estadoRegistro()
    {
        return $this->belongsTo(EstadoRegistro::class, 'estado_registro_id', 'id');
    }

    /**
     * @return Builder
     */
    public static function builderRepresentante()
    {
        $query = Representante::orderBy('created_at', request('created_at', 'DESC'));

        if (self::$search) {
            //$query->orWhere('descripcion', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function obtenerRepresentanteUser($user_id)
    {
        $query = self::builderRepresentante();
        $query->where('user_id', $user_id);

        return $query->first() ?? [];

    }

}
