<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class Libro extends Model
{
    use SoftDeletes;

    public static $paginate = 10;
    public static $search = null;
    protected $table = 'libro';
    protected $fillable = [
        'titulo',
        'resumen',
        'autor',
        'editorial',
        'edicion',
        'contenido',
        'indice_contenidos',
        'anio_lectivo_id',
        'asignatura_id',
        'curso_id',
        'portada',
        'anio_registro'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function curso()
    {
        return $this->belongsTo(Curso::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function asignatura()
    {
        return $this->belongsTo(Asignatura::class, 'asignatura_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function segmentoLibros()
    {
        return $this->hasMany(SegmentoLibro::class, 'libro_id', 'id');
    }

    public function getNombreLibroAttribute()
    {
        return "{$this->asignatura->nombre} / {$this->titulo} ({$this->curso->nombre})";
    }


    public function getAsignaturaNombreAttribute()
    {
        if($this->asignatura){
            return $this->asignatura->nombre;
        }

        return false;
    }

    /**
     * @return Builder
     */
    public static function builderLibro()
    {
        $query = Libro::orderBy('titulo', request('titulo', 'DESC'))
            ->orderBy('created_at', request('created_at', 'DESC'));

        if (self::$search) {
            $query->orWhere('titulo', 'like', '%' . self::$search . '%')
//                ->orWhere('anio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_inicio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_fin', 'like', '%' . self::$search . '%')
            ;
        }

        return $query;
    }

    /**
     * @return AnioLectivo[]|array
     */
    public static function obtenerLibroAll()
    {
        return self::builderLibro()->get() ?? [];
    }

    /**
     * @return AnioLectivo[]|array
     */
    public static function obtenerLibroCursoAll($curso_id)
    {
        $rs = self::builderLibro();
        $rs->where('curso_id', $curso_id);
        return $rs->get() ?? [];
    }

    /**
     * @param string|null $search
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerLibroPaginateAll()
    {
        return self::builderLibro()->paginate(self::$paginate) ?? [];
    }
}
