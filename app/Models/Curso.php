<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class Curso extends Model
{
    use SoftDeletes;

    protected $fillable = ['nombre', 'codigo', 'sub_nivel', 'orden'];

    public static $paginate = 10;
    public static $search = null;
    protected $table = 'curso';

    /**
     * @return Builder
     */
    public static function builderCurso()
    {
        $query = Curso::orderBy('orden', 'ASC')
            ->orderBy('nombre', 'ASC');

        if (self::$search) {
            $query->where(function ($query) {
                $query->where('nombre', 'like', '%' . self::$search . '%')
                    ->orWhere('sub_nivel', 'like', '%' . self::$search . '%')
                    ->orWhere('codigo', 'like', '%' . self::$search . '%');
            });
        }

        return $query;
    }

    /**
     * @return AnioLectivo[]|array
     */
    public static function obtenerCursoAll()
    {
        return self::builderCurso()->get() ?? [];
    }

    /**
     * @param string|null $search
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerCursoPaginateAll()
    {
        return self::builderCurso()->paginate(self::$paginate) ?? [];
    }


    /**
     * @param string|null $search
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerCursoCodigoOne($codigo)
    {
        return self::builderCurso()->where('codigo', $codigo)->first() ?? [];
    }

    /**
     * @param string|null $search
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerCursoNombreSubnivel($nombre, $subnivel)
    {
        $rs = self::builderCurso();
        $rs->where('nombre', $nombre)
            ->where('sub_nivel', $subnivel);

        //dd($rs->toSql(), $nombre, $subnivel);

        return $rs->first() ?? [];
    }
}
