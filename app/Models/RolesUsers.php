<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class RolesUsers extends Model
{
    use SoftDeletes;

    protected $table = 'roles_users';

    public static $search = null;

    protected $fillable = ['estado_registro_id', 'rol_id', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rol()
    {
        return $this->belongsTo(Roles::class, 'rol_id', 'id');
    }

    public function estadoRegistro()
    {
        return $this->belongsTo(EstadoRegistro::class, 'estado_registro_id', 'id');
    }

    /**
     * @return Builder
     */
    public static function builderRolUser()
    {
        $query = RolesUsers::orderBy('created_at', request('created_at', 'DESC'));

        if (self::$search) {
            //$query->orWhere('descripcion', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @param integer $user_id
     * @param integer $rol_id
     * @return array|Model|Builder|null|object
     */
    public static function obtenerRolUser($user_id, $rol_id)
    {
        $query = self::builderRolUser();
        $query->where('user_id', $user_id)
            ->where('rol_id', $rol_id);

        return $query->first() ?? [];
    }

    /**
     * @param integer|null $user_id
     * @param integer $rol_id
     * @return array|Model|Builder|null|object
     */
    public static function obtenerRolesUserAll($user_id = null)
    {
        $query = self::builderRolUser();

        if ($user_id) {
            $query->where('user_id', $user_id);
        }

        return $query->get() ?? [];
    }
}
