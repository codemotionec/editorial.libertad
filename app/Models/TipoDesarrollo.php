<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class TipoDesarrollo extends Model
{
    use SoftDeletes;

    public static $paginate = 10;
    public static $search = null;
    protected $table = 'tipo_desarrollo';
    protected $fillable = [
        'descripcion',
        'codigo',
    ];

    /**
     * @return Builder
     */
    public static function builderTipoDesarrollo()
    {
        $query = TipoDesarrollo::orderBy('created_at', request('created_at', 'ASC'));

        if (self::$search) {
//            $query->orWhere('descripcion', 'like', '%' . self::$search . '%')
//                ->orWhere('key', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public static function obtenerTipoDesarrolloAll()
    {
        $query = self::builderTipoDesarrollo();
        return $query->get() ?? [];
    }

}
