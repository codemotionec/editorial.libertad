<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class Docente extends Model
{
    use SoftDeletes;
    protected $table = 'docente';
    public static $search = null;
    public static $filter_canton_id = null;
    public static $filter_institucion_id = null;
    public static $paginate = 10;
    protected $fillable = ['anio_lectivo_id', 'estado_registro_id', 'user_id', 'aula_id', 'user_autoriza_id', 'representante_curso'];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        if (request('filter_canton_id')) {
            self::$filter_canton_id = request('filter_canton_id');
        }

        if (request('filter_institucion_id')) {
            self::$filter_institucion_id = request('filter_institucion_id');
        }

        if (request('search')) {
            self::$search = request('search');
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function userAutoriza()
    {
        return $this->belongsTo(User::class, 'user_autoriza_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estadoRegistro()
    {
        return $this->belongsTo(EstadoRegistro::class, 'estado_registro_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aula()
    {
        return $this->belongsTo(Aula::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function anioLectivo()
    {
        return $this->belongsTo(AnioLectivo::class);
    }

    public function getNombreDocenteAttribute()
    {
        if ($perfil = $this->user->perfil) {
            return $perfil->abreviatura->codigo . '. ' . $perfil->nombre . ' ' . $perfil->apellido;
        } else {
            return ucfirst($this->user->name);
        }
    }

    public function getNombreInstitucionAttribute()
    {
        return $this->aula->institucion->nombre;
    }

    public function getNombreAulaAttribute()
    {
        return $this->aula->curso->nombre . " ({$this->aula->paralelo})";
    }

    public function getNombreAutorizaAttribute()
    {
        if ($this->userAutoriza) {
            if ($perfil = $this->userAutoriza->perfil) {
                return "{$perfil->nombre} {$perfil->apellido}";
            }
            return ucfirst($this->userAutoriza->name);
        }

        return false;
    }

    public function getCedulaDocenteAttribute()
    {
        if ($this->user) {
            if ($perfil = $this->user->perfil) {
                return $perfil->cedula_identidad;
            }
        }

        return false;
    }

    /**
     * @return Builder
     */
    public static function builderDocente()
    {
        $query = Docente::select('docente.*')
            ->join('aula', 'aula.id', '=', 'docente.aula_id')
            ->join('institucion', 'institucion.id', '=', 'aula.institucion_id')
            ->join('users', 'users.id', '=', 'docente.user_id')
            ->join('perfil', 'perfil.id', '=', 'users.perfil_id');

        if (self::$search) {
            $query->where(function ($query) {
                $query->where('perfil.nombre', 'like', '%' . self::$search . '%')
                    ->orwhere('perfil.apellido', 'like', '%' . self::$search . '%');
            });
        }

        if (self::$filter_canton_id) {
            $query->where('institucion.canton_id', self::$filter_canton_id);
        }

        if (self::$filter_institucion_id) {
            $query->where('aula.institucion_id', self::$filter_institucion_id);
        }

        return $query;
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function obtenerDocenteUser($user_id)
    {
        $query = self::builderDocente();
        $query->where('user_id', $user_id);

        return $query->first() ?? [];
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function obtenerDocentesUserAll($user_id)
    {
        $query = self::builderDocente();
        $query->where('user_id', $user_id);

        return $query->get() ?? [];
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function obtenerDocentesAprobadosUserAll($user_id)
    {
        $query = self::builderDocente();
        $query->where('user_id', $user_id)
            ->where('estado_registro_id', ($estado = EstadoRegistro::obtenerEstadoRegistroAprobado()) ? $estado->id : null);

        return $query->get() ?? [];
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function obtenerDocenteUsuarioAulaAnioLectivo($user_id, $aula_id, $anio_lectivo_id)
    {
        $query = self::builderDocente();
        $query->where('user_id', $user_id)
            ->where('aula_id', $aula_id)
            ->where('anio_lectivo_id', $anio_lectivo_id);

        return $query->first() ?? [];
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public static function obtenerDocentesAll()
    {
        $query = self::builderDocente();
        return $query->get() ?? [];
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function obtenerDocentesUserRepresentantesCursoAll($user_id)
    {
        $query = self::builderDocente()
            ->where('user_id', (is_object($user_id)) ? $user_id->id : $user_id)
            ->where('representante_curso', 1)
            ->where('estado_registro_id', ($estado = EstadoRegistro::obtenerEstadoRegistroAprobado()) ? $estado->id : null);

        //dd($query->toSql(), $estado, $user_id);

        return $query->get() ?? [];
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function obtenerDocenteRepresentanteCursoAulaOne($aula_id)
    {
        $query = self::builderDocente()
            ->where('representante_curso', 1)
            ->where('aula_id', $aula_id)
            ->where('estado_registro_id', ($estado = EstadoRegistro::obtenerEstadoRegistroAprobado()) ? $estado->id : null);

//        dd($query->toSql(), $aula_id, $estado);
        return $query->first() ?? [];
    }
}
