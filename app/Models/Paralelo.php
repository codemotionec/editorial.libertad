<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class Paralelo extends Model
{
    use SoftDeletes;

    protected $fillable = ['descripcion', 'key'];

    public static $paginate = 10;
    public static $search = null;
    protected $table = 'paralelo';

    /**
     * @return Builder
     */
    public static function builderParalelo()
    {
        $query = Paralelo::orderBy('descripcion', request('descripcion', 'DESC'))
            ->orderBy('created_at', request('created_at', 'DESC'));

        if (self::$search) {
            $query->orWhere('descripcion', 'like', '%' . self::$search . '%')
                ->orWhere('key', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @return AnioLectivo[]|array
     */
    public static function obtenerParaleloAll()
    {
        return self::builderParalelo()->get() ?? [];
    }

    /**
     * @param string|null $search
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerParaleloPaginateAll()
    {
        return self::builderParalelo()->paginate(self::$paginate) ?? [];
    }
}
