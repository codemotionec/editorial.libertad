<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class Asignatura extends Model
{
    use SoftDeletes;

    protected $fillable = ['nombre', 'codigo'];

    public static $paginate = 10;
    public static $search = null;
    protected $table = 'asignatura';

    /**
     * @return Builder
     */
    public static function builderAsignatura()
    {
        $query = Asignatura::orderBy('nombre', request('nombre', 'DESC'))
            ->orderBy('created_at', request('created_at', 'DESC'));

        if (self::$search) {
            $query->orWhere('nombre', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @return Asignatura[]|array
     */
    public static function obtenerAsignaturaAll()
    {
        return self::builderAsignatura()->get() ?? [];
    }

    /**
     * @param string|null $search
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerAsignaturaPaginateAll()
    {
        return self::builderAsignatura()->paginate(self::$paginate) ?? [];
    }


    /**
     * @param string|null $search
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerAsignaturaCodigoOne($codigo)
    {
        return self::builderAsignatura()->where('codigo', $codigo)->first() ?? [];
    }
}
