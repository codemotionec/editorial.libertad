<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class Institucion extends Model
{
    use SoftDeletes;

    public static $paginate = 10;
    public static $search = null;
    public static $filter_canton_id = null;
    protected $table = 'institucion';

    protected $fillable = [
        'canton_id', 'parroquia_id', 'nombre', 'codigo', 'zona_inec', 'direccion', 'escolarizacion',
        'tipo_educacion', 'nivel_educacion', 'sostenimiento', 'regimen_escolar', 'jurisdiccion',
        'modalidad', 'jornada', 'imagen'
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        if (request('filter_canton_id')) {
            self::$filter_canton_id = request('filter_canton_id');
        }

        if (request('search')) {
            self::$search = request('search');
        }
    }

    public function canton()
    {
        return $this->belongsTo(Canton::class, 'canton_id', 'id');
    }

    public function parroquia()
    {
        return $this->belongsTo(Parroquia::class, 'parroquia_id', 'id');
    }

    public function getNombreCodigoAttribute()
    {
        return "{$this->codigo} - {$this->nombre}";
    }

    public function getNombreParroquiaAttribute()
    {
        if ($this->parroquia) {
            return $this->parroquia->nombre;
        }

        return null;
    }

    public function getNombreCantonAttribute()
    {
        if ($this->canton) {
            return $this->canton->nombre;
        }

        return null;
    }

    /**
     * @return Builder
     */
    public static function builderInstitucion()
    {
        $query = Institucion::orderBy('nombre', request('nombre', 'ASC'))
            ->orderBy('created_at', request('created_at', 'DESC'));

        if (self::$search) {
            $query->where(function ($query) {
                $query->where('nombre', 'like', '%' . self::$search . '%')
                    ->orWhere('codigo', 'like', '%' . self::$search . '%')
                    ->orWhere('nivel_educacion', 'like', '%' . self::$search . '%');
            });

        }

        if (self::$filter_canton_id) {
            $query->where('canton_id', self::$filter_canton_id);
        }

        return $query;
    }

    /**
     * @return AnioLectivo[]|array
     */
    public static function obtenerInstitucionAll()
    {
        return self::builderInstitucion()->get() ?? [];
    }

    /**
     * @param string|null $search
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerInstitucionPaginateAll()
    {
        return self::builderInstitucion()->paginate(self::$paginate) ?? [];
    }


    /**
     * @param string|null $search
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerInstitucionNombreCanton($nombre, $canton)
    {
        $rs = self::builderInstitucion();
        $rs->where('nombre', $nombre)
            ->where('canton_id', is_object($canton) ? $canton->id : (int)$canton);

        return $rs->first() ?? [];
    }


}
