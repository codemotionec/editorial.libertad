<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class AnioLectivo extends Model
{
    use SoftDeletes;

    protected $fillable = ['descripcion', 'anio', 'fecha_inicio', 'fecha_fin','estado_registro_id'];

    public static $paginate = 10;
    public static $search = null;
    protected $table = 'anio_lectivo';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estadoRegistro()
    {
        return $this->belongsTo(EstadoRegistro::class);
    }

    /**
     * @return Builder
     */
    public static function builderAnioLectivo()
    {
        $query = AnioLectivo::orderBy('anio', request('anio', 'DESC'))
            ->orderBy('created_at', request('created_at', 'DESC'));

        if (self::$search) {
            $query->orWhere('descripcion', 'like', '%' . self::$search . '%')
                ->orWhere('anio', 'like', '%' . self::$search . '%')
                ->orWhere('fecha_inicio', 'like', '%' . self::$search . '%')
                ->orWhere('fecha_fin', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @return AnioLectivo[]|array
     */
    public static function obtenerAnioLectivoAll()
    {
        return self::builderAnioLectivo()->get() ?? [];
    }

    /**
     * @param string|null $search
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerAnioLectivoPaginateAll()
    {
        return self::builderAnioLectivo()->paginate(self::$paginate) ?? [];
    }
}
