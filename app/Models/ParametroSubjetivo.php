<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class ParametroSubjetivo extends Model
{
    public static $search = null;
    protected $table = 'parametro_subjetivo';

    protected $fillable = [
        'descripcion', 'codigo', 'orden'
    ];


    /**
     * @return Builder
     */
    public static function builderParametroSubjetivo()
    {
        $query = ParametroSubjetivo::orderBy('orden', 'ASC');

        if (self::$search) {
            //$query->orWhere('descripcion', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public static function obtenerParametroSubjetivoAll()
    {
        return self::builderParametroSubjetivo()->get() ?? [];
    }

    /**
     * @return array|Model|Builder|null|object
     */
    public static function obtenerParametroSubjetivoExcelente()
    {
        return self::builderParametroSubjetivo()
                ->where('codigo', 'Excelente')
                ->first() ?? [];
    }

    /**
     * @return array|Model|Builder|null|object
     */
    public static function obtenerParametroSubjetivoMuybien()
    {
        return self::builderParametroSubjetivo()
                ->where('codigo', 'Muy bien')
                ->first() ?? [];
    }

    /**
     * @return array|Model|Builder|null|object
     */
    public static function obtenerParametroSubjetivoBien()
    {
        return self::builderParametroSubjetivo()
                ->where('codigo', 'Bien')
                ->first() ?? [];
    }

    /**
     * @return array|Model|Builder|null|object
     */
    public static function obtenerParametroSubjetivoRegular()
    {
        return self::builderParametroSubjetivo()
                ->where('codigo', 'Regular')
                ->first() ?? [];
    }

    /**
     * @return array|Model|Builder|null|object
     */
    public static function obtenerParametroSubjetivoMalo()
    {
        return self::builderParametroSubjetivo()
                ->where('codigo', 'Malo')
                ->first() ?? [];
    }

    public static function obtenerCalificacionMinima()
    {
        return self::builderParametroSubjetivo()
            ->select(DB::raw('MIN(calificacion_inicio) calificacion'))
            ->first();
    }

    public static function obtenerCalificacionMaxima()
    {
        return self::builderParametroSubjetivo()
            ->select(DB::raw('MAX(calificacion_fin) calificacion'))
            ->first();
    }

}
