<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoPregunta extends Model
{
    use SoftDeletes;
    public static $paginate = 10;
    public static $search = null;

    protected $fillable = ['descripcion','codigo'];
    protected $table = 'tipo_pregunta';

    /**
     * @return Builder
     */
    public static function builderTipoPregunta()
    {
        $query = TipoPregunta::orderBy('created_at', request('created_at', 'ASC'));

        if (self::$search) {
//            $query->where('institucion.nombre', 'like', '%' . self::$search . '%')
//                ->orwhere('paralelo.descripcion', 'like', '%' . self::$search . '%')
//                ->orwhere('anio_lectivo.descripcion', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public static function obtenerTipoPreguntaAll()
    {
        return self::builderTipoPregunta()->get() ?? [];
    }
}
