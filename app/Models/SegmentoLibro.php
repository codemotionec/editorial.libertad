<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SegmentoLibro extends Model
{
    use SoftDeletes;

    public static $paginate = 10;
    public static $search = null;
    protected $fillable = ['libro_id', 'unidad', 'resumen', 'documento'];
    protected $table = 'segmento_libro';

    public function libro()
    {
        return $this->belongsTo(Libro::class, 'libro_id', 'id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tareas()
    {
        return $this->hasMany(Tarea::class, 'segmento_libro_id', 'id');
    }

    /**
     * @return Builder
     */
    public static function builderLibro()
    {
        $query = SegmentoLibro::orderBy('unidad', request('unidad', 'ASC'));

        if (self::$search) {
            $query->orWhere('unidad', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @param int $libro_id
     * @return array
     */
    public static function obtenerSegmentoLibroAll($libro_id)
    {
        $query = self::builderLibro();
        $query->where('libro_id', $libro_id);
        return $query->get() ?? [];
    }
}
