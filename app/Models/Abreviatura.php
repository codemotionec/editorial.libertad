<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Abreviatura extends Model
{
    protected $fillable = ['descripcion', 'codigo'];
    public static $paginate = 10;
    public static $search = null;
    protected $table = 'abreviatura';

    /**
     * @return Builder
     */
    public static function builderAbreviatura()
    {
        $query = Abreviatura::orderBy('id', request('id', 'ASC'))
            ->orderBy('created_at', request('created_at', 'DESC'));

        if (self::$search) {
            $query->orWhere('descripcion', 'like', '%' . self::$search . '%')
                ->orWhere('codigo', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @return Abreviatura[]|array
     */
    public static function obtenerAbreviaturaAll()
    {
        return self::builderAbreviatura()->get() ?? [];
    }

    /**
     * @param string|null $search
     * @return array|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerAbreviaturaPaginateAll()
    {
        return self::builderAbreviatura()->paginate(self::$paginate) ?? [];
    }
}
