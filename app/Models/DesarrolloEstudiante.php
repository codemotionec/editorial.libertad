<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class DesarrolloEstudiante extends Model
{
    protected $table = 'desarrollo_estudiante';
    protected $fillable = [
        'material_estudiante_id', 'respuesta_desarrollo_id', 'archivo',
        'respuesta', 'tipo_calificacion_id', 'calificacion', 'user_califica_id'
    ];

    /**
     * @return Builder
     */
    public static function builder()
    {
        return DesarrolloEstudiante::select('desarrollo_estudiante.*');
    }

    public static function getDesarrolloEstudianteMaterialEstudianteRespuestaDesarrollo($materialEstudiante, $respuestaDesarrollo)
    {
        $rs = self::builder()
            ->where('material_estudiante_id', (is_object($materialEstudiante)) ? $materialEstudiante->id : $materialEstudiante);

        if (is_array($respuestaDesarrollo)) {
            $rs->whereIn('respuesta_desarrollo_id', $respuestaDesarrollo);
            return $rs->get() ?? [];
        }

        $rs->where('respuesta_desarrollo_id', (is_object($respuestaDesarrollo)) ? $respuestaDesarrollo->id : $respuestaDesarrollo);
        return $rs->first() ?? [];
    }
}
