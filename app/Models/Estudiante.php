<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class Estudiante extends Model
{
    use SoftDeletes;
    protected $table = 'estudiante';
    public static $search = null;
    public static $paginate = 10;
    protected $fillable = ['estado_registro_id', 'user_id', 'aula_id', 'anio_lectivo_id', 'user_autoriza_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userAutoriza()
    {
        return $this->belongsTo(User::class, 'user_autoriza_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aula()
    {
        return $this->belongsTo(Aula::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estadoRegistro()
    {
        return $this->belongsTo(EstadoRegistro::class, 'estado_registro_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function anioLectivo()
    {
        return $this->belongsTo(AnioLectivo::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function representantes()
    {
        return $this->hasMany(RepresentanteEstudiante::class, 'estudiante_id', 'id');
    }

    //implement the attribute
    public function getNombreEstudianteAttribute()
    {
        if ($this->user->perfil) {
            return $this->user->perfil->apellido . ' ' . $this->user->perfil->nombre;
        } else {
            return mb_strtoupper($this->user->name);
        }
    }

    /**
     * @return mixed
     */
    public function getNombreInstitucionAttribute()
    {
        return $this->aula->institucion->nombre;
    }

    /**
     * @return mixed
     */
    public function getNombreAulaAttribute()
    {
        return $this->aula->curso->nombre . " ({$this->aula->paralelo})";
    }

    /**
     * @return mixed
     */
    public function getNombreAutorizaAttribute()
    {
        if ($this->userAutoriza) {
            if ($perfil = $this->userAutoriza->perfil) {
                return $perfil->abreviatura->codigo . '. ' . $perfil->nombre . ' ' . $perfil->apellido;
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getCedulaEstudianteAttribute()
    {
        if ($perfil = $this->user->perfil) {
            return $perfil->cedula_identidad;
        } else {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function getImagenInstitucionAttribute()
    {
        return $this->aula->institucion->imagen;
    }

    /**
     * @return Builder
     */
    public static function builderEstudiante()
    {
        $query = Estudiante::select('estudiante.*')
            ->join('users', 'users.id', '=', 'estudiante.user_id')
            ->join('perfil', 'perfil.id', '=', 'users.perfil_id')
            ->orderBy('perfil.apellido', 'DESC')
            ->orderBy('perfil.nombre', 'DESC')
            ->orderBy('estudiante.created_at', request('created_at', 'DESC'));

        if (self::$search) {
            //$query->orWhere('descripcion', 'like', '%' . self::$search . '%');
        }

        return $query;
    }

    /**
     * @param integer $user_id
     * @return array|Model|Builder|null|object
     */
    public static function obtenerEstudianteUser($user_id)
    {
        $query = self::builderEstudiante();
        $query->where('user_id', $user_id);

        return $query->first() ?? [];
    }

    /**
     * @param integer $user_id
     * @param integer $curso_id
     * @return array|Model|Builder|null|object
     */
    public static function obtenerEstudianteUserAula($user_id, $aula_id)
    {
        $query = self::builderEstudiante();
        $query->where('user_id', $user_id)
            ->where('aula_id', $aula_id);

        return $query->first() ?? [];

    }

    /**
     * @param integer $user_id
     * @return array|\Illuminate\Support\Collection
     */
    public static function obtenerEstudianteUserAll($user_id)
    {
        $query = self::builderEstudiante();
        $query->where('user_id', $user_id);

        return $query->get() ?? [];

    }

    /**
     * @param $user_id
     * @return array|\Illuminate\Support\Collection
     */
    public static function obtenerEstudiantesPaginate()
    {
        $query = self::builderEstudiante();
        return $query->paginate() ?? [];
    }

    public static function obtenerEstudiantesAulasRepresentanteCursoPagination($aulas)
    {
        $rs = self::builderEstudiante();

        if (is_array($aulas)) {
            $rs->whereIn('aula_id', $aulas);
        } else if ($aulas) {
            $rs->where('aula_id', $aulas);
        }

        //dd($rs->toSql(), $aulas);
        return $rs->paginate(self::$paginate) ?? [];
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function obtenerEstudianteAprobadosUserAll($user_id)
    {
        $query = self::builderEstudiante();
        $query->where('user_id', $user_id)
            ->where('estado_registro_id', ($estado = EstadoRegistro::obtenerEstadoRegistroAprobado()) ? $estado->id : null);

        return $query->get() ?? [];
    }

    /**
     * @param Aula|integer $aula
     * @return array|Model|Builder|null|object
     */
    public static function obtenerEstudiantesAula($aula)
    {
        $query = self::builderEstudiante();
        $query->where('aula_id', (is_object($aula)) ? $aula->id : $aula);

        return $query->get() ?? [];
    }
}
