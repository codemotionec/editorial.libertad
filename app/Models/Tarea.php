<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

class Tarea extends Model
{
    use SoftDeletes;

    public static $paginate = 10;
    public static $search = null;
    protected $table = 'tarea';
    protected $fillable = [
        'tipo_desarrollo_id',
        'asignacion_docente_id',
        'segmento_libro_id',
        'tipo_calificacion_id',
        'objetivo',
        'fecha_ejecucion',
        'fecha_terminacion'
    ];

    public function segmentoLibro()
    {
        return $this->belongsTo(SegmentoLibro::class);
    }

    public function tipoDesarrollo()
    {
        return $this->belongsTo(TipoDesarrollo::class);
    }

    public function tipoCalificacion()
    {
        return $this->belongsTo(TipoCalificacion::class);
    }


    /**
     * @return Builder
     */
    public static function builderTarea()
    {
        $query = Tarea::orderBy('created_at', request('created_at', 'ASC'));

        if (self::$search) {
//            $query->orWhere('resumen', 'like', '%' . self::$search . '%')
//                ->orWhere('anio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_inicio', 'like', '%' . self::$search . '%')
//                ->orWhere('fecha_fin', 'like', '%' . self::$search . '%')
//            ;
        }

        return $query;
    }

    public static function obtenerTareasSegmentoLibroAll($segmento_libro_id)
    {
        $query = self::builderTarea();
        $query->where('segmento_libro_id', $segmento_libro_id);

        return $query->get() ?? [];
    }
}
