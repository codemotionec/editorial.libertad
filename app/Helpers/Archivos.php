<?php

namespace App\Helpers;

use App\Models\MaterialEstudiante;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Storage;

class Archivos
{

    /**
     * @param string $nombre
     * @param File $archivo
     * @param string $disk
     * @return string
     */
    public static function storeImagen($nombre, $archivo, $disk)
    {
        $nombre = CustomUrl::urlTitle($nombre);
        $nombreArchivo = strtotime("now") . '_' . str_replace(" ", "_", $nombre) . '.' . $archivo->getClientOriginalExtension();
        Storage::disk($disk)->put($nombreArchivo, file_get_contents($archivo->getRealPath()));
        return $nombreArchivo;
    }

    /**
     * @param string $nombre
     * @param File $archivo
     * @param string $disk
     * @return string
     */
    public static function storeArchivoLibro($nombre, $archivo, $disk)
    {
        $nombreArchivo = strtotime("now") . '_' . str_replace(" ", "_", $nombre) . '.' . $archivo->getClientOriginalExtension();
        Storage::disk($disk)->put($nombreArchivo, file_get_contents($archivo->getRealPath()));
        return $nombreArchivo;
    }

    /**
     * @param MaterialEstudiante $materialEstudiante
     * @param File $archivo
     * @return string
     */
    public static function storeArchivoDesarrolloEstudiante($materialEstudiante, $archivo)
    {
        $date = new Carbon();
        $rand = random_int(0, 99);
        $ext = $archivo->getClientOriginalExtension();
        $name = $materialEstudiante->id . '_' . $date->format('Ymdhis') . $rand;
        $nombreArchivo = "{$name}.{$ext}";
        $path = $materialEstudiante->estudiante_id . '/';

        if (!Storage::disk('desarrollo_estudiante')->has($path)) {
            Storage::disk('desarrollo_estudiante')->makeDirectory($path);
        }

        if (Storage::disk('desarrollo_estudiante')->put($path . $nombreArchivo, file_get_contents($archivo->getRealPath()))) {
            return $nombreArchivo;
        }

        return null;
    }

}