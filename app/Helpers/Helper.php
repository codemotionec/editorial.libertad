<?php

namespace App\Helpers;

use App\Models\Asignatura;
use App\Models\Curso;
use App\Models\Desarrollo;
use App\Models\DesarrolloEstudiante;
use App\Models\Libro;
use App\Models\Paralelo;
use App\Models\RespuestaDesarrollo;
use Carbon\Carbon;

class Helper
{

    /**
     * @param string $fecha
     * @param string $format
     * @return string
     */
    public static function formatDate($fecha, $format = 'M d Y')
    {
        return ($fecha) ? Carbon::createFromFormat('Y-m-d', $fecha)->format($format) : null;
    }

    public static function obtenerIconEstado($codigo)
    {
        switch ($codigo) {
            case 'RE':
                return '<i class="text-warning fe-alert-circle"></i>';
                break;
            case 'AP':
                return '<i class="text-success fe-check-circle"></i>';
                break;
            case 'NP':
                return '<i class="text-danger fas fa-ban"></i>';
                break;
        }

    }

    public static function obtenerEstadoEnCursoInactivo($codigo)
    {
        switch ($codigo) {
            case 'AC':
                return '<span class="align-middle badge badge-primary ml-2">En curso</span>';
                break;
            case 'IN':
                return '<span class="align-middle badge badge-secondary ml-2">Inactivo</span>';
                break;
        }

    }

    public static function validarFechaEjecucionConActual($fecha_inicio, $fecha_fin)
    {

        if ($fecha_inicio && $fecha_fin) {
            $fecha_inicio = self::formatDate($fecha_inicio, 'Y-m-d') ?? date('Y-m-d');
            $fecha_fin = self::formatDate($fecha_fin, 'Y-m-d') ?? date('Y-m-d');
            $now = new Carbon();

            if (!$now->between($fecha_inicio, $fecha_fin)) {
                return false;
            }
        }

        return true;
    }

    public static function quitarEspaciosyMayusculas($str)
    {
        return mb_strtoupper(trim($str));
    }

    public static function obtenerIconTruFalse($campo)
    {
        return ($campo) ? '<i class="text-success fe-check-circle"></i> ' . __('Si') : '<i class="text-danger fas fa-ban opacity-50"></i> ' . __('No');
    }

    public static function obtenerCodigoCurso($nombre)
    {
        $nombre = explode(' ', trim($nombre));
        if (isset($nombre[0]) && $nombre[0]) {
            $codigo = self::nombreCursoACodigo($nombre[0]);
            return $codigo . $nombre[1];
        }

        return null;
    }

    public static function obtenerOrdenCurso($nombre)
    {
        $nombre = explode(' ', trim($nombre));
        if (isset($nombre[0]) && $nombre[0]) {
            $orden = (int)self::nombreCursoACodigo($nombre[0]);
            return $orden;
        }

        return null;
    }

    public static function nombreCursoACodigo($nombre)
    {
        $nombre = trim(mb_strtolower($nombre));
        $nombre = CustomUrl::convertAccentedCharacters($nombre);

        switch ($nombre) {
            case 'inicial':
                return 'I';
                break;
            case 'primero':
                return '1';
                break;
            case 'segundo':
                return '2';
                break;
            case 'tercero':
                return '3';
                break;
            case 'cuarto':
                return '4';
                break;
            case 'quinto':
                return '5';
                break;
            case 'sexto':
                return '6';
                break;
            case 'septimo':
                return '7';
                break;
            case 'octavo':
                return '8';
                break;
            case 'noveno':
                return '9';
                break;
            case 'decimo':
                return '10';
                break;
        }

        return null;
    }

    /**
     * @param $user
     * @return bool
     */
    public static function validateAdmin($user)
    {
        if ($user->rolesUser) {
            foreach ($user->rolesUser as $userRol) {
                if ($userRol->rol && $userRol->rol->key == 'ADMIN') {
                    return true;
                }
            }
        }

        return false;
    }

    public static function obtenerAvanceTareaMaterialEstudiante($tarea, $materialEstudiante)
    {
        if ($desarrollos = Desarrollo::obtenerDesarrollosTareaAll($tarea->id)) {
            $desarrollosAll = $desarrollos->pluck('id', 'id')->all();
            $respuestasDesarrolloAll = [];

            if ($respuestasDesarrollo = RespuestaDesarrollo::obtenerRespuestaDesarrolloDesarrolloAll($desarrollosAll)) {
                $respuestasDesarrolloAll = $respuestasDesarrollo->pluck('id', 'id')->all();
            }

            $desarroEstudiante = DesarrolloEstudiante::getDesarrolloEstudianteMaterialEstudianteRespuestaDesarrollo($materialEstudiante, $respuestasDesarrolloAll);

            $countDesarrollo = $respuestasDesarrollo->count() ?? 0;
            $countDesarrolloEstudiante = $desarroEstudiante->count() ?? 0;

            return ($countDesarrollo > 0) ? round((($countDesarrolloEstudiante * 100) / $countDesarrollo)) : 0;
        }
    }
}