<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Sesiones
{
    public static function roles()
    {
        return json_decode(Session::get('roles')) ?? [];
    }

    public static function representados()
    {
        return json_decode(Session::get('representados')) ?? [];
    }

    public static function estudiante($force = false)
    {
        if (Session::has('estudiante')) {
            return json_decode(Session::get('estudiante')) ?? [];
        }

        if ($force) {
            return false;
        }

        $data_['user'] = Auth::user();
        $data_['perfil'] = Auth::user()->perfil;

        return (object)$data_;
    }
}