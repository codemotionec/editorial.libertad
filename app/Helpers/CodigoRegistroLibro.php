<?php

namespace App\Helpers;

use App\Models\Asignatura;
use App\Models\Curso;
use App\Models\Libro;

class CodigoRegistroLibro
{
//    public static $status = true;
    public static $mensajePrincipal = null;
    public static $mensajeAdicional = null;

    public static function validarRegistroLibro(Libro $libro, $codigo)
    {
        $status = true;
        self::$mensajePrincipal = null;
        self::$mensajeAdicional = null;

        $data = self::retornarCodigoAsignaturaCursoCodigoRegistro($codigo);

        if ($libro->asignatura->codigo !== $data['asignatura']) {
            self::$mensajeAdicional .= '<div><small>ERROR ASIGNATURA</small></div>';
            $status = false;
        }

        if ($libro->curso->codigo !== $data['curso']) {
            self::$mensajeAdicional .= '<div><small>ERROR CURSO</small></div>';
            $status = false;
        }

        if ($status) {
            self::$mensajePrincipal .= '<i class="text-success fe-check-circle"></i>';
        } else {
            self::$mensajePrincipal .= '<i class="text-warning fe-alert-circle"></i>';
        }

        return $status;
    }

    public static function retornarCodigoAsignaturaCursoCodigoRegistro($codigo)
    {
        $data = explode('-', $codigo);
        return ['asignatura' => $data[0], 'curso' => $data[1]];
    }


    public static function validarCodigoRegistroIngresadoLibro(Libro $libro, $codigoAsignatura, $codigoCurso)
    {
        dd($libro, $codigoAsignatura, $codigoCurso);
    }
}
