<?php

namespace App\Imports;

use App\Models\Parroquia;
use Maatwebsite\Excel\Concerns\{
    Importable, ToModel, WithHeadingRow
};

class ParroquiasImport implements ToModel, WithHeadingRow
{
    use Importable;

    public function model(array $row)
    {
        return new Parroquia([
            'id' => (int)$row['cod_parroquia'],
            'canton_id' => (int)$row['cod_canton'],
            'nombre' => $row['parroquia']
        ]);
    }
}
