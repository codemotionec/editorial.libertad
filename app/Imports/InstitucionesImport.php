<?php

namespace App\Imports;

use App\Models\Institucion;
use Maatwebsite\Excel\Concerns\{
    Importable, ToModel, WithHeadingRow
};

class InstitucionesImport implements ToModel, WithHeadingRow
{
    use Importable;

    public function model(array $row)
    {
        //dd($row);
        return new Institucion([
            'canton_id' => (int)$row['cod_canton'],
            'parroquia_id' => (int)$row['cod_parroquia'],
            'nombre' => $row['nombre_institucion'],
            'codigo' => $row['codigo_institucion'],
            'zona_inec' => $row['zona_inec'],
            'direccion' => $row['direccion_institucion'],
            'escolarizacion' => $row['escolarizacion'],
            'tipo_educacion' => $row['tipo_educacion'],
            'nivel_educacion' => $row['nivel_educacion'],
            'sostenimiento' => $row['sostenimiento'],
            'regimen_escolar' => $row['regimen_escolar'],
            'jurisdiccion' => $row['jurisdiccion'],
            'modalidad' => $row['modallidad'],
            'jornada' => $row['jornada']
        ]);
    }
}
