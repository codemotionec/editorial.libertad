<?php

namespace App\Imports;

use App\Models\Curso;
use Maatwebsite\Excel\Concerns\{
    Importable, ToModel, WithHeadingRow
};
use App\Helpers\Helper;

class CursosImport implements ToModel, WithHeadingRow
{
    use Importable;

    public function model(array $row)
    {
        return new Curso([
            'nombre' => trim($row['curso']),
            'sub_nivel' => trim($row['sub_nivel']),
            'codigo' => Helper::obtenerCodigoCurso($row['curso']) ?? null,
            'orden' => Helper::obtenerOrdenCurso($row['curso']) ?? null,
        ]);
    }
}
