<?php

namespace App\Imports;

use App\Models\Aula;
use App\Models\Institucion;
use App\Models\Curso;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\{
    Importable, ToModel, WithHeadingRow
};

class AulasImport implements ToModel, WithHeadingRow
{
    use Importable;

    public function model(array $row)
    {
        $institucion = Institucion::obtenerInstitucionNombreCanton($row['nombre_institucion'], $row['cod_canton']) ?? [];
        $curso = Curso::obtenerCursoNombreSubnivel($row['curso'], $row['sub_nivel']) ?? [];

        return new Aula([
            'curso_id' => $curso ? $curso->id : null,
            'institucion_id' => $institucion ? $institucion->id : null,
            'paralelo' => $row['paralelo'],

        ]);
    }
}
