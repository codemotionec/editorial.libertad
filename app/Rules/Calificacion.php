<?php

namespace App\Rules;

use App\Models\ParametroSubjetivo;
use Illuminate\Contracts\Validation\Rule;

class Calificacion implements Rule
{
    public $minima = null;
    public $maxima = null;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->minima = ($minima = ParametroSubjetivo::obtenerCalificacionMinima()) ? $minima->calificacion : 0;
        $this->maxima = ($maxima = ParametroSubjetivo::obtenerCalificacionMaxima()) ? $maxima->calificacion : 0;

        if ($value >= $this->minima && $value <= $this->maxima) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $min = round($this->minima,0);
        $max = round($this->maxima,0);
        return "Rango de calificación entre {$min} y {$max}";
    }
}
