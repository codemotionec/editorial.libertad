<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RangoFechas implements Rule
{


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $fechas = explode('to',$value);
        dd($value, $fechas);
        return strtoupper($value) == $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El texto no está en mayusculas.';
    }
}
