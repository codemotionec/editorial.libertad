<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class OnOff implements Rule
{


    public static $exist = ['on', 'off'];

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (in_array(mb_strtolower($value), self::$exist)) ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Solo Valores [On | Off]';
    }
}
