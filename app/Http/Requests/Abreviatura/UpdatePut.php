<?php

namespace App\Http\Requests\Abreviatura;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePut extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion' => 'required|min:5|max:500',
            'codigo' => 'required|min:2',
        ];
    }
}
