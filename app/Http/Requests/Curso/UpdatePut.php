<?php

namespace App\Http\Requests\Curso;

use App\Models\Curso;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use PHPUnit\Framework\MockObject\Rule\ParametersRule;

class UpdatePut extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:5|max:500',
            'codigo' => ['required', 'min:2', Rule::unique('curso')->ignore($this->request->get('id'))],
        ];
    }
}
