<?php

namespace App\Http\Requests\Libro;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePut extends FormRequest
{
//    public function attributes()
//    {
//        return [
//            'Descripcion' => "Descripcion año lectivo"
//        ];
//    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required|min:5',
            'resumen' => 'required|min:5',
            'contenido' => 'required|min:5',
            'autor' => 'min:3',
            'editorial' => 'min:3',
            'edicion' => 'min:3',
            'asignatura_id' => 'required|integer',
            'portada' => 'image|mimes:jpeg,png,gif,jpg|max:2048', //peso maximo 2Mb
        ];
    }
}
