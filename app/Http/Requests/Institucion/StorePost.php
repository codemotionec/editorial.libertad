<?php

namespace App\Http\Requests\Institucion;

use Illuminate\Foundation\Http\FormRequest;

class StorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public static function myRules()
    {
        return [
            'nombre' => 'required|min:5',
            'direccion' => 'required|min:5',
            'canton_id' => 'required|integer',
            'imagen' => 'required|image|mimes:jpeg,png,gif,jpg|max:2048', //peso maximo 2Mb
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->myRules();
    }
}
