<?php

namespace App\Http\Requests\Test;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePut extends FormRequest
{
//    public function attributes()
//    {
//        return [
//            'Descripcion' => "Descripcion año lectivo"
//        ];
//    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo_calificacion_id' => 'required|integer',
            'descripcion' => 'required',
//            'fecha_ejecucion' => 'date',
//            'fecha_terminacion' => 'date',
        ];
    }
}
