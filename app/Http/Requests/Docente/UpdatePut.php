<?php

namespace App\Http\Requests\Docente;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePut extends FormRequest
{
//    public function attributes()
//    {
//        return [
//            'Descripcion' => "Descripcion año lectivo"
//        ];
//    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:5',
            'direccion' => 'required|min:5',
            'canton_id' => 'required|integer',
            'imagen' => 'image|mimes:jpeg,png,gif,jpg|max:2048', //peso maximo 2Mb
        ];
    }
}
