<?php

namespace App\Http\Requests\Docente;

use App\Rules\OnOff;
use Illuminate\Foundation\Http\FormRequest;

class StorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public static function myRules()
    {
        return [
            //'institucion_id' => 'required',
            'aula_id' => 'required',
            'anio_lectivo_id' => ['required'],
            'representante_curso' => ['nullable',new OnOff()],
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->myRules();
    }
}
