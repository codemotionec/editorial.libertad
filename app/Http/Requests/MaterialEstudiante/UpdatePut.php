<?php

namespace App\Http\Requests\MaterialEstudiante;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePut extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'estudiante_id' => 'required',
            'material_docente_id' => 'required',
            'codigo' => ['required', Rule::unique('material_estudiante')->ignore($this->request->get('id'))],
        ];
    }
}
