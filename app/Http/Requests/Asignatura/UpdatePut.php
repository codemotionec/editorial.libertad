<?php

namespace App\Http\Requests\Asignatura;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePut extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:5|max:500',
            'codigo' => ['required', 'min:2', Rule::unique('asignatura')->ignore($this->request->get('id'))],
        ];
    }
}
