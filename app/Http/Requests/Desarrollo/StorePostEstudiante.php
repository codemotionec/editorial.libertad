<?php

namespace App\Http\Requests\Desarrollo;

use Illuminate\Foundation\Http\FormRequest;

class StorePostEstudiante extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public static function myRules()
    {
        return [
            'respuesta' => 'nullable',
            'respuesta_desarrollo_id' => 'nullable',
            'file.*' => 'required|file|max:2048', //peso maximo 2Mb
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->myRules();
    }
}
