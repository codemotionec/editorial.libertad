<?php

namespace App\Http\Requests\PreguntaDesarrollo;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePut extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo_pregunta_id' => 'required|integer',
            'pregunta' => 'required'
        ];
    }
}
