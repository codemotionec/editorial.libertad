<?php

namespace App\Http\Requests\OpcionPreguntaDesarrollo;

use App\Rules\OnOff;
use Illuminate\Foundation\Http\FormRequest;

class StorePostImagen extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public static function myRules()
    {
        return [
            'imagen' => 'required|max:250|image|mimes:jpeg,png,gif,jpg|max:2048',
            //'respuesta' => ['required', New OnOff()]
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->myRules();
    }
}
