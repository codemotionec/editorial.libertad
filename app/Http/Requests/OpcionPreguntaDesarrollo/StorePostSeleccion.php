<?php

namespace App\Http\Requests\OpcionPreguntaDesarrollo;

use Illuminate\Foundation\Http\FormRequest;

class StorePostSeleccion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public static function myRules()
    {
        return [
            'descripcion' => 'required|string|max:1000',
            //'respuesta' => 'required|bool'
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->myRules();
    }
}
