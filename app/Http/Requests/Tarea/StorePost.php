<?php

namespace App\Http\Requests\Tarea;

use Illuminate\Foundation\Http\FormRequest;

class StorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public static function myRules()
    {
        return [
            'tipo_desarrollo_id' => 'required|integer',
            'asignacion_docente_id' => 'integer',
            'segmento_libro_id' => 'integer',
            'tipo_calificacion_id' => 'required|integer',
            'objetivo' => 'required|min:3',
            'fecha_ejecucion' => 'nullable|date',
            'fecha_terminacion' => 'nullable|date',
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->myRules();
    }
}
