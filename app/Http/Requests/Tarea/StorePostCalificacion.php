<?php

namespace App\Http\Requests\Tarea;

use App\Rules\Calificacion;
use Illuminate\Foundation\Http\FormRequest;

class StorePostCalificacion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public static function myRules()
    {
        return [
            'observacion' => 'required',
            'calificacion' => ['required', new Calificacion()]
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->myRules();
    }
}
