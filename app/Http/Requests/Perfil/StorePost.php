<?php

namespace App\Http\Requests\Perfil;

use App\Rules\CedulaIdentidad;
use App\Rules\Letras;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public static function myRules()
    {
        return [
            'nombre' => ['required','max:255', New Letras()],
            'apellido' => ['required','max:255', New Letras()],
            'direccion' => ['max:255'],
            'abreviatura_id' => ['required','integer'],
            'cedula_identidad' => ['required','numeric', New CedulaIdentidad(), Rule::unique('perfil')],
            'fecha_nacimiento' => 'required|date',
            'telefono' => 'required|numeric|digits_between:5,10',
            'celular' => 'required|numeric|digits_between:6,12',
            'imagen' => 'required|image|mimes:jpeg,png,gif,jpg|max:2048', //peso maximo 2Mb
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->myRules();
    }
}
