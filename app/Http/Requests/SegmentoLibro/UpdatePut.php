<?php

namespace App\Http\Requests\SegmentoLibro;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePut extends FormRequest
{
//    public function attributes()
//    {
//        return [
//            'Descripcion' => "Descripcion año lectivo"
//        ];
//    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'unidad' => 'required', //peso maximo 2Mb
            'documento' => 'required|file|mimes:pdf|max:20048', //peso maximo 2Mb
            'resumen' => 'string|max:500', //peso maximo 2Mb
        ];
    }
}
