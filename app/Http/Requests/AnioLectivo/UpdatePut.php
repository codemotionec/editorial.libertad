<?php

namespace App\Http\Requests\AnioLectivo;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePut extends FormRequest
{
//    public function attributes()
//    {
//        return [
//            'Descripcion' => "Descripcion año lectivo"
//        ];
//    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion' => 'required|min:5|max:500',
            //'anio' => 'required|integer',
            'fecha_inicio' => 'required|date',
            'fecha_fin' => 'required|date',
        ];
    }
}
