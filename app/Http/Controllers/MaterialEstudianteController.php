<?php

namespace App\Http\Controllers;

use App\Models\EstadoRegistro;
use App\Models\MaterialEstudiante;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MaterialEstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'acceso-backend']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materialesEstudiante = MaterialEstudiante::obtenerMaterialEstudiantePaginate();
        return view('backend.material_estudiante.index', compact('materialesEstudiante'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MaterialEstudiante $materialEstudiante
     * @return \Illuminate\Http\Response
     */
    public function show(MaterialEstudiante $materialEstudiante)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MaterialEstudiante $materialEstudiante
     * @return \Illuminate\Http\Response
     */
    public function edit(MaterialEstudiante $materialEstudiante)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\MaterialEstudiante $materialEstudiante
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MaterialEstudiante $materialEstudiante)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MaterialEstudiante $materialEstudiante
     * @return \Illuminate\Http\Response
     */
    public function destroy(MaterialEstudiante $materialEstudiante)
    {
        //
    }

    public function aprobar(MaterialEstudiante $materialEstudiante)
    {
        DB::beginTransaction();

        try {
            $estadoAprobado = EstadoRegistro::obtenerEstadoRegistroAprobado();
            $requestData['estado_registro_id'] = $estadoAprobado->id;
            $requestData['user_autoriza_id'] = Auth::user()->id;

            if ($materialEstudiante->update($requestData)) {
                DB::commit();
                return back()->with('status', 'Libro del Estudiante aprobado con exito');
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function cancelar(MaterialEstudiante $materialEstudiante)
    {
        $estadoAprobado = EstadoRegistro::obtenerEstadoRegistroCancelado();
        $requestData['estado_registro_id'] = $estadoAprobado->id;

        $materialEstudiante->update($requestData);
        return back()->with('status', 'Docente cancelado con exito');
    }
}
