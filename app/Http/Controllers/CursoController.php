<?php

namespace App\Http\Controllers;

use App\Http\Requests\Curso\StorePost;
use App\Http\Requests\Curso\UpdatePut;
use App\Models\Curso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'acceso-backend']);
    }

    public function index(Request $request)
    {
        Curso::$search = ($request->has('search')) ? request('search') : null;
        Curso::$paginate = 20;
        $cursos = Curso::obtenerCursoPaginateAll();

        return view('backend.curso.index', ['cursos' => $cursos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Curso();
        return view('backend.curso.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        DB::beginTransaction();
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        try {
            if ($validator->fails()) {
                return redirect('admin.curso.create')
                    ->withErrors($validator)
                    ->withInput();
            }

            if ($model = Curso::create($requestData)) {
                DB::commit();
                return back()->with('status', 'Curso creado con exito');
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Curso $curso)
    {
        return view('backend.curso.show', ["model" => $curso]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Curso $curso)
    {
        return view('backend.curso.edit', ["model" => $curso]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, Curso $curso)
    {
        $curso->update($request->validated());
        return back()->with('status', 'Curso actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Curso $curso)
    {
        $curso->delete();
        return back()->with('status', 'Curso eliminado con exito');

    }
}
