<?php

namespace App\Http\Controllers\Aplicacion;

use App\Http\Controllers\Controller;
use App\Models\AsignacionAsignatura;
use Illuminate\Http\Request;

class AsignacionAsignaturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AsignacionAsignatura  $asignacionAsignatura
     * @return \Illuminate\Http\Response
     */
    public function show(AsignacionAsignatura $asignacionAsignatura)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AsignacionAsignatura  $asignacionAsignatura
     * @return \Illuminate\Http\Response
     */
    public function edit(AsignacionAsignatura $asignacionAsignatura)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AsignacionAsignatura  $asignacionAsignatura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AsignacionAsignatura $asignacionAsignatura)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AsignacionAsignatura  $asignacionAsignatura
     * @return \Illuminate\Http\Response
     */
    public function destroy(AsignacionAsignatura $asignacionAsignatura)
    {
        //
    }
}
