<?php

namespace App\Http\Controllers\Aplicacion;

use App\Http\Controllers\AsignaturaDocenteController;
use App\Http\Controllers\Controller;
use App\Http\Requests\MaterialDocente\StorePost;
use App\Models\EstadoRegistro;
use App\Models\MaterialDocente;
use App\Models\MaterialEstudiante;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MaterialDocenteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'acceso-app']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materialesDocente = MaterialDocente::obtenerMaterialDocenteUserAll(Auth::user());
        return view('aplicacion.material_docente.index', compact('materialesDocente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new MaterialDocente();
        return view('aplicacion.material_docente.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        DB::beginTransaction();
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        try {
            if ($validator->fails()) {
                return redirect('admin.material-docente.create')
                    ->withErrors($validator)
                    ->withInput();
            }

            $requestData['estado_registro_id'] = EstadoRegistro::obtenerEstadoRegistroRevision()->id;

            if (!($materialDocente = MaterialDocente::obtenerMaterialDocenteLibroOne($request->docente_id, $request->libro_id))) {
                if ($model = MaterialDocente::create($requestData)) {
                    if ($model = AsignaturaDocenteController::crearAsignaturaDocenteLibro($model->libro, $model->docente)) {
                        DB::commit();
                        return back()->with('status', 'Libro asignado con exito');
                    }
                }
            } else {
                return back()->with('status', 'Ya tienes asignado este libro');
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MaterialDocente $materialDocente
     * @return \Illuminate\Http\Response
     */
    public function show(MaterialDocente $materialDocente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MaterialDocente $materialDocente
     * @return \Illuminate\Http\Response
     */
    public function edit(MaterialDocente $materialDocente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\MaterialDocente $materialDocente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MaterialDocente $materialDocente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MaterialDocente $materialDocente
     * @return \Illuminate\Http\Response
     */
    public function destroy(MaterialDocente $materialDocente)
    {
        $materialDocente->delete();
        return back()->with('status', 'Asignación de libro eliminado con éxito');
    }
}
