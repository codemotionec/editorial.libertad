<?php

namespace App\Http\Controllers\Aplicacion;

use App\Helpers\Archivos;
use App\Http\Controllers\Controller;
use App\Http\Controllers\RolesUsersController;
use App\Http\Requests\RepresentanteEstudiante\StorePost;
use App\Http\Requests\RepresentanteEstudiante\UpdatePut;
use App\Models\EstadoRegistro;
use App\Models\Estudiante;
use App\Models\Perfil;
use App\Models\RepresentanteEstudiante;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class RepresentanteEstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = new Estudiante();
        $representanteEstudiantes = RepresentanteEstudiante::obtenerRepresentanteEstudianteUser(Auth::user());
        return view('aplicacion.representante_estudiante.index', compact('model', 'representanteEstudiantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Estudiante();
        $perfil = new Perfil();
        $perfil->telefono = Auth::user()->perfil->telefono;
        $perfil->celular = Auth::user()->perfil->celular;
        $perfil->direccion = Auth::user()->perfil->direccion;

        $institucion = [];
        $canton = [];

        return view('aplicacion.representante_estudiante.create', compact('model', 'perfil', 'institucion', 'canton'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        DB::beginTransaction();
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return redirect()
                ->route('app.representante-estudiante.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $user = new User();

            if ($perfil = Perfil::create($requestData)) {
                if ($request->hasFile('imagen')) {
                    $perfil->imagen = Archivos::storeImagen($perfil->apellido, $request->imagen, 'perfil');
                    $perfil->update();
                }

                $user->cedula_identidad = $request->cedula_identidad;
                $user->password = Hash::make($request->cedula_identidad);
                $user->verification_token = sha1($user);
                $user->perfil_id = $perfil->id;

                if ($user->save()) {
                    $estado = EstadoRegistro::obtenerEstadoRegistroRevision();
                    $requestData['estado_registro_id'] = $estado->id;
                    $requestData['user_id'] = $user->id;

                    if (!($estudiante = Estudiante::obtenerEstudianteUserAula($user->id, $request->aula_id))) {
                        if ($estudiante = Estudiante::create($requestData)) {
                            if (RolesUsersController::guardarRolEstudiante($user->id)) {
                                $dataRepresentante['user_id'] = Auth::user()->id;
                                $dataRepresentante['estudiante_id'] = $estudiante->id;
                                $dataRepresentante['estado_registro_id'] = $estado->id;
                                if ($representanteEstudiante = RepresentanteEstudiante::create($dataRepresentante)) {
                                    if (RolesUsersController::guardarRolRepresentante(Auth::user())) {
                                        DB::commit();
                                        return redirect()
                                            ->route('app.representante-estudiante.index')
                                            ->with('status', 'Registro completo, un administrador activará su registro');
                                    }
                                }
                            }
                        }
                    } else {
                        return back()->with('error', 'Ya tienes registrada una solicitud para esta institución');
                    }
                }
            }
        } catch (Throwable $e) {
            DB::rollBack();
            report($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RepresentanteEstudiante $representanteEstudiante
     * @return \Illuminate\Http\Response
     */
    public function show(RepresentanteEstudiante $representanteEstudiante)
    {
        dd($representanteEstudiante);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RepresentanteEstudiante $representanteEstudiante
     * @return \Illuminate\Http\Response
     */
    public function edit(RepresentanteEstudiante $representanteEstudiante)
    {
        $model = $representanteEstudiante->estudiante;
        $perfil = $representanteEstudiante->estudiante->user->perfil;
        $institucion = $representanteEstudiante->estudiante->aula->institucion;
        $canton = $institucion->canton;

        return view('aplicacion.representante_estudiante.edit', compact('model', 'perfil', 'institucion', 'representanteEstudiante', 'canton'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\RepresentanteEstudiante $representanteEstudiante
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, RepresentanteEstudiante $representanteEstudiante)
    {
        DB::beginTransaction();
        $requestData = $request->validated();

        try {
            $perfil = $representanteEstudiante->estudiante->user->perfil;
            $user = $representanteEstudiante->estudiante->user;
            $estudiante = $representanteEstudiante->estudiante;

            if ($perfil->update($request->validated())) {
                if ($request->hasFile('imagen')) {
                    $perfil->imagen = Archivos::storeImagen($perfil->apellido, $request->imagen, 'perfil');
                    $perfil->update();
                }

                $user->cedula_identidad = $request->cedula_identidad;
                $user->password = Hash::make($request->cedula_identidad);
                $user->verification_token = sha1($user);
                $user->perfil_id = $perfil->id;

                if ($user->update()) {
                    if ($estudiante && ($estado = EstadoRegistro::obtenerEstadoRegistroRevision())) {
                        $dataEstudiante = $request->validated();
                        $dataEstudiante['estado_registro_id'] = $estado->id;
                        $dataEstudiante['user_autoriza_id'] = null;

                        if ($estudiante->update($dataEstudiante)) {
                            DB::commit();
                            return redirect()
                                ->route('app.representante-estudiante.index')
                                ->with('status', 'Registro actualizador correctamente, un administrador revisará su registro');
                        }
                    }
                }
            }

            return back()->with('error', 'Estudiante no puede ser actualziado');

        } catch (Throwable $e) {
            DB::rollBack();
            report($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RepresentanteEstudiante $representanteEstudiante
     * @return \Illuminate\Http\Response
     */
    public function destroy(RepresentanteEstudiante $representanteEstudiante)
    {
        $representanteEstudiante->delete();
        return back()->with('status', 'Representante Eliminado con exito');
    }

    public function asignarEstudianteRepresentante(Request $request)
    {
        DB::beginTransaction();
//        $requestData = $request->validated();
//        $validator = Validator::make($requestData, StorePost::myRules());
//
//        if ($validator->fails()) {
//            return redirect()
//                ->route('app.representante-estudiante.create')
//                ->withErrors($validator)
//                ->withInput();
//        }

        try {
//            $user = new User();
//            if ($perfil = Perfil::create($requestData)) {
//                if ($request->hasFile('imagen')) {
//                    $perfil->imagen = Archivos::storeImagen($perfil->apellido, $request->imagen, 'perfil');
//                    $perfil->update();
//                }
//
//                $user->cedula_identidad = $request->cedula_identidad;
//                $user->password = Hash::make($request->cedula_identidad);
//                $user->verification_token = sha1($user);
//                $user->perfil_id = $perfil->id;
//
//                if ($user->save()) {
//            $requestData['estado_registro_id'] = $estado->id;
//            $requestData['user_id'] = $user->id;

//            if (!($estudiante = Estudiante::obtenerEstudianteUserAula($user->id, $request->aula_id))) {
//                if ($estudiante = Estudiante::create($requestData)) {
//            if (RolesUsersController::guardarRolEstudiante($user->id)) {
            if ($estudiante = Estudiante::find($request->estudiante_id)) {
                if (!($represetante = RepresentanteEstudiante::obtenerRepresentanteEstudiante(Auth::user(), $estudiante))) {
                    $dataRepresentante['user_id'] = Auth::user()->id;
                    $dataRepresentante['estudiante_id'] = $estudiante->id;
                    $dataRepresentante['estado_registro_id'] = ($estado = EstadoRegistro::obtenerEstadoRegistroRevision()) ? $estado->id : null;

                    if ($representanteEstudiante = RepresentanteEstudiante::create($dataRepresentante)) {
                        if (RolesUsersController::guardarRolRepresentante(Auth::user())) {
                            DB::commit();
                            return redirect()
                                ->route('app.representante-estudiante.index')
                                ->with('status', 'Registro completo, un administrador activará su registro');
                        }
                    }
                } else {
                    return back()->with('error', 'Ya existe una solicitud para este estudiante');
                }
            } else {
                return back()->with('error', 'No existe estudiante con los parametros enviados');
            }
//            }
//                }

//                }
//            }
        } catch (Throwable $e) {
            DB::rollBack();
            report($e);
        }

    }

    public function cambiarUsuarioEstudiante(User $user)
    {
        $data_['user'] = $user;
        $data_['perfil'] = $user->perfil;

        Session::put('estudiante', json_encode($data_));
        return redirect()->route('app.dashboard')->with('status', 'Estudiante cambiado con exito');
    }
}
