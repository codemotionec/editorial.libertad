<?php

namespace App\Http\Controllers\Aplicacion;

use App\Helpers\CodigoRegistroLibro;
use App\Helpers\Sesiones;
use App\Http\Controllers\Controller;
use App\Http\Requests\MaterialEstudiante\StorePost;
use App\Http\Requests\MaterialEstudiante\UpdatePut;
use App\Models\AsignaturaDocente;
use App\Models\AsignaturaDocenteEstudiante;
use App\Models\EstadoRegistro;
use App\Models\MaterialDocente;
use App\Models\MaterialEstudiante;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MaterialEstudianteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'acceso-app']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materialesEstudiante = MaterialEstudiante::obtenerMaterialEstudianteUser(Sesiones::estudiante()->user->id);
        return view('aplicacion.material_estudiante.index', compact('materialesEstudiante'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new MaterialEstudiante();
        return view('aplicacion.material_estudiante.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        DB::beginTransaction();

        try {
            $requestData = $request->validated();
            $validator = Validator::make($requestData, StorePost::myRules());

            if ($validator->fails()) {
                return redirect('admin.institucion.create')
                    ->withErrors($validator)
                    ->withInput();
            }

            $requestData['estado_registro_id'] = ($estado = EstadoRegistro::obtenerEstadoRegistroRevision()) ? $estado->id : null;
            $requestData['codigo'] = strtoupper($requestData['codigo']);

            if (!($materialEstudiante = MaterialEstudiante::obtenerMaterialEstudianteMaterialDocenteEstudiante($request->material_docente_id, $request->estudiante_id))) {
                if ($model = MaterialEstudiante::create($requestData)) {
                    if (CodigoRegistroLibro::validarRegistroLibro($model->materialDocente->libro, $model->codigo)) {
                        $model->estado_registro_id = ($estado = EstadoRegistro::obtenerEstadoRegistroAprobado()) ? $estado->id : $model->estado_registro_id;
                        $model->save();
                    }

                    if ($asignaturasDocente = AsignaturaDocente::obtenerAsignaturaDocenteEstudianteAll($model->estudiante_id, $model->materialDocente->libro_id)) {
                        foreach ($asignaturasDocente as $asignaturaDocente) {
                            $dataAsignatura['estudiante_id'] = $model->estudiante_id;
                            $dataAsignatura['asignatura_docente_id'] = $asignaturaDocente->id;
                            $dataAsignatura['estado_registro_id'] = $model->estado_registro_id;
                            if (!($modelAsignatura = AsignaturaDocenteEstudiante::create($dataAsignatura))) {
                                return back()->with('error', 'No existe cargada una asignatura del docente');
                            }
                        }

                        DB::commit();
                        return redirect()->route('app.material-estudiante.index')->with('status', 'Código de registro verificado con éxito, libro está aprobado para este usuario');
                    } else {
                        return redirect()->route('app.material-estudiante.index')->with('status', 'No existe asignaturas parametrizadas');
                    }
                }
            } else {
                return back()->with('error', 'Ya existe un libro registrado para esta asignatura');
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RegistroLibro $registroLibro
     * @return \Illuminate\Http\Response
     */
    public function show(RegistroLibro $registroLibro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RegistroLibro $registroLibro
     * @return \Illuminate\Http\Response
     */
    public function edit(MaterialEstudiante $materialEstudiante)
    {
        $model = $materialEstudiante;
        return view('aplicacion.material_estudiante.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\RegistroLibro $registroLibro
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, MaterialEstudiante $materialEstudiante)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->validated();

            $estado = EstadoRegistro::obtenerEstadoRegistroRevision();
            if (CodigoRegistroLibro::validarRegistroLibro($materialEstudiante->materialDocente->libro, $materialEstudiante->codigo)) {
                $estado = EstadoRegistro::obtenerEstadoRegistroAprobado();
            }

            $requestData['estado_registro_id'] = ($estado) ? $estado->id : null;
            $requestData['codigo'] = strtoupper($requestData['codigo']);

            if ($materialEstudiante->update($requestData)) {
                if ($asignaturasDocente = AsignaturaDocente::obtenerAsignaturaDocenteEstudianteAll($materialEstudiante->estudiante_id, $materialEstudiante->materialDocente->libro_id)) {
                    foreach ($asignaturasDocente as $asignaturaDocente) {
                        $dataAsignatura['estudiante_id'] = $materialEstudiante->estudiante_id;
                        $dataAsignatura['asignatura_docente_id'] = $asignaturaDocente->id;
                        $dataAsignatura['estado_registro_id'] = $materialEstudiante->estado_registro_id;

                        if (!($modelAsignatura = AsignaturaDocenteEstudiante::create($dataAsignatura))) {
                            return back()->with('error', 'No existe cargada una asignatura del docente');
                        }
                    }

                    DB::commit();
                    return redirect()->route('app.material-estudiante.index')->with('status', 'Libro registrado correctamente, un administrador aprobara su solicitud');
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RegistroLibro $registroLibro
     * @return \Illuminate\Http\Response
     */
    public function destroy(MaterialEstudiante $materialEstudiante)
    {
        $materialEstudiante->delete();
        return back()->with('status', 'Registro eliminada exitosamente');
    }
}
