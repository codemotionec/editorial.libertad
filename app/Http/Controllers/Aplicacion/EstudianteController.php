<?php

namespace App\Http\Controllers\Aplicacion;

use App\Http\Controllers\Controller;
use App\Http\Controllers\RolesUsersController;
use App\Http\Requests\Estudiante\StorePost;
use App\Models\EstadoRegistro;
use App\Models\Estudiante;
use App\Models\Libro;
use App\Models\RegistroLibro;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EstudianteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'acceso-app']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $model = new Estudiante();
        $estudiantes = Estudiante::obtenerEstudianteUserAll(Auth::user()->id);
        return view('aplicacion.estudiante.index', compact('model', 'estudiantes'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $institucion = [];
        $model = new Estudiante();
        return view('aplicacion.estudiante.create', compact('model', 'institucion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        DB::beginTransaction();
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
//            if (($curso = Curso::obtenerCursoPorInstitucionAnioLectivoParalelo($request->institucion_id, $request->anio_lectivo_id, $request->paralelo_id)) && $curso) {
            $estado = EstadoRegistro::obtenerEstadoRegistroRevision();

            $requestData['estado_registro_id'] = $estado->id;
            $requestData['user_id'] = Auth::user()->id;

            if (!($estudiante = Estudiante::obtenerEstudianteUserAula(Auth::user()->id, $request->aula_id))) {
                if ($model = Estudiante::create($requestData)) {
                    if (RolesUsersController::guardarRolEstudiante(Auth::user()->id)) {
                        DB::commit();
                        return redirect()
                            ->route('app.estudiante.index')
                            ->with('status', 'Registro completo, un administrador activará su registro');
                    }
                }
            } else {
                return back()->with('error', 'Ya tienes registrada una solicitud para esta institución');
            }
//            } else {
//                return back()->with('error', 'Curso no activo para esta institución, un administrador se comunicará contigo!!!');
//            }
        } catch (Throwable $e) {
            DB::rollBack();
            report($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Estudiante $estudiante
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estudiante $estudiante)
    {
        $estudiante->delete();
        return back()->with('status', 'Institución eliminada con éxito.');
    }

    public function detalleRegistroLibro()
    {
        $model = new RegistroLibro();
        $libros = Libro::obtenerLibroAll();
        return view('aplicacion.estudiante.index', compact('libros', 'model'));
    }

    public function registroLibro()
    {
        $libros = Libro::obtenerLibroAll();
        return view('aplicacion.estudiante.registro_libro', compact('libros'));
    }
}
