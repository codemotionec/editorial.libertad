<?php

namespace App\Http\Controllers\Aplicacion;

use App\Helpers\Archivos;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DocenteController;
use App\Http\Controllers\EstudianteController;
use App\Http\Controllers\RepresentanteController;
use App\Http\Requests\Perfil\StorePost;
use App\Http\Requests\Roles_Users\StoreRolUserPost;
use App\Models\EstadoRegistro;
use App\Models\Perfil;
use App\Models\RolesUsers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Perfil de usuarios.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function perfil()
    {
        $user = Auth::user();
        $perfil = $user->perfil()->first() ?? (new Perfil());
        $rolesUser = RolesUsers::obtenerRolesUserAll($user->id);

        return view('aplicacion.user.perfil', compact('perfil', 'user', 'rolesUser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storePerfil(StorePost $request, User $user)
    {
        DB::beginTransaction();

        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return redirect('app.user.perfil')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            if (!($perfil = $user->perfil()->first())) {
                if ($perfil = Perfil::create($requestData)) {
                    $user->perfil_id = $perfil->id;
                    if ($user->update()) {
                        if ($request->hasFile('imagen')) {
                            $perfil->imagen = Archivos::storeImagen($perfil->apellido, $request->imagen, 'perfil');
                            $perfil->update();
                        }
                    }
                }
            } else {
                if ($perfil->update($requestData)) {
                    if ($request->hasFile('imagen')) {
                        $perfil->imagen = Archivos::storeImagen($perfil->apellido, $request->imagen, 'perfil');
                        $perfil->update();
                    }
                }
            }

            DB::commit();
            return back()->with('status', 'Perfil ingresado con exito');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
                ->withErrors(['error' => $e->getMessage()])
                ->withInput();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeRolUser(StoreRolUserPost $request, User $user)
    {
        DB::beginTransaction();
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StoreRolUserPost::myRules());


        if ($validator->fails()) {
            return redirect('app.user.perfil')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $status = null;
            if (!($rol_user = RolesUsers::obtenerRolUser($user->id, $request->rol_id))) {
                $requestData['user_id'] = $user->id;
                $requestData['estado_registro_id'] = ($estado = EstadoRegistro::obtenerEstadoRegistroRevision()) ? $estado->id : null;

                if (RolesUsers::create($requestData)) {
                    $status = 'Rol ingresado con exito, pronto un administrador validará su solicitud';
//                    if (EstudianteController::crearEstudianteRol($user->id, $request->rol_id)) {
//                        $status = 'Estudiante registrado con exito, pronto un administrador validará su solicitud';
//                    }
//
//                    if (RepresentanteController::crearRepresentanteRol($user->id, $request->rol_id)) {
//                        $status = 'Representante registrado con exito, pronto un administrador validará su solicitud';
//                    }
//
//                    if (DocenteController::crearDocenteRol($user->id, $request->rol_id)) {
//                        $status = 'Docente registrado con exito, pronto un administrador validará su solicitud';
//                    }
                }
            } else {
                return back()->with('error', 'Ya existe el Rol asignado para este usuario');
            }

            DB::commit();

            return back()->with('status', $status);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
                ->withErrors(['error' => $e->getMessage()])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroyRol(RolesUsers $rolesUsers)
    {
        $rolesUsers->delete();
        return back()->with('status', 'Rol eliminado del usuario');
    }
}
