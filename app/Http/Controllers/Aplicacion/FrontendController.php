<?php

namespace App\Http\Controllers\Aplicacion;

use App\Helpers\Sesiones;
use App\Http\Controllers\Controller;
use App\Models\Libro;
use App\Models\MaterialEstudiante;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'acceso-app']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home.index');
    }

    public function dashboard()
    {
        Libro::$paginate = 10;
        $materialEstudiantes = MaterialEstudiante::obtenerMaterialEstudianteUser(Sesiones::estudiante()->user->id, true);
        return view('aplicacion.dashboard.index', ['materialEstudiantes' => $materialEstudiantes]);
    }

    public function mis_suscripciones()
    {
        return view('aplicacion.representante.suscripciones');
    }

    public function listado_tareas()
    {
        return view('aplicacion.representante.tareas');
    }

    public function listado_tests()
    {
        return view('aplicacion.representante.tests');
    }
}