<?php

namespace App\Http\Controllers\Aplicacion;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\RolesUsersController;
use App\Http\Requests\Docente\StorePost;
use App\Models\Docente;
use App\Models\EstadoRegistro;
use App\Models\Estudiante;
use App\Models\RepresentanteEstudiante;
use App\Rules\OnOff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DocenteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'acceso-app']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $model = new Docente();
        $docentes = Docente::obtenerDocentesUserAll(Auth::user()->id);
        return view('aplicacion.docente.index', compact('model', 'docentes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $institucion = [];
        $model = new Docente();
        return view('aplicacion.docente.create', compact('model', 'institucion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $onOff = New OnOff();
        DB::beginTransaction();
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $estado = EstadoRegistro::obtenerEstadoRegistroRevision();
            $requestData['estado_registro_id'] = $estado->id;
            $requestData['user_id'] = Auth::user()->id;
            $requestData['representante_curso'] = ($onOff->passes('representante_curso', $request->representante_curso)) ? 1 : 0;

            if (!($docente = Docente::obtenerDocenteUsuarioAulaAnioLectivo(Auth::user()->id, $request->aula_id, $request->anio_lectivo_id))) {
                if ($requestData['representante_curso'] && ($representante = Docente::obtenerDocenteRepresentanteCursoAulaOne($request->aula_id))) {
                    return back()->with('error', 'Ya existe un dirigente asignado para este curso');
                }

                if ($model = Docente::create($requestData)) {
                    if (RolesUsersController::guardarRolDocente(Auth::user()->id)) {
                        DB::commit();
                        return redirect()
                            ->route('app.docente.index')
                            ->with('status', 'Registro completo, un administrador activará su registro');
                    }
                }
            } else {
                return back()->with('error', 'Ya tienes registrada una solicitud para esta institución');
            }
        } catch (Throwable $e) {
            DB::rollBack();
            report($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Estudiante $estudiante
     * @return \Illuminate\Http\Response
     */
    public function destroy(Docente $docente)
    {
        $docente->delete();
        return redirect()
            ->route('app.docente.index')
            ->with('status', 'Institución eliminada con éxito.');
    }

    public function detalleEstudiantesAprobar()
    {
        $aula = [];
        $representantesCurso = Docente::obtenerDocentesUserRepresentantesCursoAll(Auth::user());
        foreach ($representantesCurso as $docente) {
            $aula[] = $docente->aula_id;
        }

        // dd($representantesCurso);

        $estudiantes = Estudiante::obtenerEstudiantesAulasRepresentanteCursoPagination($aula);
        return view('aplicacion.docente.aprobar_estudiante.index', compact( 'estudiantes'));

    }

    public function aprobarEstudiante(Estudiante $estudiante)
    {
        DB::beginTransaction();

        try {
            $estadoAprobado = EstadoRegistro::obtenerEstadoRegistroAprobado();
            $requestData['estado_registro_id'] = $estadoAprobado->id;
            $requestData['user_autoriza_id'] = Auth::user()->id;

            if ($estudiante->update($requestData)) {
                if (RolesUsersController::aprobarRolEstudiante($estudiante->user_id)) {
                    DB::commit();
                    return back()->with('status', 'Estudiante aprobado con exito');
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function cancelarEstudiante(Estudiante $estudiante)
    {
        $estadoCancelado = EstadoRegistro::obtenerEstadoRegistroCancelado();
        $requestData['estado_registro_id'] = $estadoCancelado->id;

        $estudiante->update($requestData);
        return back()->with('status', 'Estudiante cancelado con exito');
    }

    public function aprobarRepresentante(RepresentanteEstudiante $representanteEstudiante)
    {
        DB::beginTransaction();

        try {
            $estadoAprobado = EstadoRegistro::obtenerEstadoRegistroAprobado();
            $requestData['estado_registro_id'] = $estadoAprobado->id;
            $requestData['user_autoriza_id'] = Auth::user()->id;

            if ($representanteEstudiante->update($requestData)) {
                if (RolesUsersController::aprobarRolRepresentante($representanteEstudiante->user_id)) {
                    DB::commit();
                    return back()->with('status', 'Representante aprobado con exito');
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }


}
