<?php

namespace App\Http\Controllers\Aplicacion;

use App\Helpers\Archivos;
use App\Http\Controllers\Controller;
use App\Http\Requests\Desarrollo\StorePostEstudiante;
use App\Models\DesarrolloEstudiante;
use App\Models\MaterialEstudiante;
use App\Models\RespuestaDesarrollo;
use App\Models\SegmentoLibro;
use App\Models\Tarea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DesarrolloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(MaterialEstudiante $materialEstudiante)
    {
        $libro = $materialEstudiante->materialDocente->libro;
        $segmentoLibros = SegmentoLibro::obtenerSegmentoLibroAll($libro->id);
        return view('aplicacion.libro.index', compact('libro', 'segmentoLibros', 'materialEstudiante'));
    }

    /**
     * @param MaterialEstudiante $materialEstudiante
     * @param Tarea $tarea
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tarea(MaterialEstudiante $materialEstudiante, Tarea $tarea)
    {
        return view('aplicacion.libro._desarrollo', compact('tarea', 'materialEstudiante'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostEstudiante $request, MaterialEstudiante $materialEstudiante)
    {
        DB::beginTransaction();

        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePostEstudiante::myRules());

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $errors = [];
            if ($request->has('file')) {
                foreach ($request->file('file') as $respuesta_desarrollo_id => $file) {
                    if ($file) {
                        if ($respuestaDesarrollo = RespuestaDesarrollo::find($respuesta_desarrollo_id)) {
                            if ($archivo = Archivos::storeArchivoDesarrolloEstudiante($materialEstudiante, $file)) {
                                if (!($desarrolloEstudiante = DesarrolloEstudiante::getDesarrolloEstudianteMaterialEstudianteRespuestaDesarrollo($materialEstudiante, $respuestaDesarrollo))) {
                                    $desarrolloEstudiante = new DesarrolloEstudiante();
                                }

                                $desarrolloEstudiante->archivo = $archivo;
                                $desarrolloEstudiante->tipo_calificacion_id = $respuestaDesarrollo->desarrollo->tarea->tipo_calificacion_id ?? null;
                                $desarrolloEstudiante->material_estudiante_id = $materialEstudiante->id ?? null;
                                $desarrolloEstudiante->respuesta_desarrollo_id = $respuestaDesarrollo->id;

                                $desarrolloEstudiante->save();
                                //DesarrolloEstudiante::create($requestData);
                            }
                        }
                    } else {
                        $errors['file_' . $respuesta_desarrollo_id] = ['Por favor seleccione un archivo'];
                    }
                }
            } else {
                $errors['file'] = ['Por favor ingrese el contenido necesario'];
            }

            if ($request->has('respuesta')) {
                foreach ($request->get('respuesta') as $respuesta_desarrollo_id => $respuesta) {
                    if ($respuesta) {
                        if ($respuestaDesarrollo = RespuestaDesarrollo::find($respuesta_desarrollo_id)) {
                            if (!($desarrolloEstudiante = DesarrolloEstudiante::getDesarrolloEstudianteMaterialEstudianteRespuestaDesarrollo($materialEstudiante, $respuestaDesarrollo))) {
                                $desarrolloEstudiante = new DesarrolloEstudiante();
                            }

                            $desarrolloEstudiante->respuesta = $respuesta;
                            $desarrolloEstudiante->tipo_calificacion_id = $respuestaDesarrollo->desarrollo->tarea->tipo_calificacion_id ?? null;
                            $desarrolloEstudiante->material_estudiante_id = $materialEstudiante->id ?? null;
                            $desarrolloEstudiante->respuesta_desarrollo_id = $respuestaDesarrollo->id;

                            $desarrolloEstudiante->save();
                            // DesarrolloEstudiante::create($requestData);
                        }
                    } else {
                        $errors['respuesta_' . $respuesta_desarrollo_id] = ['Por favor ingrese el contenido necesario'];
                    }
                }
            }

            if (!$errors) {
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' => 'Tarea registrada exitosamente'
                ]);
            } else {
                return response()->json([
                    'errors' => $errors
                ], 400);
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MaterialEstudiante $materialEstudiante
     * @return \Illuminate\Http\Response
     */
    public function show(MaterialEstudiante $materialEstudiante)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MaterialEstudiante $materialEstudiante
     * @return \Illuminate\Http\Response
     */
    public function edit(MaterialEstudiante $materialEstudiante)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\MaterialEstudiante $materialEstudiante
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MaterialEstudiante $materialEstudiante)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MaterialEstudiante $materialEstudiante
     * @return \Illuminate\Http\Response
     */
    public function destroy(MaterialEstudiante $materialEstudiante)
    {
        //
    }

}
