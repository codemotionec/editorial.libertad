<?php

namespace App\Http\Controllers\Aplicacion;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tarea\StorePostCalificacion;
use App\Models\Desarrollo;
use App\Models\DesarrolloEstudiante;
use App\Models\Docente;
use App\Models\MaterialDocente;
use App\Models\MaterialEstudiante;
use App\Models\ParametroSubjetivo;
use App\Models\SegmentoLibro;
use App\Models\Tarea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TareaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'acceso-app']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $model = new Docente();
        $materialDocentes = MaterialDocente::obtenerMaterialDocenteUserAll(Auth::user()->id);
        return view('aplicacion.tareas.index', compact('materialDocentes', 'model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostCalificacion $request, MaterialEstudiante $materialEstudiante, Desarrollo $desarrollo)
    {
        $desarrolloEstudiante = DesarrolloEstudiante::getDesarrolloEstudianteMaterialEstudianteRespuestaDesarrollo();
        dd($materialEstudiante, $desarrollo, $request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tarea $tarea
     * @return \Illuminate\Http\Response
     */
    public function show(Tarea $tarea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tarea $tarea
     * @return \Illuminate\Http\Response
     */
    public function edit(Tarea $tarea)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Tarea $tarea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tarea $tarea)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tarea $tarea
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tarea $tarea)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tarea $tarea
     * @return \Illuminate\Http\Response
     */
    public function calificar(MaterialDocente $materialDocente, SegmentoLibro $segmentoLibro = null)
    {
        if (!$segmentoLibro) {
            if ($segmentos = $materialDocente->libro->segmentoLibros ?? []) {
                $segmentoLibro = $segmentos[0];
            }
        }

        $materialEstudiantes = MaterialEstudiante::obtenerMaterialEstudianteMaterialDocente($materialDocente);
        return view('aplicacion.tareas.calificar.index', compact('materialDocente', 'segmentoLibro', 'materialEstudiantes'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tarea $tarea
     * @return \Illuminate\Http\Response
     */
    public function calificarTarea(MaterialDocente $materialDocente, Tarea $tarea)
    {
        $calificacion_minima = ($calificacion = ParametroSubjetivo::obtenerCalificacionMinima()) ? $calificacion->calificacion : 0;
        $calificacion_maxima = ($calificacion = ParametroSubjetivo::obtenerCalificacionMaxima()) ? $calificacion->calificacion : 0;

        $materialEstudiantes = MaterialEstudiante::obtenerMaterialEstudianteMaterialDocente($materialDocente);
        return view('aplicacion.tareas.calificar.tarea', compact('materialDocente', 'tarea', 'materialEstudiantes', 'calificacion_minima', 'calificacion_maxima'));
    }

}
