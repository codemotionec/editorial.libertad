<?php

namespace App\Http\Controllers\Aplicacion;

use App\Http\Controllers\Controller;
use App\Models\Archivo;
use App\Models\Libro;
use App\Models\OpcionPreguntaDesarrollo;
use App\Models\PreguntaDesarrollo;
use App\Models\SegmentoLibro;
use App\Models\Tarea;
use App\Models\Test;
use Illuminate\Http\Request;
use Storage;

class LibroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'acceso-app']);
    }

//    public function revista(Libro $libro)
//    {
//        $segmentoLibros = SegmentoLibro::obtenerSegmentoLibroAll($libro->id);
//        $segmentoLibro = [];
//        return view('aplicacion.libro.index', compact('libro', 'segmentoLibros', 'segmentoLibro'));
//    }

    public function revista_archivo(SegmentoLibro $segmentoLibro)
    {
        $libro = $segmentoLibro->libro;
        $segmentoLibros = SegmentoLibro::obtenerSegmentoLibroAll($segmentoLibro->libro_id);

        return view('aplicacion.libro.revista_unidad', compact('segmentoLibro', 'segmentoLibros', 'libro'));
    }

    public function desarrolloTarea(Tarea $tarea)
    {
        return view('aplicacion.libro._desarrollo', compact('tarea'));
    }

    public function desarrolloTest(Test $test)
    {
        return view('aplicacion.test._desarrollo_test', compact('test'));
    }

    public static function opcionesPreguntas(Request $request = null, PreguntaDesarrollo $preguntaDesarrollo)
    {
        if ($preguntaDesarrollo->tipoPregunta->opciones) {
            $opcionesPregunta = OpcionPreguntaDesarrollo::obtenerOpcionPreguntaDesarrolloPreguntaDesarrolloAll($preguntaDesarrollo->id);
            switch ($preguntaDesarrollo->tipoPregunta->codigo) {
                case 'SELECCION':
                    return view('aplicacion.opcion_pregunta._seleccion', compact('opcionesPregunta'));
                    break;
                case 'IMAGEN':
                    return view('aplicacion.opcion_pregunta._imagen', compact('opcionesPregunta'));
                    break;

            }
        }

        switch ($preguntaDesarrollo->tipoPregunta->codigo) {
            case 'FRASE':
                return view('aplicacion.opcion_pregunta._frase', compact('preguntaDesarrollo'));
                break;
            default:
                return view('aplicacion.opcion_pregunta._texto', compact('preguntaDesarrollo'));
        }


    }

    public function downloadSegmentoLibro(SegmentoLibro $segmentoLibro)
    {
        if ($exists = Storage::disk('libros')->exists($segmentoLibro->documento)) {
            return Storage::disk('libros')->download($segmentoLibro->documento);
        }

        return redirect()->back();
    }
}
