<?php

namespace App\Http\Controllers\Corporativa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home.index');
    }

    public function acerca_de()
    {
        return view('corporativa.acerca-de');
    }

    public function contacto()
    {
        return view('corporativa.contacto');
    }

    public function libreria()
    {
        return view('corporativa.libreria');
    }

    public function home(Request $request)
    {
        return view('auth.login');
    }
}