<?php

namespace App\Http\Controllers;

use App\Models\Desarrollo;
use App\Models\RespuestaDesarrollo;
use Illuminate\Http\Request;
use App\Http\Requests\RespuestaDesarrollo\StorePost;
use App\Http\Requests\RespuestaDesarrollo\UpdatePut;
use Illuminate\Support\Facades\Validator;

class RespuestaDesarrolloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $desarrollo = Desarrollo::find($request->id);
        $html = view('backend.desarrollo._detalle_respuestas')->with('desarrollo', $desarrollo)->render();
        return response()->json(['html' => $html]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request, Desarrollo $desarrollo)
    {
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return response()->json(['errors' => 'No se puede ingresar la respuesta a desarrollar']);
        }

        $requestData['desarrollo_id'] = $desarrollo->id;
        if ($model = RespuestaDesarrollo::create($requestData)) {
            return response()->json(['success' => 'Respuesta ingresada con éxito en el desarrollo de la actividad']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RespuestaDesarrollo $respuestaDesarrollo
     * @return \Illuminate\Http\Response
     */
    public function destroy(RespuestaDesarrollo $respuestaDesarrollo)
    {
        $id = $respuestaDesarrollo->desarrollo_id;
        $respuestaDesarrollo->delete();
        return response()->json([
            'success' => 'Respuesta eliminada con éxito en el desarrollo de la actividad',
            'desarrollo_id' => $id
        ]);
    }
}
