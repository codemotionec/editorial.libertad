<?php

namespace App\Http\Controllers;

use App\Http\Requests\PreguntaDesarrollo\StorePost;
use App\Http\Requests\PreguntaDesarrollo\UpdatePut;
use App\Models\PreguntaDesarrollo;
use App\Models\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PreguntaDesarrolloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Test $test)
    {
        $preguntaDesarrollo = New PreguntaDesarrollo();
        $preguntaDesarrollos = PreguntaDesarrollo::obtenerPreguntaDesarrolloTestAll($test->id);
        return view('backend.pregunta.create', ['test' => $test, 'model' => $preguntaDesarrollo, 'preguntaDesarrollos' => $preguntaDesarrollos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request, Test $test)
    {
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $requestData['test_id'] = $test->id;
        if ($model = PreguntaDesarrollo::create($requestData)) {
            return back()->with('status', 'Pregunta creada con exito');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PreguntaDesarrollo $preguntaDesarrollo
     * @return \Illuminate\Http\Response
     */
    public function edit(PreguntaDesarrollo $preguntaDesarrollo)
    {
        $preguntaDesarrollos = PreguntaDesarrollo::obtenerPreguntaDesarrolloTestAll($preguntaDesarrollo->test_id);
        return view('backend.pregunta.create', ['test' => $preguntaDesarrollo->test, 'model' => $preguntaDesarrollo, 'preguntaDesarrollos' => $preguntaDesarrollos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\PreguntaDesarrollo $preguntaDesarrollo
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, PreguntaDesarrollo $preguntaDesarrollo)
    {
        $preguntaDesarrollo->update($request->validated());
        return back()
            ->with('status', 'Pregunta actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PreguntaDesarrollo $preguntaDesarrollo
     * @return \Illuminate\Http\Response
     */
    public function destroy(PreguntaDesarrollo $preguntaDesarrollo)
    {
        //
    }
}
