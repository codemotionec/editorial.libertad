<?php

namespace App\Http\Controllers;

use App\Helpers\Archivos;
use App\Models\Institucion;
use Illuminate\Http\Request;
use App\Http\Requests\Institucion\StorePost;
use App\Http\Requests\Institucion\UpdatePut;
use Storage;
use Illuminate\Support\Facades\Validator;

class InstitucionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'acceso-backend']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Institucion::$paginate = 10;
        $instituciones = Institucion::obtenerInstitucionPaginateAll();

        return view('backend.institucion.index', ['instituciones' => $instituciones]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Institucion();
        return view('backend.institucion.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return redirect('admin.institucion.create')
                ->withErrors($validator)
                ->withInput();
        }

        if ($model = Institucion::create($requestData)) {
            if ($request->hasFile('imagen')) {
                $model->imagen = Archivos::storeImagen($model->nombre, $request->imagen, 'instituciones');
                $model->update();
            }

            return redirect()->route('admin.institucion.index')->with('status', 'Institución creado con exito');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Institucion $institucion
     * @return \Illuminate\Http\Response
     */
    public function show(Institucion $institucion)
    {
        return view('backend.institucion.show', ["model" => $institucion]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Institucion $institucion
     * @return \Illuminate\Http\Response
     */
    public function edit(Institucion $institucion)
    {
        return view('backend.institucion.edit', ["model" => $institucion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Institucion $institucion
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, Institucion $institucion)
    {
        $requestData = $request->validated();

        if ($request->hasFile('imagen')) {
            $requestData['imagen'] = Archivos::storeImagen($institucion->nombre, $request->imagen, 'instituciones');
        }

        $institucion->update($requestData);
        return back()->with('status', 'Post actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Institucion $institucion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Institucion $institucion)
    {
        $institucion->delete();
        return back()->with('status', 'Registro eliminado con exito');
    }

//    public function storeImagen(Request $request)
//    {
//        $nombreArchivo = strtotime("now") . '_' . str_replace(" ", "_", $request->nombre) . '.' . $request->imagen->getClientOriginalExtension();
//        Storage::disk('instituciones')->put($nombreArchivo, file_get_contents($request->imagen->getRealPath()));
//        return $nombreArchivo;
//    }
}
