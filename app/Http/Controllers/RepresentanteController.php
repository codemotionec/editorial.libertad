<?php

namespace App\Http\Controllers;

use App\Models\EstadoRegistro;
use App\Models\Representante;
use App\Models\Roles;
use Illuminate\Http\Request;

class RepresentanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Representante  $representante
     * @return \Illuminate\Http\Response
     */
    public function show(Representante $representante)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Representante  $representante
     * @return \Illuminate\Http\Response
     */
    public function edit(Representante $representante)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Representante  $representante
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Representante $representante)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Representante  $representante
     * @return \Illuminate\Http\Response
     */
    public function destroy(Representante $representante)
    {
        //
    }

    public static function crearRepresentanteRol($user_id, $rol_id)
    {
        if (($rol = Roles::find($rol_id)) && $rol->key == 'R') {
            if (!($representante = Representante::obtenerRepresentanteUser($user_id))) {
                $estadoRegistro = EstadoRegistro::obtenerEstadoRegistroRevision();
                if(Representante::create(['user_id' => $user_id, 'estado_registro_id' => $estadoRegistro->id])){
                    return true;
                }
            }
        }

        return false;
    }
}
