<?php

namespace App\Http\Controllers;

use App\Models\EstadoRegistro;
use App\Models\Roles;
use App\Models\RolesUsers;
use App\User;
use Illuminate\Http\Request;

class RolesUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'acceso-backend']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rolesUsers = RolesUsers::obtenerRolesUserAll();
        return view('backend.roles_users.index', ['rolesUsers' => $rolesUsers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RolesUsers $rolesUsers
     * @return \Illuminate\Http\Response
     */
    public function show(RolesUsers $rolesUsers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RolesUsers $rolesUsers
     * @return \Illuminate\Http\Response
     */
    public function edit(RolesUsers $rolesUsers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\RolesUsers $rolesUsers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RolesUsers $rolesUsers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RolesUsers $rolesUsers
     * @return \Illuminate\Http\Response
     */
    public function destroy(RolesUsers $rolesUsers)
    {
        //
    }

    /**
     * @param RolesUsers $rolesUsers
     */
    public function aprobar(RolesUsers $rolesUsers)
    {
        $estadoAprobado = EstadoRegistro::obtenerEstadoRegistroAprobado();
        $requestData['user_id'] = $rolesUsers->user_id;
        $requestData['rol_id'] = $rolesUsers->rol_id;
        $requestData['estado_registro_id'] = $estadoAprobado->id;


        $rolesUsers->update($requestData);
        return back()->with('status', 'Rol aprobado con exito');
    }

    public function cancelar(RolesUsers $rolesUsers)
    {
        $estadoCancelado = EstadoRegistro::obtenerEstadoRegistroCancelado();
        $requestData['user_id'] = $rolesUsers->user_id;
        $requestData['rol_id'] = $rolesUsers->rol_id;
        $requestData['estado_registro_id'] = $estadoCancelado->id;


        $rolesUsers->update($requestData);
        return back()->with('status', 'Rol cancelado con exito');
    }

    /**
     * @param integer $user_id
     * @return bool
     */
    public static function guardarRolEstudiante($user_id)
    {
        $user = (is_object($user_id)) ? $user_id->id : $user_id;
        if (($rol = Roles::obtenerRolEstudiante()) && !($rolUser = RolesUsers::obtenerRolUser($user, $rol->id))) {
            $estado = EstadoRegistro::obtenerEstadoRegistroRevision();
            $registro['user_id'] = $user;
            $registro['rol_id'] = $rol->id;
            $registro['estado_registro_id'] = $estado->id;

            if (!($model = RolesUsers::create($registro))) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param User|integer $user_id
     * @return bool
     */
    public static function guardarRolRepresentante($user_id)
    {
        $user = (is_object($user_id)) ? $user_id->id : $user_id;
        if (($rol = Roles::obtenerRolRepresentante()) && !($rolUser = RolesUsers::obtenerRolUser($user, $rol->id))) {
            $estado = EstadoRegistro::obtenerEstadoRegistroRevision();
            $registro['user_id'] = $user;
            $registro['rol_id'] = $rol->id;
            $registro['estado_registro_id'] = $estado->id;

            if (!($model = RolesUsers::create($registro))) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param integer $user_id
     * @return bool
     */
    public static function guardarRolDocente($user_id)
    {
        $user = (is_object($user_id)) ? $user_id->id : $user_id;
        if (($rol = Roles::obtenerRolDocente()) && !($rolUser = RolesUsers::obtenerRolUser($user, $rol->id))) {
            $estado = EstadoRegistro::obtenerEstadoRegistroRevision();
            $registro['user_id'] = $user;
            $registro['rol_id'] = $rol->id;
            $registro['estado_registro_id'] = $estado->id;

            if (!($model = RolesUsers::create($registro))) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param integer $user_id
     * @return bool
     */
    public static function aprobarRolEstudiante($user_id)
    {
        $user = (is_object($user_id)) ? $user_id->id : $user_id;
        if (($rol = Roles::obtenerRolEstudiante()) && ($rolUser = RolesUsers::obtenerRolUser($user, $rol->id)) && $rolUser) {
            $estado = EstadoRegistro::obtenerEstadoRegistroAprobado();
            $registro['estado_registro_id'] = $estado->id;

            if (!($rolUser->update($registro))) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param integer $user_id
     * @return bool
     */
    public static function aprobarRolDocente($user_id)
    {
        $user = (is_object($user_id)) ? $user_id->id : $user_id;
        if (($rol = Roles::obtenerRolDocente()) && ($rolUser = RolesUsers::obtenerRolUser($user, $rol->id)) && $rolUser) {
            $estado = EstadoRegistro::obtenerEstadoRegistroAprobado();
            $registro['estado_registro_id'] = $estado->id;

            if (!($rolUser->update($registro))) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param integer $user_id
     * @return bool
     */
    public static function aprobarRolRepresentante($user_id)
    {
        $user = (is_object($user_id)) ? $user_id->id : $user_id;
        if (($rol = Roles::obtenerRolRepresentante()) && ($rolUser = RolesUsers::obtenerRolUser($user, $rol->id)) && $rolUser) {
            $estado = EstadoRegistro::obtenerEstadoRegistroAprobado();
            $registro['estado_registro_id'] = $estado->id;

            if (!($rolUser->update($registro))) {
                return false;
            }
        }

        return true;
    }
}
