<?php

namespace App\Http\Controllers;

use App\Http\Requests\Asignatura\StorePost;
use App\Http\Requests\Asignatura\UpdatePut;
use App\Models\Asignatura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AsignaturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'acceso-backend']);
    }

    public function index(Request $request)
    {
        Asignatura::$search = ($request->has('search')) ? request('search') : null;
        Asignatura::$paginate = 10;
        $asignaturas = Asignatura::obtenerAsignaturaPaginateAll();

        return view('backend.asignatura.index', ['asignaturas' => $asignaturas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Asignatura();
        return view('backend.asignatura.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return redirect('admin.curso.create')
                ->withErrors($validator)
                ->withInput();
        }

        if ($model = Asignatura::create($requestData)) {
            return back()->with('status', 'Asignatura creada con exito');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Asignatura $asignatura)
    {
        return view('backend.asignatura.show', ["model" => $asignatura]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Asignatura $asignatura)
    {
        return view('backend.asignatura.edit', ["model" => $asignatura]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, Asignatura $asignatura)
    {
        $asignatura->update($request->validated());
        return back()->with('status', 'Asignatura actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asignatura $asignatura)
    {
        $asignatura->delete();
        return back()->with('status', 'Asignatura eliminada con exito');

    }
}
