<?php

namespace App\Http\Controllers;

use App\Helpers\Archivos;
use App\Http\Requests\OpcionPreguntaDesarrollo\StorePostImagen;
use App\Http\Requests\OpcionPreguntaDesarrollo\StorePostSeleccion;
use App\Models\OpcionPreguntaDesarrollo;
use App\Models\PreguntaDesarrollo;
use App\Rules\OnOff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OpcionPreguntaDesarrolloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'acceso-backend']);
    }

    /**
     * @param Request|array $request
     * @param PreguntaDesarrollo $preguntaDesarrollo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function index(Request $request = null, PreguntaDesarrollo $preguntaDesarrollo)
    {
        $opcionesPreguntaDesarrollo = OpcionPreguntaDesarrollo::obtenerOpcionPreguntaDesarrolloPreguntaDesarrolloAll($preguntaDesarrollo->id);


        if (isset($_GET['ajax']) && $_GET['ajax']) {
            return ['status' => true, 'html' => view('backend.opcion_pregunta.index', ['opcionesPreguntaDesarrollo' => $opcionesPreguntaDesarrollo])->render()];
        }

        return view('backend.opcion_pregunta.index', ['opcionesPreguntaDesarrollo' => $opcionesPreguntaDesarrollo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PreguntaDesarrollo $preguntaDesarrollo
     * @return \Illuminate\Http\Response
     */
    public function create(PreguntaDesarrollo $preguntaDesarrollo)
    {
        $preguntaDesarrollos = PreguntaDesarrollo::obtenerPreguntaDesarrolloTestAll($preguntaDesarrollo->test_id);
        return view('backend.pregunta.create', ['test' => $preguntaDesarrollo->test, 'model' => $preguntaDesarrollo, 'preguntaDesarrollos' => $preguntaDesarrollos, 'opciones' => true]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeImagen(StorePostImagen $request, PreguntaDesarrollo $preguntaDesarrollo)
    {
        $onOff = New OnOff();
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePostImagen::myRules());

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('imagen')) {
            if ($_imagen = Archivos::storeImagen($preguntaDesarrollo->tipoPregunta->codigo, $request->imagen, 'opcion_imagen')) {
                $requestData['imagen'] = $_imagen;
                $requestData['pregunta_desarrollo_id'] = $preguntaDesarrollo->id;
                $requestData['tipo_calificacion_id'] = $request->hasFile('tipo_calificacion_id') ? $requestData->tipo_calificacion_id : $preguntaDesarrollo->test->tipo_calificacion_id;
                $requestData['respuesta'] = $onOff->passes('imagen', $request->respuesta);

                if ($model = OpcionPreguntaDesarrollo::create($requestData)) {
                    return ['status' => 'Opcion creada con exito'];
                }
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeSeleccion(StorePostSeleccion $request, PreguntaDesarrollo $preguntaDesarrollo)
    {
        //dd($request, $preguntaDesarrollo);
        $onOff = New OnOff();
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePostSeleccion::myRules());

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $requestData['pregunta_desarrollo_id'] = $preguntaDesarrollo->id;
        $requestData['tipo_calificacion_id'] = $request->hasFile('tipo_calificacion_id') ? $requestData->tipo_calificacion_id : $preguntaDesarrollo->test->tipo_calificacion_id;
        $requestData['respuesta'] = $onOff->passes('respuesta', $request->respuesta);

        if ($model = OpcionPreguntaDesarrollo::create($requestData)) {
            return ['status' => 'Opcion creada con exito'];
        }
    }

//    /**
//     * Update the specified resource in storage.
//     *
//     * @param  \Illuminate\Http\Request $request
//     * @param  \App\Models\OpcionPreguntaDesarrollo $opcionPreguntaDesarrollo
//     * @return \Illuminate\Http\Response
//     */
//    public function update(Request $request, OpcionPreguntaDesarrollo $opcionPreguntaDesarrollo)
//    {
//        //
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OpcionPreguntaDesarrollo $opcionPreguntaDesarrollo
     * @return \Illuminate\Http\Response
     */
    public function destroy(OpcionPreguntaDesarrollo $opcionPreguntaDesarrollo)
    {
        $opcionPreguntaDesarrollo->delete();
        return ['success' => 'Opcion eliminada correctamente'];
    }
}
