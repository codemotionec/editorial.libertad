<?php

namespace App\Http\Controllers;

use App\Helpers\Archivos;
use App\Helpers\Helper;
use App\Http\Requests\Libro\StorePost;
use App\Http\Requests\Libro\UpdatePut;
use App\Models\Archivo;
use App\Models\Libro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'acceso-backend']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Libro::$search = ($request->has('search')) ? request('search') : null;
        Libro::$paginate = 10;
        $libros = Libro::obtenerLibroPaginateAll();

        return view('backend.libro.index', ['libros' => $libros]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Libro();
        return view('backend.libro.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return redirect('admin.libro.create')
                ->withErrors($validator)
                ->withInput();
        }

        $requestData['anio_registro'] = date('Y');
        if ($model = Libro::create($requestData)) {
            if ($request->hasFile('portada')) {
                $model->portada = Archivos::storeImagen($request->titulo, $request->portada, 'portada_libro');
                $model->update();
            }

            return redirect()->route('admin.libro.index')->with('status', 'Libro creado con exito');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Libro $libro
     * @return \Illuminate\Http\Response
     */
    public function show(Libro $libro)
    {
        return view('backend.libro.show', ["model" => $libro]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Libro $libro
     * @return \Illuminate\Http\Response
     */
    public function edit(Libro $libro)
    {
        return view('backend.libro.edit', ["model" => $libro]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Libro $libro
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, Libro $libro)
    {
        if ($request->hasFile('portada')) {
            $libro->portada = Archivos::storeImagen($request->titulo, $request->portada, 'portada_libro');
        }

        $libro->update($request->validated());
        return back()->with('status', 'Libro actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Libro $libro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Libro $libro)
    {
        $libro->delete();
        return back()->with('status', 'Post eliminado con exito');
    }
}
