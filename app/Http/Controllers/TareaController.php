<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tarea\StorePost;
use App\Http\Requests\Tarea\UpdatePut;
use App\Models\SegmentoLibro;
use App\Models\Tarea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SegmentoLibro $segmentoLibro)
    {
        $tarea = New Tarea();
        $tareas = Tarea::obtenerTareasSegmentoLibroAll($segmentoLibro->id);
        return view('backend.tarea.create', ['segmentoLibro' => $segmentoLibro, 'model' => $tarea, 'tareas' => $tareas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request, SegmentoLibro $segmentoLibro)
    {
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return redirect('admin.abreviaturas.create')
                ->withErrors($validator)
                ->withInput();
        }

        $requestData['segmento_libro_id'] = $segmentoLibro->id;
        if ($model = Tarea::create($requestData)) {
            return back()->with('status', 'Tarea creada con exito');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tarea $tarea
     * @return \Illuminate\Http\Response
     */
    public function edit(Tarea $tarea)
    {
        $tareas = Tarea::obtenerTareasSegmentoLibroAll($tarea->segmento_libro_id);
        return view('backend.tarea.create', ['segmentoLibro' => $tarea->segmentoLibro, 'model' => $tarea, 'tareas' => $tareas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Tarea $tarea
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, Tarea $tarea)
    {
        $tarea->update($request->validated());
        return back()
            ->with('status', 'Tarea actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tarea $tarea
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tarea $tarea)
    {
        $tarea->delete();
        return back()->with('status','Tarea eliminada con exito.');
    }
}
