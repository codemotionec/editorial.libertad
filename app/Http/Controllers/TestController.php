<?php

namespace App\Http\Controllers;

use App\Http\Requests\Test\StorePost;
use App\Http\Requests\Test\UpdatePut;
use App\Models\SegmentoLibro;
use App\Models\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SegmentoLibro $segmentoLibro)
    {
        $test = New Test();
        $tests = Test::obtenerTestSegmentoLibroAll($segmentoLibro->id);
        return view('backend.test.create', ['segmentoLibro' => $segmentoLibro, 'model' => $test, 'tests' => $tests]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request, SegmentoLibro $segmentoLibro)
    {
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $requestData['segmento_libro_id'] = $segmentoLibro->id;
        if ($model = Test::create($requestData)) {
            return back()->with('status', 'Tarea creada con exito');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Test $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
        $tests = Test::obtenerTestSegmentoLibroAll($test->segmento_libro_id);
        return view('backend.test.create', ['segmentoLibro' => $test->segmentoLibro, 'model' => $test, 'tests' => $tests]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, Test $test)
    {
        $test->update($request->validated());
        return back()
            ->with('status', 'Test actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        $test->delete();
        return back()->with('status','Test eliminado con exito.');
    }
}
