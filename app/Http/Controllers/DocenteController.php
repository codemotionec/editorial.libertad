<?php

namespace App\Http\Controllers;

use App\Models\Docente;
use App\Models\EstadoRegistro;
use App\Models\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth','acceso-backend']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $docentes = Docente::obtenerDocentesAll();
        return view('backend.docente.index', compact('docentes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Docente $docente
     * @return \Illuminate\Http\Response
     */
    public function show(Docente $docente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Docente $docente
     * @return \Illuminate\Http\Response
     */
    public function edit(Docente $docente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Docente $docente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Docente $docente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Docente $docente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Docente $docente)
    {
        //
    }

    /**
     * @param $user_id
     * @param $rol_id
     * @return bool
     */
    public static function crearDocenteRol($user_id, $rol_id)
    {
        if (($rol = Roles::find($rol_id)) && $rol->key == 'D') {
            if (!($docente = Docente::obtenerDocenteUser($user_id))) {
                $estadoRegistro = EstadoRegistro::obtenerEstadoRegistroRevision();
                if (Docente::create(['user_id' => $user_id, 'estado_registro_id' => $estadoRegistro->id])) {
                    return true;
                }
            }
        }

        return false;
    }

    public function aprobar(Docente $docente)
    {
        DB::beginTransaction();

        try {
            $estadoAprobado = EstadoRegistro::obtenerEstadoRegistroAprobado();
            $requestData['estado_registro_id'] = $estadoAprobado->id;
            $requestData['user_autoriza_id'] = Auth::user()->id;

            if ($docente->update($requestData)) {
                if (RolesUsersController::aprobarRolDocente($docente->user_id)) {
                    DB::commit();
                    return back()->with('status', 'Estudiante aprobado con exito');
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

        $estadoAprobado = EstadoRegistro::obtenerEstadoRegistroAprobado();
        $requestData['estado_registro_id'] = $estadoAprobado->id;
        $requestData['user_autoriza_id'] = Auth::user()->id;

        $docente->update($requestData);
        return back()->with('status', 'Docente aprobado con exito');
    }

    public function cancelar(Docente $docente)
    {
        $estadoAprobado = EstadoRegistro::obtenerEstadoRegistroCancelado();
        $requestData['estado_registro_id'] = $estadoAprobado->id;

        $docente->update($requestData);
        return back()->with('status', 'Docente cancelado con exito');
    }
}
