<?php

namespace App\Http\Controllers;

use App\Http\Requests\Abreviatura\StorePost;
use App\Http\Requests\Abreviatura\UpdatePut;
use App\Models\Abreviatura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AbreviaturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'acceso-backend']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Abreviatura::$search = ($request->has('search')) ? request('search') : null;
        Abreviatura::$paginate = 10;
        $abreviaturas = Abreviatura::obtenerAbreviaturaPaginateAll();

        return view('backend.abreviatura.index', ['abreviaturas' => $abreviaturas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Abreviatura();
        return view('backend.abreviatura.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return redirect('admin.abreviaturas.create')
                ->withErrors($validator)
                ->withInput();
        }

        if ($model = Abreviatura::create($requestData)) {
            return back()->with('status', 'Abreviatura creada con exito');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Abreviatura $abreviatura
     * @return \Illuminate\Http\Response
     */
    public function show(Abreviatura $abreviatura)
    {
        return view('backend.abreviatura.show', ["model" => $abreviatura]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Abreviatura $abreviatura
     * @return \Illuminate\Http\Response
     */
    public function edit(Abreviatura $abreviatura)
    {
        return view('backend.abreviatura.edit', ["model" => $abreviatura]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Abreviatura $abreviatura
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, Abreviatura $abreviatura)
    {
        $abreviatura->update($request->validated());
        return back()->with('status', 'Abreviatura actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Abreviatura $abreviatura
     * @return \Illuminate\Http\Response
     */
    public function destroy(Abreviatura $abreviatura)
    {
        $abreviatura->delete();
        return back()->with('status', 'Abreviatura eliminada con exito');
    }
}
