<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Estudiante;
use App\Models\MaterialDocente;
use Illuminate\Http\Request;

class MaterialDocenteController extends Controller
{
    public static function materialDocenteLibroSelect2(Estudiante $estudiante)
    {
        $data = [];
        $materialDocentes = MaterialDocente::obtenerMaterialDocenteEstudianteOne(null, $estudiante, true);

        foreach ($materialDocentes as $materialDocente) {
            $libro = $materialDocente->libro;
            $data[] = ['id' => $materialDocente->id, 'text' => "{$libro->asignatura->nombre} / {$libro->titulo} ({$libro->curso->nombre})"];
        }

        return $data;
    }

    public static function inputCodigoRegistroMaterialDocente(Request $request)
    {
        $materialDocentes = MaterialDocente::find($request->id);

        $numero_curso_codigo = strlen($materialDocentes->libro->curso->codigo);
        $numero_asignatura_codigo = strlen($materialDocentes->libro->asignatura->codigo);
        $numero_registro = 5;


        $curso_codigo = $materialDocentes->libro->curso->codigo;
        $asignatura_codigo = $materialDocentes->libro->asignatura->codigo;
        $registro = '00000';

        return [
            'blocks' => [$numero_asignatura_codigo, $numero_curso_codigo, $numero_registro],
            //'blocks' => "{$numero_asignatura_codigo} {$numero_curso_codigo} {$numero_registro}",
            'placeholder' => "{$asignatura_codigo}-{$curso_codigo}-{$registro}",
            'value' => "{$asignatura_codigo}-{$curso_codigo}-",
        ];
    }
}
