<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TipoCalificacion;
use Illuminate\Http\Request;

class TipoCalificacionController extends Controller
{
    public static function tipoCalificacionSelect2(Request $request)
    {
        $data = [];
        $tipoCalificacions = TipoCalificacion::obtenerTipoCalificacionAll() ?? [];

        foreach ($tipoCalificacions as $tipoCalificacion) {
            $data[] = ['id' => $tipoCalificacion->id, 'text' => $tipoCalificacion->descripcion];
        }

        return $data;
    }
}
