<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Perfil;
use Illuminate\Http\Request;

class PerfilController extends Controller
{
    public static function obtenerPerfilCedula(Request $request)
    {
        return Perfil::obtenerPerfilCedulaIdentidad($request->cedula) ?? null;
    }
}
