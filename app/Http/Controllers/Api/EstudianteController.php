<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Aula;
use App\Models\Estudiante;
use App\User;
use Illuminate\Http\Request;

class EstudianteController extends Controller
{
    public static function estudianteUserSelect2(User $user)
    {
        $data = [];
        $estudiantes = Estudiante::obtenerEstudianteAprobadosUserAll($user->id) ?? [];

        foreach ($estudiantes as $estudiantes) {
            $data[] = ['id' => $estudiantes->id, 'text' => "{$estudiantes->nombre_institucion} - {$estudiantes->nombre_aula}"];
        }

        return $data;
    }

    public static function estudiantesPorAulaSelect2(Request $request, Aula $aula)
    {
        $data = [];
        $estudiantes = Estudiante::obtenerEstudiantesAula($aula) ?? [];

        foreach ($estudiantes as $estudiante) {
            $data[] = ['id' => $estudiante->id, 'text' => "{$estudiante->nombre_estudiante}"];
        }

        return $data;
    }
}
