<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Curso;
use Illuminate\Http\Request;

class CursoController extends Controller
{
    public static function cursoSelect2(Request $request)
    {
        $data = [];
        $cursos = Curso::obtenerCursoAll() ?? [];

        foreach ($cursos as $curso) {
            $data[] = ['id' => $curso->id, 'text' => $curso->nombre];
        }

        return $data;
    }
}
