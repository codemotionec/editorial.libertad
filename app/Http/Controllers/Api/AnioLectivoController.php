<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AnioLectivo;
use Illuminate\Http\Request;

class AnioLectivoController extends Controller
{
    public static function aniolectivoSelect2(Request $request)
    {
        $data = [];
        $aniosLectivos = AnioLectivo::obtenerAnioLectivoAll() ?? [];

        foreach ($aniosLectivos as $anioLectivo) {
            $data[] = ['id' => $anioLectivo->id, 'text' => $anioLectivo->descripcion];
        }

        return $data;
    }
}
