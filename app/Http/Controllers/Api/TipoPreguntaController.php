<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TipoPregunta;
use App\Models\TipoRespuesta;
use Illuminate\Http\Request;

class TipoPreguntaController extends Controller
{
    public static function tipoPreguntaSelect2(Request $request)
    {
        $data = [];
        $tipoPreguntas = TipoPregunta::obtenerTipoPreguntaAll() ?? [];

        foreach ($tipoPreguntas as $tipoPregunta) {
            $data[] = ['id' => $tipoPregunta->id, 'text' => $tipoPregunta->descripcion];
        }

        return $data;
    }
}
