<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TipoDesarrollo;
use Illuminate\Http\Request;

class TipoDesarrolloController extends Controller
{
    public static function tipoDesarrolloSelect2(Request $request)
    {
        $data = [];
        $tipoDesarrollos = TipoDesarrollo::obtenerTipoDesarrolloAll() ?? [];

        foreach ($tipoDesarrollos as $tipoDesarrollo) {
            $data[] = ['id' => $tipoDesarrollo->id, 'text' => $tipoDesarrollo->descripcion];
        }

        return $data;
    }
}
