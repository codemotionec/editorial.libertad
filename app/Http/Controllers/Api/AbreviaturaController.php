<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Abreviatura;
use Illuminate\Http\Request;

class AbreviaturaController extends Controller
{
    public static function abreviaturaSelect2(Request $request)
    {
        $data = [];
        $abreviaturas = Abreviatura::obtenerAbreviaturaAll() ?? [];

        foreach ($abreviaturas as $abreviatura) {
            $data[] = ['id' => $abreviatura->id, 'text' => $abreviatura->codigo];
        }

        return $data;
    }
}
