<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Docente;
use App\Models\Estudiante;
use App\Models\Libro;
use Illuminate\Http\Request;

class LibroController extends Controller
{
    public static function libroSelect2(Request $request)
    {
        $data = [];
        Libro::$search = $request->search;
        $libros = Libro::obtenerLibroAll() ?? [];

        foreach ($libros as $libro) {
            $data[] = ['id' => $libro->id, 'text' => "{$libro->titulo} ({$libro->curso->nombre})"];
        }

        return $data;
    }

    public static function libroDocenteSelect2(Docente $docente)
    {
        $data = [];
        $libros = Libro::obtenerLibroCursoAll($docente->aula->curso_id) ?? [];

        foreach ($libros as $libro) {
            $data[] = ['id' => $libro->id, 'text' => "{$libro->titulo} ({$libro->curso->nombre})"];
        }

        return $data;
    }

    public static function libroEstudianteSelect2(Estudiante $estudiante)
    {
        $data = [];
        $libros = Libro::obtenerLibroCursoAll($estudiante->aula->curso_id) ?? [];

        foreach ($libros as $libro) {
            $data[] = ['id' => $libro->id, 'text' => "{$libro->asignatura->nombre} / {$libro->titulo} ({$libro->curso->nombre})"];
        }

        return $data;
    }
}
