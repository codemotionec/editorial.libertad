<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Docente;
use App\User;
use Illuminate\Http\Request;

class DocenteController extends Controller
{
    public static function docenteUserSelect2(User $user)
    {
        $data = [];
        $docentes = Docente::obtenerDocentesAprobadosUserAll($user->id) ?? [];

        foreach ($docentes as $docente) {
            $data[] = ['id' => $docente->id, 'text' => "{$docente->nombre_institucion} - {$docente->nombre_aula}"];
        }

        return $data;
    }
}
