<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ParametroSubjetivo;
use App\Models\TipoRespuesta;
use Illuminate\Http\Request;

class ParametroSubjetivoController extends Controller
{
    public static function parametroSubjetivoSelect2(Request $request)
    {
        $data = [];
        $parametros = ParametroSubjetivo::obtenerParametroSubjetivoAll() ?? [];

        foreach ($parametros as $parametro) {
            $data[] = ['id' => $parametro->id, 'text' => $parametro->descripcion];
        }

        return $data;
    }
}
