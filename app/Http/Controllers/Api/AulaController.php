<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Aula;
use App\Models\Institucion;
use Illuminate\Http\Request;

class AulaController extends Controller
{
    public static function aulaSelect2(Request $request)
    {
        $data = [];
        Aula::$search = $request->search;
        $aulas = Aula::obtenerAulasAll() ?? [];

        foreach ($aulas as $aula) {
            $data[] = ['id' => $aula->id, 'text' => "{$aula->institucion->nombre} {$aula->curso->nombre} ({$aula->paralelo})"];
        }

        return $data;
    }

    public static function aulaPorInstitucionSelect2(Institucion $institucion)
    {
        $data = [];
        $aulas = Aula::obtenerAulasPorInstitucion($institucion) ?? [];

        foreach ($aulas as $aula) {
            $data[] = ['id' => $aula->id, 'text' => "{$aula->curso->nombre} ({$aula->paralelo})"];
        }

        return $data;
    }
}
