<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Canton;
use App\Models\Institucion;
use Illuminate\Http\Request;

class InstitucionController extends Controller
{
    public static function institucionSelect2(Request $request, Canton $canton)
    {
        $data = [];
        Institucion::$search = $request->search;
        Institucion::$filter_canton_id = $canton->id;
        $instituciones = Institucion::obtenerInstitucionAll() ?? [];

        foreach ($instituciones as $institucion) {
            $data[] = [
                'id' => $institucion->id,
                'text' => $institucion->nombre_codigo,
                'title' => $institucion->nombre_parroquia,
                //"element" => 'HTMLOptionElement',
            ];
        }

        return $data;
    }

    public function obtenerInformacionInstitucionHtml(Request $request)
    {
        $html = "";
        if ($institucion = Institucion::find($request->id)) {
            $html = view('includes.forms.institucion-info', compact('institucion'))->render();
        }

        return ['html' => $html];
    }
}
