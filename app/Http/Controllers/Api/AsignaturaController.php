<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Asignatura;
use Illuminate\Http\Request;

class AsignaturaController extends Controller
{
    public static function asignaturaSelect2(Request $request)
    {
        $data = [];
        $asignaturas = Asignatura::obtenerAsignaturaAll() ?? [];

        foreach ($asignaturas as $asignatura) {
            $data[] = ['id' => $asignatura->id, 'text' => $asignatura->nombre];
        }

        return $data;
    }
}
