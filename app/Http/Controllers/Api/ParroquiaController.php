<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Canton;
use App\Models\Parroquia;
use Illuminate\Http\Request;

class ParroquiaController extends Controller
{
    public static function parroquiaSelect2(Request $request, Canton $canton)
    {
        $data = [];
        $parroquias = Parroquia::where('canton_id', $canton->id)->get() ?? [];

        foreach ($parroquias as $parroquia) {
            $data[] = ['id' => $parroquia->id, 'text' => $parroquia->nombre];
        }

        return $data;
    }
}
