<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TipoRespuesta;
use Illuminate\Http\Request;

class TipoRespuestaController extends Controller
{
    public static function tipoRespuestaSelect2(Request $request)
    {
        $data = [];
        $tipoRespuestas = TipoRespuesta::obtenerTipoRespuestaAll() ?? [];

        foreach ($tipoRespuestas as $tipoRespuesta) {
            $data[] = ['id' => $tipoRespuesta->id, 'text' => $tipoRespuesta->descripcion];
        }

        return $data;
    }
}
