<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Roles;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    public static function rolesSelect2(Request $request)
    {
        $data = [];
        $roles = Roles::obtenerRolesRegistro() ?? [];

        foreach ($roles as $rol) {
            $data[] = ['id' => $rol->id, 'text' => $rol->name];
        }

        return $data;
    }
}
