<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\AnioLectivo\StorePost;
use App\Http\Requests\AnioLectivo\UpdatePut;
use App\Models\AnioLectivo;
use App\Models\EstadoRegistro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AnioLectivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'acceso-backend']);
    }

    public function index(Request $request)
    {
        AnioLectivo::$search = ($request->has('search')) ? request('search') : null;
        AnioLectivo::$paginate = 10;
        $aniosLectivos = AnioLectivo::obtenerAnioLectivoPaginateAll();

        return view('backend.aniolectivo.index', ['aniosLectivos' => $aniosLectivos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new AnioLectivo();
        return view('backend.aniolectivo.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return redirect('admin.anio-lectivo.create')
                ->withErrors($validator)
                ->withInput();
        }

        $requestData['anio'] = Helper::formatDate($request->fecha_inicio, 'Y');
        $requestData['estado_registro_id'] = ($estado = EstadoRegistro::obtenerEstadoRegistroActivo()) ? $estado->id : null;


        if ($anioLectivo = AnioLectivo::create($requestData)) {
            return back()->with('status', 'Año Lectivo creado con exito');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(AnioLectivo $anioLectivo)
    {
        return view('backend.aniolectivo.show', ["model" => $anioLectivo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AnioLectivo $anioLectivo)
    {
        return view('backend.aniolectivo.edit', ["model" => $anioLectivo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, AnioLectivo $anioLectivo)
    {
        $requestData = $request->validated();
        $requestData['anio'] = Helper::formatDate($request->fecha_inicio, 'Y');

        $anioLectivo->update($requestData);
        return back()->with('status', 'Post actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnioLectivo $anioLectivo)
    {
//        $estado = EstadoRegistro::obtenerEstadoRegistroInactivo();
//        $anioLectivo->estado_registro_id = $estado->id;
//        if ($anioLectivo->save()) {
//            return back()->with('status', 'Post eliminado con exito');
//        }

        $anioLectivo->delete();
        return back()->with('status', 'Post eliminado con exito');
    }
}
