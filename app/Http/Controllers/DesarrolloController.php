<?php

namespace App\Http\Controllers;

use App\Helpers\Archivos;
use App\Models\Desarrollo;
use App\Models\Tarea;
use Illuminate\Http\Request;
use App\Http\Requests\Desarrollo\StorePost;
use App\Http\Requests\Desarrollo\UpdatePut;
use Illuminate\Support\Facades\Validator;

class DesarrolloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Tarea $tarea)
    {
        $model = New Desarrollo();
        $desarrollos = Desarrollo::obtenerDesarrollosTareaAll($tarea->id);
        return view('backend.desarrollo.create', ['desarrollos' => $desarrollos, 'model' => $model, 'tarea' => $tarea]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request, Tarea $tarea)
    {
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $requestData['tarea_id'] = $tarea->id;
        if ($model = Desarrollo::create($requestData)) {
            if ($request->hasFile('imagen')) {
                $model->imagen = Archivos::storeImagen('desarrollo-' . $model->id, $request->imagen, 'desarrollo_imagen');
                $model->update();
            }

            return back()->with('status', 'Tarea creada con exito');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\desarrollo $desarrollo
     * @return \Illuminate\Http\Response
     */
    public function edit(desarrollo $desarrollo)
    {
        $desarrollos = Desarrollo::obtenerDesarrollosTareaAll($desarrollo->tarea_id);
        return view('backend.desarrollo.create', ['desarrollos' => $desarrollos, 'model' => $desarrollo, 'tarea' => $desarrollo->tarea]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\desarrollo $desarrollo
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, desarrollo $desarrollo)
    {
        $requestData = $request->validated();
        if ($request->hasFile('imagen')) {
            $requestData['imagen'] = Archivos::storeImagen('desarrollo-' . $desarrollo->id, $request->imagen, 'desarrollo_imagen');
        }

        $desarrollo->update($requestData);

        return back()
            ->with('status', 'Tarea actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\desarrollo $desarrollo
     * @return \Illuminate\Http\Response
     */
    public function destroy(desarrollo $desarrollo)
    {
        $desarrollo->delete();
        return back()->with('status', 'Desarrollo eliminado con exito de la tarea.');
    }
}
