<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\Aula\StorePost;
use App\Http\Requests\Aula\UpdatePut;
use App\Models\Aula;
use App\Models\Curso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'acceso-backend']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        dd($request);
//        Curso::$search = ($request->has('search')) ? request('search') : null;
//        Curso::$paginate = 10;
        $aulas = Aula::obtenerAulasPaginateAll();
        return view('backend.aula.index', compact('aulas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = New Aula();
        return view('backend.aula.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $requestData = $request->validated();
        $validator = Validator::make($requestData, StorePost::myRules());

        if ($validator->fails()) {
            return redirect('admin.institucion.create')
                ->withErrors($validator)
                ->withInput();
        }

        if (!($aula = Aula::obtenerAulaInstitucionOne($request->institucion_id, $request->curso_id, $request->paralelo))) {
            if ($model = Aula::create($requestData)) {
                return back()->with('status', 'Aula creada con exito');
            }
        } else {
            $msn = $aula->nombre_institucion . ' | ';
            $msn .= $aula->nombre_curso;

            return back()->with('error', 'Ya existe los parametros ingresados | ' . $msn);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Curso $curso
     * @return \Illuminate\Http\Response
     */
    public function show(Aula $aula)
    {
        dd($aula);
        return view('backend.aula.show', ["model" => $aula]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Curso $curso
     * @return \Illuminate\Http\Response
     */
    public function edit(Aula $aula)
    {
        return view('backend.aula.edit', ["model" => $aula]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Curso $curso
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePut $request, Aula $aula)
    {
        $aula = Aula::obtenerAulaInstitucionOne($request->institucion_id, $request->curso_id, $request->paralelo) ?? $aula;
        if ($aula->update($request->validated())) {
            return back()->with('status', 'Aula actualizado con exito');
        }
//        } else {
//            $msn = $curso->institucion()->first()->nombre . ' | ';
//            $msn .= $curso->paralelo()->first()->descripcion . ' | ';
//            $msn .= $curso->anioLectivo()->first()->descripcion;
//
//            return back()->with('error', 'Ya existe los parametros ingresados | ' . $msn);
//        }

//        return back()->with('status', 'Post actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Curso $curso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Aula $aula)
    {
        $aula->delete();
        return back()->with('status', 'Registro eliminado con exito');
    }
}
