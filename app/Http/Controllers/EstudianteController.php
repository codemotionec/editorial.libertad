<?php

namespace App\Http\Controllers;

use App\Models\EstadoRegistro;
use App\Models\Estudiante;
use App\Models\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estudiantes = Estudiante::obtenerEstudiantesPaginate();
        return view('backend.estudiante.index', compact('estudiantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Roles $roles)
    {
        //dd($request, $roles);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Estudiante $estudiante
     * @return \Illuminate\Http\Response
     */
    public function show(Estudiante $estudiante)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Estudiante $estudiante
     * @return \Illuminate\Http\Response
     */
    public function edit(Estudiante $estudiante)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Estudiante $estudiante
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estudiante $estudiante)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Estudiante $estudiante
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estudiante $estudiante)
    {
        //
    }

    public static function crearEstudianteRol($user_id, $rol_id)
    {
        if (($rol = Roles::find($rol_id)) && $rol->key == 'E') {
            if (!($estudiante = Estudiante::obtenerEstudianteUser($user_id))) {
                $estadoRegistro = EstadoRegistro::obtenerEstadoRegistroRevision();
                if (Estudiante::create(['user_id' => $user_id, 'estado_registro_id' => $estadoRegistro->id])) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param Estudiante $estudiante
     * @return \Illuminate\Http\RedirectResponse
     */
    public function aprobar(Estudiante $estudiante)
    {
        DB::beginTransaction();

        try {
            $estadoAprobado = EstadoRegistro::obtenerEstadoRegistroAprobado();
            $requestData['estado_registro_id'] = $estadoAprobado->id;
            $requestData['user_autoriza_id'] = Auth::user()->id;

            if ($estudiante->update($requestData)) {
                if (RolesUsersController::aprobarRolEstudiante($estudiante->user_id)) {
                    DB::commit();
                    return back()->with('status', 'Estudiante aprobado con exito');
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function cancelar(Estudiante $estudiante)
    {
        $estadoCancelado = EstadoRegistro::obtenerEstadoRegistroCancelado();
        $requestData['estado_registro_id'] = $estadoCancelado->id;

        $estudiante->update($requestData);
        return back()->with('status', 'Estudiante cancelado con exito');
    }
}
