<?php

namespace App\Http\Controllers;

use App\Helpers\Archivos;
use App\Models\Libro;
use App\Models\SegmentoLibro;
use Illuminate\Http\Request;
use App\Http\Requests\SegmentoLibro\UpdatePut;

class SegmentoLibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Libro $libro)
    {
        $segmentoLibro = New SegmentoLibro();
        $segmentoLibros = SegmentoLibro::obtenerSegmentoLibroAll($libro->id);
        return view('backend.segmento_libro.cargar', ['libro' => $libro, 'model' => $segmentoLibro, 'segmentoLibros' => $segmentoLibros]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdatePut $request, Libro $libro)
    {
        $requestData = $request->validated();
        if ($request->hasFile('documento')) {
            if (($nombre = Archivos::storeArchivoLibro($libro->titulo, $request->documento, 'libros')) && !empty($nombre)) {
                $requestData['documento'] = $nombre;
                $requestData['libro_id'] = $libro->id;

                if ($model = SegmentoLibro::create($requestData)) {
                    return back()->with('status', 'Segmento Libro cargado con exito');
                }
            }

            return back()->with('errors', '!Error Archivo Libro no ingresado en el sistema');
        }

        return back()->with('errors', 'Ya existe un libro cargado con las mismas caracteristicas');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SegmentoLibro  $segmentoLibro
     * @return \Illuminate\Http\Response
     */
    public function show(SegmentoLibro $segmentoLibro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SegmentoLibro  $segmentoLibro
     * @return \Illuminate\Http\Response
     */
    public function edit(SegmentoLibro $segmentoLibro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SegmentoLibro  $segmentoLibro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SegmentoLibro $segmentoLibro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SegmentoLibro  $segmentoLibro
     * @return \Illuminate\Http\Response
     */
    public function destroy(SegmentoLibro $segmentoLibro)
    {
        $segmentoLibro->delete();
        return back()->with('status', 'Segmento Libro eliminado con exito');
    }
}
