<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')
    ->get('/user', function (Request $request) {
        return $request->user();
    });

/*
 * Data para componente Select2
 */
Route::get('/obtener-canton-select2', 'Api\CantonController@cantonSelect2')->name('api.canton.select2');
Route::post('/obtener-canton-select2', 'Api\CantonController@cantonSelect2')->name('api.canton.select2');

Route::get('/obtener-parroquia-select2/{canton}', 'Api\ParroquiaController@parroquiaSelect2')->name('api.parroquia.select2');

Route::get('/obtener-anio-lectivo-select2', 'Api\AnioLectivoController@aniolectivoSelect2')->name('api.anio-lectivo.select2');
Route::post('/obtener-anio-lectivo-select2', 'Api\AnioLectivoController@aniolectivoSelect2')->name('api.anio-lectivo.select2');

Route::get('/obtener-asignatura-select2', 'Api\AsignaturaController@asignaturaSelect2')->name('api.asignatura.select2');
Route::post('/obtener-asignatura-select2', 'Api\AsignaturaController@asignaturaSelect2')->name('api.asignatura.select2');

Route::get('/obtener-abreviatura-select2', 'Api\AbreviaturaController@abreviaturaSelect2')->name('api.abreviatura.select2');
Route::get('/obtener-roles-select2', 'Api\RolesController@rolesSelect2')->name('api.roles.select2');


Route::get('/obtener-institucion-select2', 'Api\InstitucionController@institucionSelect2')->name('api.institucion.select2');
Route::post('/obtener-institucion-select2', 'Api\InstitucionController@institucionSelect2')->name('api.institucion.select2');
Route::get('/obtener-institucion-select2/{canton}', 'Api\InstitucionController@institucionSelect2')->name('api.institucion.select2');
Route::post('/obtener-institucion-info', 'Api\InstitucionController@obtenerInformacionInstitucionHtml')->name('api.institucion.info');

Route::get('/obtener-curso-select2', 'Api\CursoController@cursoSelect2')->name('api.curso.select2');
Route::post('/obtener-curso-select2', 'Api\CursoController@cursoSelect2')->name('api.curso.select2');

Route::get('/obtener-tipo-desarrollo-select2', 'Api\TipoDesarrolloController@tipoDesarrolloSelect2')->name('api.tipo_desarrollo.select2');

Route::get('/obtener-tipo-calificacion-select2', 'Api\TipoCalificacionController@tipoCalificacionSelect2')->name('api.tipo_calificacion.select2');

Route::get('/obtener-tipo-respuesta-select2', 'Api\TipoRespuestaController@tipoRespuestaSelect2')->name('api.tipo_respuesta.select2');

Route::get('/obtener-tipo-pregunta-select2', 'Api\TipoPreguntaController@tipoPreguntaSelect2')->name('api.tipo_pregunta.select2');

Route::get('/obtener-aula-select2', 'Api\AulaController@aulaSelect2')->name('api.aula.select2');
Route::get('/obtener-aula-select2/{institucion}', 'Api\AulaController@aulaPorInstitucionSelect2')->name('api.aula-institucion.select2');
Route::post('/obtener-aula-select2', 'Api\AulaController@aulaSelect2')->name('api.aula.select2');

Route::get('/obtener-libro-select2', 'Api\LibroController@libroSelect2')->name('api.libro.select2');
Route::post('/obtener-libro-select2', 'Api\LibroController@libroSelect2')->name('api.libro.select2');

Route::get('/obtener-libro-docente-select2/{docente}', 'Api\LibroController@libroDocenteSelect2')->name('api.libro-docente.select2');
Route::get('/obtener-libro-estudiante-select2/{estudiante}', 'Api\LibroController@libroEstudianteSelect2')->name('api.libro-estudiante.select2');

Route::get('/obtener-docente-user-select2/{user}', 'Api\DocenteController@docenteUserSelect2')->name('api.docente-user.select2');

Route::get('/obtener-estudiante-user-select2/{user}', 'Api\EstudianteController@estudianteUserSelect2')->name('api.estudiante-user.select2');

Route::get('/material-docente-select2/{estudiante}', 'Api\MaterialDocenteController@materialDocenteLibroSelect2')->name('api.material-docente.select2');
Route::post('/material-docente-codigo', 'Api\MaterialDocenteController@inputCodigoRegistroMaterialDocente')->name('api.material-docente.codigo');

Route::post('/perfil/cedula', 'Api\PerfilController@obtenerPerfilCedula')->name('api.perfil.cedula');

Route::get('/obtener-estudiantes-select2/{aula}', 'Api\EstudianteController@estudiantesPorAulaSelect2')->name('api.estudiantes-aula.select2');
Route::post('/obtener-estudiantes-select2/{aula}', 'Api\EstudianteController@estudiantesPorAulaSelect2')->name('api.estudiantes-aula.select2');

Route::get('/parametro-subjetivo-select2', 'Api\ParametroSubjetivoController@parametroSubjetivoSelect2')->name('api.parametro-subjetivo.select2');