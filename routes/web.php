<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Corporativas
Route::get('/', 'Corporativa\FrontendController@home')->name('corporativa.home');

Route::get('/test', function () {
    return view('corporativa/home');
})->name('corporativa.test');

Route::as('corporativa.')
    ->prefix('libertad')
    ->group(
        function () {
            Route::get('acerca-de', 'Corporativa\FrontendController@acerca_de')->name('acerca_de');
            Route::get('contacto', 'Corporativa\FrontendController@contacto')->name('contacto');
            Route::get('libreria', 'Corporativa\FrontendController@libreria')->name('libreria');
            Route::get('instituciones', 'Corporativa\FrontendController@instituciones')->name('instituciones');
        }
    );

// Aplicacion
Route::as('app.')
    ->prefix('app')
    ->group(
        function () {
            Route::get('/', 'Aplicacion\FrontendController@dashboard')->name('dashboard');
            Route::get('mis-suscripciones', 'Aplicacion\FrontendController@mis_suscripciones')->name('representante-suscripciones');
            Route::get('listado-de-tareas', 'Aplicacion\FrontendController@listado_tareas')->name('representante-tareas');
            Route::get('listado-de-tests', 'Aplicacion\FrontendController@listado_tests')->name('representante-tests');

            /**
             * Rutas para desarrollar los libros por el usuario
             */
            Route::get('libros', 'Aplicacion\FrontendController@listado_libros')->name('libros');
            Route::get('/archivo/{segmentoLibro}/revista', 'Aplicacion\LibroController@revista_archivo')->name('archivo.revista');
            Route::get('/archivo/{segmentoLibro}/download', 'Aplicacion\LibroController@downloadSegmentoLibro')->name('archivo.download');
//            Route::get('/tarea/{tarea}/desarrollo', 'Aplicacion\LibroController@desarrolloTarea')->name('tarea.desarrollo');
            Route::get('/test/{test}/desarrollo', 'Aplicacion\LibroController@desarrolloTest')->name('test.desarrollo');

            /**
             * Desarrollo de Tareas
             */
            Route::get('/desarrollo/{material_estudiante}', 'Aplicacion\DesarrolloController@index')
                ->name('desarrollo.index')
                ->middleware('auth');
            Route::get('/desarrollo/{material_estudiante}/{tarea}', 'Aplicacion\DesarrolloController@tarea')
                ->name('desarrollo.tarea')
                ->middleware('auth');
            Route::post('/desarrollo/{materialEstudiante}', 'Aplicacion\DesarrolloController@store')
                ->name('desarrollo.store')
                ->middleware('auth');

            /**
             * Rutas para administrar el perfil del usuario
             */
            Route::get('/user/perfil', 'Aplicacion\UserController@perfil')->name('user.perfil');
            Route::post('/user/perfil/{user}/store', 'Aplicacion\UserController@storePerfil')->name('user.perfil.store');
            Route::post('/user/perfil/{user}/store_rol_user', 'Aplicacion\UserController@storeRolUser')->name('user.rol_user.store');
            Route::delete('/user/perfil/destroy_rol_user/{rolesUsers}', 'Aplicacion\UserController@destroyRol')->name('user.rol_user.destroy');

            /**
             * Rutas para registrar los libros por el usuario
             */
            Route::resource('material-estudiante', 'Aplicacion\MaterialEstudianteController');

            /**
             * Registrar docente
             */
            Route::resource('docente', 'Aplicacion\DocenteController');
            Route::get('/docente/estudiante/detalle', 'Aplicacion\DocenteController@detalleEstudiantesAprobar')->name('docente.estudiante.index');
            Route::get('/docente/estudiante/aprobar/{estudiante}', 'Aplicacion\DocenteController@aprobarEstudiante')->name('docente.estudiante.aprobar');
            Route::get('/docente/estudiante/cancelar/{estudiante}', 'Aplicacion\DocenteController@cancelarEstudiante')->name('docente.estudiante.cancelar');
            Route::get('/docente/representante/aprobar/{representante_estudiante}', 'Aplicacion\DocenteController@aprobarRepresentante')->name('docente.representante.aprobar');
            Route::resource('material-docente', 'Aplicacion\MaterialDocenteController');

            /**
             * Registrar estudiante
             */
            Route::resource('estudiante', 'Aplicacion\EstudianteController');

            /**
             * Representante
             */
            Route::resource('representante-estudiante', 'Aplicacion\RepresentanteEstudianteController');
            Route::post('representante-estudiante/asignar', 'Aplicacion\RepresentanteEstudianteController@asignarEstudianteRepresentante')->name('representante-estudiante.asignar');
            Route::get('representante-estudiante/cambiar/{user}', 'Aplicacion\RepresentanteEstudianteController@cambiarUsuarioEstudiante')->name('representante-estudiante.cambiar');

            /**
             * Calificar tareas
             */
            Route::resource('tarea', 'Aplicacion\TareaController');
            Route::get('tarea/calificar/{materialDocente}', 'Aplicacion\TareaController@calificar')->name('tarea.calificar');
            Route::get('tarea/calificar/{materialDocente}/{segmentoLibro}', 'Aplicacion\TareaController@calificar')
                ->name('tarea.calificar.segmento');
            Route::get('tarea/calificar/{materialDocente}/tarea/{tarea}', 'Aplicacion\TareaController@calificarTarea')
                ->name('tarea.calificar.tarea');

            Route::post('tarea/{materialEstudiante}/{desarrollo}', 'Aplicacion\TareaController@store')->name('tarea.calificar.store');

//            Route::get('detalle-registro-libro', 'Aplicacion\EstudianteController@detalleRegistroLibro')->name('estudiante.detalle-registro-libro');
//            Route::get('registrar-libro', 'Aplicacion\EstudianteController@registroLibro')->name('estudiante.registrar-libro');

//            Route::get('instituciones', 'Aplicacion\InstitucionController@index')->name('institucion.index');
//            Route::get('/institucion/create', 'Aplicacion\InstitucionController@create')->name('institucion.create');
//            Route::post('/institucion/store', 'Aplicacion\InstitucionController@store')->name('institucion.store');
//            Route::delete('/institucion/destroy/{estudiante}', 'Aplicacion\InstitucionController@destroy')->name('institucion.destroy');
        }
    );

// Adminitrator
Route::get('/admin', 'HomeController@index')
    ->name('admin.home');

Route::as('admin.')
    ->prefix('admin')
    ->group(
        function () {
            Route::resource('abreviatura', 'AbreviaturaController');
            Route::resource('asignatura', 'AsignaturaController');
            Route::resource('anio-lectivo', 'AnioLectivoController');
            Route::resource('institucion', 'InstitucionController');
            Route::resource('libro', 'LibroController');
            Route::resource('curso', 'CursoController');
            Route::resource('aula', 'AulaController');

            Route::resource('segmento_libro', 'SegmentoLibroController');
            Route::get('/segmento_libro/{libro}/index', 'SegmentoLibroController@index')->name('segmento_libro.index');
            Route::post('/segmento_libro/{libro}/store', 'SegmentoLibroController@store')->name('segmento_libro.store');

            Route::resource('tarea', 'TareaController');
            Route::get('/tarea/{segmentoLibro}/index', 'TareaController@index')->name('tarea.index');
            Route::post('/tarea/{segmentoLibro}/store', 'TareaController@store')->name('tarea.store');

            Route::resource('desarrollo', 'DesarrolloController');
            Route::get('/desarrollo/{tarea}/index', 'DesarrolloController@index')->name('desarrollo.index');
            Route::post('/desarrollo/{tarea}/store', 'DesarrolloController@store')->name('desarrollo.store');

            Route::resource('respuesta-desarrollo', 'RespuestaDesarrolloController');
            Route::post('/respuesta-desarrollo/{desarrollo}/store', 'RespuestaDesarrolloController@store')->name('respuesta-desarrollo.store');

            Route::resource('test', 'TestController');
            Route::get('/test/{segmentoLibro}/index', 'TestController@index')->name('test.index');
            Route::post('/test/{segmentoLibro}/store', 'TestController@store')->name('test.store');

            Route::resource('pregunta-desarrollo', 'PreguntaDesarrolloController');
            Route::get('/pregunta-desarrollo/{test}/index', 'PreguntaDesarrolloController@index')->name('pregunta-desarrollo.index');
            Route::post('/pregunta-desarrollo/{test}/store', 'PreguntaDesarrolloController@store')->name('pregunta-desarrollo.store');

            Route::resource('opcion-pregunta-desarrollo', 'OpcionPreguntaDesarrolloController');
            Route::get('/opcion-pregunta-desarrollo/{pregunta_desarrollo}/create', 'OpcionPreguntaDesarrolloController@create')->name('opcion-pregunta-desarrollo.create');
            Route::get('/opcion-pregunta-desarrollo/{pregunta_desarrollo}/index', 'OpcionPreguntaDesarrolloController@index')->name('opcion-pregunta-desarrollo.index');
            Route::post('/opcion-pregunta-desarrollo/{pregunta_desarrollo}/store.imagen', 'OpcionPreguntaDesarrolloController@storeImagen')->name('opcion-pregunta-desarrollo.store.imagen');
            Route::post('/opcion-pregunta-desarrollo/{pregunta_desarrollo}/store.seleccion', 'OpcionPreguntaDesarrolloController@storeSeleccion')->name('opcion-pregunta-desarrollo.store.seleccion');

            Route::resource('roles_users', 'RolesUsersController');
            Route::get('/roles_users/{roles_users}/aprobar', 'RolesUsersController@aprobar')->name('roles_users.aprobar');
            Route::get('/roles_users/{roles_users}/cancelar', 'RolesUsersController@cancelar')->name('roles_users.cancelar');

            Route::resource('docente', 'DocenteController');
            Route::get('/docente/{docente}/aprobar', 'DocenteController@aprobar')->name('docente.aprobar');
            Route::get('/docente/{docente}/cancelar', 'DocenteController@cancelar')->name('docente.cancelar');

            Route::resource('estudiante', 'EstudianteController');
            Route::get('/estudiante/{estudiante}/aprobar', 'EstudianteController@aprobar')->name('estudiante.aprobar');
            Route::get('/estudiante/{estudiante}/cancelar', 'EstudianteController@cancelar')->name('estudiante.cancelar');

            Route::resource('material-estudiante', 'MaterialEstudianteController');
            Route::get('/material-estudiante/{material_estudiante}/aprobar', 'MaterialEstudianteController@aprobar')->name('material-estudiante.aprobar');
            Route::get('/material-estudiante/{material_estudiante}/cancelar', 'MaterialEstudianteController@cancelar')->name('material-estudiante.cancelar');
        }
    );

// Autenticate
Auth::routes();
Route::as('auth.')
    ->prefix('auth')
    ->group(
        function () {
            Route::post('login', 'Aplicacion\AuthController@login')->name('login');
            Route::get('register', 'Aplicacion\AuthController@create')->name('register');
            Route::post('store', 'Aplicacion\AuthController@store')->name('store');
            Route::post('logout', 'Aplicacion\AuthController@logout')->name('logout');
            //Route::get('/salir', 'Aplicacion\SuscriptorController@logout')->name('logout-suscriptor');
            //Route::get('/entrar', 'Auth\LoginController@showLoginForm')->name('entrar');
        }
    );
