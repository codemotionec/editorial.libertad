<?php
/**
 * Created by PhpStorm.
 * User: ZBOOK
 * Date: 05/08/2020
 * Time: 21:45
 */

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// Inicio
Breadcrumbs::for('admin.home', function ($trail) {
    $trail->push('Inicio', route('admin.home'));
});


Breadcrumbs::for('admin.anio-lectivo.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Listado', route('admin.anio-lectivo.index'));
    $trail->push(__('Registro'), route('admin.anio-lectivo.create'));
});

Breadcrumbs::for('admin.paralelo.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Listado', route('admin.paralelo.index'));
    $trail->push(__('Registro'), route('admin.paralelo.create'));
});

Breadcrumbs::for('admin.asignatura.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Listado', route('admin.asignatura.index'));
    $trail->push(__('Registro'), route('admin.asignatura.create'));
});

Breadcrumbs::for('admin.abreviatura.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Listado', route('admin.abreviatura.index'));
    $trail->push(__('Registro'), route('admin.abreviatura.create'));
});

Breadcrumbs::for('admin.institucion.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Listado', route('admin.institucion.index'));
    $trail->push(__('Registro'), route('admin.institucion.create'));
});

Breadcrumbs::for('admin.libro.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Listado', route('admin.libro.index'));
    $trail->push(__('Registro'), route('admin.libro.create'));
});

Breadcrumbs::for('admin.curso.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Listado', route('admin.curso.index'));
    $trail->push(__('Registro'), route('admin.curso.create'));
});

Breadcrumbs::for('admin.aula.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Listado', route('admin.aula.index'));
    $trail->push(__('Registro'), route('admin.aula.create'));
});


/**
 * Rutas APP
 */

Breadcrumbs::for('app.docente.index', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Docente', route('app.docente.index'));
});

Breadcrumbs::for('app.docente.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Docentes', route('app.docente.index'));
    $trail->push(__('Registro'), route('app.docente.create'));
});

Breadcrumbs::for('app.estudiante.index', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Estudiantes', route('app.estudiante.index'));
});

Breadcrumbs::for('app.estudiante.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Estudiantes', route('app.estudiante.index'));
    $trail->push(__('Registro'), route('app.estudiante.create'));
});

Breadcrumbs::for('app.registro-libro.index', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Registro Libros', route('app.material-estudiante.index'));
});

Breadcrumbs::for('app.registro-libro.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Registro Libros', route('app.material-estudiante.index'));
    $trail->push(__('Registro'), route('app.material-estudiante.create'));
});

Breadcrumbs::for('app.material-docente.index', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Libros asignados', route('app.material-docente.index'));
});

Breadcrumbs::for('app.material-docente.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Libros asignados', route('app.material-docente.index'));
    $trail->push(__('Registro'), route('app.material-docente.create'));
});

Breadcrumbs::for('app.representante-estudiante.index', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Mis Estudiantes', route('app.representante-estudiante.index'));
});

Breadcrumbs::for('app.representante-estudiante.create', function ($trail) {
    $trail->push('Home', route('admin.home'));
    $trail->push('Mis Estudiantes', route('app.representante-estudiante.index'));
    $trail->push(__('Registrar mi estudiante'), route('app.representante-estudiante.create'));
});
