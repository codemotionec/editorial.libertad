<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnioLectivoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anio_lectivo', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion', 500);
            $table->integer('anio');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->string('codigo', 50)->nullable();
            $table->unsignedBigInteger('institucion_id')->nullable();
            $table->unsignedBigInteger('estado_registro_id')->nullable();

            $table->foreign('institucion_id')
                ->references('id')
                ->on('institucion')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('estado_registro_id')
                ->references('id')
                ->on('estado_registro')
                ->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anio_lectivo');
    }
}
