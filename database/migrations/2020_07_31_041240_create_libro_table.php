d<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libro', function (Blueprint $table) {
            $table->id();
//            $table->unsignedBigInteger('anio_lectivo_id');
            $table->unsignedBigInteger('curso_id');
            $table->unsignedBigInteger('asignatura_id');
            $table->string('titulo', 500);
            $table->text('resumen');
            $table->text('contenido');
            $table->string('portada', 500);
            $table->integer('anio_registro');

            $table->string('autor', 500)->nullable();
            $table->string('editorial', 500)->nullable();
            $table->string('indice_contenidos', 500)->nullable();

//            $table->foreign('anio_lectivo_id')
//                ->references('id')
//                ->on('anio_lectivo')
//                ->constrained()
//                ->onDelete('cascade');

            $table->foreign('curso_id')
                ->references('id')
                ->on('curso')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('asignatura_id')
                ->references('id')
                ->on('asignatura')
                ->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libro');
    }
}
