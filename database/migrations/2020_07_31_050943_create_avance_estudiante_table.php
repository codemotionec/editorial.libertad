<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvanceEstudianteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avance_estudiante', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('test_id');
            $table->unsignedBigInteger('tarea_id');
            $table->unsignedBigInteger('material_estudiante_id');

            $table->foreign('test_id')->references('id')->on('test')->constrained()
                ->onDelete('cascade');
            $table->foreign('tarea_id')->references('id')->on('tarea')->constrained()
                ->onDelete('cascade');
            $table->foreign('material_estudiante_id')->references('id')->on('material_estudiante')->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avance_test');
    }
}
