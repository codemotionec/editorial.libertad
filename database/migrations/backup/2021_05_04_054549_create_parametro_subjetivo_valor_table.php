<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParametroSubjetivoValorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parametro_subjetivo_valor', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parametro_subjetivo_id');
            $table->decimal('calificacion', 8, 2);

            $table->foreign('parametro_subjetivo_id')
                ->references('id')
                ->on('parametro_subjetivo')
                ->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parametro_subjetivo_valor');
    }
}
