<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialDocenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_docente', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('docente_id');
            $table->unsignedBigInteger('libro_id');
            $table->unsignedBigInteger('estado_registro_id');

            $table->foreign('docente_id')
                ->references('id')
                ->on('docente')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('libro_id')
                ->references('id')
                ->on('libro')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('estado_registro_id')
                ->references('id')
                ->on('estado_registro')
                ->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_docente');
    }
}
