<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpcionPreguntaDesarrolloTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opcion_pregunta_desarrollo', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pregunta_desarrollo_id');
            $table->unsignedBigInteger('tipo_calificacion_id')->nullable();
            $table->text('descripcion')->nullable();
            $table->string('imagen', 250)->nullable();
            $table->boolean('respuesta');
            $table->foreign('pregunta_desarrollo_id')
                ->references('id')
                ->on('pregunta_desarrollo')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('tipo_calificacion_id')
                ->references('id')
                ->on('tipo_calificacion')
                ->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opcion_pregunta_desarrollo');
    }
}
