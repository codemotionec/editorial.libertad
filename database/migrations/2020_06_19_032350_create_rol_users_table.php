<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles_users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('rol_id');
            $table->unsignedBigInteger('estado_registro_id');
            $table->foreign('rol_id')
                ->references('id')
                ->on('roles')->constrained()
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')->constrained()
                ->onDelete('cascade');
            $table->foreign('estado_registro_id')
                ->references('id')
                ->on('estado_registro')->constrained()
                ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rol_users');
    }
}
