<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsignaturaDocenteEstudianteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asignatura_docente_estudiante', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('estudiante_id');
            $table->unsignedBigInteger('asignatura_docente_id');
            $table->unsignedBigInteger('estado_registro_id');

            $table->foreign('estudiante_id')
                ->references('id')
                ->on('estudiante')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('asignatura_docente_id')
                ->references('id')
                ->on('asignatura_docente')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('estado_registro_id')
                ->references('id')
                ->on('estado_registro')
                ->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asignatura_docente_estudiante');
    }
}
