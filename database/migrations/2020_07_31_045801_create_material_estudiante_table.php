<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialEstudianteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_estudiante', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('material_docente_id');
            $table->unsignedBigInteger('estudiante_id');
            $table->unsignedBigInteger('estado_registro_id');
            $table->unsignedBigInteger('user_autoriza_id')->nullable();
            $table->string('codigo', 50)->unique();

            $table->foreign('material_docente_id')
                ->references('id')
                ->on('material_docente')->constrained()
                ->onDelete('cascade');

            $table->foreign('estudiante_id')
                ->references('id')
                ->on('estudiante')->constrained()
                ->onDelete('cascade');

            $table->foreign('estado_registro_id')
                ->references('id')
                ->on('estado_registro')->constrained()
                ->onDelete('cascade');

            $table->foreign('user_autoriza_id')
                ->references('id')
                ->on('users')->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_estudiante');
    }
}
