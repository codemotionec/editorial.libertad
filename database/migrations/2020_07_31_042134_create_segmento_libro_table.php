<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSegmentoLibroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('segmento_libro', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('libro_id');
            $table->string('unidad', 200);
            $table->text('resumen');
            $table->string('documento', 500);

            $table->foreign('libro_id')->references('id')->on('libro')->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contenido_libro');
    }
}
