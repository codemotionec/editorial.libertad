<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesarrolloTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desarrollo', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tarea_id');
//            $table->unsignedBigInteger('tipo_respuesta_id')->nullable();
            $table->text('descripcion');
//            $table->text('pregunta')->nullable();

//            $table->foreign('tipo_respuesta_id')
//                ->references('id')
//                ->on('tipo_respuesta')->constrained()
//                ->onDelete('cascade');

            $table->foreign('tarea_id')
                ->references('id')
                ->on('tarea')->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desarrollo');
    }
}
