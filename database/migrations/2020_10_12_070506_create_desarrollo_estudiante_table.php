<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesarrolloEstudianteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desarrollo_estudiante', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('material_estudiante_id');
            $table->unsignedBigInteger('respuesta_desarrollo_id');
            $table->string('archivo', 250)->nullable();
            $table->text('respuesta')->nullable();
            $table->unsignedBigInteger('tipo_calificacion_id')->nullable();
            $table->string('calificacion')->nullable();
            $table->unsignedBigInteger('user_califica_id')->nullable();


            $table->foreign('material_estudiante_id')
                ->references('id')
                ->on('material_estudiante')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('respuesta_desarrollo_id')
                ->references('id')
                ->on('respuesta_desarrollo')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('tipo_calificacion_id')
                ->references('id')
                ->on('tipo_calificacion')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('user_califica_id')
                ->references('id')
                ->on('users')
                ->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desarrollo_estudiante_test');
    }
}
