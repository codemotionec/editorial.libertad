<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstitucionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institucion', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('canton_id');
            $table->unsignedBigInteger('parroquia_id');
            $table->string('nombre');
            $table->string('codigo', 500)->nullable();
            $table->string('zona_inec', 500)->nullable();
            $table->string('direccion', 500)->nullable();
            $table->string('escolarizacion', 500)->nullable();
            $table->string('tipo_educacion', 500)->nullable();
            $table->string('nivel_educacion', 500)->nullable();
            $table->string('sostenimiento', 500)->nullable();
            $table->string('regimen_escolar', 500)->nullable();
            $table->string('jurisdiccion', 500)->nullable();
            $table->string('modalidad', 500)->nullable();
            $table->string('jornada', 500)->nullable();
            $table->string('imagen', 500)->nullable();

            $table->foreign('canton_id')
                ->references('id')
                ->on('canton')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('parroquia_id')
                ->references('id')
                ->on('parroquia')
                ->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institucion');
    }
}
