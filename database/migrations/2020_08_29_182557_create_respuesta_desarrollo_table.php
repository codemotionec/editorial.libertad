<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespuestaDesarrolloTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuesta_desarrollo', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('desarrollo_id');
            $table->unsignedBigInteger('tipo_respuesta_id');
            $table->text('observacion')->nullable();

            $table->foreign('desarrollo_id')
                ->references('id')
                ->on('desarrollo')->constrained()
                ->onDelete('cascade');

            $table->foreign('tipo_respuesta_id')
                ->references('id')
                ->on('tipo_respuesta')->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuesta_desarrollo');
    }
}
