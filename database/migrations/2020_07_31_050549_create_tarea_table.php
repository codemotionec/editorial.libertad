<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTareaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarea', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tipo_desarrollo_id');
            $table->unsignedBigInteger('docente_id')->nullable();
            $table->unsignedBigInteger('segmento_libro_id');
            $table->unsignedBigInteger('tipo_calificacion_id');
            $table->text('objetivo');
            $table->date('fecha_ejecucion')->nullable();
            $table->date('fecha_terminacion')->nullable();

            $table->foreign('docente_id')
                ->references('id')
                ->on('docente')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('segmento_libro_id')
                ->references('id')
                ->on('segmento_libro')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('tipo_desarrollo_id')
                ->references('id')
                ->on('tipo_desarrollo')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('tipo_calificacion_id')
                ->references('id')
                ->on('tipo_calificacion')
                ->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tareas');
    }
}
