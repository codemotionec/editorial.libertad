<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCalificacionParametroSubjetivoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parametro_subjetivo', function (Blueprint $table) {
            $table->decimal('calificacion_inicio', 8, 2)->nullable();
            $table->decimal('calificacion_fin', 8, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parametro_subjetivo', function (Blueprint $table) {
            //
        });
    }
}
