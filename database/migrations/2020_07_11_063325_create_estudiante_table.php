<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudianteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiante', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('aula_id');
            $table->unsignedBigInteger('anio_lectivo_id');
            $table->unsignedBigInteger('estado_registro_id');
            $table->unsignedBigInteger('user_autoriza_id')->nullable();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('aula_id')
                ->references('id')
                ->on('aula')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('anio_lectivo_id')
                ->references('id')
                ->on('anio_lectivo')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('estado_registro_id')
                ->references('id')
                ->on('estado_registro')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('user_autoriza_id')
                ->references('id')
                ->on('users')
                ->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumno');
    }
}
