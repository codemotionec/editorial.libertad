<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAulaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aula', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('curso_id');
            $table->unsignedBigInteger('institucion_id');
            $table->string('paralelo', 10);

            $table->foreign('curso_id')
                ->references('id')
                ->on('curso')->constrained()
                ->onDelete('cascade');

            $table->foreign('institucion_id')
                ->references('id')
                ->on('institucion')->constrained()
                ->onDelete('cascade');

//            $table->unsignedBigInteger('estudiante_id');
//            $table->unsignedBigInteger('asignacion_docente_id');
//
//            $table->foreign('estudiante_id')->references('id')->on('estudiante')->constrained()
//                ->onDelete('cascade');
//            $table->foreign('asignacion_docente_id')->references('id')->on('asignacion_docente')->constrained()
//                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aula');
    }
}
