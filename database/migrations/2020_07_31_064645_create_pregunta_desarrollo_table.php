<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreguntaDesarrolloTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pregunta_desarrollo', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('test_id')->nullable();
            $table->unsignedBigInteger('tarea_id')->nullable();
            $table->unsignedBigInteger('tipo_pregunta_id');
            $table->text('pregunta');

            $table->foreign('test_id')
                ->references('id')
                ->on('test')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('tarea_id')
                ->references('id')
                ->on('tarea')
                ->constrained()
                ->onDelete('cascade');

            $table->foreign('tipo_pregunta_id')
                ->references('id')
                ->on('tipo_pregunta')
                ->constrained()
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregunta_test');
    }
}
