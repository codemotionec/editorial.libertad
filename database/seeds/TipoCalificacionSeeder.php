<?php

use Illuminate\Database\Seeder;
use App\Models\TipoCalificacion;

class TipoCalificacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table tipo_calificacion; SET FOREIGN_KEY_CHECKS = 1;";
        DB::connection()->getPdo()->exec($sql);

        TipoCalificacion::create(['codigo' => 'SUBJETIVA', 'descripcion' => 'Subjetiva']);
        TipoCalificacion::create(['codigo' => 'CUALITATIVA', 'descripcion' => 'Cualitativa']);
        TipoCalificacion::create(['codigo' => 'CUANTITATIVA', 'descripcion' => 'Cuantitativa']);
    }
}
