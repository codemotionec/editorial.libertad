<?php

use Illuminate\Database\Seeder;
use App\Imports\AulasImport;
use Maatwebsite\Excel\Excel;

class AulaSeeder extends Seeder
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table aula; SET FOREIGN_KEY_CHECKS = 1;";
        DB::connection()->getPdo()->exec($sql);

        $path = base_path('insumos/Aulas.xlsx');
        return $this->excel->import(new AulasImport, $path);
    }
}
