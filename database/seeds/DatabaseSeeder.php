<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesSeeder::class,
            TipoPreguntaSeeder::class,
            TipoCalificacionSeeder::class,
            TipoDesarrolloSeeder::class,
            TipoRespuestaSeeder::class,
            EstadoRegistroSeeder::class,
            AbreviaturasSeeder::class,
            ProvinciaSeeder::class,
            CantonSeeder::class,
            UserSeeder::class,
            ParametroSubjetivoSeeder::class,
            ParroquiaSeeder::class,
            InstitucionSeeder::class,
            CursoSeeder::class,
            AulaSeeder::class,
        ]);
    }
}
