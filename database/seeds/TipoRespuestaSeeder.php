<?php

use Illuminate\Database\Seeder;
use App\Models\TipoRespuesta;
class TipoRespuestaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table tipo_respuesta; SET FOREIGN_KEY_CHECKS = 1;";
        DB::connection()->getPdo()->exec($sql);

        TipoRespuesta::create(['codigo' => 'ARCHIVO', 'descripcion' => 'Cargar Archivo']);
        TipoRespuesta::create(['codigo' => 'IMAGEN', 'descripcion' => 'Cargar Imagen']);
        TipoRespuesta::create(['codigo' => 'AUDIO', 'descripcion' => 'Cargar Audio']);
        TipoRespuesta::create(['codigo' => 'VIDEO', 'descripcion' => 'Cargar Video']);
        TipoRespuesta::create(['codigo' => 'TEXTO', 'descripcion' => 'Campo Libre']);
    }
}
