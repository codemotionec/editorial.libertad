<?php

use Illuminate\Database\Seeder;
use App\Models\Abreviatura;

class AbreviaturasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table abreviatura; SET FOREIGN_KEY_CHECKS = 1;";
        DB::connection()->getPdo()->exec($sql);

        Abreviatura::create(['codigo' => 'Sr', 'descripcion' => 'Señor']);
        Abreviatura::create(['codigo' => 'Sra', 'descripcion' => 'Señora']);
        Abreviatura::create(['codigo' => 'Dr', 'descripcion' => 'Doctor']);
        Abreviatura::create(['codigo' => 'Lcdo', 'descripcion' => 'Licenciado']);
    }
}
