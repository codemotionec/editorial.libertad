<?php

use Illuminate\Database\Seeder;
use App\Models\EstadoRegistro;

class EstadoRegistroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table estado_registro; SET FOREIGN_KEY_CHECKS = 1;";
        DB::connection()->getPdo()->exec($sql);

        // Añadimos una entrada a esta tabla
        EstadoRegistro::create(array('nombre' => 'En Revisión', 'color' => 'warning', 'codigo' => 'RE'));
        EstadoRegistro::create(array('nombre' => 'Aprobado', 'color' => 'success', 'codigo' => 'AP'));
        EstadoRegistro::create(array('nombre' => 'No Aprobado', 'color' => 'danger', 'codigo' => 'NP'));

        //Estados Activos / Inactivos
        EstadoRegistro::create(array('nombre' => 'Activo', 'color' => 'danger', 'codigo' => 'AC'));
        EstadoRegistro::create(array('nombre' => 'Inactivo', 'color' => 'danger', 'codigo' => 'IN'));

    }
}
