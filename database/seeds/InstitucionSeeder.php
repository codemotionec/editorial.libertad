<?php

use Illuminate\Database\Seeder;
use App\Imports\InstitucionesImport;
use Maatwebsite\Excel\Excel;

class InstitucionSeeder extends Seeder
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table institucion; SET FOREIGN_KEY_CHECKS = 1;";
        DB::connection()->getPdo()->exec($sql);

        $path = base_path('insumos/Instituciones.xlsx');
        return $this->excel->import(new InstitucionesImport, $path);
    }
}
