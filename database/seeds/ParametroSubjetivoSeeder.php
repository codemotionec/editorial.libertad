<?php

use Illuminate\Database\Seeder;
use App\Models\ParametroSubjetivo;

class ParametroSubjetivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table parametro_subjetivo; SET FOREIGN_KEY_CHECKS = 1;";
        DB::connection()->getPdo()->exec($sql);

        ParametroSubjetivo::create([
            'orden' => 1,
            'codigo' => 'Excelente',
            'descripcion' => 'Excelente',
            'calificacion_inicio' => 9,
            'calificacion_fin' => 10,
        ]);
        ParametroSubjetivo::create([
            'orden' => 2,
            'codigo' => 'Muy bien',
            'descripcion' => 'Muy bien',
            'calificacion_inicio' => 6,
            'calificacion_fin' => 8.99,
        ]);
        ParametroSubjetivo::create([
            'orden' => 3,
            'codigo' => 'Bien',
            'descripcion' => 'Bien',
            'calificacion_inicio' => 4,
            'calificacion_fin' => 5.99,
        ]);
        ParametroSubjetivo::create([
            'orden' => 4,
            'codigo' => 'Regular',
            'descripcion' => 'Regular',
            'calificacion_inicio' => 2,
            'calificacion_fin' => 3.99,
        ]);
        ParametroSubjetivo::create([
            'orden' => 5,
            'codigo' => 'Malo',
            'descripcion' => 'No corresponde a lo que revisamos en esta unidad, hazlo de nuevo',
            'calificacion_inicio' => 0,
            'calificacion_fin' => 1.99,
        ]);
    }
}
