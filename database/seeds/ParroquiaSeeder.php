<?php

use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Excel;
use App\Imports\ParroquiasImport;

class ParroquiaSeeder extends Seeder
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table parroquia; SET FOREIGN_KEY_CHECKS = 1;";
        DB::connection()->getPdo()->exec($sql);

        $path = base_path('insumos/Parroquias.xlsx');
        return $this->excel->import(new ParroquiasImport, $path);
    }
}
