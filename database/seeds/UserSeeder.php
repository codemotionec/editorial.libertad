<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Roles;
use App\Models\RolesUsers;
use App\Models\EstadoRegistro;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SET FOREIGN_KEY_CHECKS = 0; 
        TRUNCATE table users;
        TRUNCATE table roles_users; 
        SET FOREIGN_KEY_CHECKS = 1;";

        DB::connection()->getPdo()->exec($sql);

        $user = new User();
        $user->email = 'paul.codemotion@gmail.com';
        $user->password = Hash::make('1718652462');
        $user->verification_token = sha1($user);
        $user->save();

        if ($user->save()) {
            if (($rol = Roles::obtenerRolAdmin()) && ($estado = EstadoRegistro::obtenerEstadoRegistroAprobado())) {
                RolesUsers::create([
                    'rol_id' => $rol->id,
                    'user_id' => $user->id,
                    'estado_registro_id' => $estado->id,
                ]);
            }
        }



        $user = new User();
        $user->email = 'ventas.edilibertadec@gmail.com';
        $user->password = Hash::make('Ilusionarte2020');
        $user->verification_token = sha1($user);
        $user->save();

        if ($user->save()) {
            if (($rol = Roles::obtenerRolAdmin()) && ($estado = EstadoRegistro::obtenerEstadoRegistroAprobado())) {
                RolesUsers::create([
                    'rol_id' => $rol->id,
                    'user_id' => $user->id,
                    'estado_registro_id' => $estado->id,
                ]);
            }
        }
    }
}
