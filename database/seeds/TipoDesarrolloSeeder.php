<?php

use Illuminate\Database\Seeder;
use App\Models\TipoDesarrollo;
class TipoDesarrolloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table tipo_desarrollo; SET FOREIGN_KEY_CHECKS = 1;";
        DB::connection()->getPdo()->exec($sql);

        TipoDesarrollo::create(['codigo' => 'PD', 'descripcion' => 'Proyecto de Destreza']);
        TipoDesarrollo::create(['codigo' => 'AC', 'descripcion' => 'Actividades']);
        TipoDesarrollo::create(['codigo' => 'PB', 'descripcion' => 'Proyecto de Bloque']);
        TipoDesarrollo::create(['codigo' => 'TL', 'descripcion' => 'Taller de lectura comprensiva']);
        TipoDesarrollo::create(['codigo' => 'TH', 'descripcion' => 'Taller de habilidades']);
    }
}
