<?php

use Illuminate\Database\Seeder;
use App\Models\Roles;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SET FOREIGN_KEY_CHECKS = 0;TRUNCATE table roles; SET FOREIGN_KEY_CHECKS = 1;";
        DB::connection()->getPdo()->exec($sql);

        Roles::create(['name' => 'Administrador', 'key' => 'ADMIN']);
        Roles::create(['name' => 'Estudiante', 'key' => 'E']);
        Roles::create(['name' => 'Representante', 'key' => 'R']);
        Roles::create(['name' => 'Docente', 'key' => 'D']);
        Roles::create(['name' => 'Institucion', 'key' => 'I']);
        Roles::create(['name' => 'Administrador Contenidos', 'key' => 'WEB']);
        Roles::create(['name' => 'Visitante', 'key' => 'V']);
    }
}
