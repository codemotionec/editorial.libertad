<?php

use Illuminate\Database\Seeder;
use App\Models\TipoPregunta;

class TipoPreguntaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE table tipo_pregunta; SET FOREIGN_KEY_CHECKS = 1;";
        DB::connection()->getPdo()->exec($sql);

        TipoPregunta::create(['codigo' => 'SELECCION', 'descripcion' => 'Selección multiple', 'opciones' => true]);
        TipoPregunta::create(['codigo' => 'FRASE', 'descripcion' => 'Completar la frase', 'opciones' => false]);
        TipoPregunta::create(['codigo' => 'IMAGEN', 'descripcion' => 'Selección de imágenes', 'opciones' => true]);
        TipoPregunta::create(['codigo' => 'TEXTO', 'descripcion' => 'Pregunta abierta', 'opciones' => false]);
    }
}
